//
//  NSDate+CustomDateMethods.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/25/14.
//  Copyright (c) 2014 a1. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSDate (CustomDateMethods)

-(NSDate *) startOfMonth;
-(NSDate *) endOfMonth;
-(NSDate *) nextOrPreviousMonthDate:(int)offSet;

-(NSDate *) getLocalTime;
-(NSDate *) toLocalTime;
-(NSDate *) toGlobalTime;

@end
