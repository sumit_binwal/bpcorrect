//
//  UIAlertView+AlertCustomMethod.m
//  ANDMedical
//
//  Created by deepak.Gupta on 3/14/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "UIAlertView+AlertCustomMethod.h"
#import "UIAlertView+AlertCustomMethod.h"
@implementation UIAlertView (AlertCustomMethod)


/*
 To show a alert with title and message....
 */
+(void) showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"Alert.OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
    });
    
    
}

/*
 TO show a alert with title and message with delay of time....
 */
+(void) showAlertWithDelayAndTitle:(NSString *)title message:(NSString *)message
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"Alert.OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        
    });
    
    
}


/*
 TO show a alert with error as message and title optionally....
 */
+(void) showAlertWithError:(NSError *)error shouldDisplayErrorTitle:(BOOL)shouldDisplayTitle
{
    return;
    NSString *title = shouldDisplayTitle ? error.domain : NSLocalizedString(@"Alert.Error.Title", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:error.localizedDescription
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"Alert.OK", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
}

/*
 To show a alert with exception with title as optionally....
 */
+(void) showAlertWithException:(NSException *)exception shouldDisplayExceptionTitle:(BOOL)shouldDisplayTitle
{
    return;
    
    NSString *title = shouldDisplayTitle ? exception.name : NSLocalizedString(@"Alert.Exception.Title", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:exception.description
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"Alert.OK", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
}

@end
