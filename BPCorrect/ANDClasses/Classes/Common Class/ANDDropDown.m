//
//  ANDDropDown.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 4/24/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDDropDown.h"

@interface ANDDropDown ()
@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) UIButton *btnSender;
@property (nonatomic, retain) NSArray *arr_name;
@property (nonatomic, retain) NSArray *arr_img;
@property (nonatomic, retain) NSString *animationDirection;

@end


@implementation ANDDropDown

- (id)showDropDownWithSender:(UIButton *)button
                  withHeight:(CGFloat *)height
                  tableArray:(NSArray *)arr
               andImageArray:(NSArray *)imgArray
                andDirection:(NSString *)direction
{
    _btnSender = button;
    _animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = button.frame;
        self.arr_name = [NSArray arrayWithArray:arr];
        self.arr_img = [NSArray arrayWithArray:imgArray];
        if ([direction isEqualToString:@"up"])
        {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"])
        {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
        _table.delegate = self;
        _table.dataSource = self;
        _table.layer.cornerRadius = 5;
        _table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        _table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _table.separatorColor = [UIColor grayColor];

        if ([direction isEqualToString:@"up"])
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
        else if([direction isEqualToString:@"down"])
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
        
        NSLog(@"%@",NSStringFromCGRect(self.frame));
        
        _table.frame = CGRectMake(0, 0, btn.size.width, *height);
       // [UIView commitAnimations];
        [button.superview addSubview:self];
        [self addSubview:_table];
    }
    return self;
}

-(void)hideDropDown:(UIButton *)button
{
    CGRect btn = button.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([_animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([_animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    _table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arr_name count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    else
    {
        UILabel *l = (UILabel *)[cell.contentView viewWithTag:99];
        [l removeFromSuperview];
        l = nil;
        
        UIImageView *imgV = (UIImageView *)[cell.contentView viewWithTag:999];
        [imgV removeFromSuperview];
        imgV = nil;
    }
    
    
    cell.backgroundColor = [UIColor colorWithRed:33.0/255.0 green:143/255.0 blue:183.0/255.0 alpha:1.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, 200, 40)];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.tag = 99;
    nameLabel.font = [UIFont systemFontOfSize:18.0f];
    nameLabel.text = [self.arr_name objectAtIndex:indexPath.row];
    [cell.contentView addSubview:nameLabel];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 25, 25)];
    iconImageView.tag = 999;
    [iconImageView setContentMode:UIViewContentModeScaleAspectFit];
    if ([[self.arr_img objectAtIndex:indexPath.row] isKindOfClass:[UIImage class]])
    {
        iconImageView.image = [self.arr_img objectAtIndex:indexPath.row];
        
    }
    [cell.contentView addSubview:iconImageView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideDropDown:_btnSender];
    
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    [_btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
    
    for (UIView *subview in _btnSender.subviews)
    {
        if ([subview isKindOfClass:[UIImageView class]])
            [subview removeFromSuperview];
    }
    
    [self myDelegate];
}

- (void) myDelegate
{
    [self.delegate DropDownDelegateMethod:self];
}

-(void)dealloc {
    //    [super dealloc];
    //    [table release];
    //    [self release];
}

@end
