//
//  ProgressView.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 5/13/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

@property (nonatomic, readwrite) int percent;
@property (nonatomic, readwrite) int barThickness;
@property (nonatomic, strong) UIColor *barBackgroundColor;
@property (nonatomic, strong) UIColor *barForegroundColor;
@property (nonatomic, strong) UIFont *fontSize;
@property (nonatomic, readwrite) BOOL isClockWise;
-(void)setPercent:(int)percent;
@end
