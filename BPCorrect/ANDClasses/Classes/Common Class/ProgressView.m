//
//  ProgressView.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 5/13/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ProgressView.h"

@interface ProgressView ()
{
    CGFloat startAngle;
    CGFloat endAngle;
    UIBezierPath* bezierPath;
    NSString* textContent;
    
}
@end

@implementation ProgressView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.frame = frame;
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 1.5;
        endAngle = startAngle + (M_PI * 2);
        
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    
    
    // Display our percentage as a string
    textContent = [NSString stringWithFormat:@"%d%%", _percent];
    
    UIBezierPath* bezierPathBackGround = [UIBezierPath bezierPath];
    
    // Create our arc, with the correct angles
    [bezierPathBackGround addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                                    radius:rect.size.width/2-_barThickness
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) *  startAngle
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPathBackGround.lineWidth = _barThickness;
    [_barBackgroundColor setStroke];
    [bezierPathBackGround stroke];
    
    
    
    bezierPath = [UIBezierPath bezierPath];
    
    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:rect.size.width/2-_barThickness
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (_percent / 100.0) + startAngle
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = _barThickness;
   // [_barForegroundColor setStroke];
   // [bezierPath stroke];
    
    
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    
    [progressLayer setPath: bezierPath.CGPath];
    [progressLayer setStrokeColor:_barForegroundColor.CGColor];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:_barThickness];
    [progressLayer setStrokeStart:0.0f];
    [progressLayer setStrokeEnd:1.0f];
    
    [self.layer addSublayer:progressLayer];
    
    
    CABasicAnimation *animateStrokeEnd = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
   // NSNumber *t = [NSNumber numberWithFloat:(100.0f-_percent)/100.0];
    animateStrokeEnd.duration  = 2.0f;//t.floatValue; //2.0f
    animateStrokeEnd.fromValue = [NSNumber numberWithFloat:0.0f];
    animateStrokeEnd.toValue   = [NSNumber numberWithFloat:1.0f];
    [progressLayer addAnimation:animateStrokeEnd forKey:nil];
    
    
    
    // Text Drawing
    CGSize expectedTextSize = [textContent sizeWithFont:_fontSize constrainedToSize:rect.size];
    CGRect textRect = CGRectMake((rect.size.width / 2.0) - expectedTextSize.width/2.0, (rect.size.height / 2.0) - expectedTextSize.height/2.0, expectedTextSize.width, expectedTextSize.height);
    if (self.percent>0)
        [_barForegroundColor setFill];
    else
        [_barBackgroundColor setFill];
    
    [textContent drawInRect: textRect withFont:_fontSize lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
}

-(void)setPercent:(int)percent
{
    [self.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    
    _percent = percent;
    
    
    
    [self setNeedsDisplay];
}

@end
