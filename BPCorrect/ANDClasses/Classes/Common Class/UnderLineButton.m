//
//  UnderLineButton.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 3/19/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "UnderLineButton.h"

@implementation UnderLineButton


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
   
    CGContextSetStrokeColorWithColor(context, self.titleLabel.textColor.CGColor);
   // CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    
    //CGContextSetRGBStrokeColor(context, 62.0/255.0, 62.0/255.0, 62.0/255.0, 1.0);//62.0/255.0, 62.0/255.0, 62.0/255.0, 1.0
    
    // Draw them with a 1.0 stroke width.
    CGContextSetLineWidth(context, 1.0);
    
    // Draw a single line from left to right
    CGContextMoveToPoint(context, 0, rect.size.height);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
    CGContextStrokePath(context);
}
 


@end
