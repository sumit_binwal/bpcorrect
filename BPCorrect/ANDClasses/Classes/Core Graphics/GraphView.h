//
//  GraphView.h
//  MyGraph
//
//  Created by Kush.Dixit on 12/09/13.
//  Copyright (c) 2013 Kush.Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

//  Graph Height Overall
#define kGraphHeight 200
// Graph width Overall
#define kDefaultGraphWidth 250
// Starting offset of x cordinate
#define kOffsetX -10
// margin between two x cordinates bar
#define kStepX 30

// where we need to draw graph from bottom
#define kGraphBottom 175

// where we need to draw graph from top
#define kGraphTop 0

// margin between two y cordinates
#define kStepY 25

// Starting offset of y cordinate
#define kOffsetY 0

// adjust bar width
#define kBarWidth 15

#define kCircleRadius 10

 typedef enum  {
    kSimpleBarGraph = 1,
    kBarGraphWithMultiColor = 2,
    kGraphWithCubicBazierCurveAndDots = 3
    } graphStyle;


@interface GraphView : UIView
{
    graphStyle choseGraphStyle;
    
    float stepX;
    float barWidth;
    float offsetX;
    float max;

    
    NSMutableArray* rectArray;
    UILabel* valueLabel;
    UIView* lineView;

}

@property(nonatomic, retain) NSMutableArray* targetArray;
@property(nonatomic, retain) NSMutableArray* dateArray;
@property (nonatomic , retain) UIColor *barColor;
@property(nonatomic, assign) NSInteger rowDivision;

- (void) targetDataArray:(NSMutableArray*)targetArray  dateArray:(NSMutableArray*)dateArr Color : (UIColor *)color rowDivisor:(int)rowDivisor graphStyle:(graphStyle)graphStyle;
@end
