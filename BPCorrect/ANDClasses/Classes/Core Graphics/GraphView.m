//
//  GraphView.m
//  MyGraph
//
//  Created by Kush.Dixit on 12/09/13.
//  Copyright (c) 2013 Kush.Dixit. All rights reserved.
//
#import "GraphView.h"


#define middleBarColor [UIColor colorWithRed:74.0/255.0 green:169.0/255.0 blue:13.0/255.0 alpha:1.0]
@implementation GraphView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - Draw Rect Method
- (void)drawRect:(CGRect)rect
{
    int graphBottom = kGraphBottom;
    int stepY = kGraphBottom/_rowDivision;
    
    int howManyHorizontal = (graphBottom - kGraphTop - kOffsetY) / stepY;
    if (![_targetArray count] ) {
        return;
    }
    
    NSArray *viewsToRemove = [self subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }

    float max = 0;
    
    NSLog(@"Target Array %@", _targetArray);
    
    for (int i= 0; i < [_targetArray count]; i++) {
        if (i==0) {
            max = [[_targetArray objectAtIndex:i] floatValue];
        }
        else
        {
            if ([[_targetArray objectAtIndex:i] floatValue] > max) {
                max = [[_targetArray objectAtIndex:i] floatValue];
            }
        }
        
    }
    
    
    if (max < 100)
    {
        int real = max;
        //incremented by 1 until divide by _rowDivision
        while (real%_rowDivision != 0) {
            real ++;
        }
        max = real;
    }
    else
    {
        int real = max/100;
        //incremented by 1 until divide by _rowDivision
        while (real%_rowDivision != 0) {
            real ++;
        }
        max = real* 100;
    }
    
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.6);
    CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);

    
    int howMany = (kDefaultGraphWidth - kOffsetX) / kStepX;
    
    // Here the lines go
    
    
    // Add left Vertical Line
    CGContextMoveToPoint(context, kOffsetX + 0 * kStepX + 30, kGraphTop);
    CGContextAddLineToPoint(context, kOffsetX + 0 * kStepX + 30, graphBottom);
   
    
    // Add Date on X-Axis
    for (int i = 0; i < howMany + 1; i++){
       //CGContextMoveToPoint(context, kOffsetX + i * kStepX + 30, kGraphTop);
      //CGContextAddLineToPoint(context, kOffsetX + i * kStepX + 30, graphBottom);
    
       // NSLog(@"------- %d.0 ",graphBottom);
        if ([_dateArray count] > i) {
            UILabel* myLabel = [[UILabel alloc] initWithFrame:CGRectMake(kOffsetX + (i + 0.5)* kStepX + 30,  graphBottom  , 30, 20)];
            [myLabel setFont:[UIFont boldSystemFontOfSize:10.0]];
            [myLabel setText:[_dateArray objectAtIndex:i]];
            myLabel.textAlignment = NSTextAlignmentCenter;
            myLabel.adjustsFontSizeToFitWidth = YES;
            myLabel.minimumFontSize = 7.0;
            [myLabel setBackgroundColor:[UIColor clearColor]];
            [self addSubview:myLabel];
        }
    }
    
    // Add Horizontal bottom Line
    
    CGContextMoveToPoint(context, kOffsetX + 30, graphBottom - 0 * stepY);
  CGContextAddLineToPoint(context, kDefaultGraphWidth, graphBottom  - 0* stepY);
    
    
    for (int i = 0; i < howManyHorizontal + 1; i++)
    {
        // NSLog(@"------- %d.0 ",stepY);
        // Add Horizontal Line with in graph & Y-Axis Values
        CGContextMoveToPoint(context, kOffsetX + 30, graphBottom - kOffsetY - i * stepY);
        CGContextAddLineToPoint(context, kDefaultGraphWidth, graphBottom  - i * stepY);
        
        UILabel* myLabel = [[UILabel alloc] initWithFrame:CGRectMake(kOffsetX  - 15, graphBottom - kOffsetY - (i)* stepY - 8, 40, 20)];
        [myLabel setFont:[UIFont boldSystemFontOfSize:10.0]];
        [myLabel setText:[NSString stringWithFormat:@"%d", (int)(max * i)/_rowDivision]];
        myLabel.textAlignment = NSTextAlignmentRight;
        myLabel.adjustsFontSizeToFitWidth = YES;
        myLabel.minimumFontSize = 7.0;
        [myLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:myLabel];
    }
    
    CGContextStrokePath(context);
    
    for (int i = 0; i < [_targetArray count]; i++)
    {
        // Add Bar Lines
        
        float barX = kOffsetX+30+ (i+1)*kStepX - kBarWidth/2;
        float barY = graphBottom;
        float OnePixelContainValue  = max / graphBottom;
        
        float barHeight = OnePixelContainValue ? (1/OnePixelContainValue)* [[_targetArray objectAtIndex:i] floatValue] : 0.0f;
        if (choseGraphStyle == kSimpleBarGraph)
        {
            CGRect barRect = CGRectMake(barX, barY, kBarWidth, barHeight);
            CGContextBeginPath(context);
            CGContextSetFillColorWithColor(context, self.barColor.CGColor);
            CGContextMoveToPoint(context, barRect.origin.x, barRect.origin.y);
            CGContextAddLineToPoint(context, barRect.origin.x, graphBottom - barRect.size.height);
            CGContextAddLineToPoint(context, barRect.origin.x + kBarWidth, graphBottom - barRect.size.height);
            CGContextAddLineToPoint(context,barRect.origin.x + kBarWidth , graphBottom);
            
            //NSLog(@" %f   --  ",CGRectGetMinY(rect));
            CGContextClosePath(context);
            CGContextFillPath(context);
        }
        else  if (choseGraphStyle == kBarGraphWithMultiColor)
        {
            // Bottom Part
            NSLog(@"%@",[_targetArray objectAtIndex:i]);
            
            if ([[_targetArray objectAtIndex:i] intValue] !=0) {
            
            float barHeight1st  = barHeight ? barHeight* 0.6 : 85.0;
            CGRect barRect = CGRectMake(barX, barY, kBarWidth, barHeight1st);
            CGContextBeginPath(context);
            CGContextSetFillColorWithColor(context, self.barColor.CGColor);
            CGContextMoveToPoint(context, barRect.origin.x, barRect.origin.y);
            CGContextAddLineToPoint(context, barRect.origin.x, barRect.origin.y - barRect.size.height);
            CGContextAddLineToPoint(context, barRect.origin.x + kBarWidth, barRect.origin.y - barRect.size.height);
            CGContextAddLineToPoint(context,barRect.origin.x + kBarWidth , barRect.origin.y);
            
            //NSLog(@" %f   --  ",CGRectGetMinY(rect));
            CGContextClosePath(context);
            CGContextFillPath(context);
            
            //Upper Part
            
            float barHeight2nd  = barHeight* 0.4;
            CGRect barRectNew = CGRectMake(barX, barY - barHeight1st , kBarWidth, barHeight2nd);
            CGContextBeginPath(context);
            CGContextSetFillColorWithColor(context, middleBarColor.CGColor);
            CGContextMoveToPoint(context, barRectNew.origin.x, barRectNew.origin.y);
            CGContextAddLineToPoint(context, barRectNew.origin.x,  barRectNew.origin.y - barRectNew.size.height);
            CGContextAddLineToPoint(context, barRectNew.origin.x + kBarWidth,  barRectNew.origin.y - barRectNew.size.height);
            CGContextAddLineToPoint(context,barRectNew.origin.x + kBarWidth ,  barRectNew.origin.y);
            
            //NSLog(@" %f   --  ",CGRectGetMinY(rect));
            CGContextClosePath(context);
            CGContextFillPath(context);
        }
        }else if(choseGraphStyle == kGraphWithCubicBazierCurveAndDots ){
            
            CGRect barRect = CGRectMake(barX, barY, barWidth, barHeight);
            if (rectArray == nil) {
                rectArray = [[NSMutableArray alloc] init];
            }
            
            CGContextSetLineWidth(context, 2.0);
            CGContextSetStrokeColorWithColor(context,
                                             [UIColor blueColor].CGColor);
            CGRect rectangle = CGRectMake(barRect.origin.x , barRect.origin.y - barRect.size.height - kCircleRadius/2,kCircleRadius,kCircleRadius);
            CGContextAddEllipseInRect(context, rectangle);
            CGContextStrokePath(context);
            CGContextSetFillColorWithColor(context,
                                           [UIColor blueColor].CGColor);
            CGContextFillEllipseInRect(context, rectangle);
            CGContextSetLineWidth(context, 2.0);
            CGContextSetStrokeColorWithColor(context,
                                             [UIColor blueColor].CGColor);
            [rectArray addObject:[NSValue valueWithCGRect:rectangle]];
            
            // Drawing Bazier Curve
            
            if ([_targetArray count] > i + 1) {
                float endBarX = offsetX+30+ (i+2)*stepX - barWidth/2;
                float endBarY = graphBottom;
                float endBarHeight = (1/OnePixelContainValue)* [[_targetArray objectAtIndex:i+1] floatValue];
                
                CGContextMoveToPoint(context, barRect.origin.x + barWidth/2, barRect.origin.y - barRect.size.height - kCircleRadius/2);
                CGRect endBarRect = CGRectMake(endBarX, endBarY, barWidth, endBarHeight);
                
                CGContextAddCurveToPoint(context, barRect.origin.x + barWidth/2 + kCircleRadius, barRect.origin.y - barRect.size.height + kCircleRadius, endBarRect.origin.x - kCircleRadius, barRect.origin.y - endBarRect.size.height - kCircleRadius, endBarRect.origin.x, barRect.origin.y - endBarRect.size.height);
                CGContextStrokePath(context);
            }
        }
    }
}


#pragma mark - Passing x-axis & y-axis values
- (void) targetDataArray:(NSMutableArray*)targetArray  dateArray:(NSMutableArray*)dateArr Color : (UIColor *)color rowDivisor:(int)rowDivisor graphStyle:(graphStyle)graphStyle
{
    self.barColor = color;
    _targetArray = targetArray;
    _dateArray = dateArr;
    _rowDivision = rowDivisor;
    choseGraphStyle = graphStyle;
    
    float count = [targetArray count];
    offsetX = kOffsetX - 10;
    count = count +2;
    stepX = kDefaultGraphWidth/count;
    barWidth = stepX/2;

    
    [self setNeedsDisplay];
}


- (void)removeView
{
    [valueLabel removeFromSuperview];
    [lineView removeFromSuperview];
}

#pragma mark <UIResponder class>

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self removeView];
    int graphStyle = choseGraphStyle;
    if (graphStyle == kGraphWithCubicBazierCurveAndDots)
    {
        CGPoint touchPoint = [[touches anyObject] locationInView:self];
        float OnePixelContainValue  = max / kGraphBottom;
        
        for (id rect in rectArray) {
            CGRect containsRect = [rect CGRectValue];
            
            if (CGRectContainsPoint(containsRect, touchPoint))
            {
                if (valueLabel == nil) {
                    valueLabel = [[UILabel alloc] init];
                    lineView = [[UIView alloc] init];
                }
                
                NSInteger i = [rectArray indexOfObject:rect];
                
                //[valueLabel setText:[NSString stringWithFormat:@"%.2f", OnePixelContainValue*(kGraphBottom - touchPoint.y)]];
                
                [valueLabel setText:[NSString stringWithFormat:@"%@",[_targetArray objectAtIndex:i]]];
                
                [valueLabel setFrame:CGRectMake(containsRect.origin.x, containsRect.origin.y - 200, 50, 30)];
                [valueLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
                [valueLabel setBackgroundColor:[UIColor grayColor]];
                [valueLabel setTextAlignment:NSTextAlignmentCenter];
                [self addSubview:valueLabel];
                
                [lineView setFrame:CGRectMake(containsRect.origin.x + kCircleRadius/2, 0, 1, kGraphBottom)];
                [lineView setBackgroundColor:[UIColor grayColor]];
                [self addSubview:lineView];
                
                [self performSelector:@selector(removeView) withObject:nil afterDelay:2.0];
                break;
                
            }
            
        }
        
        
    }
    
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}


@end
