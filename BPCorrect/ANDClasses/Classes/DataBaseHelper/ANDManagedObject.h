//
//  ANDManagedObject.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ANDManagedObject : NSManagedObject

@property (nonatomic, retain) NSDate    *created_date;
@property (nonatomic, retain) NSDate    *modified_date;
@property (nonatomic, retain) NSDate    *deleted_date;
@property (nonatomic, retain) NSNumber  *is_deleted;
@property (nonatomic, retain) NSNumber  *sync_required;

@end
