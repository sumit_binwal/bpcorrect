//
//  ANDManagedObject.m
//  ANDMedical
//
//  Created by Vishal Lohia on 3/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDManagedObject.h"

@implementation ANDManagedObject

@dynamic created_date;
@dynamic modified_date;
@dynamic deleted_date;
@dynamic is_deleted;
@dynamic sync_required;

@end
