//
//  ANDMedCoreDataManager.h
//  CoreDataDemo
//
//  Created by Vishal Lohia on 3/5/14.
//  Copyright (c) 2014 Vishal Lohia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Users;
@class ActivityTracker;
@class BloodPressureTracker;
@class WeightScale;
@class Thermometer; //Sim added for Thermometer

enum _activityDateType
{
    ActivityDateType_None,
    ActivityDateType_Max,
    ActivityDateType_Min
};
typedef enum _activityDateType ActivityDateType;


typedef void (^refreshActivitiesCompletion)(NSError *error);
typedef void (^refreshBloodPressureCompletion)(NSError *error);
typedef void (^refreshWeightScaleCompletion)(NSError *error);
typedef void (^refreshThermometerCompletion)(NSError *error); //Sim added for Thermometer

@interface ANDMedCoreDataManager : NSObject
{
    Users *_currentUser;
    ActivityTracker *_currentActivity;
    BloodPressureTracker *_currentBloodPressureInfo;
    WeightScale *_currentWeightScaleInfo;
    Thermometer *_currentThermometerInfo; //Sim added for Thermometer
    NSMutableArray *_arr_userActivities;
    refreshActivitiesCompletion _activityBlock;
    refreshBloodPressureCompletion _bloodPressureBlock;
}
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (readonly, strong, nonatomic) NSFetchedResultsController *fetchedControllerLiftTrackActivity;
@property (readonly, strong, nonatomic) NSFetchedResultsController *fetchedControllerBloodPressure;
@property (readonly, strong, nonatomic) NSFetchedResultsController *fetchedControllerWeightScale;
@property (readonly, strong, nonatomic) NSFetchedResultsController *fetchedControllerThermometer; //Sim added for Thermometer

@property (nonatomic, strong) Users *currentUser;
@property (nonatomic, strong) ActivityTracker *currentActivity;
@property (nonatomic, strong) BloodPressureTracker *currentBloodPressureInfo;
@property (nonatomic, strong) WeightScale *currentWeightScaleInfo;
@property (nonatomic, strong) Thermometer *currentThermometerInfo; //Sim added for Thermometer

@property (nonatomic, strong) NSMutableArray *arr_userActivities;

-(void) resetOnLogout;

+(ANDMedCoreDataManager *) sharedManager;

-(NSInteger) totalRecordInEntityName:(NSString *)entityName
                        forPredicate:(NSPredicate *)p
                           inContext:(NSManagedObjectContext *)context;

/**
 * attrFunctions
 > sum:
 > count:
 > min:
 > max:
 > average:
 > median:
 > mode:
 > stddev:
 > ceiling:
 > floor:
 > abs:
 **/

-(NSArray *) maximumOrMinimumOrCountInContext:(NSManagedObjectContext *)context
                                   entityName:(NSString *)entity
                                attributeName:(NSString *)attrName
                                    predicate:(NSPredicate *)p
                            aggregateFunction:(NSString *)aggrFunction;

-(BOOL) initializeUserWithUserName:(NSString *)userName
                       andPassword:(NSString *)password
                             error:(NSError **)error;

-(BOOL) insertAndInitializeGuestUser:(NSString *)userName
                         andPassword:(NSString *)password
                               error:(NSError **)error;

-(NSPredicate *) getPredicateForCurrentUserForEntity:(NSString *)entity;

/**
 * This method would return the max and min date for entity.
 */
-(NSString *) getMaxMinDateStringFromDBForEntity:(NSString *)entity
                                 forActivityType:(ActivityDateType)activitydateType;

/**
 * Tthis method would get the whole data of entity
 */

-(NSArray *) getAllActivityDataForEntityName:(NSString *)entity;


-(void) refreshDataManagerWithUserActivities:(refreshActivitiesCompletion)blk;
-(BOOL) hasPreviousActivity;
-(BOOL) hasNextActivity;
-(BOOL) nextActivity;
-(BOOL) previousActivity;
-(NSString *) getActivityMaxMinDateStringFromDB:(ActivityDateType)activitydateType;

-(void) refreshDataManagerWithUserBloodPressureInfo:(refreshBloodPressureCompletion)blk;
-(BOOL) hasPreviousBloodPressureInfoAvailable;
-(BOOL) hasNextBloodPressureInfoAvailable;
-(BOOL) nextBloodPressureInfo;
-(BOOL) previousBloodPressureInfo;
-(NSDate *) getBloodPressureMaxMinDateStringFromDB:(ActivityDateType)activitydateType;

-(void) refreshDataManagerWithUserWeightScaleInfo:(refreshWeightScaleCompletion)blk;
-(NSDate *) getWeightScaleMaxMinDateStringFromDB:(ActivityDateType)activitydateType;
-(NSDate *) getNextOrPreviousWeightScaleInfoDate:(BOOL)isNextActivity
									 toDateString:(NSDate *)dateString;

-(BOOL) hasPreviousWeightScaleInfoAvailable;
-(BOOL) hasNextWeightScaleInfoAvailable;
-(BOOL) nextWeightScaleInfo;
-(BOOL) previousWeightScaleInfo;

/* Sim added for Thermometer */
-(void) refreshDataManagerWithUserThermometerInfo:(refreshThermometerCompletion)blk;
-(NSDate *) getThermometerMaxMinDateStringFromDB:(ActivityDateType)activitydateType;
-(NSDate *) getNextOrPreviousThermometerInfoDate:(BOOL)isNextActivity
                                    toDateString:(NSDate *)dateString;


-(BOOL) hasPreviousThermometerInfoAvailable;
-(BOOL) hasNextThermometerInfoAvailable;
-(BOOL) nextThermometerInfo;
-(BOOL) previousThermometerInfo;
@end
