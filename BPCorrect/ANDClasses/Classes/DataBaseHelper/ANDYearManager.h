//
//  ANDYearManager.h
//  ANDMedical
//
//  Created by Vishal Lohia on 5/9/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

/*
This class is used to create the year's graph. This carries the array of week objects.
Year graph is divided into weeks.
*/

#import <Foundation/Foundation.h>

@class ANDWeek;

@interface ANDYear : NSObject

@property (nonatomic, strong) NSString *entityName;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSDate *start_date;
@property (nonatomic, strong) NSDate *end_date;
@property (nonatomic, strong) NSMutableArray *arr_weeksInYear;

@end


@interface ANDYearManager : NSObject
{
}
@property (nonatomic, strong) NSMutableArray *arr_weeksInYear;

+(ANDYearManager *) sharedManager;

/*
This method would create the weeks object which are still filled with days object.
 In a year there can be 53/54 weaks.
 
*/
-(ANDYear *) prepareWeekForYearUsingDateString:(NSString *)dateString
                                 forEntityName:(NSString *)entityName;

/*
 Following methods are used to get the next/previous year.
 */
-(ANDYear *) nextYearInfoUsingYear:(ANDYear *)year;
-(ANDYear *) previousYearInfoUsingYear:(ANDYear *)year;

/*
These methods are used to check the recoreds in the following year or trailing year.
*/
-(BOOL) hasNextYearInfoAvailableFromYear:(ANDYear *)year;
-(BOOL) hasPreviousYearInfoAvailableFromYear:(ANDYear *)year;

@end
