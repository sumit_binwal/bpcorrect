//
//  ANDYearManager.m
//  ANDMedical
//
//  Created by Vishal Lohia on 5/9/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDYearManager.h"
#import "ANDDateManager.h"
#import "NSException+ExceptionToError.h"
#import "UIAlertView+AlertCustomMethod.h"
#import "ANDMedCoreDataManager.h"

@implementation ANDYear

-(NSMutableArray *) arr_weeksInYear
{
    if (_arr_weeksInYear == nil)
        _arr_weeksInYear = [[NSMutableArray alloc] init];
    
    return _arr_weeksInYear;
}

@end

@implementation ANDYearManager

+(ANDYearManager *) sharedManager
{
    static ANDYearManager *initObj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        initObj = [[self alloc] init];
    });
    
    return initObj;
}

#pragma mark-
#pragma mark Methods...

-(ANDYear *) prepareWeekForYearUsingDateString:(NSString *)dateString
                                 forEntityName:(NSString *)entityName
{
    ANDYear *objYear = nil;
    @try {
        
        if (dateString == nil) {
            return nil;
        }
        
        //Converted the dateString like 114/03/04 to logical date.. of 04/03/2014
        NSDate *date = [ANDDateManager convertLifeTrackWatchDateToLogicalDateObject:dateString];
        NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay)
                                                                          fromDate:date];
        
        //Setting the component of day and month as 1 and keep the year as such.Bcz we are creating date 01/01/YEAR
        [dateComponent setDay:1];
        [dateComponent setMonth:1];
        
        NSDate *startDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
        
        //To create end date set day as 31 and month as 12 and keep the year part as such so that created date points to 31-12-YEAR
        [dateComponent setDay:31];
        [dateComponent setMonth:12];
        
        NSDate *endDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
        
        //Getting total weeks in the year
        int totalWeeks = [ANDDateManager weaksBetween:startDate toDate:endDate];
     
        //Creating year object and assign the start date and end date to it.
        objYear = [[ANDYear alloc] init];
        objYear.dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:startDate];
        objYear.start_date = startDate;
        objYear.end_date = endDate;
        objYear.entityName = entityName;
        

        
        for (int weekIndex = 0; weekIndex < totalWeeks; weekIndex++)
        {
            //For first week we calling method with dateString and entity name with bool to get next and include the current date in the week.
            if (objYear.arr_weeksInYear.count == 0) {
                NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:startDate];
            }
        }
        return objYear;

    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        [UIAlertView showAlertWithError:error shouldDisplayErrorTitle:NO];
        return objYear;
    }
    @finally {
    }
    
}

-(ANDYear *) nextYearInfoUsingYear:(ANDYear *)year
{
    @try {

        if ([self hasNextYearInfoAvailableFromYear:year]) {
        
        NSDate *endDate = year.end_date;
        NSDate *next_yearStartDate = [ANDDateManager getDayDateToNext:1 fromDate:endDate];
        
        NSCalendarUnit unit = (NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                               |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond);
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:unit
                                                                       fromDate:next_yearStartDate];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        
        NSDate *fetchDate = [[NSCalendar currentCalendar] dateFromComponents:components];
        NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:fetchDate];
       
        return [self prepareWeekForYearUsingDateString:dateString
                                         forEntityName:year.entityName];
    }
    
    return nil;
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        [UIAlertView showAlertWithError:error shouldDisplayErrorTitle:NO];
        
    }
    @finally {
    }

}

-(ANDYear *) previousYearInfoUsingYear:(ANDYear *)year
{
    @try {

    if ([self hasPreviousYearInfoAvailableFromYear:year]) {
        
        NSDate *startDate = year.start_date;
        NSDate *next_yearStartDate = [ANDDateManager getDayDateToNext:-1 fromDate:startDate];
        
        NSCalendarUnit unit = (NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                               |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond);
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:unit
                                                                       fromDate:next_yearStartDate];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        
        NSDate *fetchDate = [[NSCalendar currentCalendar] dateFromComponents:components];
        NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:fetchDate];
        
        return [self prepareWeekForYearUsingDateString:dateString
                                         forEntityName:year.entityName];
    }
    
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        [UIAlertView showAlertWithError:error shouldDisplayErrorTitle:NO];
        
    }
    @finally {
    }

    return nil;
        
}

#pragma mark-
#pragma mark Next and Previous Year Data Checking...

-(BOOL) hasNextYearInfoAvailableFromYear:(ANDYear *)year
{
    NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:year.end_date];
    
    if (dateString == nil) {
        return NO;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateString > %@",dateString];
    NSManagedObjectContext *context = [ANDMedCoreDataManager sharedManager].managedObjectContext;
    
    NSArray *arr_Info = [[ANDMedCoreDataManager sharedManager] maximumOrMinimumOrCountInContext:context
                                                                                     entityName:year.entityName
                                                                                  attributeName:@"dateString"
                                                                                      predicate:predicate
                                                                              aggregateFunction:@"min:"];
    
    return arr_Info.count && [[arr_Info lastObject] allKeys].count ? YES : NO;
    
    
}

-(BOOL) hasPreviousYearInfoAvailableFromYear:(ANDYear *)year
{
    NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:year.start_date];
    
    if (dateString == nil) {
        return NO;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateString < %@",dateString];
    NSManagedObjectContext *context = [ANDMedCoreDataManager sharedManager].managedObjectContext;
    
    NSArray *arr_Info = [[ANDMedCoreDataManager sharedManager] maximumOrMinimumOrCountInContext:context
                                                                                     entityName:year.entityName
                                                                                  attributeName:@"dateString"
                                                                                      predicate:predicate
                                                                              aggregateFunction:@"max:"];
    
    return arr_Info.count && [[arr_Info lastObject] allKeys].count ? YES : NO;

}


@end
