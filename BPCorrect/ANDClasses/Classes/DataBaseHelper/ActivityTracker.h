//
//  ActivityTracker.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/6/14.
//  Copyright (c) 2014 A & D All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Users;
@class ADActivityMonitor;

@interface ActivityTracker : NSManagedObject

@property (nonatomic, retain) NSDate    *created_date;
@property (nonatomic, retain) NSDate    *modified_date;
@property (nonatomic, retain) NSDate    *deleted_date;
@property (nonatomic, retain) NSNumber  *is_deleted;
@property (nonatomic, retain) NSNumber  *sync_required;

@property (nonatomic, retain) NSNumber  *steps;
@property (nonatomic, retain) NSNumber  *bpm;
@property (nonatomic, retain) NSNumber  *sleep;
@property (nonatomic, retain) NSNumber  *calories;
@property (nonatomic, retain) NSNumber  *distances;
@property (nonatomic, retain) NSNumber  *blockIndex;
@property (nonatomic, retain) NSString  *endTimeInterval;
@property (nonatomic, retain) NSString  *startTimeInterval;
@property (nonatomic, retain) NSString  *dateString;
@property (nonatomic, retain) NSString  *deviceType;
@property (nonatomic, retain) NSDate    *measurementRecieveDateTime;
@property (nonatomic, retain) NSString * unit;
@property (nonatomic, retain) Users     *userActivity;
@property (nonatomic, retain) NSString  *uuid;
@property (nonatomic, strong) NSString *dailyData; //Added to track daily steps
@property (nonatomic, strong) NSString *dailyDistanceData; //Added to track daily distance , WEL-285
@property (nonatomic, strong) NSString *dailyCalorieData; //Added to track daily calorie , WEL-285
@property (nonatomic, strong) NSNumber *sleepStatus; //Sleep status parameter added for UW-302
@property (nonatomic, strong) NSNumber *sleepHours; //Sleep hours sent by UW-302
@property (nonatomic, strong) NSNumber *sleepMin; //Sleep min sent by UW-302
@property (nonatomic, retain) NSNumber *calorie302;

-(NSString *) measurementDisplayDate;

-(NSNumber *) DistanceAsPerUnit;

+(void) fillTemporaryObjectForUser:(Users *)user inContext:(NSManagedObjectContext *)context;


+(NSArray *) getSortedStartDateActiveActivitiesForContext:(NSManagedObjectContext *)context
                                                  forUser:(Users *)usr
                                                    error:(NSError **)err;

+(NSArray *) getSortedStartDateActiveActivitiesForContext:(NSManagedObjectContext *)context
                                                  forUser:(Users *)usr
                                                predicate:(NSPredicate *)p
                                                    error:(NSError **)err;


+(void) insertORUpdateActivitiesRecords:(NSArray *)adActivityMonitorObjArray
                                forUser:(Users *)user
                             forContext:(NSManagedObjectContext *)context
                                  error:(NSError **)err;

+(void)moveActivitiesRecords:(ActivityTracker *)tracker
               forCurrentUser:(Users *)currentUser
              forSelectedUser:(Users *)selectedUser
                   forContext:(NSManagedObjectContext *)context
                        error:(NSError *__autoreleasing *)err;
@end
