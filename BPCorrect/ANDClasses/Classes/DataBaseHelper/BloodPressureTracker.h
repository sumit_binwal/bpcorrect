//
//  BloodPressureTracker.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ANDManagedObject.h"

@class Users;

@interface BloodPressureTracker : NSManagedObject

@property (nonatomic, retain) NSDate    *created_date;
@property (nonatomic, retain) NSDate    *modified_date;
@property (nonatomic, retain) NSDate    *deleted_date;
@property (nonatomic, retain) NSNumber  *is_deleted;
@property (nonatomic, retain) NSNumber  *sync_required;

@property (nonatomic, retain) NSNumber *systolic;
@property (nonatomic, retain) NSNumber *mean;
@property (nonatomic, retain) NSNumber *pulse;
@property (nonatomic, retain) NSNumber *diastolic;
@property (nonatomic, retain) NSString *dateString;
@property (nonatomic, retain) NSString *deviceType;
@property (nonatomic, retain) NSString *device_name;
@property (nonatomic, retain) NSString *uuid;
@property (nonatomic, retain) NSDate   *measurementRecieveDateTime;
@property (nonatomic, retain) Users    *userBloodPressure;

+(void) fillTemporaryObjectForUser:(Users *)user inContext:(NSManagedObjectContext *)context;


+(NSArray *) getSortedStartDateBloodPressureInfoForContext:(NSManagedObjectContext *)context
                                                  forUser:(Users *)usr
                                                predicate:(NSPredicate *)p
                                                    error:(NSError **)err;


+(void) insertORUpdateBloodPressureRecords:(NSArray *)arr_bloodPressureInfo
                                forUser:(Users *)user
                             forContext:(NSManagedObjectContext *)context
                                  error:(NSError **)err;

+(void)moveBloodPressureRecords:(BloodPressureTracker *)tracker
                 forCurrentUser:(Users *)currentUser
                forSelectedUser:(Users *)selectedUser
                     forContext:(NSManagedObjectContext *)context
                          error:(NSError *__autoreleasing *)err;

@end
