//
//  BloodPressureTracker.m
//  ANDMedical
//
//  Created by Vishal Lohia on 3/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "BloodPressureTracker.h"
#import "Users.h"
#import "ANDDevice.h"
#import "ANDDateManager.h"
#import "ANDMedGlobalConstants.h"


@implementation BloodPressureTracker

@dynamic sync_required;
@dynamic is_deleted;
@dynamic created_date;
@dynamic deleted_date;
@dynamic modified_date;
@dynamic systolic;
@dynamic mean;
@dynamic pulse;
@dynamic uuid;
@dynamic diastolic;
@dynamic dateString;
@dynamic device_name;
@dynamic measurementRecieveDateTime;
@dynamic deviceType;
@dynamic userBloodPressure;


+(void) fillTemporaryObjectForUser:(Users *)user inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"BloodPressureTracker" inManagedObjectContext:context];
    
    NSError *error = nil;
    NSArray *savedObjects = [context executeFetchRequest:request error:&error];
    
    NSDate *currentDate = [NSDate date];
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:unit
                                                                      fromDate:currentDate];
    
    [dateComponent setDay:6];
    [dateComponent setMonth:7];
    [dateComponent setHour:7];
    [dateComponent setMinute:0];
    [dateComponent setSecond:0];
    
    NSDate *startDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
    
    [dateComponent setDay:10];
    [dateComponent setMonth:7];
    
    NSDate *endDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
    
    while ([startDate compare:endDate] != NSOrderedDescending)
    {
        int randomIndex = arc4random() % savedObjects.count; // gives no .between 1 to 15 ..
        
        for (int hourIndex = 7; hourIndex < 18; hourIndex++) {
            
            NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:unit
                                                                              fromDate:startDate];
            
            [dateComponent setHour:hourIndex];
            
            NSDate *readingDateTime = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
            
            BloodPressureTracker *randomObject = savedObjects[randomIndex];
            NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDateTime];
            
            BloodPressureTracker *tracker = [NSEntityDescription insertNewObjectForEntityForName:@"BloodPressureTracker"
                                                                 inManagedObjectContext:context];
            
            
            tracker.dateString = dateString;
            tracker.created_date = readingDateTime;
            tracker.modified_date = readingDateTime;
            tracker.device_name = randomObject.device_name;
            
            tracker.deviceType = randomObject.deviceType;
            tracker.systolic = randomObject.systolic;
            tracker.diastolic = randomObject.diastolic;
            tracker.pulse = randomObject.pulse;
            tracker.mean = randomObject.mean;
            tracker.uuid = randomObject.uuid;
            tracker.sync_required = [NSNumber numberWithBool:YES];
            tracker.measurementRecieveDateTime = readingDateTime;
            
            [user addUserBloodPressureObject:tracker];
            [tracker setUserBloodPressure:user];
            
            NSError *errorSave = nil;
            if ([context save:&errorSave] == NO) {
                NSLog(@"Error Description :: %@",errorSave.description);
            }
        }
        
        
        NSDate *nextDate = [ANDDateManager getDayDateToNext:1 fromDate:startDate];
        startDate = nextDate;
    }
    
}




+(NSArray *) getSortedStartDateBloodPressureInfoForContext:(NSManagedObjectContext *)context
                                                   forUser:(Users *)usr
                                                 predicate:(NSPredicate *)p
                                                     error:(NSError **)err
{
    @try
    {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSSortDescriptor *sortDisc = [[NSSortDescriptor alloc] initWithKey:@"measurementRecieveDateTime"
                                                                 ascending:kIsActivityFetchOrderAscending];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userBloodPressure.user_name = %@",usr.user_name];
        
        
        NSCompoundPredicate *compoundPredicate =  nil;
        
        if (p)
           compoundPredicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType
                                                           subpredicates:@[p,predicate]];
        
        fetchRequest.entity = entity;
        fetchRequest.predicate = p ? (NSPredicate *)compoundPredicate : predicate;
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDisc];
        
        NSError *e = nil;
        NSArray *users = [context executeFetchRequest:fetchRequest
                                                error:&e];
        
        *err = e;
        
        return users;

    }
    @catch (NSException *exception)
    {
        NSError *error = [exception exceptionError];
        *err = error;
        return nil;
    }
    @finally {
    }
}


+(void) insertORUpdateBloodPressureRecords:(NSArray *)arr_bloodPressureInfo
                                   forUser:(Users *)user
                                forContext:(NSManagedObjectContext *)context
                                     error:(NSError **)err
{
    @try
    {
        if (arr_bloodPressureInfo.count == 0) {
            return;
        }
        
        /*Below code is commented, as now we are doing smart sync, so we should not overwrite
         * the local database with all data received from server
         */
      /*  NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSDictionary *dictBP = [arr_bloodPressureInfo lastObject];
        if ([[dictBP objectForKey:@"SYNC_TYPE"] isEqualToValue: @(SYNC_TYPE_SERVER)]) {
            
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            request.entity = entity;
            NSError *error = nil;
            NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"userBloodPressure.user_name = %@",[ANDMedCoreDataManager sharedManager].currentUser.user_name];
            request.predicate = userpredicate;
            NSArray *tempArray = [context executeFetchRequest:request error:&error];
            for (BloodPressureTracker *tr in tempArray) {
                [context deleteObject:tr];
            }
        } */
        
        for (NSDictionary *dict_bloodPressureInfo in arr_bloodPressureInfo)
        {
            NSString *type = [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_DEVICE_TYPE];
            if ([type isEqualToString:@"bp"] == NO) {
                return;
            }
            
            
            NSNumber *num_sys = [dict_bloodPressureInfo objectForKey:BP_DATA_KEY_SYSTOLIC];
            NSNumber *num_dia = [dict_bloodPressureInfo objectForKey:BP_DATA_KEY_DIASTOLIC];
            
            if (num_sys.intValue == 0 && num_dia.intValue == 0) {
                return;
            }
            
            
            
            NSString *dateString = [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
            NSDate *dateRecDateTime =  [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_MEASUREMENTDATETIME];
            dateRecDateTime = dateRecDateTime ? dateRecDateTime : [NSDate date];
            
            BloodPressureTracker *tracker = nil;
            NSDate *currentDate = [NSDate date];
            NSError *fetchError = nil;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",dateRecDateTime];
            tracker = [[self getSortedStartDateBloodPressureInfoForContext:context
                                                                   forUser:user
                                                                 predicate:predicate
                                                                     error:&fetchError] lastObject];
            
            if (tracker == nil) {
                tracker = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                        inManagedObjectContext:context];
                
                [user addUserBloodPressureObject:tracker];
                [tracker setUserBloodPressure:user];
            }
            
            tracker.dateString = dateString;
            tracker.modified_date = currentDate;
            tracker.deviceType = [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_DEVICE_TYPE];
            tracker.device_name = [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_CBPERIPHERAL_NAME];
            tracker.systolic = [dict_bloodPressureInfo objectForKey:BP_DATA_KEY_SYSTOLIC];
            tracker.diastolic = [dict_bloodPressureInfo objectForKey:BP_DATA_KEY_DIASTOLIC];
            tracker.pulse = [dict_bloodPressureInfo objectForKey:BP_DATA_KEY_PULSE];
            tracker.mean = [dict_bloodPressureInfo objectForKey:BP_DATA_KEY_MEAN];
            tracker.uuid = [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_DEVICE_ID];
            tracker.created_date = currentDate;
            tracker.modified_date = currentDate;
            tracker.sync_required =  ([dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_SYNC_REQUIRED]
                                      ? [dict_bloodPressureInfo objectForKey:BP_USERINFO_KEY_SYNC_REQUIRED] :  [NSNumber numberWithInt:YES]);
            
            tracker.measurementRecieveDateTime = dateRecDateTime;
            
            
            NSError *error = nil;
            if (![context save:&error]) {
                NSLog(@"%@",error.description);
            }
        }
        

    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        *err = error;
    }
    @finally {
    }
}


+(void)moveBloodPressureRecords:(BloodPressureTracker *)tracker
               forCurrentUser:(Users *)currentUser
              forSelectedUser:(Users *)selectedUser
                   forContext:(NSManagedObjectContext *)context
                        error:(NSError *__autoreleasing *)err
{
    @try
    {
        //Insert Record...
      //  NSDate *date = [NSDate date];
        [selectedUser addUserBloodPressureObject:tracker];
        [currentUser removeUserBloodPressureObject:tracker];
        
        [tracker setUserBloodPressure:selectedUser];
        tracker.sync_required = [NSNumber numberWithBool:YES];
//        tracker.dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:date];

       // tracker.dateString = [NSString stringWithFormat:@"%@",date];

      //  tracker.measurementRecieveDateTime = date;
        NSError *error = nil;
        
        if (![context save:&error]) {
            NSLog(@"%@",error.description);
        }
        
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        *err = error;
    }
    @finally {
    }
}



@end
