//
//  ManageUser.h
//  ANDMedical
//
//  Created by deepak.Gupta on 6/27/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ManageUser : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * password;




+(ManageUser *) addUpdateUserWithUserName:(NSString *)email
                             withPassword:(NSString *)pwd
                                inContext:(NSManagedObjectContext *)context
                                    error:(NSError **)error;

@end
