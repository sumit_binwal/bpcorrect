//
//  ManageUser.m
//  ANDMedical
//
//  Created by deepak.Gupta on 6/27/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ManageUser.h"


@implementation ManageUser

@dynamic email;
@dynamic password;




+(ManageUser *) addUpdateUserWithUserName:(NSString *)email
                     withPassword:(NSString *)pwd
                        inContext:(NSManagedObjectContext *)context
                            error:(NSError **)error
{
    ManageUser *objUser = nil;
    
    @try
    {
        //Fetch from local database if user exists with username and password...
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

        fetchRequest.entity = entity;
        
        NSError *err = nil;
        NSArray *users = [context executeFetchRequest:fetchRequest
                                                error:&err];

        if ([users count]<=4) {
            
            // Insert Into the database
            if (objUser == nil)
                objUser = [NSEntityDescription insertNewObjectForEntityForName:@"ManageUser"
                                                        inManagedObjectContext:context];
            objUser.email = email;
            objUser.password = pwd;

            
            NSError *saveError = nil;
            if (![context save:&saveError]) {
                
                if (saveError && error) {
                    *error = saveError;
                }
            }

        }
        

        
        
        }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
    }

    }


@end
