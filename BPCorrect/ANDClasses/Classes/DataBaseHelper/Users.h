//
//  Users.h
//  ANDMedical
//
//  Created by surixon.pal on 6/28/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#ifndef appType //US app
@class ActivityTracker, BloodPressureTracker, Groups, WeightScale;

#else
@class ActivityTracker, BloodPressureTracker, Groups, WeightScale, Thermometer; //Sim modified for Thermometer

#endif

@interface Users : NSManagedObject

@property (nonatomic, retain) NSDate * created_date;
@property (nonatomic, retain) NSDate * deleted_date;
@property (nonatomic, retain) NSNumber * is_deleted;
@property (nonatomic, retain) NSDate * modified_date;
@property (nonatomic, retain) NSNumber * sync_required;

@property (nonatomic, retain) NSString * auth_token;
@property (nonatomic, retain) NSDate * expiration_date;
@property (nonatomic, retain) NSNumber * is_validated;
@property (nonatomic, retain) NSNumber * user_autoLogin;
@property (nonatomic, retain) NSDate * user_dob;
@property (nonatomic, retain) NSString * user_encrypted_password;
@property (nonatomic, retain) NSString * user_firstName;
@property (nonatomic, retain) NSString * user_gender;
@property (nonatomic, retain) NSNumber * user_height;
@property (nonatomic, retain) NSNumber * user_weight;
@property (nonatomic, retain) NSString * user_lastName;
@property (nonatomic, retain) NSString * user_metric;
@property (nonatomic, retain) NSString * user_name;
@property (nonatomic, retain) NSString * user_password;
@property (nonatomic, retain) NSString * user_server_id;
@property (nonatomic, retain) NSSet *userActivity;
@property (nonatomic, retain) NSSet *userBloodPressure;
@property (nonatomic, retain) NSSet *userWeight;
@property (nonatomic, retain) NSSet *userThermometer; //Sim added for Thermometer
@property (nonatomic, retain) NSNumber * is_metric;
//WEL-484, UW-302
@property (nonatomic, retain) NSString * uw_name;
@property (nonatomic, retain) NSString * pairedDevices; //To keep a track of device type paired

@property (nonatomic, retain) Groups *groups;
@end

@interface Users (CoreDataGeneratedAccessors)

- (void)addUserActivityObject:(ActivityTracker *)value;
- (void)removeUserActivityObject:(ActivityTracker *)value;
- (void)addUserActivity:(NSSet *)values;
- (void)removeUserActivity:(NSSet *)values;

- (void)addUserBloodPressureObject:(BloodPressureTracker *)value;
- (void)removeUserBloodPressureObject:(BloodPressureTracker *)value;
- (void)addUserBloodPressure:(NSSet *)values;
- (void)removeUserBloodPressure:(NSSet *)values;

- (void)addUserWeightObject:(WeightScale *)value;
- (void)removeUserWeightObject:(WeightScale *)value;
- (void)addUserWeight:(NSSet *)values;
- (void)removeUserWeight:(NSSet *)values;

/*Sim added for Thermometer */
//- (void)addUserThermometerObject:(Thermometer *)value;
//- (void)removeUserThermometerObject:(Thermometer *)value;
- (void)addUserThermometer:(NSSet *)values;
- (void)removeUserThermometer:(NSSet *)values;

//User Defined Methods...
+(NSArray *) getAllUsersInContext:(NSManagedObjectContext *)contest
                            error:(NSError **)error;

+(Users *) getUserWithName:(NSString *)usrName
               andPassword:(NSString *)password
                 inContext:(NSManagedObjectContext *)context
                     error:(NSError **)error;

+(NSArray *) getUsersInfoForContext:(NSManagedObjectContext *)context
                          predicate:(NSPredicate *)p
                              error:(NSError **)error;

+(BOOL) isUserExistsWithUserName:(NSString *)usrName
                     andPassword:(NSString *)password
                       inContext:(NSManagedObjectContext *)context
                           error:(NSError **)error;


+(Users *) createUserProfileFromDict:(NSMutableDictionary *)dict
                             withDOB:(NSDate *)date;

/*
 This method should be called at the time when we get the login webservice response.
 then we need to create/update the userinfo
 */
+(Users *) addUpdateUserLoginInfo:(NSDictionary *)loginResposeInfo
                     withUserName:(NSString *)usrname
                     withPassword:(NSString *)pwd
                        inContext:(NSManagedObjectContext *)context
                            error:(NSError **)error;

/*
 This method should be called at the time when we recieved find profile response and update that for user.
 */
+(BOOL) updateUser:(Users *)user withUserProfileInfo:(NSDictionary *)dictProfileInfo
         inContext:(NSManagedObjectContext *)context error:(NSError **)error;


/*
 This method should be called at the time when we Guest user created the account.
 */

+(BOOL) updateUserForGuest:(Users *)user withUserProfileInfo:(NSDictionary *)dictProfileInfo
                 inContext:(NSManagedObjectContext *)context error:(NSError **)error;


+(BOOL) addInformationForNewAccount:(NSDictionary *)dictCreateProfile
                          inContext:(NSManagedObjectContext *)context
                              error:(NSError*__autoreleasing *)error;


/* This method is used to check whether user is having any record in database
 */
+(BOOL) isAnyRecordExistsInDBForUser:(Users *)user;

/*
 This method is used to add the device type once a BLE device is paired
 */
+(Users *) addPairedDevices:(NSString *)deviceType
               withUserName:(NSString *)usrname
                  inContext:(NSManagedObjectContext *)context
                      error:(NSError **)error;

@end
