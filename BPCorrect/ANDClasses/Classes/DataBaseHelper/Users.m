//
//  Users.m
//  ANDMedical
//
//  Created by surixon.pal on 6/28/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "Users.h"
#import "ActivityTracker.h"
#import "BloodPressureTracker.h"
#import "WeightScale.h"


@implementation Users

@dynamic auth_token;
@dynamic created_date;
@dynamic deleted_date;
@dynamic expiration_date;
@dynamic is_deleted;
@dynamic is_validated;
@dynamic modified_date;
@dynamic sync_required;
@dynamic user_autoLogin;
@dynamic user_dob;
@dynamic user_encrypted_password;
@dynamic user_firstName;
@dynamic user_gender;
@dynamic user_height;
@dynamic user_lastName;
@dynamic user_metric;
@dynamic user_name;
@dynamic user_password;
@dynamic user_server_id;
@dynamic user_weight;
@dynamic userActivity;
@dynamic userBloodPressure;
@dynamic userWeight;
@dynamic userThermometer; //Sim added for Thermometer
@dynamic groups;
//WEL-484, UW-302
@dynamic uw_name;
@dynamic pairedDevices; //Sim added for keeping list of Paired devices

#pragma mark-
#pragma mark User Defined Methods....

#pragma mark - Get All User In The Databse

+(NSArray *) getAllUsersInContext:(NSManagedObjectContext *)contest
                            error:(NSError **)error
{
    @try {
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:contest];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_deleted = %d"
                                  ,[NSNumber numberWithBool:NO]];
        
        NSLog(@"%@",predicate);
        fetchRequest.entity = entity;
        // fetchRequest.predicate = predicate;
        
        NSError *err = nil;
        NSArray *users = [contest executeFetchRequest:fetchRequest
                                                error:&err];
        
        *error = err;
        
        return users;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

+(NSArray *) getUsersInfoForContext:(NSManagedObjectContext *)context
                          predicate:(NSPredicate *)p
                              error:(NSError **)error
{
    
    @try {
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_deleted = %d"
                                  ,[NSNumber numberWithBool:NO]];
        NSLog(@"%@",predicate);
        
        fetchRequest.entity = entity;
        if (p) {
            NSArray *predicates = @[p];//p ? @[p,predicate]: @[predicate];
            NSCompoundPredicate *com_pred = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType
                                                                        subpredicates:predicates];
            
            fetchRequest.predicate = com_pred;
            
        }
        
        NSError *err = nil;
        NSArray *users = [context executeFetchRequest:fetchRequest
                                                error:&err];
        
        if (error) {
            *error = err;
        }
        
        return users;
    }
    @catch (NSException *exception) {
        NSError *err = [exception exceptionError];
        if (error)
            *error = err;
        return nil;
    }
    @finally {
    }
}


#pragma mark - Get User Data Model

+(Users *) getUserWithName:(NSString *)usrName
               andPassword:(NSString *)password
                 inContext:(NSManagedObjectContext *)context
                     error:(NSError **)error
{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                              inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_name = %@ AND user_password = %@ AND is_deleted = %d"
                              ,usrName,password,NO];
    
    fetchRequest.entity = entity;
    fetchRequest.predicate = predicate;
    
    NSError *err = nil;
    NSArray *arr_User = [context executeFetchRequest:fetchRequest
                                               error:&err];
    
    if (error)
        *error = err;
    
    return [arr_User lastObject];
}

#pragma mark - Create User Profile

+(Users *) createUserProfileFromDict :(NSMutableDictionary *)dict
                              withDOB:(NSDate *)date
{
    
    DLog(@"%@",dict);
    
    Users *obj_user = [NSEntityDescription
                       insertNewObjectForEntityForName:@"Users"
                       inManagedObjectContext:[[ANDMedCoreDataManager sharedManager] managedObjectContext]];
    
    obj_user.user_firstName = [dict valueForKey:@"firstName"];
    obj_user.user_lastName = [dict valueForKey:@"lastName"];
    obj_user.user_password = [dict valueForKey:@"password"];
    obj_user.user_name = [dict valueForKey:@"emailAddress"];
    obj_user.user_dob = date;
    obj_user.user_metric = [dict valueForKey:@"heightUnit"];
    NSString *str = [dict valueForKey:@"defaultHeight"];
    float num = [str floatValue];
    obj_user.user_height = [NSNumber numberWithFloat:num];
    obj_user.user_gender = [dict valueForKey:@"gender"];
    obj_user.created_date = [NSDate date];
    obj_user.modified_date = [NSDate date];
    obj_user.sync_required = [NSNumber numberWithBool:NO];
    
    NSError *err = nil;
    [[[ANDMedCoreDataManager sharedManager] managedObjectContext] save:&err];
    
    [ANDMedCoreDataManager sharedManager].currentUser = obj_user;
    
    return obj_user;
}





#pragma mark - Add & Update User
+(Users *) addUpdateUserLoginInfo:(NSDictionary *)loginResposeInfo
                     withUserName:(NSString *)usrname
                     withPassword:(NSString *)pwd
                        inContext:(NSManagedObjectContext *)context
                            error:(NSError **)error
{
    Users *objUser = nil;
    
    @try
    {
        //Fetch from local database if user exists with username and password...
        NSError *fetchError = nil;
        objUser = [self getUserWithName:usrname andPassword:pwd inContext:context error:&fetchError];
        
        //If any error occured during the fetch process then return with error and objUser...
        if (fetchError && error) {
            *error = fetchError;
            return objUser;
        }
        
        //If user does not exists then we need to create the user and add the info...
        if (objUser == nil)
            objUser = [NSEntityDescription insertNewObjectForEntityForName:@"Users"
                                                    inManagedObjectContext:context];
        
        NSDictionary *dictEntity = [loginResposeInfo objectForKey:@"entity"];
        NSString *userId = [dictEntity objectForKey:@"userId"];
        
        if (userId) {
            
                objUser.user_server_id = userId.length ? userId : nil;
        }
        
        objUser.user_firstName = [dictEntity objectForKey:@"firstName"];
        objUser.user_lastName = [dictEntity objectForKey:@"lastName"];
        objUser.auth_token = [dictEntity objectForKey:@"authToken"];
        
        if ([[[dictEntity objectForKey:@"authorizedServices"] valueForKey:@"expirationDate"] isKindOfClass:[NSArray class]])
            objUser.expiration_date = [CommonClass getDateFromTimeStamp:[[[dictEntity objectForKey:@"authorizedServices"] valueForKey:@"expirationDate"] lastObject]];
        else
            objUser.expiration_date = [CommonClass getDateFromTimeStamp:[[dictEntity objectForKey:@"authorizedServices"] valueForKey:@"expirationDate"]];
        
        objUser.user_name = usrname;
        objUser.user_encrypted_password = pwd;
        objUser.user_password = pwd;
        
        //Save method will create or update the user in user table. If any error then send error pointer...
        NSError *saveError = nil;
        if (![context save:&saveError]) {
            
            if (saveError && error) {
                *error = saveError;
            }
        }
        [ANDMedCoreDataManager sharedManager].currentUser = objUser;
        return objUser;
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
    }
    
    [ANDMedCoreDataManager sharedManager].currentUser = objUser;
    return objUser;
}



#pragma mark - Update User
+(BOOL) updateUser:(Users *)user withUserProfileInfo:(NSDictionary *)dictProfileInfo
         inContext:(NSManagedObjectContext *)context error:(NSError **)error
{
    
    @try {
        
        NSDictionary *dictEntity = [dictProfileInfo objectForKey:@"entity"];
        user.user_firstName = [dictEntity objectForKey:@"firstName"];
        user.user_lastName = [dictEntity objectForKey:@"lastName"];
        user.user_name = [dictEntity objectForKey:@"emailAddress"] ? [dictEntity objectForKey:@"emailAddress"] : user.user_name;
        user.user_encrypted_password = [dictEntity objectForKey:@"password"] ? [dictEntity objectForKey:@"password"] : user.user_encrypted_password;
        // user.user_password = [dictEntity objectForKey:@"password"] ? [dictEntity objectForKey:@"password"] : user.user_password;
        
        NSTimeInterval timeInSecond = ceil([[dictEntity objectForKey:@"dateOfBirth"] longLongValue]);
        
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"YYYY-MM-dd"];
        NSDate *minDate = [df dateFromString:@"1900-01-01"];
        
      //  NSDate *dd = [NSDate dateWithTimeInterval:timeInSecond sinceDate:minDate];//AJAY:7 July
        
        NSDate *dd = [CommonClass getDateFromTimeStamp:[NSString stringWithFormat:@"%f",timeInSecond]];
        
        user.user_dob = dd;//[CommonClass getDateFromTimeStamp:[dictEntity objectForKey:@"dateOfBirth"]];
        user.user_gender = [dictEntity objectForKey:@"gender"];
        user.user_height = [NSNumber numberWithFloat:[[dictEntity valueForKey:@"defaultHeight"] floatValue]];
        user.user_name = [dictEntity objectForKey:@"emailAddress"];
        user.user_metric = [dictEntity objectForKey:@"heightUnit"];
        user.is_metric = [dictEntity objectForKey:@"is_metric"] ? [dictEntity objectForKey:@"is_metric"] : [user.user_metric isEqualToString:@"cm"] ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
        
        user.user_weight = [dictEntity objectForKey:@"user_weight"] ? [dictEntity objectForKey:@"user_weight"] : user.user_weight;
        
        NSError *updateError = nil;
        if (![context save:&updateError]) {
            *error = updateError;
            return FALSE;
        }
        
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        [ANDMedCoreDataManager sharedManager].currentUser = user;
    }
    
    
    return TRUE;
    
}

#pragma mark - Update User for Guest
+(BOOL) updateUserForGuest:(Users *)user withUserProfileInfo:(NSDictionary *)dictProfileInfo
                 inContext:(NSManagedObjectContext *)context error:(NSError **)error
{
    
    @try {
        
        NSDictionary *dictEntity = [dictProfileInfo objectForKey:@"entity"];
        user.user_firstName = [dictEntity objectForKey:@"firstName"];
        user.user_lastName = [dictEntity objectForKey:@"lastName"];
        user.user_name = [dictEntity objectForKey:@"emailAddress"];
        user.user_encrypted_password = [dictEntity objectForKey:@"password"];
        
        user.user_password = [dictEntity objectForKey:@"password"];
        
        
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init] ;
        [df1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        NSString *str_date =[dictEntity objectForKey:@"dateOfBirth"];
        NSDate *dtPostDate = [df1 dateFromString:str_date];
        
        user.user_dob = dtPostDate;
        
        user.user_gender = [dictEntity objectForKey:@"gender"];
        
        NSString *str = [dictEntity valueForKey:@"defaultHeight"];
        float num = [str floatValue];
        user.user_height = [NSNumber numberWithFloat:num];
        
        user.user_name = [dictEntity objectForKey:@"emailAddress"];
        user.user_metric = [dictEntity objectForKey:@"heightUnit"];
        
        NSError *updateError = nil;
        if (![context save:&updateError]) {
            *error = updateError;
            return FALSE;
        }
        
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        [ANDMedCoreDataManager sharedManager].currentUser = user;
    }
    
    
    return TRUE;
}

#pragma mark - Add User Profile Information

+(BOOL) addInformationForNewAccount:(NSDictionary *)dictCreateProfile
                          inContext:(NSManagedObjectContext *)context
                              error:(NSError*__autoreleasing *)error
{
    @try {
        
        Users *user = [NSEntityDescription insertNewObjectForEntityForName:@"Users"
                                                    inManagedObjectContext:context];
        
        NSDictionary *dictCreateProfileResponse = [dictCreateProfile objectForKey:@"CreateProfileResponse"];
        NSDictionary *dictEntity = [dictCreateProfileResponse objectForKey:@"entity"];
        user.user_firstName = [dictEntity objectForKey:@"firstName"];
        user.user_lastName = [dictEntity objectForKey:@"lastName"];
        
        NSString *userId = [dictEntity objectForKey:@"userId"];
        
        if (userId) {
            
            user.user_server_id = userId.length ? userId : nil;
            
        }
        
        user.user_encrypted_password = [dictEntity objectForKey:@"password"];
        
        /*
         NSDateFormatter *df1 = [[NSDateFormatter alloc] init] ;
         [df1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
         NSString *str_date =[dictCreateProfile objectForKey:@"dateOfBirth"];
         NSDate *dtPostDate = [df1 dateFromString:str_date];
         */
        
        user.user_dob = [dictCreateProfile objectForKey:@"dateOfBirth"];
        user.user_gender = [dictCreateProfile objectForKey:@"gender"];
        
        NSString *str = [dictCreateProfile valueForKey:@"defaultHeight"];
        float num = [str floatValue];
        user.user_height = [NSNumber numberWithFloat:num];
        user.user_name = [dictCreateProfile objectForKey:@"emailAddress"];
        user.user_password = [dictCreateProfile objectForKey:@"password"];
        user.user_metric = [dictCreateProfile objectForKey:@"heightUnit"];
        user.user_weight = [dictCreateProfile objectForKey:@"user_weight"];
        user.is_metric = [dictCreateProfile objectForKey:@"is_metric"];
        
        NSError *updateError = nil;
        if (![context save:&updateError]) {
            *error = updateError;
            return FALSE;
        }
        
        return TRUE;
    }
    @catch (NSException *exception) {
        NSError *err = [CommonClass getErrorForException:exception];
        *error = err;
        return NO;
    }
    @finally {}
}



#pragma mark - checking whether user is having any BLE data in the local database.
+(BOOL) isAnyRecordExistsInDBForUser:(Users *)user
{
    NSPredicate *pred_bp = [NSPredicate predicateWithFormat:@"userBloodPressure.user_name = %@",user.user_name];
    NSPredicate *pred_ws = [NSPredicate predicateWithFormat:@"user.user_name = %@",user.user_name];
    NSPredicate *pred_am = [NSPredicate predicateWithFormat:@"userActivity.user_name = %@",user.user_name];
    NSPredicate *pred_th = [NSPredicate predicateWithFormat:@"userThermometer.user_name = %@",user.user_name]; //Sim added for Thermometer
    NSManagedObjectContext *context = [[ANDMedCoreDataManager sharedManager] managedObjectContext];
    
    NSInteger totalActivity = [[ANDMedCoreDataManager sharedManager] totalRecordInEntityName:@"ActivityTracker"
                                                                                forPredicate:pred_am
                                                                                   inContext:context];
    
    if (totalActivity) {
        return YES;
    }
    
    NSInteger totalBloodPressure = [[ANDMedCoreDataManager sharedManager] totalRecordInEntityName:@"BloodPressureTracker"
                                                                                     forPredicate:pred_bp
                                                                                        inContext:context];
    if (totalBloodPressure) {
        return YES;
    }
    
    
    NSInteger totalWeighScale = [[ANDMedCoreDataManager sharedManager] totalRecordInEntityName:@"WeightScale"
                                                                                  forPredicate:pred_ws
                                                                                     inContext:context];
    if (totalWeighScale) {
        return YES;
    }
    //Sim added for Thermometer
    NSInteger totalThermometer = [[ANDMedCoreDataManager sharedManager] totalRecordInEntityName:@"Thermometer"
                                                                                  forPredicate:pred_th
                                                                                     inContext:context];
    if (totalThermometer) {
        return YES;
    }
    
    return NO;
}

//Sim adding list of paired devices
//Sim added to support the store of password received from transtek app
#pragma mark - Add & Update User
+(Users *) addPairedDevices:(NSString *)deviceType
               withUserName:(NSString *)usrname
                  inContext:(NSManagedObjectContext *)context
                      error:(NSError **)error
{
    Users *objUser = nil;
    
    @try
    {
        //Fetch the current user from database
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_name = %@"
                                  ,usrname];
        
        fetchRequest.entity = entity;
        fetchRequest.predicate = predicate;
        
        NSError *err = nil;
        
        
        if (error)
            *error = err;
        
        Users *objUser =  [ANDMedCoreDataManager sharedManager].currentUser;
        
        
        //If any error occured during the fetch process then return with error and objUser...
        if (err) {
            NSLog(@"Received some error");
            return objUser;
        }
        
        //If user does not exists then we need to create the user and add the info...
        if (objUser == nil) {
            return NULL;
        }
        else
        {
            
            if (objUser.pairedDevices == NULL) {
                objUser.pairedDevices =deviceType;
            } else {
                NSString *tempString = objUser.pairedDevices;
                objUser.pairedDevices = [tempString stringByAppendingString:deviceType];
            }
            
            
        }
        
        
        
        //Save method will create or update the user in user table. If any error then send error pointer...
        NSError *saveError = nil;
        if (![context save:&saveError]) {
            
            if (saveError && error) {
                *error = saveError;
            }
        }
        [ANDMedCoreDataManager sharedManager].currentUser = objUser;
        return objUser;
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
    }
    
    [ANDMedCoreDataManager sharedManager].currentUser = objUser;
    return objUser;
}


@end
