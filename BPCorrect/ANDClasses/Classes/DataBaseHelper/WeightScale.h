//
//  WeightScale.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 4/17/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Users;

@interface WeightScale : NSManagedObject

@property (nonatomic, retain) NSDate * created_date;
@property (nonatomic, retain) NSDate * deleted_date;
@property (nonatomic, retain) NSNumber * is_deleted;
@property (nonatomic, retain) NSDate * modified_date;
@property (nonatomic, retain) NSNumber * sync_required;

@property (nonatomic, retain) NSString * dateString;
@property (nonatomic, retain) NSDate * measurementRecieveDateTime;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSString * device_name;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * unit;
@property (nonatomic, retain) Users *user;

-(NSNumber *) weightAppType;

-(NSNumber *) BMIAppType;

+(void) fillTemporaryObjectForUser:(Users *)user inContext:(NSManagedObjectContext *)context;

+(NSArray *) getSortedStartDateWeightScaleInfoForContext:(NSManagedObjectContext *)context
                                                   forUser:(Users *)usr
                                                 predicate:(NSPredicate *)p
                                                     error:(NSError **)err;


+(void) insertORUpdateWeightScaleRecords:(NSArray *)arr_weightScaleInfo
                                   forUser:(Users *)user
                                forContext:(NSManagedObjectContext *)context
                                     error:(NSError **)err;

+(void)moveWeightScaleRecords:(WeightScale *)tracker
               forCurrentUser:(Users *)currentUser
              forSelectedUser:(Users *)selectedUser
                   forContext:(NSManagedObjectContext *)context
                        error:(NSError *__autoreleasing *)err;
@end
