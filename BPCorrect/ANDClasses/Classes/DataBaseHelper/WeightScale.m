//
//  WeightScale.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 4/17/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "WeightScale.h"
#import "Users.h"
#import "ANDDevice.h"
#import "ANDMedCoreDataManager.h"
#import "ANDDateManager.h"
#import "NSException+ExceptionToError.h"
#import "ANDMedGlobalConstants.h"

@implementation WeightScale

@dynamic weight;
@dynamic dateString;
@dynamic measurementRecieveDateTime;
@dynamic deviceType;
@dynamic device_name;
@dynamic uuid;
@dynamic unit;
@dynamic created_date;
@dynamic deleted_date;
@dynamic is_deleted;
@dynamic modified_date;
@dynamic sync_required;
@dynamic user;

-(NSNumber *) weightAppType
{
    NSNumber *number = [NSNumber numberWithInt:0];
 
    if ([ANDMedCoreDataManager sharedManager].currentUser.is_metric.boolValue)//kg
    {
        if ([self.unit isEqualToString:@"lb"]) {
            //ConvertToKg
            float lbWeight = self.weight.floatValue / 2.2046;
            number = [NSNumber numberWithFloat:lbWeight];
        }
        else{
            number = self.weight;
        }
        
    }
    else {
        if ([self.unit isEqualToString:@"kg"]) {
            //ConvertTolb
            float kgWeight = self.weight.floatValue * 2.2046;
            number = [NSNumber numberWithFloat:kgWeight];
        }
        else{
            number = self.weight;
        }
        
    }
    
    return number;
}


-(NSNumber *) BMIAppType
{
    NSNumber *number = [NSNumber numberWithInt:0];
    
    float bmiValue = 0;
    
    if ([ANDMedCoreDataManager sharedManager].currentUser.is_metric.boolValue == YES)//kg
    {
        float lbWeight = (self.weightAppType.floatValue);
        float height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]/100;
        
        bmiValue = lbWeight/(height*height);
        
        number = [NSNumber numberWithFloat:bmiValue];
    }
    else
    {
        float lbWeight = (self.weightAppType.floatValue);
        float height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue];
        
        bmiValue = lbWeight*703/(height*height);
        
        number = [NSNumber numberWithFloat:bmiValue];
        
    }
    
    return number;
}

/*
-(NSNumber *) BMIAppType
{
    NSNumber *number = [NSNumber numberWithInt:0];
    
    float bmiValue = 0;
    
    if ([ANDMedCoreDataManager sharedManager].currentUser.is_metric.boolValue == YES)//kg
    {
         if ([self.unit isEqualToString:@"lb"])
         {
         //ConvertToKg
         float lbWeight = (self.weight.floatValue/10) / 2.2046;
         float height = 0;
         if ([[[ANDMedCoreDataManager sharedManager].currentUser user_metric] isEqualToString:@"feet"]) {
         //covert to cm (US)
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]/3.2808;
         }
         else{
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue];
         }
         
         if (height != 0)
         bmiValue = lbWeight/(height*height);
         
         number = [NSNumber numberWithFloat:bmiValue];
         }
         else
         {
         float height = 0;
         if ([[[ANDMedCoreDataManager sharedManager].currentUser user_metric] isEqualToString:@"feet"]) {
         //covert to cm (US)
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]/3.2808;
         }
         else{
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue];
         }
         
         if (height != 0)
         bmiValue = (self.weight.floatValue/10)/(height*height);
         
         number = [NSNumber numberWithFloat:bmiValue];
         }
         
    }
    else
    {
         if ([self.unit isEqualToString:@"kg"])
         {
         //ConvertTolb
         float lbWeight = (self.weight.floatValue/10) * 2.2046;
         float height = 0;
         if ([[[ANDMedCoreDataManager sharedManager].currentUser user_metric] isEqualToString:@"feet"]) {
         //covert to cm (US)
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]*12.0;
         }
         else{
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]*0.39;
         }
         
         if (height != 0)
         bmiValue = lbWeight*703/(height*height);
         
         
         number = [NSNumber numberWithFloat:bmiValue];
         }
         else
         {
         float height = 0;
         if ([[[ANDMedCoreDataManager sharedManager].currentUser user_metric] isEqualToString:@"feet"]) {
         //covert to cm (US)
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]*12.0;
         }
         else{
         height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue]*0.39;
         }
         
         if (height != 0)
         bmiValue = (self.weight.floatValue/10)*703/(height*height);
         
         number = [NSNumber numberWithFloat:bmiValue];
         }
         
    }
    
    return number;
}
*/


+(void) fillTemporaryObjectForUser:(Users *)user inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"WeightScale" inManagedObjectContext:context];
    
    NSError *error = nil;
    NSArray *savedObjects = [context executeFetchRequest:request error:&error];
    
    NSDate *currentDate = [NSDate date];
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:unit
                                                                      fromDate:currentDate];
    
    [dateComponent setDay:1];
    [dateComponent setMonth:7];
    [dateComponent setHour:7];
    [dateComponent setMinute:0];
    [dateComponent setSecond:0];
    
    NSDate *startDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
    
    [dateComponent setDay:9];
    [dateComponent setMonth:7];
    
    NSDate *endDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
    
    while ([startDate compare:endDate] != NSOrderedDescending)
    {
        int randomIndex = arc4random() % savedObjects.count; // gives no .between 1 to 15 ..
        NSArray *weightArray = @[@650,@651,@655,@658,@698,@661];
        
        int indexWeight = arc4random() % weightArray.count;
        
        NSNumber *weightValue =weightArray[indexWeight];
        
        
        for (int hourIndex = 7; hourIndex < 15; hourIndex++) {
            
            NSDateComponents *dateComponent = [[NSCalendar currentCalendar] components:unit
                                                                              fromDate:startDate];

            [dateComponent setHour:hourIndex];
            
            NSDate *readingDateTime = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
            
            WeightScale *randomObject = savedObjects[randomIndex];
            NSString *dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDateTime];
            
            WeightScale *tracker = [NSEntityDescription insertNewObjectForEntityForName:@"WeightScale"
                                                                 inManagedObjectContext:context];
            
            
            tracker.dateString = dateString;
            tracker.created_date = readingDateTime;
            tracker.modified_date = readingDateTime;
            
            tracker.deviceType = randomObject.deviceType;
            tracker.weight = weightValue;
            tracker.unit = @"kg";
            tracker.uuid = randomObject.uuid;
            tracker.sync_required = [NSNumber numberWithBool:YES];
            tracker.measurementRecieveDateTime = readingDateTime;
            
            [user addUserWeightObject:tracker];
            [tracker setUser:user];
            
            NSError *errorSave = nil;
            if ([context save:&errorSave] == NO) {
                NSLog(@"Error Description :: %@",errorSave.description);
            }
        }
        
        
        NSDate *nextDate = [ANDDateManager getDayDateToNext:1 fromDate:startDate];
        startDate = nextDate;
    }
    
}

+(NSArray *) getSortedStartDateWeightScaleInfoForContext:(NSManagedObjectContext *)context
                                                 forUser:(Users *)usr
                                               predicate:(NSPredicate *)p
                                                   error:(NSError **)err
{
    @try
    {
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
        NSSortDescriptor *sortDisc = [[NSSortDescriptor alloc] initWithKey:@"measurementRecieveDateTime"
                                                                 ascending:kIsActivityFetchOrderAscending];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user.user_name = %@",usr.user_name];
        
        
        NSCompoundPredicate *compoundPredicate =  nil;
        
        if (p)
            compoundPredicate = [[NSCompoundPredicate alloc] initWithType:NSAndPredicateType
                                                            subpredicates:@[p,predicate]];
        
        fetchRequest.entity = entity;
        fetchRequest.predicate = p ? (NSPredicate *)compoundPredicate : predicate;
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDisc];
        
        NSError *e = nil;
        NSArray *users = [context executeFetchRequest:fetchRequest
                                                error:&e];
        
        *err = e;
        
        return users;
        
    }
    @catch (NSException *exception)
    {
        NSError *error = [exception exceptionError];
        *err = error;
        return nil;
    }
    @finally {
    }
}



+(void)insertORUpdateWeightScaleRecords:(NSArray *)arr_weightScaleInfo
                                forUser:(Users *)user
                             forContext:(NSManagedObjectContext *)context
                                  error:(NSError *__autoreleasing *)err
{
    if (arr_weightScaleInfo.count == 0) {
        return;
    }
    
    /*Below code is commented, as now we are doing smart sync, so we should not overwrite
     * the local database with all data received from server
     */
    
   /* NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([self class])
                                                  inManagedObjectContext:context];
        
    NSDictionary *dict = [arr_weightScaleInfo lastObject];
    if ([[dict objectForKey:@"SYNC_TYPE"] isEqualToValue: @(SYNC_TYPE_SERVER)]) {
            
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = entity;
        NSError *error = nil;
        NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"user.user_name = %@",[ANDMedCoreDataManager sharedManager].currentUser.user_name];
        request.predicate = userpredicate;
        NSArray *tempArray = [context executeFetchRequest:request error:&error];
        for (WeightScale *tr in tempArray) {
            [context deleteObject:tr];
        }
    } */
        
    
        for (NSDictionary *dict_weightScaleInfo in arr_weightScaleInfo)
        {
            if ([[dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_DEVICE_TYPE] isEqualToString:@"weight"] == NO) {
                return;
            }
            @try {
                WeightScale *tracker = nil;
                NSDate *currentDate = [NSDate date];
                NSString *dateString = [dict_weightScaleInfo objectForKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                NSDate *measurementDateTime = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_RECEIVEDDATETIME];
                measurementDateTime = measurementDateTime ? measurementDateTime : currentDate;
                
                NSError *fetchError = nil;
                NSPredicate *p = [NSPredicate predicateWithFormat:@"measurementRecieveDateTime = %@",measurementDateTime];
                tracker =     [[self getSortedStartDateWeightScaleInfoForContext:context
                                                                         forUser:user
                                                                       predicate:p
                                                                           error:&fetchError] lastObject];
                
                //Insert Record...
                if (tracker == nil) {
                    tracker = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                            inManagedObjectContext:context];
                    
                    [user addUserWeightObject:tracker];
                    [tracker setUser:user];
                }
                
                tracker.dateString = dateString;
                tracker.deviceType = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_DEVICE_TYPE];
                tracker.device_name = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_CBPERIPHERAL_NAME];
                tracker.uuid = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_DEVICE_ID];
                tracker.weight = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_WEIGHT];
                tracker.unit = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_UNIT];
                tracker.sync_required = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_SYNC_REQUIRED];
                
                tracker.created_date = currentDate;
                tracker.modified_date = currentDate;
                
                tracker.measurementRecieveDateTime = measurementDateTime;
                
                NSDate *date = [dict_weightScaleInfo objectForKey:WS_USERINFO_KEY_RECEIVEDDATETIME];
                tracker.dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:date];
                
                NSError *error = nil;
                if (![context save:&error]) {
                    NSLog(@"%@",error.description);
                }
            }
            @catch (NSException *exception) {
                NSError *error = [exception exceptionError];
                *err = error;
            }
            @finally {
            }
        }
        
}



+(void)moveWeightScaleRecords:(WeightScale *)tracker
              forCurrentUser:(Users *)currentUser
             forSelectedUser:(Users *)selectedUser
                  forContext:(NSManagedObjectContext *)context
                       error:(NSError *__autoreleasing *)err
{
    @try
    {
        //Insert Record...
      //  NSDate *date = [NSDate date];
        [selectedUser addUserWeightObject:tracker];
        [currentUser removeUserWeightObject:tracker];
        
        [tracker setUser:selectedUser];
        tracker.sync_required = [NSNumber numberWithBool:YES];
//        tracker.dateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:date];

      //  tracker.dateString = [NSString stringWithFormat:@"%@",date];
      //  tracker.measurementRecieveDateTime = date;
        NSError *error = nil;
        
        if (![context save:&error]) {
            NSLog(@"%@",error.description);
        }
        
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        *err = error;
    }
    @finally {
    }
}









@end
