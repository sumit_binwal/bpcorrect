//
//  ANDDateManager.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/12/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANDDateManager : NSObject

+(NSTimeZone *) timeZone;

/**
 *  This function convert the life track date 114/12/02 YYY/MM/dd to 2014/12/03
 *  @lifeTrackDate is the date string recieved from life track activity watch.
 **/
+(NSString *) convertLifeTrackWatchDateToLogicalDate:(NSString *)lifeTrackDate;

/**
 * This function converts the life track date string (114/MM/dd) to valid date object 2014/MM/dd hh:mm:ss +z
 * @lifeTrackDate is the date string recieved from life track activity watch.
 **/
+(NSDate *) convertLifeTrackWatchDateToLogicalDateObject:(NSString *)lifeTrackDate;

/**
 * This function converts the date object to life track date string format type 114/MM/dd
 * @date Date object which needs to be changed.
 **/
+(NSString *) convertDateObjectToLifeTrackWatchDateString:(NSDate *)date;

/**
 * This function will calculate the days between two dates.
 **/

+(int) daysBetween:(NSDate *)fromDate toDate:(NSDate *)dateTo;

/**
 *  This function will calculates the number of weeks between two dates. Weeks calculates as system weeks (Sun...Sat)
 **/

+(int) weaksBetween:(NSDate *)fromDate toDate:(NSDate *)dateTo;

/**
 *  Calcualte the months between two dates
 **/

+(int) monthsBetween:(NSDate *)fromDate toDate:(NSDate *)dateTo;

/**
 * @weekOffSet should be given as +1 to move next week which exclude the given date.
 * -1 to move backward which exclude the given date.
 * @date: date parameter which will used to calculate next days excluding the ref. date
 **/

+(NSDate *) getWeekDayToNext:(int)weekOffSet fromDate:(NSDate *)date;

/*!
 * @method getDayDateToNext:fromDate:
 * @return NSDate
 * @discussion
 * @weekOffSet can be passed as +1 or -1 to move forward or backward one week.
 * @date refrence date.
 * @shouldInclude bool value. If passed as yes the refrence date will be in the list of weekdays else not.
 */
+(NSDate *) getDayDateToNext:(int)dayOffSet fromDate:(NSDate *)date;

/* +(NSDate *) getYearDateToNext:(int)yearOffSet fromDate:(NSDate *)date;
 * @yearOffSet: should be given as +1 to move next year or -1 to move previous year date
 * @date: date parameter which will used to calculate next year excluding the ref. date
 */

+(NSDate *) getYearDateToNext:(int)yearOffSet fromDate:(NSDate *)date;

/**
 * @weekOffSet can be passed as +1 or -1 to move forward or backward one week.
 * @date refrence date.
 * @shouldInclude bool value. If passed as yes the refrence @date will be in the list of weekdays else not.
 **/


+(NSDate *) getWeekDayToNext:(int)weekOffSet fromDate:(NSDate *)date shouldIncludeFromDateInWeek:(BOOL)shouldInclude;

+(NSInteger) numberOfDaysInMonthUsingDate:(NSDate *)date;

+(NSInteger) getWeekOfMonthForDate:(NSDate *)date;

+(NSInteger) getDayOfWeekForDate:(NSDate *)date;

+(NSInteger) getWeekOfYearForDate:(NSDate *)date;

/*
 @"yyyy/MM/dd hh:mm a"
 */

+ (NSInteger)getDayNumberFromDate:(NSString *)dateString;

+ (NSString *) getDateTimeFromDate:(NSDate *)date;

+ (NSString *) getTimeFormatFromDate:(NSDate *)date;

+ (NSString *) gettingStringFromDate : (NSDate *)date;

+ (NSString *) gettingOnlyDateFromDateString:(NSString *)str_date;

+ (NSString *) gettingOnlyDateFromDate : (NSDate *)date;

+ (NSString *) gettingMonthFromDate : (NSDate *)date;

+ (NSString *) gettingYearFromDate : (NSDate *)date;

+ (NSString *)gettingLongStringWithDateString:(NSString *)dateString;

+ (NSString *)gettingReportGraphStringWithDateString:(NSString *)dateString;

+ (NSDate *) dateWithString:(NSString *)string;

+ (int) getMinuteFromDate:(NSDate *)date;

+ (int) getHourFromDate:(NSDate *)date;
@end
