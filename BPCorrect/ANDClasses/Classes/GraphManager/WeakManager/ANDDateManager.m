//
//  ANDDateManager.m
//  ANDMedical
//
//  Created by Vishal Lohia on 3/12/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDDateManager.h"
#import "NSDate+CustomDateMethods.h"

@implementation ANDDateManager

+(NSTimeZone *) timeZone
{
    return [NSTimeZone localTimeZone];
}

+(NSString *) convertLifeTrackWatchDateToLogicalDate:(NSString *)lifeTrackDate
{
    //Life Track Date is like 114/03/02 y/m/d
    //Need to format year part...
    NSRange slashRange = [lifeTrackDate rangeOfString:@"/"];
    NSString *finalYear = lifeTrackDate;//default;
    
    
    if (slashRange.location != NSNotFound) {
        
        NSString *yearPart = [lifeTrackDate substringWithRange:NSMakeRange(0, slashRange.location)];
        NSString *monthDayPath = [lifeTrackDate substringWithRange:NSMakeRange(slashRange.location+1,
                                                                               lifeTrackDate.length - (slashRange.location+1))];
        
       int year = [yearPart intValue];
       int modifiedYear = 2014 + (year - 114);
       finalYear = [NSString stringWithFormat:@"%d/%@",modifiedYear,monthDayPath];
       
    }
    
    return finalYear;
}

+(NSDate *) convertLifeTrackWatchDateToLogicalDateObject:(NSString *)lifeTrackDate
{
    NSString *finalDateString = [self convertLifeTrackWatchDateToLogicalDate:lifeTrackDate];
    
    
    
    //finalDateString = [finalDateString stringByAppendingString:@" 00:00:00 +0530"];
    //finalDateString = [finalDateString stringByAppendingString:@" 00:00:00 +0530"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
   // [df setDateFormat:@"yyyy/MM/dd hh:mm:ss Z"];
      [df setDateFormat:@"yyyy/MM/dd"];
    
    NSDate *date = [df dateFromString:finalDateString];
   // return date;
    return [date toLocalTime];
}

+(NSString *) convertDateObjectToLifeTrackWatchDateString:(NSDate *)date
{
    date =  [date toLocalTime];
    NSUInteger unitFlags = NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    //WEL-400
  //  NSCalendar *calendar = [NSCalendar currentCalendar];
     NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];//End of WEL-400
    
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    int watchYear = components.year - 2014 + 114;
    
    return [NSString stringWithFormat:@"%d/%02d/%02d",watchYear,components.month,components.day];
}

+(int) daysBetween:(NSDate *)fromDate toDate:(NSDate *)dateTo
{
    fromDate = [fromDate toLocalTime];
    dateTo = [dateTo toLocalTime];
    
    NSUInteger unitFlags = NSDayCalendarUnit;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:fromDate toDate:dateTo options:0];
    
    return [components day]+1;
}

+(int) weaksBetween:(NSDate *)fromDate toDate:(NSDate *)dateTo
{
    NSUInteger unitFlags = NSWeekCalendarUnit;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:fromDate toDate:dateTo options:0];
    return [components week]+1;
}

+(int) monthsBetween:(NSDate *)fromDate toDate:(NSDate *)dateTo
{
    NSUInteger unitFlags = NSMonthCalendarUnit;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:fromDate toDate:dateTo options:0];
    return [components month];
}

+(NSDate *) getWeekDayToNext:(int)weekOffSet fromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay: weekOffSet >= 0 ? (weekOffSet * 8) : (weekOffSet * 7)];
    
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}

+(NSDate *) getWeekDayToNext:(int)weekOffSet fromDate:(NSDate *)date shouldIncludeFromDateInWeek:(BOOL)shouldInclude
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    int previousDays = shouldInclude ?(weekOffSet * 6) :(weekOffSet * 7);
    int nextDays = weekOffSet * (shouldInclude ? 6 : 7);
    
    [dateComponents setDay: weekOffSet >= 0 ? nextDays : previousDays];
    
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}

+(NSDate *) getDayDateToNext:(int)dayOffSet fromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay: dayOffSet];
    
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}

+(NSDate *) getYearDateToNext:(int)yearOffSet fromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:yearOffSet];
    
    return [calendar dateByAddingComponents:dateComponents toDate:date options:0];
}

+(NSInteger ) numberOfDaysInMonthUsingDate:(NSDate *)date
{
    NSDate *today = date; //Get a date object for today's date
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:today];
    
    return days.location != NSNotFound ? days.length : NSNotFound;
    
}

+(NSInteger) getWeekOfMonthForDate:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSWeekOfMonthCalendarUnit fromDate:date];
    
    NSInteger week = [components weekOfMonth]; // here your have a Integer with the weeknr of yourDate
    
    return week;
}

+(NSInteger) getDayOfWeekForDate:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSWeekdayCalendarUnit fromDate:date];
    NSInteger week = [components weekday]; // here your have a Integer with the weeknr of yourDate
    
    return week;
}

+ (NSInteger) getWeekOfYearForDate:(NSDate *)date
{
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitWeekOfYear fromDate:date];
    NSInteger week = [components weekOfYear]; // here your have a Integer with the weeknr of yourDate
    
    return week;
    
}

+ (NSString *) getDateTimeFromDate:(NSDate *)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //[df setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [df setDateStyle:NSDateFormatterMediumStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
//    [df setDateFormat:@"MM/dd/YYYY hh:mm a"];
    return [df stringFromDate:date];
}

#pragma mark - Some date to string or string to date method...

+ (NSString *) getTimeFormatFromDate:(NSDate *)date
{
    NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
  //  [df2 setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    [df2 setDateStyle:NSDateFormatterMediumStyle];
    [df2 setTimeStyle:NSDateFormatterShortStyle];
//    [df2 setDateFormat:@"MMMM dd, h:mm a"];//  //MMMM dd, hh:mm a
    
    return [df2 stringFromDate:date];
}

+ (NSInteger) getDayNumberFromDate:(NSString *)dateString
{
    NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
   // [df2 setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [df2 setDateFormat:@"yy/MM/dd"];
    
    NSDate *savedMeasurementDate = [df2 dateFromString:dateString];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:savedMeasurementDate];
    
    return [components day];
}

+ (NSString *) gettingStringFromDate : (NSDate *)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    //WEL-400
    //[f setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];//End of WEL-400
    
    [f setDateStyle:NSDateFormatterMediumStyle];
  //  [f setTimeStyle:NSDateFormatterShortStyle];
    //[f setDateFormat:@"dd MMMM yyyy"];
    NSString *str_date = [f stringFromDate:date];
    
    return str_date;
}

+ (NSString *) gettingOnlyDateFromDateString:(NSString *)str_date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [dateFormat setDateFormat:@"yyy/MM/dd"];
    NSDate *date = [dateFormat dateFromString:str_date];
    [dateFormat setDateFormat:@"dd"];
    NSString* datStr = [dateFormat stringFromDate:date];
    
    return datStr;
}
+ (NSString *) gettingOnlyDateFromDate : (NSDate *)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
   // [f setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [f setDateFormat:@"dd"];
    NSString *str_date = [f stringFromDate:date];
    
    return str_date;
}

+ (NSString *) gettingMonthFromDate : (NSDate *)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    //[f setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [f setDateFormat:@"MMMM yyyy"];
    NSString *str_date = [f stringFromDate:date];
    
    return str_date;
}

+ (NSString *) gettingYearFromDate : (NSDate *)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
   // [f setLocale:[[[NSBundle mainBundle] preferredLocalizations] count] ? [[NSLocale alloc] initWithLocaleIdentifier:[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]] : [NSLocale currentLocale]];
    
    [f setDateFormat:@"yyyy"];
    NSString *str_date = [f stringFromDate:date];
    
    return str_date;
}

+ (NSString *)gettingLongStringWithDateString:(NSString *)dateString {
    NSDate *date = [self dateWithString:dateString];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    f.dateStyle = NSDateFormatterLongStyle;
    f.timeStyle = NSDateFormatterNoStyle;
    
    return [f stringFromDate:date];
}

+ (NSString *)gettingReportGraphStringWithDateString:(NSString *)dateString {
    NSDate *date = [self dateWithString:dateString];
    NSDateFormatter *f = [NSDateFormatter new];
    f.dateFormat = @"MM/dd";
    
    return [f stringFromDate:date];
}

+ (NSDate *) dateWithString:(NSString *)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    return [formatter dateFromString:string];
}

+ (int) getMinuteFromDate:(NSDate *)date
{
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComps = [gregorianCal components: (NSHourCalendarUnit | NSMinuteCalendarUnit)
                                                  fromDate: date];
    // Then use it
    [dateComps minute];
    [dateComps hour];
    
    
    
    return dateComps.minute;
    
}

+ (int) getHourFromDate:(NSDate *)date
{
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComps = [gregorianCal components: (NSHourCalendarUnit | NSMinuteCalendarUnit)
                                                  fromDate: date];
    // Then use it
    [dateComps minute];
    [dateComps hour];
    
    
    
    return dateComps.hour;
    
}

@end
