//
//  ADThermometer.h
//  ANDMedical
//
//  Created by Simantini Bhattacharya on 10/1/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ANDDevice.h"

typedef NS_ENUM(NSInteger, ADTHOrder) {
    TH_DESC,
    TH_ASC
};

@interface ADThermometer : ANDDevice

@property (nonatomic, strong) NSNumber *measurementID;
@property (nonatomic, strong) NSString *measurementTime;
@property (nonatomic, strong) NSString *measurementReceivedTime;

/**
 * Temperature in NSnumber
 */
@property (nonatomic, strong) NSNumber *THTemperature;
@property (nonatomic, strong) NSString *units;
@property (nonatomic, strong) NSString *temperaturType;
@property (nonatomic, strong) NSNumber *userID;
@property (nonatomic, strong) NSNumber *isManualInput;


- (id) initWithMT: (NSString *) measurementTime
              MRT: (NSString *) measurementReceivedTime
              Temperature: (NSString *) temperature
              Unit: (NSString *) unit
              TempType: (NSString *) type
              UID: (NSString *) UserID
              isM: (NSString *) isManualInput;

- (id) initWithLatestMeasurement;
//reversed is no longer needed, created a order to combine two method into one
+ (NSArray *) listOfWSMeasurementReversed;
+ (NSArray *) listOfMeasurement: (ADTHOrder) order;
+ (NSInteger) count;
+ (NSString *)save: (ADThermometer *)th;
- (NSString *)save;
+ (NSInteger) maxValue: (NSString *)value;
+ (NSInteger) minValue: (NSString *)value;
+ (void) deleteMreasurementAt: (NSNumber *) measurementID;
+ (void) createTable;
- (NSDictionary *) printProperTemperature;


- (id)initWithDevice:(ANDDevice *)device;
- (void)pair;
- (void)readMeasurement;
-(void) readMeasurementForSetup;

@end
