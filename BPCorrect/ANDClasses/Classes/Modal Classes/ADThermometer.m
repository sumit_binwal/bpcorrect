//
//  ADThermometer.m
//  ANDMedical
//
//  Created by Simantini Bhattacharya on 10/1/14.
//  Copyright (c) 2014 a1. All rights reserved.
//
#import "ADThermometer.h"
#import "ANDBLEDefines.h"
#import <Foundation/Foundation.h>

@implementation ADThermometer
@synthesize measurementID = _measurementID;
@synthesize measurementTime = _measurementTime;
@synthesize measurementReceivedTime = _measurementReceivedTime;
@synthesize THTemperature = _THTemperature;
@synthesize units = _units;
@synthesize temperaturType = _temperaturType;

- (id)initWithMT:(NSString *)measurementTime MRT:(NSString *)measurementReceivedTime Temperature:(NSString *)temperature Unit:(NSString *)unit TempType:(NSString *)type UID:(NSString *)UserID isM:(NSString *)isManualInput
{
    _measurementTime = measurementTime;
    _measurementReceivedTime = measurementReceivedTime;
    _THTemperature = [NSNumber numberWithFloat: [temperature floatValue]];
    _units =  unit;
    _temperaturType = type;
    _userID = [NSNumber numberWithInt:[UserID intValue]];
    _isManualInput = [NSNumber numberWithInt:[isManualInput intValue]];
    
    return self;
}

/*
 * Simple debugging info.
 */
-(NSString *) description
{
    return [NSString stringWithFormat:@"%@ Thermometer: %@ %@ %@", self.measurementTime, self.THTemperature, self.units, self.temperaturType];
}

#pragma mark - private methods
- (NSNumber *)tempInCelsius
{
    NSNumber *result = self.THTemperature;
    if ([self.units isEqualToString: @"F"]) { //Temperature received is Fahrenheit, conversion needed
        double tmp = ([self.THTemperature doubleValue] - 32)/1.8000; //Converting Farenheit to Celsius
        NSLog(@"temp in Celcius is %f", tmp);
        result = [NSNumber numberWithDouble:tmp];
    }
    NSString *tmp2 = [NSString stringWithFormat:@"%.2f", [result doubleValue]];
    result =[NSNumber numberWithDouble:[tmp2 doubleValue]];
    
    NSLog(@"Celsius result is %f", [result doubleValue]);
    return result;
}

- (NSNumber *)tempInFahrenheit
{
    NSNumber *result = self.THTemperature;
    if ([self.units isEqualToString: @"C"]) { //Temperature received is in Celsius unit , conversion needed
        double tmp = ([self.THTemperature doubleValue] * 1.8000) + 32.00; //Converting from Celsius to Fahrenheit
        NSLog(@"tmp in Fahrenheit %f", tmp);
        result =[NSNumber numberWithDouble:tmp];
    }
    NSString *tmp2 = [NSString stringWithFormat:@"%.2f", [result doubleValue]];
    result =[NSNumber numberWithDouble:[tmp2 doubleValue]];
    
    NSLog(@"Fahrenheit result is %f", [result doubleValue]);
    return result;
}


-(id)initWithDevice:(ANDDevice *)device
{
    self = [super init];
    if (self) {
        self.activePeripheral = device.activePeripheral;
        self.CM = device.CM;
        self.peripherials = device.peripherials;
        self.delegate = device.delegate;
    }
    return self;
}

-(void)readMeasurementForSetup
{
    [self notification:[CBUUID UUIDWithString:HealthThermometer_Service]
    characteristicUUID:[CBUUID UUIDWithString:TemperatureMeasurement_Char]
                     p:self.activePeripheral on:YES];
}

#pragma mark - Read From Thermometer
- (void)readMeasurement
{
    NSLog(@"thermometer readMeasurement");
    
    
    [self readValue:[CBUUID UUIDWithString:HealthThermometer_Service]
 characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                  p:self.activePeripheral];
    
   /* [self notification:[CBUUID UUIDWithString:HealthThermometer_Service]
    characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                     p:self.activePeripheral on:YES];*/
    
    
    [self readValue:[CBUUID UUIDWithString:HealthThermometer_Service]
 characteristicUUID:[CBUUID UUIDWithString:TemperatureMeasurement_Char]
                  p:self.activePeripheral];
    
    [self notification:[CBUUID UUIDWithString:HealthThermometer_Service]
    characteristicUUID:[CBUUID UUIDWithString:TemperatureMeasurement_Char]
                     p:self.activePeripheral on:YES];
    
 
}

#pragma mark - Write Time Stamp For Weight Scale
- (void) setTime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    
    NSInteger year = components.year;
    NSMutableData *yearData = [[NSMutableData alloc] initWithBytes:&year length:sizeof(year)];
    int year1 = *(int *)[[yearData subdataWithRange:NSMakeRange(0, 1)] bytes];
    int year2 = *(int *)[[yearData subdataWithRange:NSMakeRange(1, 1)] bytes];

    
    int month = components.month;
    
    int day = components.day;
    
    int hour = components.hour;
    
    int min = components.minute;
    
    int second = components.second;
    
    char bytes[7];
    
    bytes[0] = year1;
    
    bytes[1] = year2;
    
    bytes[2] = month;
    
    bytes[3] = day;
    
    bytes[4] = hour;
    
    bytes[5] = min;
    
    bytes[6] = second;
    
    
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    NSLog(@"data is %@", data);
    
    [self writeValue:[CBUUID UUIDWithString: HealthThermometer_Service]
  characteristicUUID:[CBUUID UUIDWithString: DateTime_Char]
                   p:self.activePeripheral
                data:data];
    
}

@end
