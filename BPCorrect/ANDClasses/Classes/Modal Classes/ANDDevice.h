//
//  BLECalls.h
//  BLETester
//
//  Created by Chenchen Zheng on 12/10/13.
//  Copyright (c) 2013 Chenchen Zheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

extern NSString * const BP_DATA_KEY_SYSTOLIC;
extern NSString * const BP_DATA_KEY_DIASTOLIC;
extern NSString * const BP_DATA_KEY_PULSE;
extern NSString * const BP_DATA_KEY_MEAN;

extern NSString * const BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK;
extern NSString * const BP_USERINFO_KEY_CBPERIPHERAL_NAME;
extern NSString * const BP_USERINFO_KEY_DEVICE_TYPE;
extern NSString * const BP_USERINFO_KEY_DEVICE_ID;
extern NSString * const BP_USERINFO_KEY_SYNC_REQUIRED;
extern NSString * const BP_USERINFO_KEY_MEASUREMENTDATETIME;

extern NSString * const WS_USERINFO_KEY_WEIGHT;
extern NSString * const WS_USERINFO_KEY_UNIT;
extern NSString * const WS_USERINFO_KEY_DEVICE_ID;
extern NSString * const WS_USERINFO_KEY_DEVICE_TYPE;
extern NSString * const WS_USERINFO_KEY_CBPERIPHERAL_NAME;
extern NSString * const WS_USERINFO_KEY_RECEIVEDDATETIME;
extern NSString * const WS_USERINFO_KEY_SYNC_REQUIRED;

/*Sim added for Thermometer */
extern NSString * const TH_USERINFO_KEY_TEMPERATURE;
extern NSString * const TH_USERINFO_KEY_UNIT ;
extern NSString * const TH_USERINFO_KEY_DEVICE_ID ;
extern NSString * const TH_USERINFO_KEY_DEVICE_TYPE  ;
extern NSString * const TH_USERINFO_KEY_CBPERIPHERAL_NAME ;
extern NSString * const TH_USERINFO_KEY_RECEIVEDDATETIME ;
extern NSString * const TH_USERINFO_KEY_SYNC_REQUIRED;
extern NSString * const TH_USERINFO_TEMPERTATURE_TYPE ;

//WEL-484, Activity Data
extern NSString * const ACTIVITY_DATA_KEY_STEPS;
extern NSString * const ACTIVITY_DATA_KEY_DISTANCE;
extern NSString * const ACTIVITY_DATA_KEY_CALORIE;
extern NSString * const ACTIVITY_DATA_KEY_MEASUREMENTDATETIME;
extern NSString * const ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME;
extern NSString * const ACTIVITY_DATA_KEY_MEASUREMENTENDTIME;
extern NSString * const ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING;
extern NSString * const ACTIVITY_DATA_KEY_PERIPHERAL_NAME;
extern NSString * const ACTIVITY_DATA_KEY_PERIPHERAL_UUUID;
extern NSString * const ACTIVITY_USERINFO_KEY_SYNC_REQUIRED;
extern NSString * const ACTIVITY_DATA_KEY_SLEEP ;
extern NSString * const ACTIVITY_DATA_KEY_SLEEP_STATUS;
extern NSString * const ACTIVITY_DATA_KEY_SLEEP_HOURS ;
extern NSString * const ACTIVITY_DATA_KEY_SLEEP_MINS;

typedef enum : NSUInteger
{
    SYNC_TYPE_LOCAL = 0,
    SYNC_TYPE_SERVER = 1
} SYNC_TYPE;

@protocol  ANDDeviceDelegate <NSObject>
@optional

//Added by Vishal Lohia
- (void) deviceReadyWithInfoPeripheral:(CBPeripheral *)peripheral;
- (void) gotActivityWithData:(NSData *)data forPeripheral:(CBPeripheral *)peripheral;
- (void) gotBloodPressure:(NSDictionary *)dictData forPeripheral:(CBPeripheral *)peripheral;
- (void) gotActivityAD:(NSDictionary *)data andOtherData:(NSDictionary *)medicalData; //WEL-484, UW-302
- (void) trackerReconnect:(CBPeripheral *) peripheral; //WEL-484, UW-302
- (void) trackerDeviceDisconnect:(CBPeripheral *)peripheral; //WEL-484, UW-302
- (void) deviceReady;
- (void) gotWeight:(NSDictionary *) data;
- (void) gotBloodPressure:(NSDictionary *)data;
- (void) gotTemperature:(NSDictionary *) data; //Sim added for Thermometer
- (void) gotDevice:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData;// TODO:SN
- (void) gotActivity:(NSData *)data;
- (void) gotErrorWhileConnectivity:(NSError *)error;
- (void) gotDeviceWithOutPairing:(NSString *)device_name;
- (void) deviceDisconnectWithDeviceName:(NSString *)device_name;
- (void) gotBloodPressureCufError:(NSString *)device_name;
- (void) gotDeviceUpdateNotificationWithError:(NSError *)error;
@end



@interface ANDDevice : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate,CBPeripheralManagerDelegate>
{
    BOOL _shouldScan;
    BOOL isWeightScale;
}

@property (nonatomic) float batteryLevel;
@property (nonatomic, assign) SYNC_TYPE sync_type;
@property (nonatomic, weak) id <ANDDeviceDelegate> delegate;
@property (nonatomic) NSMutableArray *peripherials;
@property (nonatomic) CBCentralManager *CM;
@property (nonatomic) CBPeripheral *activePeripheral;
@property (nonatomic) CBPeripheralManager *cperipheralManger;
@property (nonatomic) NSData *data;
//@property (nonatomic) NSDate *time;
@property (nonatomic) NSString *connectionStats;

- (id) initWithPeripheral:(CBPeripheral *)peripheral;
+(ANDDevice *) currentInstance;
- (void) readDeviceInformation;
- (void) readANDDeviceInformation;
- (void) setTime;
- (void) setTimeble; //Sim added for WELWORLD-29
- (void) stopScanning;

/*!
 *  @method controlSetup:
 *
 *  @param s Not used
 *
 *  @return Allways 0 (Success)
 *
 *  @discussion controlSetup enables CoreBluetooths Central Manager and sets delegate to TIBLECBKeyfob class
 *
 */
- (void) controlSetup;


-(void) readBattery:(CBPeripheral *)p;


/*!
 *  @method writeValue:
 *
 *  @param serviceUUID Service UUID to write to (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to write to (e.g. 0x2401)
 *  @param data Data to write to peripheral
 *  @param p CBPeripheral to write to
 *
 *  @discussion Main routine for writeValue request, writes without feedback. It converts integer into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, value is written. If not nothing is done.
 *
 */
-(void) writeValue:(CBUUID *)serviceUUID characteristicUUID:(CBUUID *)characteristicUUID  p:(CBPeripheral *)p data:(NSData *)data;

/*!
 *  @method readValue:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for read value request. It converts integers into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the read value is started. When value is read the didUpdateValueForCharacteristic
 *  routine is called.
 *
 *  @see didUpdateValueForCharacteristic
 */
-(void) readValue: (CBUUID *)serviceUUID characteristicUUID:(CBUUID *)characteristicUUID  p:(CBPeripheral *)p;

/*!
 *  @method notification:
 *
 *  @param serviceUUID S
 ervice UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for enabling and disabling notification services. It converts integers
 *  into CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the notfication is set.
 *
 */
-(void) notification:(CBUUID *)serviceUUID characteristicUUID:(CBUUID *)characteristicUUID  p:(CBPeripheral *)p on:(BOOL)on;

//-(void) turnOffNotifyWithService: (NSString *) serviceUUID andCharacteristic: (NSString *) characteristicUUID toPeripheral: (CBPeripheral *)p;

/*!
 *  @method findBLEPeripherals:
 *
 *  @param timeout timeout in seconds to search for BLE peripherals
 *
 *  @return 0 (Success), -1 (Fault)
 *
 *  @discussion findBLEPeripherals searches for BLE peripherals and sets a timeout when scanning is stopped
 *
 */
- (int) findBLEPeripherals;


- (void) writeValue:(NSData *) data serviceUUID:(CBUUID *) serviceUUID characteristicUUID: (CBUUID *) characteristicUUID descriptorUUID: (CBUUID *)descriptorUUID p: (CBPeripheral *) p;


- (void) connectPeripheral:(CBPeripheral *)peripheral;



-(void)recievedatafromPeripheral;
//Sim changes for cool design
- (void) deviceTypeConnect :(NSString *)deviceType; //Defines the type of device connecting

@end
