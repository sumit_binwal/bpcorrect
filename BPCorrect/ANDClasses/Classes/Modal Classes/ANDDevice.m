//
//  BLECalls.m
//  BLETester
//
//  Created by Chenchen Zheng on 12/10/13.
//  Copyright (c) 2013 Chenchen Zheng. All rights reserved.
//

#import "ANDDevice.h"
#import "ANDBLEDefines.h"
#import "ANDMedGlobalConstants.h"
#import "CommonClass.h"
#import "ANDDateManager.h"

NSString * const BP_DATA_KEY_SYSTOLIC = @"systolic";
NSString * const BP_DATA_KEY_DIASTOLIC = @"diastolic";
NSString * const BP_DATA_KEY_PULSE = @"pulse";
NSString * const BP_DATA_KEY_MEAN = @"mean";
NSString * const BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK = @"lifetrack_kind_datestring";
NSString * const BP_USERINFO_KEY_CBPERIPHERAL_NAME = @"peripheral_name";
NSString * const BP_USERINFO_KEY_DEVICE_TYPE = @"device_type";
NSString * const BP_USERINFO_KEY_DEVICE_ID = @"device_id";
NSString * const BP_USERINFO_KEY_SYNC_REQUIRED = @"sync_required";
NSString * const BP_USERINFO_KEY_MEASUREMENTDATETIME = @"measurementreceiveddatetime";

NSString *const WS_USERINFO_KEY_WEIGHT = @"weight";
NSString *const WS_USERINFO_KEY_UNIT = @"unit";
NSString *const WS_USERINFO_KEY_DEVICE_ID = @"device_id";
NSString *const WS_USERINFO_KEY_DEVICE_TYPE = @"device_type";
NSString *const WS_USERINFO_KEY_CBPERIPHERAL_NAME = @"peripheral_name";
NSString *const WS_USERINFO_KEY_RECEIVEDDATETIME = @"measurementReceivedDateTime";
NSString * const WS_USERINFO_KEY_SYNC_REQUIRED = @"sync_required";

//Sim added for Thermometer
NSString *const TH_USERINFO_KEY_TEMPERATURE = @"Thermometer";
NSString *const TH_USERINFO_KEY_UNIT = @"unit";
NSString *const TH_USERINFO_KEY_DEVICE_ID = @"device_id";
NSString *const TH_USERINFO_KEY_DEVICE_TYPE = @"device_type";
NSString *const TH_USERINFO_KEY_CBPERIPHERAL_NAME = @"peripheral_name";
NSString *const TH_USERINFO_KEY_RECEIVEDDATETIME = @"measurementReceivedDateTime";
NSString *const TH_USERINFO_KEY_SYNC_REQUIRED = @"sync_required";
NSString *const TH_USERINFO_TEMPERTATURE_TYPE = @"temp_type";

//WEL-484, UW-302 Implementation
NSString * const ACTIVITY_DATA_KEY_STEPS = @"activity_steps";
NSString * const ACTIVITY_DATA_KEY_DISTANCE = @"activity_distance";
NSString * const ACTIVITY_DATA_KEY_CALORIE = @"activity_calorie";
NSString * const ACTIVITY_DATA_KEY_MEASUREMENTDATETIME = @"activity_datetime";
NSString * const ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME = @"activity_starttime";
NSString * const ACTIVITY_DATA_KEY_MEASUREMENTENDTIME = @"activity_endtime";
NSString * const ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING = @"activity_datestring";
NSString * const ACTIVITY_DATA_KEY_PERIPHERAL_NAME = @"activity_peripheral_uuid";
NSString * const ACTIVITY_DATA_KEY_PERIPHERAL_UUUID = @"activity_peripheral_name";
NSString * const ACTIVITY_USERINFO_KEY_SYNC_REQUIRED = @"sync_required";
NSString * const ACTIVITY_DATA_KEY_SLEEP_STATUS = @"activity_sleepstatus";
NSString * const ACTIVITY_DATA_KEY_SLEEP_HOURS = @"activity_sleep_hours";
NSString * const ACTIVITY_DATA_KEY_SLEEP_MINS = @"activity_sleep_min";
NSString * const ACTIVITY_DATA_KEY_SLEEP = @"activity_sleep";

//Sim added for Transtek
NSString * deviceTypeConnect = @"";
int activityDataCount = 0;
NSMutableDictionary *activityDataConversion;
NSMutableData *dataActivity; //Structure to store value for Activity
NSMutableData *dataBP; //Structure to store value for BP
NSMutableData *dataWeight; //Structure to store value for weight
int dataArrayCount = 0;
int trackerWeightData = 0; //To keep track of the number of weight data sent by UW-302
int trackerBpData = 0; //To keep track of number of BP data sent by UW-302
BOOL dataPacketEnd = FALSE;
NSTimer *trackerReconnectTimer;
NSMutableArray *reconnectTimer;
int tracker_timer_count = 0;
NSMutableArray *trackerStringDate ; //Added code for optimization
NSMutableDictionary *medicalDataConversion; //Added code for optimization


@interface ANDDevice ()

@property (strong) NSMutableData *activityData;
@property int dataLength;

@end

@implementation ANDDevice
@synthesize connectionStats;
@synthesize peripherials;
@synthesize activePeripheral;

ANDDevice *currentInstance = nil;
- (ANDDevice *) init
{
    self.connectionStats = @"disconnected";
    currentInstance = self;
    
    return self;
}


- (id) initWithPeripheral:(CBPeripheral *)peripheral
{
    self.connectionStats = @"disconnected";
    self.activePeripheral = peripheral;
    currentInstance = self;
    return self;
}

+(ANDDevice *) currentInstance
{
    return currentInstance;
    
}

- (void) setANDDeviceInformation
{
    NSLog(@"setDeviceInformation");
    char bytes[13];
    bytes[0] = 0x0A;
    bytes[1] = 0x01;
    bytes[2] = 0xA0;
    bytes[3] = 0x30;
    bytes[4] = 0x31;
    bytes[5] = 0x32;
    bytes[6] = 0x33;
    bytes[7] = 0x34;
    bytes[8] = 0x35;
    bytes[9] = 0x36;
    bytes[10] = 0x37;
    bytes[11] = 0x38;
    bytes[12] = 0x39;
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    [self writeValue:[CBUUID UUIDWithString:AND_Service] characteristicUUID:[CBUUID UUIDWithString:AND_Char] p:self.activePeripheral data:data];
}


- (void) readANDDeviceInformation
{
    NSLog(@"read DeviceInformation");
    char bytes[3];
    bytes[0] = 0x02;
    bytes[1] = 0x00;
    bytes[2] = 0xD0
    ;
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    [self writeValue:[CBUUID UUIDWithString:AND_Service]
  characteristicUUID:[CBUUID UUIDWithString:AND_Char]
                   p:self.activePeripheral
                data:data];
    [self notification:[CBUUID UUIDWithString:AND_Service]
    characteristicUUID:[CBUUID UUIDWithString:AND_Char]
                     p:self.activePeripheral
                    on:YES];
}


- (void) setTime
{
    NSLog(@"setTime");
    // last year, first year, month, day, hour, mintues, seconds
    //  int year1 = 221;
    int year = 13;
    int month = 12;
    int day = 16;
    int hour = 11;
    int min = 45;
    int second = 35;
    
    char bytes[9];
    bytes[0] = 0x08;
    bytes[1] = 0x01;
    bytes[2] = 0x01;
    bytes[3] = year;
    bytes[4] = month;
    bytes[5] = day;
    bytes[6] = hour;
    bytes[7] = min;
    bytes[8] = second;
    
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    [self writeValue:[CBUUID UUIDWithString: AND_Service]
  characteristicUUID:[CBUUID UUIDWithString: AND_Char]
                   p:self.activePeripheral
                data:data];
    
}


- (void) readDeviceInformation
{
    [self readValue:[CBUUID UUIDWithString:DeviceInformation_Service]
 characteristicUUID:[CBUUID UUIDWithString:ManufacturerNameString_Char]
                  p:self.activePeripheral];
    [self readValue:[CBUUID UUIDWithString:DeviceInformation_Service]
 characteristicUUID:[CBUUID UUIDWithString:ModelNumberString_Char]
                  p:self.activePeripheral];
    [self readValue:[CBUUID UUIDWithString:DeviceInformation_Service]
 characteristicUUID:[CBUUID UUIDWithString:FirmwareRevisionString_Char]
                  p:self.activePeripheral];
    [self readValue:[CBUUID UUIDWithString:DeviceInformation_Service]
 characteristicUUID:[CBUUID UUIDWithString:SoftwareRevisionString_Char]
                  p:self.activePeripheral];
    [self readValue:[CBUUID UUIDWithString:DeviceInformation_Service]
 characteristicUUID:[CBUUID UUIDWithString:SystemID_Char]
                  p:self.activePeripheral];
}


/*!
 *  @method controlSetup:
 *
 *  @param s Not used
 *
 *  @return Allways 0 (Success)
 *
 *  @discussion controlSetup enables CoreBluetooths Central Manager and sets delegate to TIBLECBKeyfob class
 *
 */
- (void) controlSetup
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
   // self.cperipheralManger = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];//AJAY:to check for peripheral request//not worked:28th April

    self.CM = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}


/*!
 *  @method readBattery:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
-(void) readBattery:(CBPeripheral *)p
{
    //  NSLog(@"readBattery!");
    [self readValue:[CBUUID UUIDWithString: Battery_Service] characteristicUUID: [CBUUID UUIDWithString:BatteryLevel_Char] p:p];
}


/*!
 *  @method writeValue:
 *
 *  @param serviceUUID Service UUID to write to (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to write to (e.g. 0x2401)
 *  @param data Data to write to peripheral
 *  @param p CBPeripheral to write to
 *
 *  @discussion Main routine for writeValue request, writes without feedback. It converts integer into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, value is written. If not nothing is done.
 *
 */
-(void) writeValue:(CBUUID *)serviceUUID characteristicUUID:(CBUUID *)characteristicUUID p:(CBPeripheral *)p data:(NSData *)data2

{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    CBService *service = [self findServiceFromUUID:serviceUUID p:p];
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:characteristicUUID service:service p:p];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:characteristicUUID],[self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    [p writeValue:data2 forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}

- (void) writeValue:(NSData *) data serviceUUID:(CBUUID *) serviceUUID characteristicUUID: (CBUUID *) characteristicUUID descriptorUUID: (CBUUID *)descriptorUUID p: (CBPeripheral *) p
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    CBService *service = [self findServiceFromUUID:serviceUUID p:p];
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n", [self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:characteristicUUID service:service p:p];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n", [self CBUUIDToString:characteristicUUID], [self CBUUIDToString:serviceUUID], [p.identifier UUIDString]);
        return;
    }
    CBDescriptor *descriptor = [self findDescriptorFromUUID:descriptorUUID characteristic:characteristic];
    if (! descriptor) {
        NSLog(@"Could not find descriptor with UUID %s on characteristic with UUID %s on service with UUID %s\r\n", [self CBUUIDToString:descriptorUUID], [self CBUUIDToString:characteristicUUID], [self CBUUIDToString:serviceUUID]);
        return;
    }
    NSLog(@"descriptor.uuid %@", descriptor.UUID);
    [p writeValue:data forDescriptor:descriptor];
}


/*!
 *  @method readValue:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for read value request. It converts integers into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the read value is started. When value is read the didUpdateValueForCharacteristic
 *  routine is called.
 *
 *  @see didUpdateValueForCharacteristic
 */
-(void) readValue: (CBUUID *)serviceUUID characteristicUUID:(CBUUID *)characteristicUUID p:(CBPeripheral *)p
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    CBService *service = [self findServiceFromUUID:serviceUUID p:p];
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:characteristicUUID service:service p:p];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:characteristicUUID],[self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    [p readValueForCharacteristic:characteristic];
}


/*!
 *  @method notification:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for enabling and disabling notification services. It converts integers
 *  into CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the notfication is set.
 *
 */
-(void) notification:(CBUUID *)serviceUUID characteristicUUID:(CBUUID *)characteristicUUID p:(CBPeripheral *)p on:(BOOL)on
{
    CBService *service = [self findServiceFromUUID:serviceUUID p:p];
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:characteristicUUID service:service p:p];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:characteristicUUID],[self CBUUIDToString:serviceUUID],[p.identifier UUIDString]);
        return;
    }
    NSLog(@"calling the setnotify function ");
    [p setNotifyValue:on forCharacteristic:characteristic];
}


/*!
 *  @method swap:
 *
 *  @param s Uint16 value to byteswap
 *
 *  @discussion swap byteswaps a UInt16
 *
 *  @return Byteswapped UInt16
 */
-(UInt16) swap:(UInt16)s
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    UInt16 temp = s << 8;
    temp |= (s >> 8);
    return temp;
}


/*
 *  @method findServiceFromUUID:
 *
 *  @param UUID CBUUID to find in service list
 *  @param p Peripheral to find service on
 *
 *  @return pointer to CBService if found, nil if not
 *
 *  @discussion findServiceFromUUID searches through the services list of a peripheral to find a
 *  service with a specific UUID
 *
 */
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    NSLog(@"Name: %@, UUID: %@",p.name,p.identifier.UUIDString);
    
    for(int i = 0; i < p.services.count; i++)
    {
        CBService *s = [p.services objectAtIndex:i];
        if ([p.name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
        {
            //NSLog(@"WEIGHT SCALE Ser %@",s.UUID.UUIDString);
            
            return s;//AJAY
        } else if ([p.name rangeOfString:@"A&D_UT201"].location != NSNotFound) //Sim added for Thermometer
        {
            return s;
        } else if([p.name rangeOfString:@"UW-302"].location != NSNotFound)
        {
            if ([s.UUID isEqual:UUID]) {
                return s;
            }
        }
        else
        {
            if ([self compareCBUUID:s.UUID UUID2:UUID])
                return s;
        }
    }
    return nil; //Service not found on this peripheral
}


/*
 *  @method compareCBUUID
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUID compares two CBUUID's to each other and returns 1 if they are equal and 0 if they are not
 *
 */
-(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    
   // NSLog(@"CHECK OUT: %@, %@",UUID1.UUIDString,UUID2.UUIDString);// it cause the crash since Version:1.36
    
    if (memcmp(b1, b2, UUID1.data.length) == 0)
        return 1;
    else
        return 0;
}


/*
 *  @method CBUUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion CBUUIDToString converts the data of a CBUUID class to a character pointer for easy printout using printf()
 *
 */
-(const char *) CBUUIDToString:(CBUUID *) UUID
{

    DLog(@"%s",__PRETTY_FUNCTION__);

    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}


/*
 *  @method compareCBUUIDToInt
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UInt16 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUIDToInt compares a CBUUID to a UInt16 representation of a UUID and returns 1
 *  if they are equal and 0 if they are not
 *
 */
-(int) compareCBUUIDToInt:(CBUUID *)UUID1 UUID2:(UInt16)UUID2
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    char b1[16];
    [UUID1.data getBytes:b1];
    UInt16 b2 = [self swap:UUID2];
    if (memcmp(b1, (char *)&b2, 2) == 0) return 1;
    else return 0;
}


/*
 *  @method CBUUIDToInt
 *
 *  @param UUID1 UUID 1 to convert
 *
 *  @returns UInt16 representation of the CBUUID
 *
 *  @discussion CBUUIDToInt converts a CBUUID to a Uint16 representation of the UUID
 *
 */
-(UInt16) CBUUIDToInt:(CBUUID *) UUID
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
}


/*
 *  @method IntToCBUUID
 *
 *  @param UInt16 representation of a UUID
 *
 *  @return The converted CBUUID
 *
 *  @discussion IntToCBUUID converts a UInt16 UUID to a CBUUID
 *
 */
-(CBUUID *) IntToCBUUID:(UInt16)UUID
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    char t[16];
    t[0] = ((UUID >> 8) & 0xff); t[1] = (UUID & 0xff);
    NSData *data2 = [[NSData alloc] initWithBytes:t length:16];
    return [CBUUID UUIDWithData:data2];
}


/*
 *  @method findCharacteristicFromUUID:
 *
 *  @param UUID CBUUID to find in Characteristic list of service
 *  @param service Pointer to CBService to search for charateristics on
 *
 *  @return pointer to CBCharacteristic if found, nil if not
 *
 *  @discussion findCharacteristicFromUUID searches through the characteristic list of a given service
 *  to find a characteristic with a specific UUID
 *
 */
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service p:(CBPeripheral *)p
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    for(int i=0; i < service.characteristics.count; i++)
    {
        CBCharacteristic *c = [service.characteristics objectAtIndex:i];
        /*if ([p.name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
        {
            NSLog(@"WEIGHT SCALE Char %@",c.UUID.UUIDString);
            return c;//AJAY
        } */
        if ([p.name rangeOfString:@"UW-302"].location != NSNotFound) {
            if ([c.UUID isEqual:UUID]) {
                return c;
            }
            
        } else
        {
            if ([self compareCBUUID:c.UUID UUID2:UUID])
            {
                return c;
            }
        }
        
        
    }
    return nil; //Characteristic not found on this service
}


- (CBDescriptor *) findDescriptorFromUUID:(CBUUID *)UUID characteristic:(CBCharacteristic *) characteristic
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    for (int i = 0 ; i < characteristic.descriptors.count; i++)
    {
        CBDescriptor *d = [characteristic.descriptors objectAtIndex:i];
        if ([self compareCBUUID:d.UUID UUID2:UUID]) {
            return d;
        }
    }
    return  nil;
}




/*!
*
* CBCentralManagerDelegate protocol methods beneeth here Documented in CoreBluetooth documentation
*
*/
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    NSLog(@"Status of CoreBluetooth central manager changed %d (%s)\r\n",central.state,[self centralManagerStateToString:central.state]);
    
    
    // Client Code
    if (_shouldScan)
        [self performSelector:@selector(findBLEPeripherals) withObject:nil];
    
}


/*!
 *  @method centralManagerStateToString:
 *
 *  @param state State to print info of
 *
 *  @discussion centralManagerStateToString prints information text about a given CBCentralManager state
 *
 */
- (const char *) centralManagerStateToString: (int)state
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    switch(state) {
        case CBCentralManagerStateUnknown:
        {
            return "State unknown (CBCentralManagerStateUnknown)";
        }
        case CBCentralManagerStateResetting:
            return "State resetting (CBCentralManagerStateUnknown)";
        case CBCentralManagerStateUnsupported:
            return "State BLE unsupported (CBCentralManagerStateResetting)";
        case CBCentralManagerStateUnauthorized:
            return "State unauthorized (CBCentralManagerStateUnauthorized)";
        case CBCentralManagerStatePoweredOff:
            return "State BLE powered off (CBCentralManagerStatePoweredOff)";
        case CBCentralManagerStatePoweredOn:
            return "State powered up and ready (CBCentralManagerStatePoweredOn)";
        default:
            return "State unknown";
    }
    return "Unknown state";
}


- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    [self.delegate gotDevice:peripheral advertisementData:(NSDictionary *)advertisementData];
    
    NSLog(@"didDiscoverPeripheral: %@\r\n", peripheral.name);
}

/*
 * method disconnectPeripheral
 *
 *
 *
 */
- (void) disconnectPeripheral:(CBPeripheral *)peripheral
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    //Reset the connection device type
    deviceTypeConnect = @" ";
    [self.CM cancelPeripheralConnection:peripheral];
    _shouldScan = NO;
    [self.CM stopScan];
}


//Sim added for the tracker
- (void) deviceTypeConnect :(NSString *)deviceType
{
    deviceTypeConnect = deviceType;
    
}

/*!
 *  @method connectPeripheral:
 *
 *  @param p Peripheral to connect to
 *
 *  @discussion connectPeripheral connects to a given peripheral and sets the activePeripheral property of TIBLECBKeyfob.
 *
 */
- (void) connectPeripheral:(CBPeripheral *)peripheral
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    NSLog(@"Connecting to peripheral with UUID : %@\r\n",[peripheral.identifier UUIDString]);
    self.activePeripheral = peripheral;
    self.activePeripheral.delegate = self;
    [self.CM connectPeripheral:self.activePeripheral options:nil];
    self.connectionStats = @"Connecting..";
    _shouldScan = YES;
    [self.CM stopScan];
}


- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    NSLog(@"Connection to peripheral with UUID : %@ successful\r\n",[peripheral.identifier UUIDString]);
    self.activePeripheral = peripheral;
    [self.activePeripheral discoverServices:nil];
    self.connectionStats = @"Connected";
    _shouldScan = YES;
    [central stopScan];
}


- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    DLog(@"%@",[error description]);
    if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
    [self.delegate gotErrorWhileConnectivity:error];
}


- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    DLog(@"%@ \n Centeral State = %d \n Peripheral State = %d",[error description],central.state,peripheral.state);

    if (peripheral.name != nil)
    {
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC));
        dispatch_after(time, dispatch_get_main_queue(), ^{
            
            if ([self.delegate respondsToSelector:@selector(deviceDisconnectWithDeviceName:)])
                [self.delegate deviceDisconnectWithDeviceName:peripheral.name];
            
        });
        
    }
    
    
    self.CM = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    if (_shouldScan)
        [self performSelector:@selector(findBLEPeripherals) withObject:nil afterDelay:5.0f];    
}



#pragma mark - Stop Scanning Method
-(void) stopScanning
{
    @synchronized(self) {
        _shouldScan = NO;
        [self.CM stopScan];
    }
}

#pragma mark - CBPeripheralManager Delagate Methods

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    switch (self.CM.state) {
        case CBCentralManagerStatePoweredOn:
           // [self.CM scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:BloodPressure_Service]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES] }];
              [self.CM scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES] }];
            break;
            
        default:
            NSLog(@"%i",self.CM.state);
            break;
    }
}


/*!
 *  @method peripheralManager:willRestoreState:
 *
 *  @param peripheral	The peripheral manager providing this information.
 *  @param dict			A dictionary containing information about <i>peripheral</i> that was preserved by the system at the time the app was terminated.
 *
 *  @discussion			For apps that opt-in to state preservation and restoration, this is the first method invoked when your app is relaunched into
 *						the background to complete some Bluetooth-related task. Use this method to synchronize your app's state with the state of the
 *						Bluetooth system.
 *
 *  @seealso            CBPeripheralManagerRestoredStateServicesKey;
 *  @seealso            CBPeripheralManagerRestoredStateAdvertisementDataKey;
 *
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral willRestoreState:(NSDictionary *)dict
{
    
}

/*!
 *  @method peripheralManagerDidStartAdvertising:error:
 *
 *  @param peripheral   The peripheral manager providing this information.
 *  @param error        If an error occurred, the cause of the failure.
 *
 *  @discussion         This method returns the result of a @link startAdvertising: @/link call. If advertisement could
 *                      not be started, the cause will be detailed in the <i>error</i> parameter.
 *
 */
- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error
{
    
}

/*!
 *  @method peripheralManager:didAddService:error:
 *
 *  @param peripheral   The peripheral manager providing this information.
 *  @param service      The service that was added to the local database.
 *  @param error        If an error occurred, the cause of the failure.
 *
 *  @discussion         This method returns the result of an @link addService: @/link call. If the service could
 *                      not be published to the local database, the cause will be detailed in the <i>error</i> parameter.
 *
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error
{
    
}

/*!
 *  @method peripheralManager:central:didSubscribeToCharacteristic:
 *
 *  @param peripheral       The peripheral manager providing this update.
 *  @param central          The central that issued the command.
 *  @param characteristic   The characteristic on which notifications or indications were enabled.
 *
 *  @discussion             This method is invoked when a central configures <i>characteristic</i> to notify or indicate.
 *                          It should be used as a cue to start sending updates as the characteristic value changes.
 *
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic
{
    
}

/*!
 *  @method peripheralManager:central:didUnsubscribeFromCharacteristic:
 *
 *  @param peripheral       The peripheral manager providing this update.
 *  @param central          The central that issued the command.
 *  @param characteristic   The characteristic on which notifications or indications were disabled.
 *
 *  @discussion             This method is invoked when a central removes notifications/indications from <i>characteristic</i>.
 *
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic
{
    
}

/*!
 *  @method peripheralManager:didReceiveReadRequest:
 *
 *  @param peripheral   The peripheral manager requesting this information.
 *  @param request      A <code>CBATTRequest</code> object.
 *
 *  @discussion         This method is invoked when <i>peripheral</i> receives an ATT request for a characteristic with a dynamic value.
 *                      For every invocation of this method, @link respondToRequest:withResult: @/link must be called.
 *
 *  @see                CBATTRequest
 *
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request
{
    
}

/*!
 *  @method peripheralManager:didReceiveWriteRequests:
 *
 *  @param peripheral   The peripheral manager requesting this information.
 *  @param requests     A list of one or more <code>CBATTRequest</code> objects.
 *
 *  @discussion         This method is invoked when <i>peripheral</i> receives an ATT request or command for one or more characteristics with a dynamic value.
 *                      For every invocation of this method, @link respondToRequest:withResult: @/link should be called exactly once. If <i>requests</i> contains
 *                      multiple requests, they must be treated as an atomic unit. If the execution of one of the requests would cause a failure, the request
 *                      and error reason should be provided to <code>respondToRequest:withResult:</code> and none of the requests should be executed.
 *
 *  @see                CBATTRequest
 *
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests
{
    
}

/*!
 *  @method peripheralManagerIsReadyToUpdateSubscribers:
 *
 *  @param peripheral   The peripheral manager providing this update.
 *
 *  @discussion         This method is invoked after a failed call to @link updateValue:forCharacteristic:onSubscribedCentrals: @/link, when <i>peripheral</i> is again
 *                      ready to send characteristic value updates.
 *
 */
- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral
{
    
}

-(void)recievedatafromPeripheral
{
    CBPeripheralManager *ccb = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];

    
}

#pragma mark - CBPeripheral Delegate Methods

/*
 *  @method didDiscoverCharacteristicsForService
 *
 *  @param peripheral Pheripheral that got updated
 *  @param service Service that characteristics where found on
 *  @error error Error message if something went wrong
 *
 *  @discussion didDiscoverCharacteristicsForService is called when CoreBluetooth has discovered
 *  characteristics on a service, on a peripheral after the discoverCharacteristics routine has been called on the service
 *
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
   
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    DLog(@"%@",service.characteristics);
    
    if (!error) {
        NSLog(@"Characteristics of service with UUID : %s found\r\n",[self CBUUIDToString:service.UUID]);
        for(int i=0; i < service.characteristics.count; i++) {
            CBCharacteristic *c = [service.characteristics objectAtIndex:i];
            NSLog(@"Found characteristic %s\r\n",[ self CBUUIDToString:c.UUID]);
            [peripheral discoverDescriptorsForCharacteristic:c];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
            [self.delegate gotErrorWhileConnectivity:error];
        
        NSLog(@"Characteristic discorvery unsuccessful !\r\n");
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    if (!error)
    {
        NSLog(@"Descriptors of Characteristic withUUID : %s found\r\n", [self CBUUIDToString:characteristic.UUID]);
        for (int i = 0; i < [characteristic.descriptors count]; i++) {
            CBDescriptor *d = [characteristic.descriptors objectAtIndex:i];
            NSLog(@"Found descriptor %s\r\n", [self CBUUIDToString:d.UUID]);
        }
       
        CBService *s = [peripheral.services objectAtIndex:(peripheral.services.count - 1)];
        CBCharacteristic *c = [s.characteristics objectAtIndex:(s.characteristics.count - 1)];
        
        {
           if([self compareCBUUID:characteristic.UUID UUID2:c.UUID]) {
                NSLog(@"Finished discovering characteristics");
                
                if ([self.delegate respondsToSelector:@selector(deviceReadyWithInfoPeripheral:)]) {
                    [self.delegate deviceReadyWithInfoPeripheral:peripheral];
                }
                else {
                    [self.delegate deviceReady];
                }
            }

        }
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error
{
    DLog(@"%s",__PRETTY_FUNCTION__);

}


/*
 *  @method didDiscoverServices
 *
 *  @param peripheral Pheripheral that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didDiscoverServices is called when CoreBluetooth has discovered services on a
 *  peripheral after the discoverServices routine has been called on the peripheral
 *
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    if (!error)
    {
        NSLog(@"Services of peripheral with UUID : %@ found\r\n",[peripheral.identifier UUIDString]);
        [self getAllCharacteristicsFromDevice:peripheral];
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(gotDeviceWithOutPairing:)] && peripheral.name != nil  )
        {
            NSLog(@"Enter didDiscoverServices and scan is no");
            [self.delegate gotDeviceWithOutPairing:peripheral.name];
            _shouldScan = NO;
        }
        NSLog(@"Service discovery was unsuccessful ! %@\r\n", error);
    }
}


/*!
 *  @method findBLEPeripherals:
 *
 *  @param timeout timeout in seconds to search for BLE peripherals
 *
 *  @return 0 (Success), -1 (Fault)
 *
 *  @discussion findBLEPeripherals searches for BLE peripherals and sets a timeout when scanning is stopped
 *
 */
- (int) findBLEPeripherals
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    _shouldScan = YES;
    
    NSLog(@"findBLEPeripherals = %d", self.CM.state);
    NSLog(@"State = %d (%s)\r\n",self.CM.state,[self centralManagerStateToString:self.CM.state]);
    
    
    // Client Code
    if (self.CM.state  == CBCentralManagerStatePoweredOn)
        [self.CM scanForPeripheralsWithServices:nil options:nil];
      //  [self.CM scanForPeripheralsWithServices:[NSArray arrayWithObject:[CBUUID UUIDWithString:BloodPressure_Service]] options:nil];
    
    
   // [self.CM scanForPeripheralsWithServices:nil options:nil];// comment after uncomment client code
    self.connectionStats = @"Scanning";
    
    NSLog(@"Scanning");
    return 0; // Started scanning OK !
}


/*
 *  @method getAllServicesFromKeyfob
 *
 *  @param p Peripheral to scan
 *
 *
 *  @discussion getAllServicesFromKeyfob starts a service discovery on a peripheral pointed to by p.
 *  When services are found the didDiscoverServices method is called
 *
 */
-(void) getAllServicesFromDevice:(CBPeripheral *)p
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    self.connectionStats = @"Discovering services..";
    [p discoverServices:nil]; // Discover all services without filter
}


/*
 *  @method getAllCharacteristicsFromKeyfob
 *
 *  @param p Peripheral to scan
 *
 *
 *  @discussion getAllCharacteristicsFromKeyfob starts a characteristics discovery on a peripheral
 *  pointed to by p
 *
 */
-(void) getAllCharacteristicsFromDevice:(CBPeripheral *)p
{
    
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    self.connectionStats = @"Discovering characteristics..";
    for (int i=0; i < p.services.count; i++)
    {
        CBService *s = [p.services objectAtIndex:i];
        NSLog(@"Fetching characteristics for service with UUID : %s\r\n",[self CBUUIDToString:s.UUID]);
        [p discoverCharacteristics:nil forService:s];
    }
}


/*
 *  @method 
 
 *
 *  @param peripheral Pheripheral that got updated
 *  @param characteristic Characteristic that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didUpdateNotificationStateForCharacteristic is called when CoreBluetooth has updated a
 *  notification state for a characteristic
 *
 */

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    // AJAY: call our 4th pop up method.....

    if (([peripheral.name rangeOfString:@"UW-302"].location != NSNotFound))  { //WEL-484, UE-302
    }  else {
        if ([self.delegate respondsToSelector:@selector(gotDeviceUpdateNotificationWithError:)]) {
            [self.delegate gotDeviceUpdateNotificationWithError:error];
        }
        
    }
    
    DLog(@"%s",__PRETTY_FUNCTION__);

    
    if (!error)
    {
        NSLog(@"Updated notification state for characteristic with UUID %s on service with  UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:characteristic.UUID],[self CBUUIDToString:characteristic.service.UUID],[peripheral.identifier UUIDString]);
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:BloodPressureMeasurement_Char]]) {
            self.data = [[NSData alloc] initWithData:characteristic.value];
            NSLog(@"data is %@", self.data);
        }
    }
    else
    {
        /*
        if ([self.delegate respondsToSelector:@selector(gotDeviceWithOutPairing:)] && peripheral.name != nil  )
        {
            [self.delegate gotDeviceWithOutPairing:peripheral.name];
            _shouldScan = NO;
        }
        */

        
        
        //  if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
        //  [self.delegate gotErrorWhileConnectivity:error];

        NSLog(@"Error in setting notification state for characteristic with UUID %s on service with  UUID %s on peripheral with UUID %@\r\n",[self CBUUIDToString:characteristic.UUID],[self CBUUIDToString:characteristic.service.UUID],[peripheral.identifier UUIDString]);
        NSLog(@"Error code was %s\r\n",[[error description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy]);
    }
    
}


/*
 *  @method didUpdateValueForCharacteristic
 *
 *  @param peripheral Pheripheral that got updated
 *  @param characteristic Characteristic that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didUpdateValueForCharacteristic is called when CoreBluetooth has updated a
 *  characteristic for a peripheral. All reads and notifications come here to be processed.
 *
 */
/*
[data setValue:[NSNumber numberWithInteger:sys] forKey:@"systolic"];
[data setValue:[NSNumber numberWithInteger:dia] forKey:@"diastolic"];
[data setValue:[NSNumber numberWithInteger:pul] forKey:@"pulse"];
[data setValue:[NSNumber numberWithInteger:mean] forKey:@"mean"];
*/

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    DLog(@"Peripheral State = %d",[peripheral state]);
    
    //  UInt16 characteristicUUID = [self CBUUIDToInt:characteristic.UUID];
    
    CBUUID *charUUID = characteristic.UUID;
    NSLog(@"charUUID is %@", charUUID);
    NSLog(@"charValue is %@", characteristic.value);
    NSLog(@"error is %@", error);
    
    if (!error)
    {
        [self.CM stopScan];
        
        if ([charUUID isEqual:[CBUUID UUIDWithString:ManufacturerNameString_Char]])
        {
            NSString* manufacturer = [NSString stringWithUTF8String:[characteristic.value bytes]];
            NSLog(@"Manufacturer: %@", manufacturer);
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]]) {
            NSData *activityData = [[NSData alloc] initWithData:characteristic.value];
            int opcode = *(int *)[[activityData subdataWithRange:NSMakeRange(2, 1)] bytes];
            if (opcode == 17) {

            }else if (opcode == 25) {
                int status_setType = *(int *)[[activityData subdataWithRange:NSMakeRange(4, 1)] bytes];
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *components = [calendar components:NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
                
                NSInteger year = components.year;
                NSMutableData *yearData = [[NSMutableData alloc] initWithBytes:&year length:sizeof(year)];
                int year1 = *(int *)[[yearData subdataWithRange:NSMakeRange(0, 1)] bytes];
                int year2 = *(int *)[[yearData subdataWithRange:NSMakeRange(1, 1)] bytes];
                
                
                //int year1 = 222;
                //int year2 = 7;
                
                int month = components.month;
                
                int day = components.day;
                
                int hour = components.hour;
                
                int min = components.minute;
                
                int second = components.second;
                
                char bytes[7];
                
                bytes[0] = year1;
                
                bytes[1] = year2;
                
                bytes[2] = month;
                
                bytes[3] = day;
                
                bytes[4] = hour;
                
                bytes[5] = min;
                
                bytes[6] = second;
                
                
                NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
                NSLog(@"data while setting the date for activity tracker is %@", data);
                
                [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_2]
              characteristicUUID:[CBUUID UUIDWithString: DateTime_Char]
                               p:self.activePeripheral
                            data:data];
                
                // [NSThread sleepForTimeInterval:0.2f];  // Improve for fails to set time.
                
            }else if (opcode == 32) {
                NSLog(@"Enter case of response for profile setting");
                if ([self.delegate respondsToSelector:@selector(trackerDeviceDisconnect:)]) {
                    NSLog(@"DBG, call the disconnect function for the tracker, case of pairing");
                    //Save the tracker name as paired device
                    Users *loginUser = [ANDMedCoreDataManager sharedManager].currentUser;
                    loginUser.uw_name = peripheral.name;
                    
                    //Disable the CCCD notification - WELWORLD-145
                    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_1]
                    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                     p:self.activePeripheral
                                    on:NO];
                    
                    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_2]
                    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_2]
                                     p:self.activePeripheral
                                    on:NO];
                    //End of WELWORLD-145
                    [self.delegate trackerDeviceDisconnect:peripheral];
                    [self.delegate gotDeviceUpdateNotificationWithError:nil];
                } else {
                   NSLog(@"DBG, requesting for activity data");
                   //Start with requesting for write data
                    char dataRequest[5];
                    dataRequest[0] = 0x04;
                    dataRequest[1] = 0x01;
                    dataRequest[2] = 0x58;
                    dataRequest[3] = 0x00;
                    dataRequest[4] = 0x00; //0x01 for all data, 0x00 for unsent data
                    NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
                    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                   p:self.activePeripheral
                                data:dataRequestData];
                    
                    
                    
                }
            } else if (opcode == 64) {
                int status = *(int *)[[activityData subdataWithRange:NSMakeRange(4, 1)] bytes];
                NSLog(@"DBG, received the initialization complete with status %i", status);
                
                if ([self.delegate respondsToSelector:@selector(trackerDeviceDisconnect:)]) {
                    if (status == 1) {
                        NSLog(@"DBG, do nothing this is for pairing");
                        //Saving tracker name as a safety net
                        Users *loginUser = [ANDMedCoreDataManager sharedManager].currentUser;
                        loginUser.uw_name = peripheral.name;
                        
                        //Setting 0X19 for the device Type
                        //Sim writing the 0X19 value
                        
                        
                         char setDeviceType[5];
                         setDeviceType[0] = 0x04;
                         setDeviceType[1] = 0x01;
                         setDeviceType[2] = 0x19;
                         setDeviceType[3] = 0x00;
                         setDeviceType[4] = 0x01;
                        
                        
                        
                        // Kunigita added:
                        // Reset sector data area
                        // For tracker cannot receive BP or WS reading issue.
                      /*  char setDeviceType[4];
                        setDeviceType[0] = 0x03;
                        setDeviceType[1] = 0x01;
                        setDeviceType[2] = 0x12;
                        setDeviceType[3] = 0x07;*/
                        
                        
                        NSData *setDeviceTypeData = [[NSData alloc] initWithBytes:&setDeviceType length:sizeof(setDeviceType)];
                        [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                      characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                       p:self.activePeripheral
                                    data:setDeviceTypeData];
                        
                        
                        //Set time - comment this for now
                        /*
                         NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                         NSDateComponents *components = [calendar components:NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
                         
                         NSInteger year = components.year;
                         NSMutableData *yearData = [[NSMutableData alloc] initWithBytes:&year length:sizeof(year)];
                         int year1 = *(int *)[[yearData subdataWithRange:NSMakeRange(0, 1)] bytes];
                         int year2 = *(int *)[[yearData subdataWithRange:NSMakeRange(1, 1)] bytes];
                         
                         
                         //int year1 = 222;
                         //int year2 = 7;
                         
                         int month = components.month;
                         
                         int day = components.day;
                         
                         int hour = components.hour;
                         
                         int min = components.minute;
                         
                         int second = components.second;
                         
                         char bytes[7];
                         
                         bytes[0] = year1;
                         
                         bytes[1] = year2;
                         
                         bytes[2] = month;
                         
                         bytes[3] = day;
                         
                         bytes[4] = hour;
                         
                         bytes[5] = min;
                         
                         bytes[6] = second;
                         
                         
                         NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
                         NSLog(@"data while setting the date for activity tracker is %@", data);
                         
                         [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_2]
                         characteristicUUID:[CBUUID UUIDWithString: DateTime_Char]
                         p:self.activePeripheral
                         data:data];
                         */
                        
                        
                        
                        
                        
                        
                        
                    }
                    
                    /*//Save the tracker name as paired device
                     Users *loginUser = [ANDMedCoreDataManager sharedManager].currentUser;
                     loginUser.uw_name = peripheral.name;
                     [self.delegate trackerDeviceDisconnect:peripheral];
                     [self.delegate gotDeviceUpdateNotificationWithError:nil];*/
                    
                } else {
                    if (status ==1 ) {
                        NSLog(@"DBG, setting the date and time and personal setting");
                        //Hack for now
                      /*  NSLog(@"DBG, requesting for new data, should happen only once");
                        char dataRequest[5];
                        dataRequest[0] = 0x04;
                        dataRequest[1] = 0x01;
                        dataRequest[2] = 0x58;
                        dataRequest[3] = 0x00;
                        dataRequest[4] = 0x00; //0x01 for all data, 0x00 for new data
                        NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
                        [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                      characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                       p:self.activePeripheral
                                    data:dataRequestData]; */
                        
                         //Set time
                         NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                         NSDateComponents *components = [calendar components:NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
                         
                         NSInteger year = components.year;
                         NSMutableData *yearData = [[NSMutableData alloc] initWithBytes:&year length:sizeof(year)];
                         int year1 = *(int *)[[yearData subdataWithRange:NSMakeRange(0, 1)] bytes];
                         int year2 = *(int *)[[yearData subdataWithRange:NSMakeRange(1, 1)] bytes];
                         
                         
                         //int year1 = 222;
                         //int year2 = 7;
                         
                         int month = components.month;
                         
                         int day = components.day;
                         
                         int hour = components.hour;
                         
                         int min = components.minute;
                         
                         int second = components.second;
                         
                         char bytes[7];
                         
                         bytes[0] = year1;
                         
                         bytes[1] = year2;
                         
                         bytes[2] = month;
                         
                         bytes[3] = day;
                         
                         bytes[4] = hour;
                         
                         bytes[5] = min;
                         
                         bytes[6] = second;
                         
                         
                         NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
                         NSLog(@"data while setting the date for activity tracker is, right before receiving readings %@", data);
                         
                         [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_2]
                         characteristicUUID:[CBUUID UUIDWithString: DateTime_Char]
                         p:self.activePeripheral
                         data:data];
                        
                       // [NSThread sleepForTimeInterval:0.2f]; // Improve for fails to set time.
                        
                        /*   //Write Profile data
                         char profileData[17];
                         profileData[0] = 0x10;
                         profileData[1] = 0x01;
                         profileData[2] = 0x20;
                         profileData[3] = 0x00;
                         profileData[4] = 0xFF;
                         profileData[5] = 0x36;
                         profileData[6] = 0xB0;
                         profileData[7] = 0x06;
                         profileData[8] = 0xA4;
                         profileData[9] = 0x00;
                         profileData[10] = 0x64;
                         profileData[11] = 0x32;
                         profileData[12] = 0x07;
                         profileData[13] = 0xB2;
                         profileData[14] = 0x02;
                         profileData[15] = 0x01;
                         profileData[16] = 0x01;
                         
                         
                         NSData *profile = [[NSData alloc] initWithBytes:&profileData length:sizeof(profileData)];
                         [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                         characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                         p:self.activePeripheral
                         data:profile]; */
                    }
                    
                    
                }
                
                
                
            } else if (opcode == 88) {
                
                NSLog(@"Response for Data for request new data and data is %@", activityData);
                int requestType = *(int *)[[activityData subdataWithRange:NSMakeRange(3, 1)] bytes];
                
                NSLog(@"DBG, the request type is %i", requestType);
                if (requestType == 0) {
                    //This is coming for new data request
                    //Reinitialize all variables
                    NSLog(@"Sim response for new data and  data is %@", activityData);
                    //Killing the timer
                    if (trackerReconnectTimer) {
                        [trackerReconnectTimer invalidate];
                        trackerReconnectTimer = nil;
                    }
                    for (NSTimer *timer in reconnectTimer) {
                        [timer invalidate];
                        
                    }
                    [reconnectTimer removeAllObjects];
                    reconnectTimer = [[NSMutableArray alloc] init];
                    dataArrayCount = 0;
                    [activityDataConversion removeAllObjects];
                    [trackerStringDate removeAllObjects]; //Added for optimization
                    [medicalDataConversion removeAllObjects]; // Added for optimization
                    [dataActivity setLength:0];
                    activityDataCount = 0;
                    activityDataConversion = [[NSMutableDictionary alloc] init];
                    medicalDataConversion =[[NSMutableDictionary alloc] init]; //Addded for optimization
                    trackerStringDate = [[NSMutableArray alloc] init]; //Added for optimization
                    
                    dataPacketEnd = FALSE;
                    dataActivity = [NSMutableData dataWithData:dataActivity];
                    trackerWeightData = 0;
                    trackerBpData = 0;
                   
                    //Right now did not receive any data from uw-302 hence set to 0
                    //dataBP = [NSMutableData dataWithData:dataBP];
                    //dataWeight = [NSMutableData dataWithData:dataWeight];
                    //[dataBP setLength:0];
                    //[dataWeight setLength:0];

                    
                } else if (requestType == 1) {
                    NSData *countData = [activityData subdataWithRange:NSMakeRange(6, 2)];
                    NSString *countString = [self NSDataToHex:countData];
                    NSLog(@"DBG, the countString is %@", countString);
                    if ([countString isEqualToString:@"0001"]) { //Implies the next set of 13 notifications is the last
                        NSLog(@"DBG, the data packet end is true ");
                        dataPacketEnd = TRUE;
                    }
                    if (activityDataCount >= 13) {
                        activityDataCount  = 0;
                        NSLog(@"Sim ,going to receive the next set of data");
                        //This will contain the starting header, we need to parse and set-type
                        //Need to check if BP or Activity or Weight
                    }
                } else if (requestType == 3) {
                    NSLog(@"DBG, received the end of data request issue a disconnect and dataArrayCountis %i", dataArrayCount);
                    //Disable the CCCD notification - WELWORLD-145
                    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_1]
                    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                     p:self.activePeripheral
                                    on:NO];
                    
                    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_2]
                    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_2]
                                     p:self.activePeripheral
                                    on:NO];
                    //End of WELWORLD-145
                    [activityDataConversion setValue:[NSNumber numberWithInt:dataArrayCount] forKey:@"total_keys"];
                    [self optimizeDataOptimize3:activityDataConversion]; //Reduce the number of database inserts
                    
                }
                
                
            }
            
            
            
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_2]]) {
            NSData *ac_dataata = [[NSData alloc] initWithData:characteristic.value];
            int length = [ac_dataata length];
            NSLog(@"DBG, the dataActivity is %@", dataActivity);
            
            //Appending the NSData for  each set of 13 notifications
            [dataActivity appendData:ac_dataata];
            //Parse the data here , get to know what type it is and add it to the relevant type
            activityDataCount++;
            NSLog(@"DBG, the length of data is %i", length);
            NSLog(@"DBG, the activity data count is %i", activityDataCount);
            
            if ((activityDataCount == 10) && (length == 20)) {
                NSLog(@"DBG, need to request for next data" );
                char dataRequest[4];
                dataRequest[0] = 0x03;
                dataRequest[1] = 0x01;
                dataRequest[2] = 0x58;
                dataRequest[3] = 0x01;
                
                NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
                //Start a 2 seconds timer. (if data comes in before that disconnect the timer)
                if (trackerReconnectTimer) {
                    [trackerReconnectTimer invalidate];
                    trackerReconnectTimer = nil;
                }
                NSUInteger size = [reconnectTimer count];
                NSLog(@"DBG, the length of the reconnect  time before requesting next data is %lu", size);
                for (NSTimer *timer in reconnectTimer) {
                    [timer invalidate];
                    
                }
                [reconnectTimer removeAllObjects];
                
                
                [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
              characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                               p:self.activePeripheral
                            data:dataRequestData];
                
                trackerReconnectTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                         target:self
                                                                       selector:@selector(reconnectTracker)
                                                                       userInfo:nil
                                                                        repeats:NO ];
                [reconnectTimer addObject:trackerReconnectTimer];
                
            } else if (activityDataCount == 13) {
                //Killing the timer
                if (trackerReconnectTimer) {
                    [trackerReconnectTimer invalidate];
                    trackerReconnectTimer = nil;
                }
               
                for (NSTimer *timer in reconnectTimer) {
                    [timer invalidate];
                    
                }
                [reconnectTimer removeAllObjects];
                
                //Now we need to append this NSData
                NSLog(@"the data getting appended here for parsing is Sim %@", dataActivity);
                //Logic to see if this is steps data or Blood Pressure/ Weight data
                int header_type = *(int *)[[dataActivity subdataWithRange:NSMakeRange(0, 1)] bytes];
                int flag_data = *(int *)[[dataActivity subdataWithRange:NSMakeRange(1, 1)] bytes];
                if (header_type == 80) {
                    //Can be either BP or activity data
                    if (flag_data == 39) {
                        NSLog(@"Sim. receiving activity data");
                        //First we need to replace all the sleep status
                        int sleep_position = 12;
                        int sleep_count = 1;
                        for (sleep_count =1; sleep_count <= 20; sleep_count++) {
                            //Loop through the dataActivity structure to see if there are 02/01/00 for sleep data
                            int temp_sleep = *(int *)[[dataActivity subdataWithRange:NSMakeRange(sleep_position, 1)] bytes];
                            if (temp_sleep == 2)
                            {
                                char buf[1];
                                buf[0] = 1;
                                [dataActivity replaceBytesInRange:NSMakeRange(sleep_position, 1) withBytes:buf];
                            }
                            sleep_position = sleep_position + 12;
                        }
                        
                        NSLog(@"DBG, the NSData that is getting appended after sleep modification is %@", dataActivity);
                        NSData *timeData = [dataActivity subdataWithRange:NSMakeRange(3, 4)];
                        NSString *timeString = [self NSDataToHex:timeData];
                        NSString *startDateString; //Added for optimization
                        int steps = *(int *)[[dataActivity subdataWithRange:NSMakeRange(8, 1)] bytes];
                        int calorie  = *(int *)[[dataActivity subdataWithRange:NSMakeRange(11, 1)] bytes];
                         NSLog(@"DBG, the steps being added for 1 min of data outside the for loop is %i", steps);
                         NSLog(@"DBG, the calories being added for 1 min of data outside the for loop is %i", calorie);
                        float height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue];
                        NSString *ht_unit = [[ANDMedCoreDataManager sharedManager].currentUser user_metric];
                        float stride_avg = 0.0;
                        if ([ht_unit isEqualToString:@"in"]){
                            //Height in inches
                            float stride_in = height * 0.413;
                           // NSLog(@"DBG,the stride in inches is %f", stride_in);
                            float stride_ft = 0.08333 * stride_in;
                          //  NSLog(@"DBG, the stride in feet is %f",stride_ft);
                            stride_avg = 5280/stride_ft;
                          //  NSLog(@"DBG, the average steps per mile is %f", stride_avg);
                            
                        } else {
                            //Height in cm
                           // NSLog(@"DBG,enter case of cm unit");
                            float string_inches = height * 0.393701;
                            float stride_in = string_inches * 0.413;
                            //NSLog(@"DBG,the stride in inches is %f", stride_in);
                            float stride_ft = 0.08333 * stride_in;
                            //NSLog(@"DBG, the stride in feet is %f",stride_ft);
                            stride_avg = 5280/stride_ft;
                            //NSLog(@"DBG, the average steps per mile is %f", stride_avg);
                        }
                        float distance = steps / stride_avg; //End of WELWORLD-135
                        int sleep = *(int *)[[dataActivity subdataWithRange:NSMakeRange(12, 1)] bytes];
                        if (sleep == 0) {
                            sleep = 20;
                        } else {
                            sleep = 0;
                        }
                        int sleepHours = *(int *)[[dataActivity subdataWithRange:NSMakeRange(13, 1)] bytes];
                        int sleepMin = *(int *)[[dataActivity subdataWithRange:NSMakeRange(14, 1)] bytes];
                        int position = 8;
                        int sleepPosition = 12;
                        int caloriePosition = 11;
                        BOOL isSleepPresent = false;
                        NSLog(@"DBG, the steps are %i", steps);
                        NSLog(@"DBG, the calorie before the for loop are %i", calorie);
                        if ([timeString isEqualToString:@"00000000"])  {
                            NSLog(@"DBG, this is a time setting in future hence dont add it ");
                        } else {
                            //Getting the date logic now
                            NSUInteger hexAsInt = 0;
                            [[NSScanner scannerWithString:timeString] scanHexInt:&hexAsInt];
                            NSString *binary = [NSString stringWithFormat:@"%@", [self toBinary:hexAsInt]];
                            // NSLog(@"DBG, the binary value is %@",binary);
                            int lengthBinary = [binary length];
                            //NSLog(@"DBG, length of binary is %i", lengthBinary);
                            if (lengthBinary == 33) {
                                NSLog(@"This has become a negative value hence ignore the first 1");
                                binary = [binary substringFromIndex:1];
                                
                            }
                            //NSLog(@"DBG, the final binary string before for loop is  %@", binary);
                            NSString *last3String=[binary substringFromIndex:MAX((int)[binary length]-3, 0)];
                            NSString *first3String = [binary substringToIndex:3];
                            NSString *year = [last3String stringByAppendingString:first3String];
                            long yearValue = strtol([year UTF8String], NULL, 2);
                            yearValue = 2005 + yearValue;
                            NSString *month = [binary substringWithRange:NSMakeRange(3, 4)];
                            long monthValue = strtol([month UTF8String], NULL, 2);
                            monthValue = monthValue + 1;
                            NSString *day = [binary substringWithRange:NSMakeRange(7, 5)];
                            long dayValue = strtol([day UTF8String], NULL, 2);
                            NSString *hour = [binary substringWithRange:NSMakeRange(12, 5)];
                            long hourValue = strtol([hour UTF8String], NULL, 2);
                            NSString *min = [binary substringWithRange:NSMakeRange(17, 6)];
                            long minValue = strtol([min UTF8String], NULL, 2);
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            NSDateComponents *components = [[NSDateComponents alloc] init];
                            [components setDay:dayValue];
                            [components setMonth:monthValue];
                            [components setYear:yearValue];
                            [components setHour:hourValue];
                            [components setMinute:minValue];
                            NSDate *received_date = [calendar dateFromComponents:components];
                            NSString *startTime = [NSString stringWithFormat:@"%ld:%ld:%d", hourValue, minValue, 0];
                            NSString *endTime = [NSString stringWithFormat:@"%ld:%ld:%d", hourValue, minValue, 0];
                            NSString *dateString = [NSString stringWithFormat:@"%ld/%02ld/%02ld",yearValue,monthValue,dayValue];
                            
                            //Calculating date String and Time received - WELWORLD-129
                            NSString *str_timeInterval = [CommonClass getTimeStampFromDate:received_date ? received_date : [NSDate date]];
                            NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                            readingDate = readingDate ? readingDate : [NSDate date];
                            //End of WELWORLD-129
                            
                            
                           // if (yearValue <= 2017) //Its a hack hence commented out
                           {
                                dataArrayCount++;
                                NSMutableDictionary *tempActivityDict = [NSMutableDictionary dictionary];
                                //Logic to add sleep into the dictionary
                                [tempActivityDict setValue:[NSNumber numberWithInteger:steps] forKey:ACTIVITY_DATA_KEY_STEPS];
                                [tempActivityDict setValue:[NSNumber numberWithInt:calorie] forKey:ACTIVITY_DATA_KEY_CALORIE];
                               
                                [tempActivityDict setValue:[NSNumber numberWithInteger:sleep] forKey:ACTIVITY_DATA_KEY_SLEEP_STATUS];
                                [tempActivityDict setValue:[NSNumber numberWithInteger:sleepHours] forKey:ACTIVITY_DATA_KEY_SLEEP_HOURS];
                                [tempActivityDict setValue:[NSNumber numberWithInteger:sleepMin] forKey:ACTIVITY_DATA_KEY_SLEEP_MINS]; //End of adding sleep logic
                                [tempActivityDict setValue:[NSNumber numberWithFloat:distance] forKey:ACTIVITY_DATA_KEY_DISTANCE];
                                int totalSleepMin = (sleepHours * 60) + sleepMin;
                                [tempActivityDict setValue:[NSNumber numberWithInteger:totalSleepMin] forKey:ACTIVITY_DATA_KEY_SLEEP];
                                [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
                                [tempActivityDict setValue:endTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTENDTIME];
                                //WELWORLD-129
                                 [tempActivityDict setValue:readingDate forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
                                 NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                                if (lifeTrackKindDateString)
                                [tempActivityDict setValue:lifeTrackKindDateString forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
                                //End of WELWORLD-129
                                [tempActivityDict setValue:peripheral.name forKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
                                [tempActivityDict setValue:@"UW-302" forKey:ACTIVITY_DATA_KEY_PERIPHERAL_UUUID];
                                [activityDataConversion setObject:tempActivityDict forKey:[NSNumber numberWithInt:dataArrayCount]];
                                NSLog(@"DBG,the value of the dataArrayCount for case 13 temp first extraction is %i", dataArrayCount);
                               
                                NSLog(@"DBG, the dateString getting appeneded is %@", dateString);
                               NSLog(@"DBG, the date received printed outside the for loop is %@", received_date);
                               startDateString = lifeTrackKindDateString;
                              // NSLog(@"DBG, the startDateString outside the for loop is %@", startDateString);
                              
                               if ([trackerStringDate containsObject:startDateString]) {
                                   //Dont do anything since it alreadt contains the object
                               } else {
                                   [trackerStringDate addObject:startDateString];
                               }
                                NSLog(@"DBG, the array trackerdate string outside the for loop is %@", trackerStringDate);
                                
                            }
                            
                        }
                        
                        //Get the timestring, if it matches then start for loop to add all data
                        
                        for ( int i = 1; i < 20; i++) {
                            
                            position = position + 12;
                            caloriePosition = position + 3;
                            // distancePosition = position + 4;
                            sleepPosition = position + 4;
                            NSData *timeData = [dataActivity subdataWithRange:NSMakeRange((position - 5), 4)];
                            NSString *timeString = [self NSDataToHex:timeData];
                            NSLog(@"DBG, the convert NSString for time case 13 inside for loop is %@", timeString);
                            if ([timeString isEqualToString:@"00000000"]) {
                                NSLog(@"DBG, this is a time in future hence ignore");
                                continue;
                            }
                            //Getting the date logic now
                            NSUInteger  temphexAsInt = 0;
                            [[NSScanner scannerWithString:timeString] scanHexInt:&temphexAsInt];
                            //NSLog(@"DBG, the hex as Int within loop is %lu", (unsigned long)temphexAsInt);
                            NSString *binary = [NSString stringWithFormat:@"%@", [self toBinary:temphexAsInt]];
                            int lengthBinary = [binary length];
                            if (lengthBinary == 33) {
                                NSLog(@"This has become a negative value hence ignore the first 1");
                                binary = [binary substringFromIndex:1];
                                
                            }
                            //NSLog(@"DBG, the final binary string inside for loop is  %@", binary);
                            NSString *last3String=[binary substringFromIndex:MAX((int)[binary length]-3, 0)];
                            NSString *first3String = [binary substringToIndex:3];
                            NSString *year = [last3String stringByAppendingString:first3String];
                            long yearValue = strtol([year UTF8String], NULL, 2);
                            yearValue = 2005 + yearValue;
                           // NSLog(@"DBG, the year value is %lu", yearValue);
                           /* if (yearValue > 2017) { //This is a hack hence commenting it
                                
                                NSLog(@"DBG, this is the yearValue %li", yearValue);
                                continue;
                            } */
                            NSString *month = [binary substringWithRange:NSMakeRange(3, 4)];
                            long monthValue = strtol([month UTF8String], NULL, 2);
                            monthValue = monthValue + 1;
                            NSString *day = [binary substringWithRange:NSMakeRange(7, 5)];
                            long dayValue = strtol([day UTF8String], NULL, 2);
                            NSString *hour = [binary substringWithRange:NSMakeRange(12, 5)];
                            long hourValue = strtol([hour UTF8String], NULL, 2);
                            NSString *min = [binary substringWithRange:NSMakeRange(17, 6)];
                            long minValue = strtol([min UTF8String], NULL, 2);
                            
                            NSCalendar *calendar = [NSCalendar currentCalendar];
                            NSDateComponents *components = [[NSDateComponents alloc] init];
                            [components setDay:dayValue];
                            [components setMonth:monthValue];
                            [components setYear:yearValue];
                            [components setHour:hourValue];
                            [components setMinute:minValue];
                            NSDate *received_date = [calendar dateFromComponents:components];
                            //Calculating date String and Time received - WELWORLD-129
                            NSString *str_timeInterval = [CommonClass getTimeStampFromDate:received_date ? received_date : [NSDate date]];
                            NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                            readingDate = readingDate ? readingDate : [NSDate date];
                            //End of WELWORLD-129
                            NSString *startTime = [NSString stringWithFormat:@"%ld:%ld:%d", hourValue, minValue, 0];
                            NSString *endTime = [NSString stringWithFormat:@"%ld:%ld:%d", hourValue, minValue, 0];
                            NSString *dateString = [NSString stringWithFormat:@"%ld/%02ld/%02ld",yearValue,monthValue,dayValue];
                            NSLog(@"DBG, inside the for loop the dateString is %@", dateString);
                            NSLog(@"DBG, the date received printed in case 13 for loop is %@", received_date);
                            dataArrayCount++;
                            int tempSteps = *(int *)[[dataActivity subdataWithRange:NSMakeRange(position, 1)] bytes];
                            int tempCalorie = *(int *)[[dataActivity subdataWithRange:NSMakeRange(caloriePosition, 1)] bytes];
                            // float tempCalorieFloat = tempCalorieValue;
                            // float tempCalorie =  tempCalorieFloat/10;
                            NSLog(@"DBG, the calorie value integer inside for loop is %i", tempCalorie);
                            NSLog(@"DBG, the steps value integer inside for loop is %i", tempSteps);
                            //Start of WELWORLD-135
                            float height = [[[ANDMedCoreDataManager sharedManager].currentUser user_height] floatValue];
                            NSString *ht_unit = [[ANDMedCoreDataManager sharedManager].currentUser user_metric];
                            float stride_avg = 0.0;
                            if ([ht_unit isEqualToString:@"in"]){
                                //Height in inches
                                float stride_in = height * 0.413;
                                //NSLog(@"DBG,the stride in inches inside the forloop  is %f", stride_in);
                                float stride_ft = 0.08333 * stride_in;
                                //NSLog(@"DBG, the stride in feet inside the forloop is %f",stride_ft);
                                stride_avg = 5280/stride_ft;
                                //NSLog(@"DBG, the average steps per mile inside the forloop is %f", stride_avg);
                                
                            } else {
                                //Height in cm
                                //NSLog(@"DBG,enter case of cm unit within for loop");
                                float string_inches = height * 0.393701;
                                float stride_in = string_inches * 0.413;
                                //NSLog(@"DBG,the stride in inches is %f", stride_in);
                                float stride_ft = 0.08333 * stride_in;
                                //NSLog(@"DBG, the stride in feet is %f",stride_ft);
                                stride_avg = 5280/stride_ft;
                                //NSLog(@"DBG, the average steps per mile is %f", stride_avg);
                            }
                            float tempDistance = tempSteps / stride_avg; //End of WELWORLD-135
                            //NSLog(@"DBG, the distance temp inside the loop is %f", tempDistance);
                            //float tempDistance = *(int *)[[dataActivity subdataWithRange:NSMakeRange(distancePosition, 1)] bytes];
                            //End of WELWORLD-125
                            
                            int tempSleepStatus = *(int *)[[dataActivity subdataWithRange:NSMakeRange(sleepPosition, 1)] bytes];
                            if (tempSleepStatus == 0) {
                                tempSleepStatus = 20;
                            } else {
                                tempSleepStatus = 0;
                            }
                            int tempsleepHours = *(int *)[[dataActivity subdataWithRange:NSMakeRange((sleepPosition + 1), 1)] bytes];
                            int tempsleepMin = *(int *)[[dataActivity subdataWithRange:NSMakeRange((sleepPosition + 2), 1)] bytes];
                            
                            steps = steps + tempSteps;
                            calorie = calorie + tempCalorie;
                            
                            //Logic to add this into the dictionary
                            NSMutableDictionary *tempActivityDict = [NSMutableDictionary dictionary];
                            [tempActivityDict setValue:[NSNumber numberWithInteger:tempSteps] forKey:ACTIVITY_DATA_KEY_STEPS];
                            [tempActivityDict setValue:[NSNumber numberWithInt:tempCalorie] forKey:ACTIVITY_DATA_KEY_CALORIE];
                            
                            //Sim adding sleep data
                            [tempActivityDict setValue:[NSNumber numberWithInteger:tempSleepStatus] forKey:ACTIVITY_DATA_KEY_SLEEP_STATUS];
                            [tempActivityDict setValue:[NSNumber numberWithInteger:tempsleepHours] forKey:ACTIVITY_DATA_KEY_SLEEP_HOURS];
                            [tempActivityDict setValue:[NSNumber numberWithInteger:tempsleepMin] forKey:ACTIVITY_DATA_KEY_SLEEP_MINS];
                            [tempActivityDict setValue:[NSNumber numberWithFloat:tempDistance] forKey:ACTIVITY_DATA_KEY_DISTANCE];
                            [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
                            [tempActivityDict setValue:endTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTENDTIME];
                            
                            //WELWORLD-129
                            [tempActivityDict setValue:readingDate forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
                            NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                            if (lifeTrackKindDateString)
                                [tempActivityDict setValue:lifeTrackKindDateString forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
                            //End of WELWORLD-129
                            //Code for optimization
                            NSLog(@"DBG, the lifetrackdate string inside the for loop is %@", lifeTrackKindDateString);
                            //NSLog(@"DBG, the start date string inside the for loop is %@", startDateString);
                            NSLog(@"DBG, the trackerStringDate inside the for loop is %@", trackerStringDate);
                            if ([lifeTrackKindDateString isEqualToString:startDateString]) {
                                //Dont do anything
                            } else {
                                if ([trackerStringDate containsObject:lifeTrackKindDateString]) {
                                    //Dont do anything since it alreadt contains the object
                                } else {
                                    [trackerStringDate addObject:lifeTrackKindDateString];
                                }
                                startDateString = lifeTrackKindDateString;
                            }
                            
                            //End of code for optimization
                            [tempActivityDict setValue:peripheral.name forKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
                            [tempActivityDict setValue:@"UW-302" forKey:ACTIVITY_DATA_KEY_PERIPHERAL_UUUID];
                            int temptotalSleepMin = (tempsleepHours * 60) + tempsleepMin;
                            [tempActivityDict setValue:[NSNumber numberWithInteger:temptotalSleepMin] forKey:ACTIVITY_DATA_KEY_SLEEP];
                            
                            // NSLog(@"DBG, the sleep hours inside the loop is %i", tempsleepHours);
                            // NSLog(@"DBG, the sleep min inside the loop is %i", tempsleepMin);
                            // NSLog(@"DBG, the total sleep inside the for loop is %i", temptotalSleepMin);
                            //NSLog(@"DBG, the received time inside the for loop is %@", readingDate);
                            [activityDataConversion setObject:tempActivityDict forKey:[NSNumber numberWithInt:dataArrayCount]];
                            NSLog(@"DBG,the value of the dataArrayCount in for loop case 13 is %i", dataArrayCount);
                            
                        }
                       // NSLog(@"DBG, the total steps are for a single chunk in case 13 is %i", steps);
                        //NSLog(@"DBG, the total calories are for a single chunk in case 13 is %i",  calorie);
                        [dataActivity setLength:0];  //Clearing the NSMutableData for the next set of notification
                       
                        
                    } else if (flag_data == 40) {
                        NSLog(@"DBG, receiving BP data");
                        int fw_data_length = *(int *)[[dataActivity subdataWithRange:NSMakeRange(51, 1)] bytes];
                        NSLog(@"DBG, the fw length is %i", fw_data_length);
                        // K unigita:  Get FW revision string. (But need not to use.)
                        //NSString *fw_data = [[NSString alloc] initWithData:[dataActivity subdataWithRange:NSMakeRange(52, fw_data_length)] encoding:NSUTF8StringEncoding];
                        //NSLog(@"KN. FW REV is : %@", fw_data);
                        
                        /* Kunigita
                         About firmware revision of UA-651BLE. It's different byte length by version.
                         (e) version is 14.
                         Ohter older version is 12.
                         And in sector data, start bytes of BP measurement is not fixed.
                         So calculate from fw revision data length.
                         */
                        
                        
                        /*
                         int sys = *(int *)[[dataActivity subdataWithRange:NSMakeRange(96, 2)] bytes];
                         int dia = *(int *)[[dataActivity subdataWithRange:NSMakeRange(98, 2)] bytes];
                         // int mean = *(int *)[[self.data subdataWithRange:NSMakeRange(5, 2)] bytes];
                         int year = *(int *)[[dataActivity subdataWithRange:NSMakeRange(102, 2)] bytes];
                         int month = *(int *)[[dataActivity subdataWithRange:NSMakeRange(104, 1)] bytes];
                         int day = *(int *)[[dataActivity subdataWithRange:NSMakeRange(105, 1)] bytes];
                         int hour = *(int *)[[dataActivity subdataWithRange:NSMakeRange(106, 1)] bytes];
                         int minutes = *(int *)[[dataActivity subdataWithRange:NSMakeRange(107, 1)] bytes];
                         int seconds = *(int *)[[dataActivity subdataWithRange:NSMakeRange(108, 1)] bytes];
                         int pul = *(int *)[[dataActivity subdataWithRange:NSMakeRange(109, 2)] bytes];
                         */
                        
                        /* Kunigita: Caluculate SYS byte array
                         FW_REV length data = 51th
                         FW_REV length = [1 + variable(fw_data_length)]
                         OTHER = SW_REV[1+4] + IEEE[1+22] + BP_MEASURE_LENGTH[1] + BP_FLAG[1] = [30]
                         RESULT == 51 + 1 + fw_data_length + 30  == fw_data_length + 82
                         */
                        
                        /*Adding logic to parse Blood Pressure reading sent from tracker
                         The tracker receives the BP readings from UA-1200 and UB-1100
                         WELWORLD-139 . For these the FW revision is 5 bytes and software revision is 5 bytes
                         */
                        int sys = 0;
                        int dia= 0;
                        int year = 0;
                        int month =0;
                        int day = 0;
                        int hour = 0;
                        int minutes = 0;
                        int seconds = 0;
                        int pul = 0;
                        
                        if (fw_data_length == 5) {
                            sys = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+83, 2)] bytes];
                            dia = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+85, 2)] bytes];
                            // int mean = *(int *)[[self.data subdataWithRange:NSMakeRange(fw_data_length+86, 2)] bytes];
                            year = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+89, 2)] bytes];
                            month = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+91, 1)] bytes];
                            day = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+92, 1)] bytes];
                            hour = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+93, 1)] bytes];
                            minutes = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+94, 1)] bytes];
                            seconds = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+95, 1)] bytes];
                            pul = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+96, 2)] bytes];
                        } else {
                            //Case of UA-651
                            sys = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+82, 2)] bytes];
                            dia = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+84, 2)] bytes];
                            // int mean = *(int *)[[self.data subdataWithRange:NSMakeRange(fw_data_length+86, 2)] bytes];
                            year = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+88, 2)] bytes];
                            month = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+90, 1)] bytes];
                            day = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+91, 1)] bytes];
                            hour = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+92, 1)] bytes];
                            minutes = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+93, 1)] bytes];
                            seconds = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+94, 1)] bytes];
                            pul = *(int *)[[dataActivity subdataWithRange:NSMakeRange(fw_data_length+95, 2)] bytes];
                           

                        } //End of WELWORLD-139
                        
                       
                        NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
                        NSLog(@"DBG, the systolic received is %i", sys);
                        NSLog(@"DBG, the diastolic received is %i", dia);
                        NSDateComponents *components = [[NSDateComponents alloc] init];
                        [components setYear:year];
                        [components setMonth:month];
                        [components setDay:day];
                        [components setHour:hour];
                        [components setMinute:minutes];
                        [components setSecond:seconds];
                        
                        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
                        NSDate *time = [calendar dateFromComponents:components];
                        
                        if (sys == 255 && dia == 255 && pul == 255) {
                            
                            if ([self.delegate respondsToSelector:@selector(gotBloodPressureCufError:)]) {
                                [self.delegate gotBloodPressureCufError:@"bp"];
                            }
                            return;
                        }
                        NSMutableDictionary *tempActivityBp = [NSMutableDictionary dictionary];
                        
                        [tempActivityBp setValue:[NSNumber numberWithInteger:sys] forKey:BP_DATA_KEY_SYSTOLIC];
                        [tempActivityBp setValue:[NSNumber numberWithInteger:dia] forKey:BP_DATA_KEY_DIASTOLIC];
                        [tempActivityBp setValue:[NSNumber numberWithInteger:pul] forKey:BP_DATA_KEY_PULSE];
                        //[data setValue:[NSNumber numberWithInteger:mean] forKey:BP_DATA_KEY_MEAN];
                        [tempActivityBp setValue:peripheral.identifier.UUIDString forKey:BP_USERINFO_KEY_DEVICE_ID];
                        [tempActivityBp setValue:[NSNumber numberWithBool:YES] forKey:BP_USERINFO_KEY_SYNC_REQUIRED];
                        
                        NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : [NSDate date]];
                        NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                        
                        readingDate = readingDate ? readingDate : [NSDate date];
                        
                        [tempActivityBp setObject:readingDate forKey:BP_USERINFO_KEY_MEASUREMENTDATETIME];
                        [tempActivityBp setValue:peripheral.name ? peripheral.name : @"UW-302-BP" forKey:BP_USERINFO_KEY_CBPERIPHERAL_NAME];
                        
                        //WEL-400
                        NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];//End of WEL-400
                        if (lifeTrackKindDateString)
                            [tempActivityBp setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                                                
                        [tempActivityBp setValue:@"bp" forKey:BP_USERINFO_KEY_DEVICE_TYPE];
                        
                       // [self updateBloodPressureWithDictionary:tempActivityBp forPeripheral:peripheral];
                        
                        
                        NSLog(@"DBG, resetting the dataActivity for Blood Pressure monitor scale ");
                        trackerBpData++;
                        [dataActivity setLength:0];
                        //[activityDataConversion setValue:[NSNumber numberWithInteger:trackerBpData] forKey:@"bp_count"];
                        [medicalDataConversion setValue:[NSNumber numberWithInteger:trackerBpData] forKey:@"bp_count"];
                        NSString *bp_key =[@"bp" stringByAppendingFormat:@"%d",trackerBpData];
                        NSLog(@"DBG, the bp key is %@", bp_key);
                       // [activityDataConversion setObject:tempActivityBp forKey:bp_key];
                        [medicalDataConversion setObject:tempActivityBp forKey:bp_key];

                        
                    } else if (flag_data == 41) {
                        NSLog(@"DBG, header case of 50 entered for weight scale now");
                        
                        {
                            NSMutableDictionary *tempActivityWs = [NSMutableDictionary dictionary];
                            int fw_data_length = *(int *)[[dataActivity subdataWithRange:NSMakeRange(51, 1)] bytes];
                            NSLog(@"DBG, the extracted fw_data_length is %i", fw_data_length);
                            
                            //Extracting the flag data and getting the unit
                            int index = 52 + fw_data_length + 29;
                            NSLog(@"DBG, the index is %i", index);
                            
                            int unit = *(int *)[[dataActivity subdataWithRange:NSMakeRange(index, 1)] bytes];
                            if (unit == 0 || unit == 2) {
                                NSLog(@"unit is kg");
                                [tempActivityWs setValue:@"kg" forKey:WS_USERINFO_KEY_UNIT];
                            } else if (unit == 1 || unit == 3) {
                                NSLog(@"unit is lb");
                                [tempActivityWs setValue:@"lb" forKey:WS_USERINFO_KEY_UNIT];
                            }
                            
                            //Calculating the weight received - Right now UW-302 receives value only from Continua scale i.e the standard BLE Protocol Weight scale which has resolution of 200. Hence the below
                            int  value = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 1, 2)] bytes];
                            NSLog(@"Sim. the weight through tracker is %i", value);
                            value = value * 5;
                            float valueFloat = value;
                            float weight = valueFloat/1000;
                            NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                            [tempActivityWs setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                            
                            
                            
                            int year = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 3, 2)] bytes];
                            int month = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 5, 1)] bytes];
                            int day = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 6, 1)] bytes];
                            int hour = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 7, 1)] bytes];
                            int minutes = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 8, 1)] bytes];
                            int seconds = *(int *)[[dataActivity subdataWithRange: NSMakeRange(index + 9, 1)] bytes];
                           
                            
                            NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
                          
                            
                            NSDateComponents *components = [[NSDateComponents alloc] init];
                            [components setYear:year];
                            [components setMonth:month];
                            [components setDay:day];
                            [components setHour:hour];
                            [components setMinute:minutes];
                            [components setSecond:seconds];
                            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
                            NSDate *time = [calendar dateFromComponents:components];
                         
                            NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : [NSDate date]];
                            NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                            
                            readingDate = readingDate ? readingDate : [NSDate date];

                            NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];//End of WEL-400
                            if (lifeTrackKindDateString)
                               [tempActivityWs setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                            [tempActivityWs setValue:peripheral.identifier.UUIDString forKey:WS_USERINFO_KEY_DEVICE_ID];
                            [tempActivityWs setValue:peripheral.name forKey:WS_USERINFO_KEY_CBPERIPHERAL_NAME];
                            [tempActivityWs setValue:@"weight" forKey:WS_USERINFO_KEY_DEVICE_TYPE]; //Sim check this
                            [tempActivityWs setValue:readingDate forKey:TH_USERINFO_KEY_RECEIVEDDATETIME];
                            [tempActivityWs setValue:[NSNumber numberWithBool:YES] forKey:TH_USERINFO_KEY_SYNC_REQUIRED];
                   
                            
                       //Update the counter that tells how many weight readings have come in
                       trackerWeightData++;
                       NSLog(@"DBG, resetting the dataActivity for weight scale ");
                       [dataActivity setLength:0];
                      // [activityDataConversion setValue:[NSNumber numberWithInteger:trackerWeightData] forKey:@"weight_count"];
                        [medicalDataConversion setValue:[NSNumber numberWithInteger:trackerWeightData] forKey:@"weight_count"];
                        NSString *weight_key =[@"weight" stringByAppendingFormat:@"%d",trackerWeightData];
                        NSLog(@"DBG, the weight key is %@", weight_key);
                      //  [activityDataConversion setObject:tempActivityWs forKey:weight_key];
                       [medicalDataConversion setObject:tempActivityWs forKey:weight_key];
                            
                        }
                        
                    }

                    
                }
                else if (header_type == 48) {
                    if (flag_data == 41) {
                        NSMutableDictionary *data = [NSMutableDictionary new];
                        
                        NSData *weightData = [[NSData alloc] initWithData:characteristic.value];
                        NSLog(@"WEIGHT LENGTH inside the activity case = %lu",(unsigned long)[weightData length]);
                        if (weightData.length > 3)
                        {
                            //Extracting the flag data and getting the unit
                            int unit = *(int *)[[weightData subdataWithRange:NSMakeRange(96, 1)] bytes];
                            if (unit == 0 || unit == 2) {
                                NSLog(@"unit is kg");
                                [data setValue:@"kg" forKey:WS_USERINFO_KEY_UNIT];
                            } else if (unit == 1 || unit == 3) {
                                NSLog(@"unit is lb");
                                [data setValue:@"lb" forKey:WS_USERINFO_KEY_UNIT];
                            }
                            NSLog(@"DBG, the unit is %d", unit);
                            //Calculating the weight received
                            int  value = *(int *)[[weightData subdataWithRange: NSMakeRange(97, 2)] bytes];
                            //Sim changes for WELWORLD-29
                            if ([charUUID isEqual:[CBUUID UUIDWithString:WeightScalebleMeasurement_Char]]) { //Sig implemented protocol
                                if ((unit & 0x01) == 0) {
                                    NSLog(@"Unit coming in is of kg");
                                    value = value * 5;
                                    float valueFloat = value;
                                    float weight = valueFloat/1000;
                                    NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                                    [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                                    
                                } else {
                                    NSLog(@"Unit coming in is of lb");
                                    float valueFloat = value;
                                    float weight = valueFloat/100;
                                    NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                                    [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                                    
                                }
                            } else { //Case of ADC Proprietary weight scale protocol
                                NSLog(@"DBG, this is continua protocol");
                                value = *(int *)[[weightData subdataWithRange: NSMakeRange(97, 2)] bytes];
                                float valueFloat = value;
                                float weight = valueFloat/10.0;
                                NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                                [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                                
                            }
                            NSLog(@"Sim. case of activity and the value is %d", value);
                            //Calculating time of receiving measurement
                            int year = *(int *)[[weightData subdataWithRange:NSMakeRange(99, 2)] bytes];
                            int month = *(int *)[[weightData subdataWithRange:NSMakeRange(101, 1)] bytes];
                            int day = *(int *)[[weightData subdataWithRange:NSMakeRange(102, 1)] bytes];
                            int hour = *(int *)[[weightData subdataWithRange:NSMakeRange(103, 1)] bytes];
                            int minutes = *(int *)[[weightData subdataWithRange:NSMakeRange(104, 1)] bytes];
                            int seconds = *(int *)[[weightData subdataWithRange:NSMakeRange(105, 1)] bytes];
                            NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
                            
                            NSDateComponents *components = [[NSDateComponents alloc] init];
                            [components setYear:year];
                            [components setMonth:month];
                            [components setDay:day];
                            [components setHour:hour];
                            [components setMinute:minutes];
                            [components setSecond:seconds];
                            //WEL-400
                            // NSCalendar *calendar = [NSCalendar currentCalendar];
                            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];//End of WEL-400
                            NSDate *time = [calendar dateFromComponents:components];
                            
                            //Getting [NSDate date] date in Gregorian format
                            NSDate* currentDate = [NSDate date];
                            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                            NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                            NSDateComponents *comps = [[NSDateComponents alloc] init];
                            [comps setDay:[gregorianComponents day]];
                            [comps setMonth:[gregorianComponents month]];
                            [comps setYear:[gregorianComponents year]];
                            [comps setHour:[gregorianComponents hour]];
                            [comps setMinute:[gregorianComponents minute]];
                            [comps setSecond:[gregorianComponents second]];
                            NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                            NSDate *today = [currentCalendar dateFromComponents:comps];
                            NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : today];
                            
                            
                            NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                            NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                            
                            //TODO:: Need to track...
                            readingDate = readingDate ? readingDate : [NSDate date];
                            
                            [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                            [data setValue:peripheral.identifier.UUIDString forKey:WS_USERINFO_KEY_DEVICE_ID];
                            [data setValue:peripheral.name forKey:WS_USERINFO_KEY_CBPERIPHERAL_NAME];
                            [data setValue:@"weight" forKey:WS_USERINFO_KEY_DEVICE_TYPE];
                            [data setValue:readingDate forKey:WS_USERINFO_KEY_RECEIVEDDATETIME];
                            [data setValue:[NSNumber numberWithBool:YES] forKey:WS_USERINFO_KEY_SYNC_REQUIRED];
                            
                            
                        }
                        else
                        {
                            //Extracting the flag data and getting the unit
                            int unit = *(int *)[[weightData subdataWithRange:NSMakeRange(96, 1)] bytes];
                            if (unit == 0 || unit == 2) {
                                NSLog(@"unit is kg");
                                [data setValue:@"kg" forKey:WS_USERINFO_KEY_UNIT];
                            } else if (unit == 1 || unit == 3) {
                                NSLog(@"unit is lb");
                                [data setValue:@"lb" forKey:WS_USERINFO_KEY_UNIT];
                            }
                            
                            //Calculating the weight received
                            int  value = *(int *)[[weightData subdataWithRange: NSMakeRange(97, 2)] bytes];
                            //Sim changes for WELWORLD-29
                            if ([charUUID isEqual:[CBUUID UUIDWithString:WeightScalebleMeasurement_Char]]) { //Sig implemented protocol
                                if ((unit & 0x01) == 0) {
                                    NSLog(@"Unit coming in is of kg");
                                    value = value * 5;
                                    float valueFloat = value;
                                    float weight = valueFloat/1000;
                                    NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                                    [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                                    
                                } else {
                                    NSLog(@"Unit coming in is of lb");
                                }
                            } else { //Case of ADC Proprietary weight scale protocol
                                value = *(int *)[[weightData subdataWithRange: NSMakeRange(97, 2)] bytes];
                                float valueFloat = value;
                                float weight = valueFloat/10.0;
                                NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                                [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                                
                            }
                            
                            //Getting [NSDate date] in Gregorian format
                            NSDate* currentDate = [NSDate date];
                            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                            NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                            NSDateComponents *comps = [[NSDateComponents alloc] init];
                            [comps setDay:[gregorianComponents day]];
                            [comps setMonth:[gregorianComponents month]];
                            [comps setYear:[gregorianComponents year]];
                            [comps setHour:[gregorianComponents hour]];
                            [comps setMinute:[gregorianComponents minute]];
                            [comps setSecond:[gregorianComponents second]];
                            NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                            NSDate *today = [currentCalendar dateFromComponents:comps];
                            
                            // NSString *str_timeInterval = [CommonClass getTimeStampFromDate:[NSDate date]];
                            NSString *str_timeInterval = [CommonClass getTimeStampFromDate:today]; //End of WEL-400
                            NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                            NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                            //TODO:: Need to track...
                            readingDate = readingDate ? readingDate : [NSDate date];
                            
                            [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                            [data setValue:peripheral.identifier.UUIDString forKey:WS_USERINFO_KEY_DEVICE_ID];
                            [data setValue:peripheral.name forKey:WS_USERINFO_KEY_CBPERIPHERAL_NAME];
                            [data setValue:@"weight" forKey:WS_USERINFO_KEY_DEVICE_TYPE]; //Sim check this
                            [data setValue:readingDate forKey:TH_USERINFO_KEY_RECEIVEDDATETIME];
                            [data setValue:[NSNumber numberWithBool:YES] forKey:TH_USERINFO_KEY_SYNC_REQUIRED];
                        }
                        
                        [self updateWeightWithDictionary:data];
                       
                        
                    }
                }
                
                
                
                
                //Check if this was the last set of data
                if (dataPacketEnd) {
                    NSLog(@"DBG, requesting the end now");
                    char dataRequest[4];
                    dataRequest[0] = 0x03;
                    dataRequest[1] = 0x01;
                    dataRequest[2] = 0x58;
                    dataRequest[3] = 0x03;
                    
                    NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
                    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                   p:self.activePeripheral
                                data:dataRequestData];
                } else {
                    NSLog(@"DBG, not the end request, just next data packet");
                    char dataRequest[4];
                    dataRequest[0] = 0x03;
                    dataRequest[1] = 0x01;
                    dataRequest[2] = 0x58;
                    dataRequest[3] = 0x01;
                    
                    NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
                    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                   p:self.activePeripheral
                                data:dataRequestData];
                    
                    if (trackerReconnectTimer) {
                        [trackerReconnectTimer invalidate];
                        trackerReconnectTimer = nil;
                    }
                    for (NSTimer *timer in reconnectTimer) {
                        [timer invalidate];
                    }
                    [reconnectTimer removeAllObjects];
                    
                    //Do not want to start the timer again since we are already having a timer to get the write request
                    //Start a 2 seconds timer. (if data comes in before that disconnect the timer)
                 /*   trackerReconnectTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                             target:self
                                                                           selector:@selector(reconnectTracker)
                                                                           userInfo:nil
                                                                            repeats:NO ];
                    [reconnectTimer addObject:trackerReconnectTimer]; */
                    
                }
                
            }
            
            
            
            
        } //End of WEL-484, Activity Tracker UW-302
        else if ([charUUID isEqual:[CBUUID UUIDWithString:ModelNumberString_Char]])
        {
            NSString *modelNumber = [NSString stringWithUTF8String:[characteristic.value bytes]];
            NSLog(@"Model Number: %@", modelNumber);
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:FirmwareRevisionString_Char]])
        {
            NSString *firmware = [NSString stringWithUTF8String:[characteristic.value bytes]];
            NSLog(@"Firmware: %@", firmware);
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:SoftwareRevisionString_Char]])
        {
            NSString *software = [NSString stringWithUTF8String:[characteristic.value bytes]];
            NSLog(@"Software: %@", software);
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:SystemID_Char]]) {
            NSString *systemID = [NSString stringWithUTF8String:[characteristic.value bytes]];
            NSLog(@"System ID: %@", systemID);
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:BatteryLevel_Char]])
        {
            char batlevel;
            [characteristic.value getBytes:&batlevel length:BatteryLevel_Length];
            self.batteryLevel = (float)batlevel;
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:BloodPressureMeasurement_Char]])
        {
            NSLog(@"DBG, number of times entering this loop here ");
            NSData *bpmData = [[NSData alloc] initWithData:characteristic.value];
            NSLog(@"BPM LENGTH = %d",[bpmData length]);
            bool userid = false;
            self.data = [[NSData alloc] initWithData:characteristic.value];
            
            
            if (bpmData.length >= 12)
            {
                NSMutableDictionary *data = [[NSMutableDictionary alloc]init];
                
                int flag = *(int *)[[self.data subdataWithRange:NSMakeRange(0, 1)] bytes];
                
                if ((flag & 0x08) == 0) {// Userid is not present in the flag bit
                    
                    userid = false;
                } else {
                    
                    userid = true;
                }
                
                int sys = *(int *)[[self.data subdataWithRange:NSMakeRange(1, 2)] bytes];
                
                int dia = *(int *)[[self.data subdataWithRange:NSMakeRange(3, 2)] bytes];
                
                int mean = *(int *)[[self.data subdataWithRange:NSMakeRange(5, 2)] bytes];
                
                int year = *(int *)[[self.data subdataWithRange:NSMakeRange(7, 2)] bytes];
                
                int month = *(int *)[[self.data subdataWithRange:NSMakeRange(9, 1)] bytes];
                
                int day = *(int *)[[self.data subdataWithRange:NSMakeRange(10, 1)] bytes];
                
                int hour = *(int *)[[self.data subdataWithRange:NSMakeRange(11, 1)] bytes];
                
                int minutes = *(int *)[[self.data subdataWithRange:NSMakeRange(12, 1)] bytes];
                
                int seconds = *(int *)[[self.data subdataWithRange:NSMakeRange(13, 1)] bytes];
                
                int pul = *(int *)[[self.data subdataWithRange:NSMakeRange(14, 2)] bytes];
                
                //WEL-361
                if (userid) {
                   
                    int flag_ihb = *(int *)[[self.data subdataWithRange:NSMakeRange(17, 2)] bytes];
                    
                    if ((flag_ihb & 0x04) == 0) {
                      
                        //No IHB  detected , hence do nothing
                    } else {
                        //IHB detected
                        if (sys != 2047) //We ensure its not the error condition
                        sys = 1000 + sys; // (It will be a 4 digit number)
                    } //WEL-361
                } else {
                    
                    int flag_ihb = *(int *)[[self.data subdataWithRange:NSMakeRange(16, 2)] bytes];
                    NSLog(@"DBG, the userid is not present and flag is %d", flag_ihb);
                    if ((flag_ihb & 0x04) == 0) {
                         NSLog(@"Sim ihb is userid not detected");
                        //No IHB  detected , hence do nothing
                    } else {
                        NSLog(@"Sim ihb is userid detected");
                        //IHB detected
                        if (sys != 2047) //We ensure its not the error condition
                        sys = 1000 + sys; // (It will be a 4 digit number)
                    } //WEL-361
                }
            
               
                NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
                
                NSDateComponents *components = [[NSDateComponents alloc] init];
                [components setYear:year];
                [components setMonth:month];
                [components setDay:day];
                [components setHour:hour];
                [components setMinute:minutes];
                [components setSecond:seconds];
                //WEL-400
                /*NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *time = [calendar dateFromComponents:components];*/
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
                NSDate *time = [calendar dateFromComponents:components];
                
                if (sys == 255 && dia == 255 && pul == 255) {
                    
                    if ([self.delegate respondsToSelector:@selector(gotBloodPressureCufError:)]) {
                        [self.delegate gotBloodPressureCufError:@"bp"];
                    }
                    return;
                }
           
                [data setValue:[NSNumber numberWithInteger:sys] forKey:BP_DATA_KEY_SYSTOLIC];
                [data setValue:[NSNumber numberWithInteger:dia] forKey:BP_DATA_KEY_DIASTOLIC];
                [data setValue:[NSNumber numberWithInteger:pul] forKey:BP_DATA_KEY_PULSE];
                [data setValue:[NSNumber numberWithInteger:mean] forKey:BP_DATA_KEY_MEAN];
                [data setValue:peripheral.identifier.UUIDString forKey:BP_USERINFO_KEY_DEVICE_ID];
                [data setValue:[NSNumber numberWithBool:YES] forKey:BP_USERINFO_KEY_SYNC_REQUIRED];
                
                NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : [NSDate date]];
                NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                
                readingDate = readingDate ? readingDate : [NSDate date];
                
                [data setObject:readingDate forKey:BP_USERINFO_KEY_MEASUREMENTDATETIME];
                [data setValue:peripheral.name ? peripheral.name : @"Unknown" forKey:BP_USERINFO_KEY_CBPERIPHERAL_NAME];
                
                //WEL-400
                NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];//End of WEL-400
                if (lifeTrackKindDateString)
                    [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                
                NSString *deviceType = [CommonClass getDeviceTypeForPeripheral:peripheral.name];
                
                if (deviceType)
                    [data setValue:deviceType forKey:BP_USERINFO_KEY_DEVICE_TYPE];
                
                DLog(@"sys %d dia %d pul %d mean %d", sys, dia, pul, mean);
                
                // Writing to DD
                NSString *str_path =  [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"BP"];
                [self.data writeToFile:str_path atomically:YES];
                

                
                [self updateBloodPressureWithDictionary:data forPeripheral:peripheral];
            
            }
            else
            {
                NSMutableDictionary *data = [[NSMutableDictionary alloc]init];
                
                int flag = *(int *)[[self.data subdataWithRange:NSMakeRange(0, 1)] bytes];
                int sys = *(int *)[[self.data subdataWithRange:NSMakeRange(1, 1)] bytes];
                int dia = *(int *)[[self.data subdataWithRange:NSMakeRange(3, 1)] bytes];
                int mean = *(int *)[[self.data subdataWithRange:NSMakeRange(5, 1)] bytes];
                int pul = *(int *)[[self.data subdataWithRange:NSMakeRange(7, 1)] bytes];
                

                
                if (sys == 255 && dia == 255 && pul == 255) {
                    
                    if ([self.delegate respondsToSelector:@selector(gotBloodPressureCufError:)]) {
                        [self.delegate gotBloodPressureCufError:@"bp"];
                    }
                    return;
                }
                
                [data setValue:[NSNumber numberWithInteger:sys] forKey:BP_DATA_KEY_SYSTOLIC];
                [data setValue:[NSNumber numberWithInteger:dia] forKey:BP_DATA_KEY_DIASTOLIC];
                [data setValue:[NSNumber numberWithInteger:pul] forKey:BP_DATA_KEY_PULSE];
                [data setValue:[NSNumber numberWithInteger:mean] forKey:BP_DATA_KEY_MEAN];
                [data setValue:peripheral.identifier.UUIDString forKey:BP_USERINFO_KEY_DEVICE_ID];
                [data setValue:[NSNumber numberWithBool:YES] forKey:BP_USERINFO_KEY_SYNC_REQUIRED];
                
                //WEL-400, Getting today's date in Gregorian format
                NSDate* currentDate = [NSDate date];
                NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                [comps setDay:[gregorianComponents day]];
                [comps setMonth:[gregorianComponents month]];
                [comps setYear:[gregorianComponents year]];
                [comps setHour:[gregorianComponents hour]];
                [comps setMinute:[gregorianComponents minute]];
                [comps setSecond:[gregorianComponents second]];
                NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                NSDate *today = [currentCalendar dateFromComponents:comps];
                NSString *str_timeInterval = [CommonClass getTimeStampFromDate:today];
                NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                
                readingDate = readingDate ? readingDate : [NSDate date];
                
                [data setObject:readingDate forKey:BP_USERINFO_KEY_MEASUREMENTDATETIME];
                [data setValue:peripheral.name ? peripheral.name : @"Unknown" forKey:BP_USERINFO_KEY_CBPERIPHERAL_NAME];
                
                NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                if (lifeTrackKindDateString)
                    [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                
                NSString *deviceType = [CommonClass getDeviceTypeForPeripheral:peripheral.name];
                
                if (deviceType)
                    [data setValue:deviceType forKey:BP_USERINFO_KEY_DEVICE_TYPE];
                
                DLog(@"sys %d dia %d pul %d mean %d", sys, dia, pul, mean);
                
                [self updateBloodPressureWithDictionary:data forPeripheral:peripheral];
            }
            
        }
        else if (([charUUID isEqual:[CBUUID UUIDWithString:WeightScaleMeasurement_Char]]) || ([charUUID isEqual:[CBUUID UUIDWithString:WeightScalebleMeasurement_Char]])) //Sim modified for WELWORLD-29
        {
            NSMutableDictionary *data = [NSMutableDictionary new];
            
            NSData *weightData = [[NSData alloc] initWithData:characteristic.value];
            NSLog(@"WEIGHT LENGTH = %d",[weightData length]);
            if (weightData.length > 3)
            {
                //Extracting the flag data and getting the unit
                int unit = *(int *)[[weightData subdataWithRange:NSMakeRange(0, 1)] bytes];
                if (unit == 0 || unit == 2) {
                    NSLog(@"unit is kg");
                    [data setValue:@"kg" forKey:WS_USERINFO_KEY_UNIT];
                } else if (unit == 1 || unit == 3) {
                    NSLog(@"unit is lb");
                    [data setValue:@"lb" forKey:WS_USERINFO_KEY_UNIT];
                }
                
                //Calculating the weight received
                int  value = *(int *)[[weightData subdataWithRange: NSMakeRange(1, 2)] bytes];
                //Sim changes for WELWORLD-29
                if ([charUUID isEqual:[CBUUID UUIDWithString:WeightScalebleMeasurement_Char]]) { //Sig implemented protocol
                    if ((unit & 0x01) == 0) {
                        NSLog(@"Unit coming in is of kg");
                        value = value * 5;
                        float valueFloat = value;
                        float weight = valueFloat/1000;
                        NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                        [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];

                    } else {
                        NSLog(@"Unit coming in is of lb");
                        float valueFloat = value;
                        float weight = valueFloat/100;
                        NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                        [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];

                    }
                } else { //Case of ADC Proprietary weight scale protocol
                    value = *(int *)[[weightData subdataWithRange: NSMakeRange(1, 2)] bytes];
                    float valueFloat = value;
                    float weight = valueFloat/10.0;
                    NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                    [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];

                }
                //Calculating time of receiving measurement
                int year = *(int *)[[weightData subdataWithRange:NSMakeRange(3, 2)] bytes];
                int month = *(int *)[[weightData subdataWithRange:NSMakeRange(5, 1)] bytes];
                int day = *(int *)[[weightData subdataWithRange:NSMakeRange(6, 1)] bytes];
                int hour = *(int *)[[weightData subdataWithRange:NSMakeRange(7, 1)] bytes];
                int minutes = *(int *)[[weightData subdataWithRange:NSMakeRange(8, 1)] bytes];
                int seconds = *(int *)[[weightData subdataWithRange:NSMakeRange(9, 1)] bytes];
                NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
                
                NSDateComponents *components = [[NSDateComponents alloc] init];
                [components setYear:year];
                [components setMonth:month];
                [components setDay:day];
                [components setHour:hour];
                [components setMinute:minutes];
                [components setSecond:seconds];
                //WEL-400
               // NSCalendar *calendar = [NSCalendar currentCalendar];
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];//End of WEL-400
                NSDate *time = [calendar dateFromComponents:components];
                
                //Getting [NSDate date] date in Gregorian format
                NSDate* currentDate = [NSDate date];
                NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                [comps setDay:[gregorianComponents day]];
                [comps setMonth:[gregorianComponents month]];
                [comps setYear:[gregorianComponents year]];
                [comps setHour:[gregorianComponents hour]];
                [comps setMinute:[gregorianComponents minute]];
                [comps setSecond:[gregorianComponents second]];
                NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                NSDate *today = [currentCalendar dateFromComponents:comps];
                NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : today];
                
              //  NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : [NSDate date]];
                NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                
                //TODO:: Need to track...
                readingDate = readingDate ? readingDate : [NSDate date];
                
                [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                [data setValue:peripheral.identifier.UUIDString forKey:WS_USERINFO_KEY_DEVICE_ID];
                [data setValue:peripheral.name forKey:WS_USERINFO_KEY_CBPERIPHERAL_NAME];
                [data setValue:@"weight" forKey:WS_USERINFO_KEY_DEVICE_TYPE];
                [data setValue:readingDate forKey:WS_USERINFO_KEY_RECEIVEDDATETIME];
                [data setValue:[NSNumber numberWithBool:YES] forKey:WS_USERINFO_KEY_SYNC_REQUIRED];
                

            }
            else
            {
                //Extracting the flag data and getting the unit
                int unit = *(int *)[[weightData subdataWithRange:NSMakeRange(0, 1)] bytes];
                if (unit == 0 || unit == 2) {
                    NSLog(@"unit is kg");
                    [data setValue:@"kg" forKey:WS_USERINFO_KEY_UNIT];
                } else if (unit == 1 || unit == 3) {
                    NSLog(@"unit is lb");
                    [data setValue:@"lb" forKey:WS_USERINFO_KEY_UNIT];
                }
                
                //Calculating the weight received
                int  value = *(int *)[[weightData subdataWithRange: NSMakeRange(1, 2)] bytes];
                //Sim changes for WELWORLD-29
                if ([charUUID isEqual:[CBUUID UUIDWithString:WeightScalebleMeasurement_Char]]) { //Sig implemented protocol
                    if ((unit & 0x01) == 0) {
                        NSLog(@"Unit coming in is of kg");
                        value = value * 5;
                        float valueFloat = value;
                        float weight = valueFloat/1000;
                        NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                        [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                        
                    } else {
                        NSLog(@"Unit coming in is of lb");
                    }
                } else { //Case of ADC Proprietary weight scale protocol
                    value = *(int *)[[weightData subdataWithRange: NSMakeRange(1, 2)] bytes];
                    float valueFloat = value;
                    float weight = valueFloat/10.0;
                    NSNumber* weightValue= [NSNumber numberWithFloat:weight];
                    [data setValue:weightValue forKey:WS_USERINFO_KEY_WEIGHT];
                    
                }
                
                //Getting [NSDate date] in Gregorian format
                NSDate* currentDate = [NSDate date];
                NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                [comps setDay:[gregorianComponents day]];
                [comps setMonth:[gregorianComponents month]];
                [comps setYear:[gregorianComponents year]];
                [comps setHour:[gregorianComponents hour]];
                [comps setMinute:[gregorianComponents minute]];
                [comps setSecond:[gregorianComponents second]];
                NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                NSDate *today = [currentCalendar dateFromComponents:comps];
                
               // NSString *str_timeInterval = [CommonClass getTimeStampFromDate:[NSDate date]];
                NSString *str_timeInterval = [CommonClass getTimeStampFromDate:today]; //End of WEL-400
                NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                //TODO:: Need to track...
                readingDate = readingDate ? readingDate : [NSDate date];
                
                [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                [data setValue:peripheral.identifier.UUIDString forKey:WS_USERINFO_KEY_DEVICE_ID];
                [data setValue:peripheral.name forKey:WS_USERINFO_KEY_CBPERIPHERAL_NAME];
                [data setValue:@"weight" forKey:WS_USERINFO_KEY_DEVICE_TYPE]; //Sim check this
                [data setValue:readingDate forKey:TH_USERINFO_KEY_RECEIVEDDATETIME];
                [data setValue:[NSNumber numberWithBool:YES] forKey:TH_USERINFO_KEY_SYNC_REQUIRED];
            }
            
           [self updateWeightWithDictionary:data];
           
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:TemperatureMeasurement_Char]]) //Sim added for Thermometer
        {
            NSMutableDictionary *data = [NSMutableDictionary new];
            
            NSData *thermometerData = [[NSData alloc] initWithData:characteristic.value];
            NSLog(@"Thermometer LENGTH = %d",[thermometerData length]);
            if (thermometerData.length > 10) //Implies time stamp data is present
            {
                NSLog(@"Sim printing the data %@", thermometerData);
                int flag = *(Byte *)[[thermometerData subdataWithRange:NSMakeRange(0, 1)] bytes];
                int temperature = *(int *)[[thermometerData subdataWithRange: NSMakeRange(1, 2)] bytes];  //This is where the temperature value is coming in
                int precision = *(int *)[[thermometerData subdataWithRange: NSMakeRange(3, 2)] bytes]; //Extract bytes to get the precision of the thermometer
                int year = *(int *)[[thermometerData subdataWithRange:NSMakeRange(5, 2)] bytes];
                int month = *(int *)[[thermometerData subdataWithRange:NSMakeRange(7, 1)] bytes];
                int day = *(int *)[[thermometerData subdataWithRange:NSMakeRange(8, 1)] bytes];
                int hour = *(int *)[[thermometerData subdataWithRange:NSMakeRange(9, 1)] bytes];
                int minutes = *(int *)[[thermometerData subdataWithRange:NSMakeRange(10, 1)] bytes];
                int seconds = *(int *)[[thermometerData subdataWithRange:NSMakeRange(11, 1)] bytes];
                int temperatureType = *(int *)[[thermometerData subdataWithRange:NSMakeRange(12, 1)] bytes];
                NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
                /*************************************/ //Sim start converting the temperature data
                float tempFloat = temperature;
                float temperatureData = 0;
                if (precision == 65024) {//FE00
                    temperatureData = (tempFloat/100.0); //This thermometer displays a 2 point precision
                } else if (precision == 65280) { //FF00
                    temperatureData = (tempFloat/10.0); //This Thermometer displays a one point precision
                } else { //Using an alternate logic by seeing the length
                   NSString *strValue = [@(temperature) stringValue];
                    if (strValue.length == 4) {
                        temperatureData = (tempFloat/100.0);
                    } else if (strValue.length == 3) {
                        temperatureData = (tempFloat/10.0);
                    } else {
                        temperatureData = (tempFloat/10);
                    }
                }
                
                NSLog(@"Sim the temperature_Data is %f", temperatureData);
                NSNumber* temperatureValue= [NSNumber numberWithFloat:temperatureData];
                NSLog(@"Sim the value of temp is %@", temperatureValue);
                [data setValue:temperatureValue forKey:TH_USERINFO_KEY_TEMPERATURE];
/**************************************************************************************/ //Sim end of temperature Data conversion
                NSDateComponents *components = [[NSDateComponents alloc] init];
                [components setYear:year];
                [components setMonth:month];
                [components setDay:day];
                [components setHour:hour];
                [components setMinute:minutes];
                [components setSecond:seconds];
                //WEL-400
                //NSCalendar *calendar = [NSCalendar currentCalendar];
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];//End of  WEL-400
                NSDate *time = [calendar dateFromComponents:components];
                
                //Checking what is the unit of temperature :
                if ((flag & 0x01) == 0) {
                    NSLog(@"unit is Celsius");
                    [data setValue:@"°C" forKey:TH_USERINFO_KEY_UNIT];
                } else {
                    NSLog(@"unit is Fahrenheit");
                    [data setValue:@"°F" forKey:TH_USERINFO_KEY_UNIT];
                }
                
                //Checking if temperature Type is present or not
                
                if ((flag & 0x04) == 1) {
                    //Temperature type is present
                    int temperatureTyep = *(int *)[[thermometerData subdataWithRange:NSMakeRange(12, 1)] bytes];
                    [data setValue:[NSString stringWithFormat:@"%d",temperatureTyep] forKey:TH_USERINFO_TEMPERTATURE_TYPE];
                } else {
                   //Temperature type is not present
                }
                
                //WEL-400, Converting [NSDate date] to Gregorian format
                NSDate* currentDate = [NSDate date];
                NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                [comps setDay:[gregorianComponents day]];
                [comps setMonth:[gregorianComponents month]];
                [comps setYear:[gregorianComponents year]];
                [comps setHour:[gregorianComponents hour]];
                [comps setMinute:[gregorianComponents minute]];
                [comps setSecond:[gregorianComponents second]];
                NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                NSDate *today = [currentCalendar dateFromComponents:comps];
                
                //NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : [NSDate date]];
                NSString *str_timeInterval = [CommonClass getTimeStampFromDate:time ? time : today];
                NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
                
                NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
                
                //TODO:: Need to track...
                readingDate = readingDate ? readingDate : [NSDate date];
                
                [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                [data setValue:peripheral.identifier.UUIDString forKey:TH_USERINFO_KEY_DEVICE_ID];
                [data setValue:peripheral.name forKey:TH_USERINFO_KEY_CBPERIPHERAL_NAME];
                [data setValue:readingDate forKey:TH_USERINFO_KEY_RECEIVEDDATETIME];
                [data setValue:[NSNumber numberWithBool:YES] forKey:TH_USERINFO_KEY_SYNC_REQUIRED];
               // NSString *deviceType = [CommonClass getDeviceTypeForPeripheral:peripheral.name];
               // if (deviceType)
                [data setValue:@"Thermometer" forKey:TH_USERINFO_KEY_DEVICE_TYPE];
                
                
            }
            else
            {
                int flag = *(Byte *)[[thermometerData subdataWithRange:NSMakeRange(0, 1)] bytes];
                int temperature = *(int *)[[thermometerData subdataWithRange: NSMakeRange(1, 2)] bytes];  //This is where the temperature value is coming in
                int precision = *(int *)[[thermometerData subdataWithRange: NSMakeRange(3, 2)] bytes];
                //WEL-400 Getting [NSDate date] in gregorian format
                NSDate* currentDate = [NSDate date];
                NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *gregorianComponents = [gregorianCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                [comps setDay:[gregorianComponents day]];
                [comps setMonth:[gregorianComponents month]];
                [comps setYear:[gregorianComponents year]];
                [comps setHour:[gregorianComponents hour]];
                [comps setMinute:[gregorianComponents minute]];
                [comps setSecond:[gregorianComponents second]];
                NSCalendar *currentCalendar = [NSCalendar autoupdatingCurrentCalendar];
                NSDate *today = [currentCalendar dateFromComponents:comps];
                
                NSString *str_timeInterval = [CommonClass getTimeStampFromDate:today];
               // NSString *str_timeInterval = [CommonClass getTimeStampFromDate:[NSDate date]]; //End of WEL-400
                NSDate *readingDate = [CommonClass getDateFromTimeStamp:str_timeInterval];
               
                /*************************************/ //Sim start converting the temperature data
                float tempFloat = temperature;
                float temperatureData = 0;
                if (precision == 65024) {//FE00
                    temperatureData = (tempFloat/100.0); //This thermometer displays a 2 point precision
                } else if (precision == 65280) { //FF00
                    temperatureData = (tempFloat/10.0); //This Thermometer displays a one point precision
                } else { //Using an alternate logic by seeing the length
                    NSString *strValue = [@(temperature) stringValue];
                    if (strValue.length == 4) {
                        temperatureData = (tempFloat/100.0);
                    } else if (strValue.length == 3) {
                        temperatureData = (tempFloat/10.0);
                    } else {
                        temperatureData = (tempFloat/10);
                    }
                }
                
                NSLog(@"Sim the temperature_Data is %f", temperatureData);
                NSNumber* temperatureValue= [NSNumber numberWithFloat:temperatureData];
                NSLog(@"Sim the value of temp is %@", temperatureValue);
                [data setValue:temperatureValue forKey:TH_USERINFO_KEY_TEMPERATURE];
                /**************************************************************************************/ //Sim end of temperature Data conversion
               
                NSString *lifeTrackKindDateString = [ANDDateManager convertDateObjectToLifeTrackWatchDateString:readingDate];
             
                [data setValue:lifeTrackKindDateString forKey:BP_USERINFO_KEY_DATESTRING_LIKE_LIFETRACK];
                [data setValue:peripheral.identifier.UUIDString forKey:TH_USERINFO_KEY_DEVICE_ID];
                [data setValue:peripheral.name forKey:TH_USERINFO_KEY_CBPERIPHERAL_NAME];
                [data setValue:readingDate forKey:TH_USERINFO_KEY_RECEIVEDDATETIME];
                [data setValue:[NSNumber numberWithBool:YES] forKey:TH_USERINFO_KEY_SYNC_REQUIRED];
            //    NSString *deviceType = [CommonClass getDeviceTypeForPeripheral:peripheral.name];
              //  if (deviceType)
                  [data setValue:@"Thermometer" forKey:TH_USERINFO_KEY_DEVICE_TYPE];
            }
            
              [self updateTemperatureWithDictionary:data];
            
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:DateTime_Char]])
        {
            NSData *weight = [[NSData alloc] initWithData:characteristic.value];
            
            NSLog(@"WEIGHT LENGTH = %d",[weight length]);
            
            NSData *date = [[NSData alloc] initWithData:characteristic.value];
            int year = *(int *)[[date subdataWithRange:NSMakeRange(0, 2)] bytes];
            int month = *(int *)[[date subdataWithRange:NSMakeRange(2, 1)] bytes];
            int day = *(int *)[[date subdataWithRange:NSMakeRange(3, 1)] bytes];
            int hour = *(int *)[[date subdataWithRange:NSMakeRange(4, 1)] bytes];
            int minutes = *(int *)[[date subdataWithRange:NSMakeRange(5, 1)] bytes];
            int seconds = *(int *)[[date subdataWithRange:NSMakeRange(6, 1)] bytes];
            NSLog(@"year %d month %d day %d hour %d minutes %d second %d", year, month, day, hour, minutes, seconds);
            
            NSDateComponents *components = [[NSDateComponents alloc] init];
            [components setYear:year];
            [components setMonth:month];
            [components setDay:day];
            [components setHour:hour];
            [components setMinute:minutes];
            [components setSecond:seconds];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *time = [calendar dateFromComponents:components];
            
        }
        else if ([charUUID isEqual:[CBUUID UUIDWithString:ActivityMonitorRead_Char]])
        {
            NSLog(@"got Activity Data");
            if (self.activityData == nil)
            {
                self.activityData = [[NSMutableData alloc] initWithData:characteristic.value];
                NSData *data = [[NSData alloc] initWithData:characteristic.value];
                self.dataLength = *(int *)[[data subdataWithRange:NSMakeRange(0, 2)] bytes];
                NSLog(@"data length is %d", self.dataLength);
                //get the checksum and start grabing data...
            }
            else
            {
                NSLog(@"activityData.length is %lu", (unsigned long)self.activityData.length);
                [self.activityData appendData:characteristic.value];
                
                if (self.dataLength <= self.activityData.length) {
                    NSLog(@"got full packet because datalength = %d and activityData.length = %lu", self.dataLength, (unsigned long)self.activityData.length);
                    NSData *final = [[NSData alloc] initWithData:self.activityData];
                    self.activityData = nil;
                    self.dataLength = 0;
                    
                    if ([self.delegate respondsToSelector:@selector(gotActivityWithData:forPeripheral:)]) {
                        [self.delegate gotActivityWithData:final forPeripheral:peripheral];
                    }
                    else {
                        [self.delegate gotActivity:final];
                    }
                }
            }
        }
    }
    else
    {
            if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
                [self.delegate gotErrorWhileConnectivity:error];
            NSLog(@"updateValueForCharacteristic failed ! %@",[error description]);
    }
    
}


- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    NSLog(@"didUpdateValueForDescriptor called!");
    
    if (error) {
            if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
                [self.delegate gotErrorWhileConnectivity:error];
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"didWriteValueForCharacteristic called!");
    
    if (error) {
        NSLog(@"didWriteValueForCharacteristic ERROR: %@",error.userInfo);
        
            if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
                [self.delegate gotErrorWhileConnectivity:error];
    }
    else {
        if ([peripheral.name rangeOfString:@"UW-302"].location != NSNotFound) {
            CBUUID *charUUID = characteristic.UUID;
            NSLog(@"charUUID is %@", charUUID);
            NSLog(@"charValue is in write characteristic %@", characteristic.value);
            
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:DateTime_Char ]])  {
                NSLog(@"Successfully wrote the date time characteristic");
                
                
                if ([self.delegate respondsToSelector:@selector(trackerDeviceDisconnect:)]) {
                    NSLog(@"DBG, enter case of tracker device disconnect");
                    
                    //Commenting the disconnect now and trying to set profile data
                    /* Users *loginUser = [ANDMedCoreDataManager sharedManager].currentUser;
                     loginUser.uw_name = peripheral.name;
                     [self.delegate trackerDeviceDisconnect:peripheral];
                     [self.delegate gotDeviceUpdateNotificationWithError:nil];*/
                    
                    //Set the Profile information now
                    NSLog(@"DBG, setting the personal information now");
                    
                    
                    //Write Profile data
                    char profileData[17];
                    profileData[0] = 0x10;
                    profileData[1] = 0x01;
                    profileData[2] = 0x20;
                    profileData[3] = 0x00;
                    profileData[4] = 0xFF;
                    profileData[5] = 0x36;
                    profileData[6] = 0xB0;
                    profileData[7] = 0x06;
                    profileData[8] = 0xA4;
                    profileData[9] = 0x00;
                    profileData[10] = 0x64;
                    profileData[11] = 0x32;
                    profileData[12] = 0x07;
                    profileData[13] = 0xB2;
                    profileData[14] = 0x02;
                    profileData[15] = 0x01;
                    profileData[16] = 0x01;
                    
                    
                    NSData *profile = [[NSData alloc] initWithBytes:&profileData length:sizeof(profileData)];
                    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                   p:self.activePeripheral
                                data:profile];
                    
                    
                }
                else {
                    NSLog(@"DBG, not pairing case request for new data , enters here to get data");
                    //Start with requesting for write data
                    char dataRequest[5];
                    dataRequest[0] = 0x04;
                    dataRequest[1] = 0x01;
                    dataRequest[2] = 0x58;
                    dataRequest[3] = 0x00;
                    dataRequest[4] = 0x00; //0x01 for all data, 0x00 for new data
                    NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
                    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
                  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                                   p:self.activePeripheral
                                data:dataRequestData];
                }
                
            } else {
                //Not Date time characteristic for UW-302
                
            }
        }
    }
    //SIM TODO, ADD A DISCONNECT LOGIC FOR UNBIND FOR  TRANSTEK
}


- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    NSLog(@"didWriteValueFOrDescriptor called!");
    if (error) {
            if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
                [self.delegate gotErrorWhileConnectivity:error];
    }
}


- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"peripheralDidupdateRSSI called!");
    
    if(error){
            if ([self.delegate respondsToSelector:@selector(gotErrorWhileConnectivity:)])
                [self.delegate gotErrorWhileConnectivity:error];
    }
}


#pragma mark - Update Dashboard with Weight Scale Value

- (void) updateWeightWithDictionary: (NSDictionary *)dict{
    if ([self.delegate respondsToSelector:@selector(gotWeight:)]) {
        [self.delegate gotWeight:dict];
    }

}

/* Sim added for Thermometer */
- (void) updateTemperatureWithDictionary: (NSDictionary *)dict{
    
    if ([self.delegate respondsToSelector:@selector(gotTemperature:)]) {
        [self.delegate gotTemperature:dict];
    }
}
    


-(void) updateBloodPressureWithDictionary:(NSDictionary *)dict forPeripheral:(CBPeripheral *)peripheral{
    
    NSLog(@"Entered updateBloodPressureWithDictionary");
    if ([self.delegate respondsToSelector:@selector(gotBloodPressure:forPeripheral:)]) {
        [self.delegate gotBloodPressure:dict forPeripheral:peripheral];
    }
    else if ([self.delegate respondsToSelector:@selector(gotBloodPressure:)]) {
        
        [self.delegate gotBloodPressure:dict];
    }
    else {
        DLog(@"Please add delegate in controller for Blood Preasure....");
    }
}
//WEL-484 UW-302
- (void) updateActivityWithDictionary: (NSDictionary *)dict andOtherData:(NSDictionary *)medicalData
{
    
    if ([self.delegate respondsToSelector:@selector(gotActivityAD:andOtherData:)]) {
        NSLog(@"DBG, calling the gotActivityAD function");
        [self.delegate gotActivityAD:dict andOtherData:medicalData];
        
    }
    
}

- (void) parseData {
    
    
    dataArrayCount = 0;
    NSUInteger totalLength = [dataActivity length];
    NSLog(@"DBG, the total length is %i", totalLength);
    NSLog(@"DBG, the NSData to be parsed is %@", dataActivity);
    int startPosition = 8;
    int step =0;
    for (int i = 0; i < totalLength; ) {
        int position = startPosition;
        for ( int j = 1; j < 20; j++) {
            
            int caloriePosition = position + 3;
            int distancePosition = position + 4;
            NSData *timeData = [dataActivity subdataWithRange:NSMakeRange((position - 5), 4)];
            NSString *timeString = [self NSDataToHex:timeData];
            NSLog(@"DBG, the convert NSString for time is %@", timeString);
            if ([timeString isEqualToString:@"00000000"]) {
                NSLog(@"DBG, this is a time in future hence ignore");
                continue;
            }
            
            //Getting the date logic now
            NSUInteger  hexAsInt;
            [[NSScanner scannerWithString:timeString] scanHexInt:&hexAsInt];
            NSString *binary = [NSString stringWithFormat:@"%@", [self toBinary:hexAsInt]];
            NSLog(@"DBG, the binary string inside the loop is %@", binary);
            int lengthBinary = [binary length];
            if (lengthBinary == 33) {
                NSLog(@"This has become a negative value hence ignore the first 1");
                binary = [binary substringFromIndex:1];
                NSLog(@"DBG, the final binary string is  %@", binary);
            }
            NSString *last3String=[binary substringFromIndex:MAX((int)[binary length]-3, 0)];
            NSString *first3String = [binary substringToIndex:3];
            NSString *year = [last3String stringByAppendingString:first3String];
            long yearValue = strtol([year UTF8String], NULL, 2);
            
            yearValue = 2005 + yearValue;
            if (yearValue > 2017) {
                continue;
            }
            NSString *month = [binary substringWithRange:NSMakeRange(3, 4)];
            long monthValue = strtol([month UTF8String], NULL, 2);
            monthValue = monthValue + 1;
            NSString *day = [binary substringWithRange:NSMakeRange(7, 5)];
            long dayValue = strtol([day UTF8String], NULL, 2);
            NSString *hour = [binary substringWithRange:NSMakeRange(12, 5)];
            long hourValue = strtol([hour UTF8String], NULL, 2);
            NSString *min = [binary substringWithRange:NSMakeRange(17, 6)];
            long minValue = strtol([min UTF8String], NULL, 2);
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [[NSDateComponents alloc] init];
            [components setDay:dayValue];
            [components setMonth:monthValue];
            [components setYear:yearValue];
            [components setHour:hourValue];
            [components setMinute:minValue];
            NSDate *received_date = [calendar dateFromComponents:components];
            NSLog(@"DBG, the date received printed in this case here is %@", received_date);
            NSString *startTime = [NSString stringWithFormat:@"%ld:%ld:%d", hourValue, minValue, 0];
            NSString *endTime = [NSString stringWithFormat:@"%ld:%ld:%d", hourValue, minValue, 0];
            NSString *dateString = [NSString stringWithFormat:@"%ld/%02ld/%02ld",yearValue,monthValue,dayValue];
            
            dataArrayCount++;
            int tempSteps = *(int *)[[dataActivity subdataWithRange:NSMakeRange(position, 1)] bytes];
            int tempCalorie = *(int *)[[dataActivity subdataWithRange:NSMakeRange(caloriePosition, 1)] bytes];
            float tempDistance = *(int *)[[dataActivity subdataWithRange:NSMakeRange(distancePosition, 1)] bytes];
            step = step + tempSteps;
            int sleepHours = *(int *)[[dataActivity subdataWithRange:NSMakeRange((distancePosition + 1), 1)] bytes];
            int sleepMin = *(int *)[[dataActivity subdataWithRange:NSMakeRange((distancePosition + 2), 1)] bytes];
            NSLog(@"steps count temp is  %i", tempSteps);
            
            
            //Logic to add this into the dictionary
            NSMutableDictionary *tempActivityDict = [NSMutableDictionary dictionary];
            [tempActivityDict setValue:[NSNumber numberWithInteger:tempSteps] forKey:ACTIVITY_DATA_KEY_STEPS];
            [tempActivityDict setValue:[NSNumber numberWithInteger:tempCalorie] forKey:ACTIVITY_DATA_KEY_CALORIE];
            [tempActivityDict setValue:[NSNumber numberWithFloat:tempDistance] forKey:ACTIVITY_DATA_KEY_DISTANCE];
            [tempActivityDict setValue:received_date forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
            [tempActivityDict setValue:endTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTENDTIME];
            [tempActivityDict setValue:dateString forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
            [tempActivityDict setValue:@"UW-302" forKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
            [tempActivityDict setValue:@"UW-302" forKey:ACTIVITY_DATA_KEY_PERIPHERAL_UUUID];
            
            //Arrange for the sleep status
            int temptotalSleepMin = (sleepHours * 60) + sleepMin;
            [tempActivityDict setValue:[NSNumber numberWithInteger:temptotalSleepMin] forKey:ACTIVITY_DATA_KEY_SLEEP];
            // [tempActivityDict setValue:[NSNumber numberWithInteger:0] forKey:ACTIVITY_DATA_KEY_SLEEP];
            
            /*[tempActivityDict setValue:peripheral.name forKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
             [tempActivityDict setValue:peripheral.identifier.UUIDString forKey:ACTIVITY_DATA_KEY_PERIPHERAL_UUUID];
             if (sleepHours != 0 && !isSleepPresent) {
             NSLog(@"Sleep Value is present and this is the  first occurence");
             isSleepPresent = TRUE;
             int totalSleepMin = (sleepHours * 60) + sleepMin;
             [tempActivityDict setValue:[NSNumber numberWithInteger:totalSleepMin] forKey:ACTIVITY_DATA_KEY_SLEEP];
             }*/
            [activityDataConversion setObject:tempActivityDict forKey:[NSNumber numberWithInt:dataArrayCount]];
            NSLog(@"DBG,the value of the dataArrayCount in parseData is %i", dataArrayCount);
            position = position + 12;
            
        }
        i = i + 256;
        startPosition = i + 8;
        
    }
    
}//End of WEL-484

//WEL-484, Implement UW-302
-(NSString *) getStringFromHex:(NSData *)timeData
{
    int length = timeData.length;
    NSString *hexedString = @"";
    for (int i = 0; i < length; i++) {
        int number = *(int *)[[timeData subdataWithRange:NSMakeRange(i, 1)] bytes];
        
        if (number == 32) {
            break; //End the loop if we encounter a 0X20
        }
        if (number < 10) {
            NSString *paddedNumber = [NSString stringWithFormat:@"%02d", number];
            hexedString = [hexedString stringByAppendingString:paddedNumber];
        } else {
            NSString *temp = [NSString stringWithFormat:@"%X",number];
            hexedString = [hexedString stringByAppendingString:temp];
        }
    }
    return hexedString;
}

-(NSString *)toBinary:(NSUInteger)input
{
    if (input == 1 || input == 0)
        return [NSString stringWithFormat:@"%u", input];
    return [NSString stringWithFormat:@"%@%u", [self toBinary:input / 2], input % 2];
}

static inline char itoh(int i) {
    if (i > 9) return 'A' + (i - 10);
    return '0' + i;
}

-(NSString *)NSDataToHex:(NSData *)data
{
    NSUInteger i, len;
    unsigned char *buf, *bytes;
    
    len = data.length;
    bytes = (unsigned char*)data.bytes;
    buf = malloc(len*2);
    
    for (i=0; i<len; i++) {
        buf[i*2] = itoh((bytes[i] >> 4) & 0xF);
        buf[i*2+1] = itoh(bytes[i] & 0xF);
    }
    
    return [[NSString alloc] initWithBytesNoCopy:buf
                                          length:len*2
                                        encoding:NSASCIIStringEncoding
                                    freeWhenDone:YES];
}

//Add a tracker reconnect once timer expires
- (void)reconnectTracker{
    NSLog(@"DBG, entered the case of timer expired and did not receive response and value of tracker is %i", tracker_timer_count);
    if (trackerReconnectTimer) {
        [trackerReconnectTimer invalidate];
        trackerReconnectTimer = nil;
    }
    if (tracker_timer_count < 2) {
        tracker_timer_count++;
        [self writeData];
    } else {
        tracker_timer_count = 0;
        [self findBLEPeripherals];
    }
   
    
    
}

-(void) writeData {
    NSLog(@"DBG, called the writeData function");
    //Request Retry again
    char dataRequest[4];
    dataRequest[0] = 0x03;
    dataRequest[1] = 0x01;
    dataRequest[2] = 0x58;
    dataRequest[3] = 0x01;
    NSData *dataRequestData = [[NSData alloc] initWithBytes:&dataRequest length:sizeof(dataRequest)];
    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                   p:self.activePeripheral
                data:dataRequestData];
    if (trackerReconnectTimer) {
        [trackerReconnectTimer invalidate];
        trackerReconnectTimer = nil;
    }
    for (NSTimer *timer in reconnectTimer) {
        [timer invalidate];
    }
    [reconnectTimer removeAllObjects];
    
    //Start a 2 seconds timer. (if data comes in before that disconnect the timer)
    NSLog(@"DBG, calling the reconnectracker within the write data function");
    trackerReconnectTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                             target:self
                                                           selector:@selector(reconnectTracker)
                                                           userInfo:nil
                                                            repeats:NO ];
    [reconnectTimer addObject:trackerReconnectTimer];
}

-(void)optimizeData2:(NSMutableDictionary *) data
{
    int count = [[data objectForKey:@"total_keys"] intValue];
    [data removeObjectForKey:@"total_keys"]; //Else predicate function fails
    NSArray *activiyDictionary = [data allValues]; //Converting dictionary object to array for easy parsing
    
    NSMutableDictionary *activityfinalData = [[NSMutableDictionary alloc] init];
    int activityCount = 0;
    for (int i =0 ; i < [trackerStringDate count]; i++) {
        //Get the total data for each dateString
        NSLog(@"DBG, getting the date for dateString %@", trackerStringDate[i]);
        NSArray *testData = [activiyDictionary filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(activity_datestring LIKE[cd] %@)", trackerStringDate[i]]];
        int   totalSteps =0 ;
        float totalDistance = 0;
        int totalCalories =0;
        int cumulativeSleep = 0;
        int finalSleepstatus = 0;
        
        //Sorting the array based on the measurement Received time
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"activity_datetime"
                                                                     ascending:YES];
        NSArray *resultsData = [testData
                                sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        Boolean entered;
        for (int i = 0; i < [resultsData count]; i++) {
            NSMutableDictionary *individualData = resultsData[i];
      //  for (int i = 0; i < [testData count]; i++) {
           // NSMutableDictionary *individualData = testData[i];
            NSDate *measurementDateTime = [individualData objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            NSLog(@"Sim. the measurement received time is %@", measurementDateTime);
            int totalSleep;
            int steps = [[individualData objectForKey:ACTIVITY_DATA_KEY_STEPS] intValue];
            int sleepStatus = [[individualData objectForKey:ACTIVITY_DATA_KEY_SLEEP_STATUS] intValue];
            float distances = [[individualData objectForKey:ACTIVITY_DATA_KEY_DISTANCE] floatValue];
            int calories = [[individualData objectForKey:ACTIVITY_DATA_KEY_CALORIE] intValue];
            NSNumber *sleep = [individualData objectForKey:ACTIVITY_DATA_KEY_SLEEP];
            if (sleep != nil){
                totalSleep = [sleep intValue];
                NSLog(@"Sleep data is present do nothing and is %i", totalSleep);
            } else {
                totalSleep  = 0 ;
            }
            totalSteps = steps + totalSteps;
            totalDistance = totalDistance + distances;
            totalCalories = totalCalories + calories;
            if (totalSleep >= cumulativeSleep) {
                cumulativeSleep = totalSleep;
            }
            finalSleepstatus = sleepStatus;
            entered = TRUE;
            NSLog(@"DBG, the individual steps is %i", steps );
            NSLog(@"DBG, the individual calories are %i", calories);
           
        }//End of for loop to sum up a day's data
        
        if (entered) {
            //Getting rest of the values from the last object, only if we have valid data
            entered = FALSE;
            NSMutableDictionary *lastDictObject = [testData lastObject];
            NSString *startTime = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
            NSDate *measurementDateTime = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            NSString *dateString = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
            NSString *peripheral_name = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
            
            //Time to package this into a dictionary structure
            
            NSMutableDictionary *tempActivityDict = [NSMutableDictionary dictionary];
            [tempActivityDict setValue:[NSNumber numberWithInteger:totalSteps] forKey:ACTIVITY_DATA_KEY_STEPS];
            [tempActivityDict setValue:[NSNumber numberWithInt:totalCalories] forKey:ACTIVITY_DATA_KEY_CALORIE];
            [tempActivityDict setValue:[NSNumber numberWithFloat:totalDistance] forKey:ACTIVITY_DATA_KEY_DISTANCE];
            [tempActivityDict setValue:[NSNumber numberWithInteger:finalSleepstatus] forKey:ACTIVITY_DATA_KEY_SLEEP_STATUS];
            [tempActivityDict setValue:[NSNumber numberWithInteger:cumulativeSleep] forKey:ACTIVITY_DATA_KEY_SLEEP];
            [tempActivityDict setValue:@"UW-302" forKey:ACTIVITY_DATA_KEY_PERIPHERAL_UUUID];
            [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
            [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTENDTIME];
            [tempActivityDict setValue:measurementDateTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            [tempActivityDict setValue:dateString forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
            [tempActivityDict setValue:peripheral_name forKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
            
            //Printing Values
            NSLog(@"DBG, cumulative steps is %i", totalSteps);
            NSLog(@"DBG, the dateString that we are populating is %@", dateString);
            NSLog(@"DBG, the cumulative calories are %i", totalCalories);
            NSLog(@"DBG, the final measured time is %@", measurementDateTime);
            //Adding the temp dict to the final structure
            activityCount++;
            [activityfinalData setObject:tempActivityDict forKey:[NSNumber numberWithInt:activityCount]];
            [lastDictObject removeAllObjects]; //This gets populated for the last object of each day
        }
       

     
    }
    NSLog(@"DBG, the total activityCount is %i", activityCount);
    //Calling the dashboard function - copy all the items of activityDataConversion
    [activityfinalData setValue:[NSNumber numberWithInt:activityCount] forKey:@"total_keys"];
    NSLog(@"DBG, the BP and weight data are %@", medicalDataConversion);
    [self updateActivityWithDictionary:activityfinalData andOtherData:medicalDataConversion];
    char disconnect[4];
    disconnect[0] = 0x03;
    disconnect[1] = 0x01;
    disconnect[2] = 0x13;
    disconnect[3] = 0x00;
    NSData *disconnectData = [[NSData alloc] initWithBytes:&disconnect length:sizeof(disconnect)];
    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                   p:self.activePeripheral
                data:disconnectData];
    
}

-(void)optimizeDataOptimize3:(NSMutableDictionary *) data
{
    
    [data removeObjectForKey:@"total_keys"]; //Else predicate function fails
    NSArray *activiyDictionary = [data allValues]; //Converting dictionary object to array for easy parsing
    NSMutableDictionary *activityfinalData = [[NSMutableDictionary alloc] init];
    int activityCount = 0;
    for (int i =0 ; i < [trackerStringDate count]; i++) {
        //Get the total data for each dateString
        NSLog(@"DBG, getting the date for dateString %@", trackerStringDate[i]);
        NSArray *testData = [activiyDictionary filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(activity_datestring LIKE[cd] %@)", trackerStringDate[i]]];
        int   totalSteps =0 ;
        float totalDistance = 0;
        int totalCalories =0;
        int cumulativeSleep = 0;
        int finalSleepstatus = 0;
        //Get the last tracker entry for a dateString
        ActivityTracker *tracker = [ANDMedCoreDataManager sharedManager].currentActivity;
        NSDate *lastEntered = tracker.measurementRecieveDateTime;
        NSLog(@"DBG, the last entered date in the database is %@", lastEntered);
        //Sorting the array based on the measurement Received time
        //Same date string in ascending order
        
        NSArray *arrayWithDateFiltered = [testData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(activity_datetime > %@)", lastEntered]];
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"activity_datetime"
                                                                     ascending:YES];
        NSArray *resultsData;
        if (lastEntered) {
            NSLog(@"DBG, there is a last entered date inside the database");
            resultsData = [arrayWithDateFiltered
                           sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
          
        } else {
            NSLog(@"DBG, implies there is no entry on this date in the database and hence enter all the values");
            resultsData  = [testData
                            sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        }
        
        //For the dateString get the last entered date and time --and then from the existing array add the date time more than the given date time--- 2 predicates
        Boolean entered;
        for (int i = 0; i < [resultsData count]; i++) {
            NSMutableDictionary *individualData = resultsData[i];
            //  for (int i = 0; i < [testData count]; i++) {
            // NSMutableDictionary *individualData = testData[i];
            NSDate *measurementDateTime = [individualData objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            NSLog(@"Sim. the measurement received time is %@", measurementDateTime);
            int totalSleep;
            int steps = [[individualData objectForKey:ACTIVITY_DATA_KEY_STEPS] intValue];
            int sleepStatus = [[individualData objectForKey:ACTIVITY_DATA_KEY_SLEEP_STATUS] intValue];
            float distances = [[individualData objectForKey:ACTIVITY_DATA_KEY_DISTANCE] floatValue];
                int calories = [[individualData objectForKey:ACTIVITY_DATA_KEY_CALORIE] intValue];
                NSNumber *sleep = [individualData objectForKey:ACTIVITY_DATA_KEY_SLEEP];
                if (sleep != nil){
                    totalSleep = [sleep intValue];
                    NSLog(@"Sleep data is present do nothing and is %i", totalSleep);
                } else {
                    totalSleep  = 0 ;
                }
                totalSteps = steps + totalSteps;
                totalDistance = totalDistance + distances;
                totalCalories = totalCalories + calories;
                if (totalSleep >= cumulativeSleep) {
                    cumulativeSleep = totalSleep;
                }
                finalSleepstatus = sleepStatus;
                entered = TRUE;
                NSLog(@"DBG, the individual steps is %i", steps );
                NSLog(@"DBG, the individual calories are %i", calories);
            
            }//End of for loop to sum up a day's data
        
        if (entered) {
            //Getting rest of the values from the last object, only if we have valid data
            entered = FALSE;
            NSMutableDictionary *lastDictObject = [resultsData lastObject];
            NSString *startTime = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
            NSDate *measurementDateTime = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            NSString *dateString = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
            NSString *peripheral_name = [lastDictObject objectForKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
            
            //Time to package this into a dictionary structure
            
            NSMutableDictionary *tempActivityDict = [NSMutableDictionary dictionary];
            [tempActivityDict setValue:[NSNumber numberWithInteger:totalSteps] forKey:ACTIVITY_DATA_KEY_STEPS];
            [tempActivityDict setValue:[NSNumber numberWithInt:totalCalories] forKey:ACTIVITY_DATA_KEY_CALORIE];
            [tempActivityDict setValue:[NSNumber numberWithFloat:totalDistance] forKey:ACTIVITY_DATA_KEY_DISTANCE];
            [tempActivityDict setValue:[NSNumber numberWithInteger:finalSleepstatus] forKey:ACTIVITY_DATA_KEY_SLEEP_STATUS];
            [tempActivityDict setValue:[NSNumber numberWithInteger:cumulativeSleep] forKey:ACTIVITY_DATA_KEY_SLEEP];
            [tempActivityDict setValue:@"UW-302" forKey:ACTIVITY_DATA_KEY_PERIPHERAL_UUUID];
            [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTSTARTTIME];
            [tempActivityDict setValue:startTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTENDTIME];
            [tempActivityDict setValue:measurementDateTime forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATETIME];
            [tempActivityDict setValue:dateString forKey:ACTIVITY_DATA_KEY_MEASUREMENTDATESTRING];
            [tempActivityDict setValue:peripheral_name forKey:ACTIVITY_DATA_KEY_PERIPHERAL_NAME];
            
            //Printing Values
            NSLog(@"DBG, cumulative steps is %i", totalSteps);
            NSLog(@"DBG, the dateString that we are populating is %@", dateString);
            NSLog(@"DBG, the cumulative calories are %i", totalCalories);
            NSLog(@"DBG, the final measured time is %@", measurementDateTime);
            //Adding the temp dict to the final structure
            activityCount++;
            [activityfinalData setObject:tempActivityDict forKey:[NSNumber numberWithInt:activityCount]];
            [lastDictObject removeAllObjects]; //This gets populated for the last object of each day
        }
        
    }
    NSLog(@"DBG, the total activityCount is %i", activityCount);
    //Calling the dashboard function - copy all the items of activityDataConversion
    [activityfinalData setValue:[NSNumber numberWithInt:activityCount] forKey:@"total_keys"];
    [self updateActivityWithDictionary:activityfinalData andOtherData:medicalDataConversion];
    char disconnect[4];
    disconnect[0] = 0x03;
    disconnect[1] = 0x01;
    disconnect[2] = 0x13;
    disconnect[3] = 0x00;
    NSData *disconnectData = [[NSData alloc] initWithBytes:&disconnect length:sizeof(disconnect)];
    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                   p:self.activePeripheral
                data:disconnectData];
    
}


@end
