//
//  ADActivityMonitor.h
//  BLE2
//
//  Created by Chenchen Zheng on 1/8/14.
//  Copyright (c) 2014 Chenchen Zheng. All rights reserved.
//

#import "ANDDevice.h"
typedef NS_ENUM(NSInteger, ADAMOrder) {
    AM_DESC,
    AM_ASC
};

@interface ADActivityMonitor : ANDDevice

@property (nonatomic, strong) NSNumber *measurementID;
@property (nonatomic, strong) NSString *measurementReceivedTime;
@property (nonatomic, strong) NSDate *measurementReceivedDateTime;
@property (nonatomic, strong) NSNumber *date;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSNumber *steps;
@property (nonatomic, strong) NSNumber *distances;
@property (nonatomic, strong) NSNumber *calories;
@property (nonatomic, strong) NSNumber *sleep;
@property (nonatomic, strong) NSNumber *bpm;
@property (nonatomic, strong) NSNumber *blockIndex;
@property (nonatomic, strong) NSNumber *sync_required;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *startTimeInterval;
@property (nonatomic, strong) NSString *endTimeInterval;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSString *deviceType;
@property (nonatomic, strong) NSString *dailyData; //Sim added for daily data
@property (nonatomic, strong) NSString *dailyDistanceData; //Sim added for daily Distance data,WEL-285
@property (nonatomic, strong) NSString *dailyCalorieDate; //Sim added for daily Calorie data, WEL-285
@property (nonatomic, assign) BOOL isRecordSavedFromThread;
@property (nonatomic, strong) NSNumber *sleepStatus; //Sleep status parameter added for UW-302
@property (nonatomic, strong) NSNumber *sleepHours; //Sleep hours sent by UW-302
@property (nonatomic, strong) NSNumber *sleepMin; //Sleep min sent by UW-302
@property (nonatomic, strong) NSNumber *calorie302; //Added for UW-302

- (id) initWithDate: (NSNumber *) date
          startTime: (NSString *) startTime
            endTime: (NSString *) endTime
              steps: (NSNumber *) steps
          distances: (NSNumber *) distances
           calories: (NSNumber *) calories
              sleep: (NSNumber *) sleep
                bpm:(NSNumber *)bpm;

-(ADActivityMonitor *)getallStoredValue;
- (id) initWithLatestMeasurement;
- (NSString *)save;
- (id)initWithDevice:(ANDDevice *)device;
- (void)readHeader;
- (void)endConnection;
- (void)readMeasurementForADTracker; //WEL-484, UW-302
- (void)setDisconnectForADTracker; //WEL-484, UW-302
- (void)setUpTrackerForMeasurement; //WEL-484, UW-302


+ (void) createTable;
+ (void) deleteMeasurementAt: (NSNumber *) measurementID;
+ (NSArray *) listOfMeasurement: (ADAMOrder) order;
+ (NSInteger) count;
+ (NSString *)save: (ADActivityMonitor *)am;
+ (NSInteger) maxValue: (NSString *)value;
+ (NSInteger) minValue: (NSString *)value;


@end
