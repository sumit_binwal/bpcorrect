//
//  ADActivityMonitor.m
//  BLE2
//
//  Created by Chenchen Zheng on 1/8/14.
//  Copyright (c) 2014 Chenchen Zheng. All rights reserved.
//

#import "ADActivityMonitor.h"
#import "ANDBLEDefines.h"

@implementation ADActivityMonitor
@synthesize measurementID = _measurementID;
@synthesize measurementReceivedTime = _measurementReceivedTime;
@synthesize date = _date;
@synthesize startTime = _startTime;
@synthesize endTime = _endTime;
@synthesize steps = _steps;
@synthesize distances = _distances;
@synthesize calories = _calories;
@synthesize startTimeInterval = _startTimeInterval;
@synthesize endTimeInterval = _endTimeInterval;
@synthesize unit = _unit;
@synthesize calorie302 = _calorie302; //UW-302

-(id)init
{
    self = [super init];
    return self;
}


- (id) initWithDate: (NSNumber *) date
          startTime: (NSString *) startTime
            endTime: (NSString *) endTime
              steps: (NSNumber *) steps
          distances: (NSNumber *) distances
           calories: (NSNumber *) calories
              sleep:(NSNumber *)sleep
              bpm:(NSNumber *)bpm

{
    // _measurementReceivedTime = [WCTime printDateTimeWithDateNow:displayType];
    _date = date;
    _startTime = startTime;
    _endTime = endTime;
    _steps = steps;
    _distances = distances;
    _calories = calories;
    _sleep = sleep;
    _bpm = bpm;
    _sync_required = [NSNumber numberWithBool:YES];
    
    return self;
}

-(ADActivityMonitor *)getallStoredValue
{
    return self;
}

- (id) initWithLatestMeasurement
{
    return nil;
}


-(id)initWithDevice:(ANDDevice *)device
{
    self = [super init];
    if (self) {
        self.activePeripheral = device.activePeripheral;
        self.CM = device.CM;
        self.peripherials = device.peripherials;
        self.delegate = device.delegate;
    }
    return self;
}


- (void)readHeader
{
    NSLog(@"AM readHeader");
    
    [self notification:[CBUUID UUIDWithString:ActivityMonitor_Serivce]
    characteristicUUID:[CBUUID UUIDWithString:ActivityMonitorRead_Char]
                     p:self.activePeripheral
                    on:YES];
    
    UInt8 ip[] = {0x01,0xa0};
    NSData *data = [[NSData alloc] initWithBytes:ip length:sizeof(ip)];
    NSLog(@"data is %@", data);
    [self writeValue:[CBUUID UUIDWithString:ActivityMonitor_Serivce] characteristicUUID:[CBUUID UUIDWithString:ActivityMonitorWrite_Char] p:self.activePeripheral data:data];
    
}

- (void) setTime
{
    NSLog(@"setTime");
    // last year, first year, month, day, hour, mintues, seconds
    //  int year1 = 221;
    int year = 14;
    int month = 07;
    int day = 10;
    int hour = 11;
    int min = 45;
    int second = 35;
    
    char bytes[6];
    //bytes[0] = 0x08;
   // bytes[1] = 0x01;
   // bytes[2] = 0x01;
    bytes[0] = year;
    bytes[1] = month;
    bytes[2] = day;
    bytes[3] = hour;
    bytes[4] = min;
    bytes[5] = second;
    
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    NSLog(@"data is %@", data);
    
    [self writeValue:[CBUUID UUIDWithString: ActivityMonitor_Serivce]
  characteristicUUID:[CBUUID UUIDWithString: ActivityMonitorWrite_Char]
                   p:self.activePeripheral
                data:data];
    
}


- (void)endConnection
{
    
    UInt8 ip[] = {0x01,0x46};
    NSData *data = [[NSData alloc] initWithBytes:ip length:sizeof(ip)];
    NSLog(@"data is %@", data);
    /*
    NSLog(@"setTime");
    // last year, first year, month, day, hour, mintues, seconds
    //  int year1 = 221;
    int year = 114;
    int month = 12;
    int day = 16;
    int hour = 11;
    int min = 45;
    int second = 35;
    
    char bytes[9];
    bytes[0] = 0x08;
    bytes[1] = 0x01;
    bytes[2] = 0x01;
    bytes[3] = year;
    bytes[4] = month;
    bytes[5] = day;
    bytes[6] = hour;
    bytes[7] = min;
    bytes[8] = second;
    
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    NSLog(@"data is %@", data);
    */
    [self writeValue:[CBUUID UUIDWithString:ActivityMonitor_Serivce] characteristicUUID:[CBUUID UUIDWithString:ActivityMonitorWrite_Char] p:self.activePeripheral data:data];
    
}

//Sim added for the activity tracker UW-302 wel-484
- (void)readMeasurementForADTracker
{
    //Remove all pairing information of all smart phones
    /* char pair[4];
     pair[0] = 0x03;
     pair[1] = 0x01;
     pair[2] = 0x10;
     pair[3] = 0x00;
     NSData *pairData = [[NSData alloc] initWithBytes:&pair length:sizeof(pair)];
     [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
     characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
     p:self.activePeripheral
     data:pairData];*/
    
    NSLog(@"DBG, read measurement for setup for activity");
    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_1]
    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                     p:self.activePeripheral
                    on:YES];
    
    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_2]
    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_2]
                     p:self.activePeripheral
                    on:YES];
    
    NSLog(@"DBG,also setting the  name here");
    
    //Write the user profile
    char profile[9];
    profile[0] = 0x0F;
    profile[1] = 0x01;
    profile[2] = 0x11;
    profile[3] = 0x00;
    profile[4] = 0x69;
    profile[5] = 0x70;
    profile[6] = 0x68;
    profile[7] = 0x6f;
    profile[8] = 0x6e;
    
    NSData *profileData = [[NSData alloc] initWithBytes:&profile length:sizeof(profile)];
    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                   p:self.activePeripheral
                data:profileData];
    
    
}

//Call the tracker disconnect
-(void) setDisconnectForADTracker {
    NSLog(@"DBG, enter the function of the tracker disconnect");
    //Issue a disconnect to the tracker
    char disconnect[4];
    disconnect[0] = 0x03;
    disconnect[1] = 0x01;
    disconnect[2] = 0x13;
    disconnect[3] = 0x00;
    NSData *disconnectData = [[NSData alloc] initWithBytes:&disconnect length:sizeof(disconnect)];
    [self writeValue:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_SERVICE_1]
  characteristicUUID:[CBUUID UUIDWithString: UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                   p:self.activePeripheral
                data:disconnectData];
    
}

- (void)setUpTrackerForMeasurement {
    
    NSLog(@"DBG, set up tracker to start getting measurement reading");
    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_1]
    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_1_1]
                     p:self.activePeripheral
                    on:YES];
    
    [self notification:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_SERVICE_2]
    characteristicUUID:[CBUUID UUIDWithString:UUIDSTR_CUSTOM_CHARACTERISTIC_2]
                     p:self.activePeripheral
                    on:YES];
    
    
    
}


@end
