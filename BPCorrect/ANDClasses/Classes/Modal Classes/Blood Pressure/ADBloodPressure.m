//
//  ADBloodPressure.m
//  BLE2
//
//  Created by Chenchen Zheng on 1/6/14.
//  Copyright (c) 2014 Chenchen Zheng. All rights reserved.
//

#import "ADBloodPressure.h"
#import "ANDBLEDefines.h"


@implementation ADBloodPressure
@synthesize measurementID = _measurementID;
@synthesize measurementTime = _measurementTime;
@synthesize measurementReceivedTime = _measurementReceivedTime;
@synthesize systolic = _systolic;
@synthesize diastolic = _diastolic;
@synthesize WCPulse = _WCPulse;
@synthesize map = _map;
@synthesize userID = _userID;
@synthesize isManualInput = _isManualInput;

- (id) initwithObject: (id) object
{
  _measurementTime = [object objectForKey:@"measurementTime"];
  
  return self;
}

//'MeasurementTime', 'MeasurementReceivedTime','Systolic', 'Diastolic', 'Pulse', 'UserID', 'isManualInput'
- (id) initWithMT: (NSString *) measurementTime MRT: (NSString *) measurementReceivedTime Sys:(NSString *)Systolic Dia:(NSString *)Diastolic Pul:(NSString *)Pulse UID: (NSString *) UserID isM: (NSString *)isManualInput
{
  _measurementTime = measurementTime;
  _measurementReceivedTime = measurementReceivedTime;
  _systolic = [NSNumber numberWithInt:[Systolic intValue]];
  _diastolic = [NSNumber numberWithInt:[Diastolic intValue]];
  _WCPulse = [NSNumber numberWithInt:[Pulse intValue]];
  _userID = [NSNumber numberWithInt:[UserID intValue]];
  _isManualInput = [NSNumber numberWithInt:[isManualInput intValue]];
  
  return self;
}

//  'BloodPressureMeasurements'
//  'MeasurementID' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,     0
//  'MeasurementTime' TEXT,                                         1
//  'MeasurementReceivedTime' TEXT,                                 2
//  'Systolic' INTEGER,                                             3
//  'Diastolic' INTEGER,                                            4
//  'PULSE' INTEGER,                                                5
//  'MAP' INTEGER,                                                  6
//  'UserID' INTEGER,                                               7
//  'isManualInput' INTEGER                                         8


//descMeasurementsByDateFromSQLite
//  'BloodPressureMeasurements'
//  'MeasurementID' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,     0
//  'MeasurementTime' TEXT,                                         1
//  'MeasurementReceivedTime' TEXT,                                 2
//  'Systolic' INTEGER,                                             3
//  'Diastolic' INTEGER,                                            4
//  'PULSE' INTEGER,                                                5
//  'MAP' INTEGER,                                                  6
//  'UserID' INTEGER,                                               7
//  'isManualInput' INTEGER                                         8

/*
 * Simple debugging info.
 */

- (NSString *)description
{
  return [NSString stringWithFormat:@"%@ ADBP: %@ %@ %@", self.measurementTime, self.systolic, self.diastolic, self.WCPulse];
}



#pragma mark - BLE
-(id)initWithDevice:(ANDDevice *)device
{
  self = [super init];
  if (self) {
    self.activePeripheral = device.activePeripheral;
    self.CM = device.CM;
  }
  return self;
}


- (void)pair
{
  
  const unsigned char bytes[] = {0x02, 0x00};
  NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
  [self writeValue: data
       serviceUUID: [CBUUID UUIDWithString:BloodPressure_Service]
characteristicUUID: [CBUUID UUIDWithString:BloodPressureMeasurement_Char]
    descriptorUUID: [CBUUID UUIDWithString:Pair_Char]
                 p: self.activePeripheral];
  //  [self writeValue:[CBUUID UUIDWithString:AND_Service]
  //characteristicUUID:[CBUUID UUIDWithString:AND_Char]
  //                 p:self.activePeripheral
  //              data:data];
  
}


//need set time during pairing
#pragma mark - Write Time Stamp For Blood Pressure
- (void) setTime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    
    NSInteger year = components.year;
    NSMutableData *yearData = [[NSMutableData alloc] initWithBytes:&year length:sizeof(year)];
    int year1 = *(int *)[[yearData subdataWithRange:NSMakeRange(0, 1)] bytes];
    int year2 = *(int *)[[yearData subdataWithRange:NSMakeRange(1, 1)] bytes];

    //int year1 = 176;
    //int year2 = 8;
    
    int month = components.month;
    
    int day = components.day;
    
    int hour = components.hour;
    
    int min = components.minute;
    
    int second = components.second;
    
    char bytes[7];
    
    bytes[0] = year1;
    
    bytes[1] = year2;
    
    bytes[2] = month;
    
    bytes[3] = day;
    
    bytes[4] = hour;
    
    bytes[5] = min;
    
    bytes[6] = second;
    
    
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    NSLog(@"data is %@", data);
    
    [self writeValue:[CBUUID UUIDWithString: BloodPressure_Service]
  characteristicUUID:[CBUUID UUIDWithString: DateTime_Char]
                   p:self.activePeripheral
                data:data];
    
}

#pragma mark - Read From Blood Pressure

- (void)readMeasurementForSetup
{
  /*[self readValue:[CBUUID UUIDWithString:BloodPressure_Service]
characteristicUUID:[CBUUID UUIDWithString:BloodPressureMeasurement_Char]
                p:self.activePeripheral];*/
  
  [self notification:[CBUUID UUIDWithString:BloodPressure_Service]
  characteristicUUID:[CBUUID UUIDWithString:BloodPressureMeasurement_Char]
                   p:self.activePeripheral
                  on:YES];
  
}

- (void)readMeasurement
{
    [self readValue:[CBUUID UUIDWithString:BloodPressure_Service]
 characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                  p:self.activePeripheral];
    
    [self notification:[CBUUID UUIDWithString:BloodPressure_Service]
    characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                     p:self.activePeripheral
                    on:YES];
    
    [self readValue:[CBUUID UUIDWithString:BloodPressure_Service]
 characteristicUUID:[CBUUID UUIDWithString:BloodPressureMeasurement_Char]
                  p:self.activePeripheral];
    
    [self notification:[CBUUID UUIDWithString:BloodPressure_Service]
    characteristicUUID:[CBUUID UUIDWithString:BloodPressureMeasurement_Char]
                     p:self.activePeripheral
                    on:YES];
    
    
}

@end
