//
//  ANDActivityDashboardCell.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/10/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ANDActivityDashboardCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *btn_next;
@property (nonatomic, weak) IBOutlet UIButton *btn_previous;
@property (nonatomic, weak) IBOutlet UILabel  *lbl_cellName;
@property (nonatomic, weak) IBOutlet UILabel  *lbl_totalSteps;
@property (nonatomic, weak) IBOutlet UILabel  *lbl_miles;
@property (nonatomic, weak) IBOutlet UILabel  *lbl_cal;
@property (nonatomic, weak) IBOutlet UILabel  *lbl_bpm;
@property (nonatomic, weak) IBOutlet UILabel  *lbl_date;
@property (nonatomic, weak) IBOutlet UIView    *view_base;
@property (weak, nonatomic) IBOutlet UIButton *activityButton;
@property (weak, nonatomic) IBOutlet UILabel *lbl_unit;

@property (weak, nonatomic) IBOutlet UIButton *btn_sync;
@property (weak, nonatomic) IBOutlet UIImageView *activityIcon;


+(ANDActivityDashboardCell *) getCell;

@end
