//
//  ANDActivityDashboardCell.m
//  ANDMedical
//
//  Created by Vishal Lohia on 3/10/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDActivityDashboardCell.h"

@implementation ANDActivityDashboardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(ANDActivityDashboardCell *) getCell
{
    return [[[NSBundle mainBundle] loadNibNamed:@"ANDActivityDashboardCell"
                                         owner:self
                                        options:nil] lastObject];
}

@end
