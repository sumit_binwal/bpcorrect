//
//  ANDBloodPressureDashboardCell.h
//  ANDMedical
//
//  Created by Vishal Lohia on 3/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ANDBloodPressureDashboardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

@property (nonatomic, weak) IBOutlet UILabel   *lbl_systolic;
@property (nonatomic, weak) IBOutlet UILabel   *lbl_diastolic;
@property (nonatomic, weak) IBOutlet UILabel   *lbl_pulse;

@property (nonatomic, weak) IBOutlet UIButton  *btn_next;
@property (nonatomic, weak) IBOutlet UIButton  *btn_previous;
@property (nonatomic, weak) IBOutlet UIView    *view_base;
@property (weak, nonatomic) IBOutlet UIImageView *imageIHB; //WEL-361

+(ANDBloodPressureDashboardCell *) getCell;
-(NSString *) reuseIdentifier;

@end
