//
//  ANDBloodPressureDashboardCell.m
//  ANDMedical
//
//  Created by Vishal Lohia on 3/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDBloodPressureDashboardCell.h"

@implementation ANDBloodPressureDashboardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+(ANDBloodPressureDashboardCell *) getCell
{
    ANDBloodPressureDashboardCell *objCell = [[[NSBundle mainBundle] loadNibNamed:@"ANDBloodPressureDashboardCell"
                                                                           owner:nil
                                                                          options:nil] lastObject];
    
    return objCell;
}

-(NSString *) reuseIdentifier
{
    return @"ANDBloodPressureDashboardCell";
}

@end
