//
//  ANDThermometerDashboardCell.h
//  ANDMedical
//
//  Created by Simantini Bhattacharya on 10/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ANDThermometerDashboardCell : UITableViewCell
{
    
}

@property (weak, nonatomic) IBOutlet UIButton *btn_previous;

@property (weak, nonatomic) IBOutlet UIButton *btn_next;

@property (weak, nonatomic) IBOutlet UILabel *lbl_date;


@property (weak, nonatomic) IBOutlet UILabel *lbl_temp;
@property (weak, nonatomic) IBOutlet UILabel *lbl_temp_unit;

@property (weak, nonatomic) IBOutlet UIView *view_base;


+(ANDThermometerDashboardCell *) getCell;
-(NSString *) reuseIdentifier;
@end