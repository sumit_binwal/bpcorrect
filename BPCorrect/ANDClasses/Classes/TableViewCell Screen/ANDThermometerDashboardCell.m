//
//  ANDThermometerDashboardCell.m
//  ANDMedical
//
//  Created by Simantini Bhattacharya on 10/22/14.
//  Copyright (c) 2014 a1. All rights reserved.
//


#import "ANDThermometerDashboardCell.h"

@implementation ANDThermometerDashboardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+(ANDThermometerDashboardCell *) getCell
{
    ANDThermometerDashboardCell *objCell = [[[NSBundle mainBundle] loadNibNamed:@"ANDThermometerDashboardCell"
                                                                          owner:nil
                                                                        options:nil] lastObject];
    
    return objCell;
}

-(NSString *) reuseIdentifier
{
    return @"ANDThermometerDashboardCell";
}

@end
