//
//  ANDWeightScaleDashboardCell.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 4/17/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDWeightScaleDashboardCell.h"

@implementation ANDWeightScaleDashboardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+(ANDWeightScaleDashboardCell *) getCell
{
    ANDWeightScaleDashboardCell *objCell = [[[NSBundle mainBundle] loadNibNamed:@"ANDWeightScaleDashboardCell"
                                                                            owner:nil
                                                                          options:nil] lastObject];
    
    return objCell;
}

-(NSString *) reuseIdentifier
{
    return @"ANDWeightScaleDashboardCell";
}

@end
