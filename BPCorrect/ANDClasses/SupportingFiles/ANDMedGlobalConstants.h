
#import <Availability.h>
#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#endif

#import "ANDProgressHUD.h"
#import "NSError+CustomErrors.h"
#import "UIAlertView+AlertCustomMethod.h"
#import "NSException+ExceptionToError.h"
#import "NSDate+CustomDateMethods.h"
#import "WeightScale.h"
#import "BloodPressureTracker.h"
#import "ANDYearManager.h"
#import "Users.h"

#define progressColor [UIColor colorWithRed:78.0/255.0 green:174.0/255.0 blue:178.0/255.0 alpha:1.0]
#define HistoryColor [UIColor colorWithRed:99.0/255.0 green:204.0/255.0 blue:241.0/255.0 alpha:1.0]

#pragma mark - Loading defines........

#define SCREEN_WINDOW                   [[[UIApplication sharedApplication] delegate] window]
#define HIDE_LOADING()                  [ANDProgressHUD hideHUDForView:SCREEN_WINDOW animated:YES]
#define SHOW_LOADING()                  [ANDProgressHUD showHUDAddedTo:SCREEN_WINDOW animated:NO]


#pragma mark - NSLocalization........

#define LOCALISE_STRING(TEXT)         NSLocalizedString(TEXT, nil)



#pragma mark - Screen resoulution check.....

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
//#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define is_iOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define IS_IPHONE_5 [UIScreen mainScreen].bounds.size.height == 568


#pragma mark - Network Reachabilty Constants........

//#define IS_NETWORK_AVAILABLE()   ( [ANDHTTPClient sharedInstance].networkReachabilityStatus <= 0 ? (BOOL)NO : (BOOL)YES)        //[[Reachability reachabilityForInternetConnection] currentReachabilityStatus]

#define IS_NETWORK_AVAILABLE()  (BOOL)([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] <= 0 ? NO : YES)


#define SHOW_NETWORK_ERROR_ALERT()        [UIAlertView showAlertWithTitle:LOCALISE_STRING(@"Alert.Title") message:LOCALISE_STRING(@"ReachabilityNoNetwork.Message")]



#pragma mark - APP ID and Headers...........

//#define appID @"0a2886a0"
#define kAND_APP_ID @"0a2886a0"

#define kAND_WS_ENABLE_HEADERS 1
#define kAND_WS_HEADER_KEY_APPID @"X-WellnessConnected-AppId"
#define kAND_WS_HEADER_KEY_AUTHTOKEN @"X-WellnessConnected-AuthToken"



#pragma mark - URL constants.................

//#define AND_API_BASE_URL @"https://dummy/"
//Readings and login version are two different ones

//Sim added for Flag check on whether its global or US
#define appType @"Global" - //Sim , defined this is commented for the US app

#pragma mark -  Some methods for number to string...........

#define NUMBER_INT_TO_STRING(_numberObj_) _numberObj_ ? [NSString stringWithFormat:@"%d", [_numberObj_  integerValue]] : @""
#define NUMBER_FLOAT_TO_STRING(_numberObj_) _numberObj_ ? [NSString stringWithFormat:@"%.2f", [_numberObj_ floatValue]] : @""



#pragma mark - DLog when the DEBUG variable is set..............

#ifdef  TARGET_IPHONE_SIMULATOR //DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

#define IS_VALID_STRING(_str_) _str_ && ![_str_ isEqual:[NSNull null]] && ![_str_ isEqualToString:@"(null)"]  ? TRUE : FALSE

#define GET_VALID_STRING(_str_) _str_ && ![_str_ isEqualToString:@"(null)"] && ![_str_ isEqual:[NSNull null]]  ? _str_ : @""

#define GetBundleImagePath(_imgName_,_ext_) [[NSBundle mainBundle] pathForResource:_imgName_ ofType:_ext_]

#define kIsActivityFetchOrderAscending NO

#define CONVERT_TO_CAL(__energy__) __energy__ ? [__energy intValue]/60 : 0


#pragma mark - Password Minimum Length........

#define kMinimumPasswordLength 7


#pragma mark - NSUserDefault all Key should be here......

#define kUsername                       @"username"
#define kPassword                       @"password"
#define kSavedAllAlarm                  @"arr_dict"
#define kSessionTime                    @"sessionTime"
#define kAccountCreated                 @"AccountCreated"
#define kIsTermAndConditionShowing      @"isTermAndConditionShowing"
#define kRememberPassword               @"RememberPassword"
#define isMetric                        @"SetUnit"

#define kRemindMeNotificationDataKey  @"remindTitle"

#define kReloadBlueToothSettings  @"BLESetting"

#pragma mark - enum ActivityType......

enum _activityType
{
    ACTIVITY_TYPE_NONE = 0,
    ACTIVITY_TYPE_CAL = 1,
    ACTIVITY_TYPE_STEP = 2,
    ACTIVITY_TYPE_BPM = 3,
    ACTIVITY_TYPE_MILES = 4,
    ACTIVITY_TYPE_SLEEP = 5,
    Activity_TYPE_BP_SYS = 6,
    ACTIVITY_TYPE_BP_DIAS = 7,
    ACTIVITY_TYPE_BP_PUL = 8
};
typedef enum _activityType ActivityType;



#pragma mark - enum BloodPressureType.....

enum _bloodPressureType
{
    PRESSURE_TYPE_SYS = 5,
    PRESSURE_TYPE_DIAS = 6,
    PRESSURE_TYPE_PUL = 7
};

typedef enum _bloodPressureType BloodPressureType;;

