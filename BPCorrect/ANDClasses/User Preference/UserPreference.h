//
//  UserPreference.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 5/7/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

/*
 This class is used to save objects and bool value in NSUserDefaults and also we can fetch object/bool using this class
 */
#import <Foundation/Foundation.h>

@interface UserPreference : NSObject


+ (void) setObject:(id)object forKey:(NSString *)key;

+ (id) objectForKey:(NSString *)key;

+ (void) setBool:(BOOL)object forKey:(NSString *)key;

+ (BOOL) boolForKey:(NSString *)key;

@end
