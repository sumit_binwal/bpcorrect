//
//  UserPreference.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 5/7/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "UserPreference.h"

@implementation UserPreference

/*
 * set id type object in NSUserDefault......
 */
+ (void) setObject:(id)object forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/*
 * return id type object from NSUserDefault....
 */
+ (id) objectForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
}


/*
 * set Bool value in NSUserDefault......
 */
+ (void) setBool:(BOOL)object forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setBool:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/*
 * return Bool value from NSUserDefault....
 */
+ (BOOL) boolForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
    
}
@end
