//
//  APIManager.swift
//  BPCorrect
//
//  Created by "" on 22/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import MobileCoreServices
import AFNetworking

class APIManager: NSObject {
    
    var baseUrl: String  = "http://67.211.223.164:8080/ProtechSentinel/"
  
    @objc
    open class var sharedInstance: APIManager {
        struct Singleton {
            static let instance : APIManager = APIManager()
        }
        return Singleton.instance
    }
  

  func registerUser(params:Parameters, completionHandler:@escaping (_ result:RegisterModel)-> (),failure:@escaping (_ error:NSError)-> ())-> Void {
    
    let URL = "\(baseUrl)common/register/patient"
    
    Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.methodDependent, headers:nil).responseObject { (response: DataResponse<RegisterModel>) in
      
      if  response.response?.statusCode == 200 {
        let registerResponse = response.result.value
        if(registerResponse != nil) {
          completionHandler(registerResponse!)
        } else {
          failure(response.error! as NSError)
        }
      } else {
        failure(response.error! as NSError)
      }
    }
  }
  

    func loginUser(params:Parameters, completionHandler:@escaping (_ result:LoginModel)-> (),failure:@escaping (_ error:NSError)-> ())-> Void {
        
      let URL = "\(baseUrl)oauth/token?client_id=\(params["client_id"] ?? "")&grant_type=\(params["grant_type"] ?? "")&username=\(params["username"] ?? "")&password=\(params["password"] ?? "")"
        print("URL IS : - \(URL)")
        print("Parameter is : - \(params)")
      Alamofire.SessionManager.default.requestWithCacheOrLoad(URL, method: .get, headers:nil).responseObject { (response: DataResponse<LoginModel>) in
        
        
        if  response.response?.statusCode == 200 {
          let loginResponse = response.result.value
          if(loginResponse != nil) {
            
            
              let access_token : String = loginResponse?.value ?? ""
              #warning ("save access_token_exp")
              let userId : Int32 = loginResponse?.additionalInformation?.id ?? 0
            
              if access_token != "" && userId != 0 {
              
              BPUserDefaults.setAuthToken(access_token)
              BPUserDefaults.setUserId("\(userId)")
                print("AccessToken : \(access_token)")
                
              let params : Parameters = ["access_token":access_token,
                                         "patientUserId":"\(userId)"]
              
                //Get User Profile
                
                self.userProfile(params: params, completionHandler: { (userModel) in
                  //Save User Profile
                  BPUserDefaults.saveUserModel(user: userModel)
                  self.getAllBPReadings()
                  completionHandler(loginResponse!)
                  
                }, failure: { (error) in
                  failure(response.error! as NSError)
                })
            }
            
            
            
            
          } else {
            failure(response.error! as NSError)
          }
        } else {
         
          if response.response?.statusCode == 400 || response.response?.statusCode == 401{
            #warning("Proper error model is still needs to provided by API Server")
            let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Invalid Crdentials"])
            failure(error)
          } else {
            let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
            failure(error)
          }
        }
      }
    }
  
  
  @objc func getAllBPReadings() {
    self.getBPReading(params: ["" : ""], completionHandler: { (bpReadingsResponseModel) in
      
    }, failure: { (error) in
    })
  }
    
  
  func forgotPassword(params:Parameters, completionHandler:@escaping (_ result:RegisterModel)-> (),failure:@escaping (_ error:NSError)-> ())-> Void {
    
    let URL = "\(baseUrl)basic/forgot/password"
    
    Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.methodDependent, headers:nil).responseObject { (response: DataResponse<RegisterModel>) in
      if  response.response?.statusCode == 200 {
       
        let forgotPasswordResponse = response.result.value
        if(forgotPasswordResponse != nil) {
          completionHandler(forgotPasswordResponse!)
        } else {
          let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
          failure(error)
        }

      } else {
        failure(response.error! as NSError)
      }
    }
  }
  
  func userProfile(params:Parameters, completionHandler:@escaping (_ result:UserModel)-> (),
                   failure:@escaping (_ error:NSError)-> ())-> Void {
    
    let URL = "\(baseUrl)common/get/patient/information?access_token=\(params["access_token"] ?? "")&patientUserId=\(params["patientUserId"] ?? "")"
    print(URL)
    Alamofire.SessionManager.default.requestWithCacheOrLoad(URL, method: .get, headers:nil).responseObject { (response: DataResponse<UserModel>) in
      
      if  response.response?.statusCode == 200 {
        let userProfileResponse = response.result.value
        print("response.result.value : \(response.result.value)")
        if(userProfileResponse != nil) {
          completionHandler(userProfileResponse!)
        } else {
          let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
          failure(error)
        }
      } else {
        let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
        failure(error)
      }
    }
  }
  
  func editUserProfile(params:Parameters, completionHandler:@escaping (_ result:UserModel)-> (),
                   failure:@escaping (_ error:NSError)-> ())-> Void {
    
    let URL = "\(baseUrl)common/update/patient?access_token=\(params["access_token"] ?? "")&firstname=\(params["firstname"] ?? "")&lastname=\(params["lastname"] ?? "")&dob=\(params["dob"] ?? "")&address1=\(params["address1"] ?? "")&email=\(params["email"] ?? "")&mobile1=\(params["mobile1"] ?? "")&gender=\(params["gender"] ?? "")&city=\(params["city"] ?? "")&state=\(params["state"] ?? "")&zipcode=\(params["zipcode"] ?? "")&userId=\(params["userId"] ?? "")&height=\(params["height"] ?? "")&weight=\(params["weight"] ?? "")&photo_url=\(params["photo_url"] ?? "")"
    
   Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.queryString, headers:nil).responseObject { (response: DataResponse<UserModel>) in
      
      if  response.response?.statusCode == 200 {
        let userProfileResponse = response.result.value
        if(userProfileResponse != nil) {
          completionHandler(userProfileResponse!)
        } else {
          let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
          failure(error)
        }
      } else {
        let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
        failure(error)
      }
    }
  }
  
 
  func sendBPReading(params:Parameters, completionHandler:@escaping (_ result:BPReadingResponseModel)-> (),
                   failure:@escaping (_ error:NSError)-> ())-> Void {
    
    
    let URL = "\(baseUrl)common/add/patient/reading?access_token=\(params["access_token"] ?? "")"
    
   Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.methodDependent, headers:nil).responseObject { (response: DataResponse<BPReadingResponseModel>) in
      
      if  response.response?.statusCode == 200 {
        let bpReadingResponse = response.result.value
        if(bpReadingResponse != nil) {
          
          self.getBPReading(params: ["" : ""], completionHandler: { (chartDataResponse) in
            
          }, failure: { (error) in
            failure(response.error! as NSError)
          })
          completionHandler(bpReadingResponse!)
        } else {
          let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
          failure(error)
        }
      } else {
        let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
        failure(error)
    }
    }
  }
  
  func getBPReading(params:Parameters, completionHandler:@escaping (_ result:ChartDataResponseModel)-> (),
                    failure:@escaping (_ error:NSError)-> ())-> Void {
    
    
    let userModel : UserModel = BPUserDefaults.getUserModel()
    
    print(BPUserDefaults.getUserModel())
    
    let patientId : Int = userModel.data![0].patientId!
    
    let toDate = Date()
    let fromDate = toDate.addingTimeInterval(-30*24*60*60)
    let access_Token : String = BPUserDefaults.accessToken()
    
    
    let URL = "\(baseUrl)common/get/chart/data/according/to/user?access_token=\(access_Token)&patientUserId=\(patientId)&dayno=0&fromdate=\(String(format: "%.0f", fromDate.timeIntervalSince1970))&todate=\(String(format: "%.0f", toDate.timeIntervalSince1970))"
    
    Alamofire.SessionManager.default.requestWithCacheOrLoad(URL, method: .get, headers:nil).responseObject { (response: DataResponse<ChartDataResponseModel>) in
      
      if  response.response?.statusCode == 200 {
        let chartDataResponseModel = response.result.value
        if(chartDataResponseModel != nil) {
          
          if ((chartDataResponseModel?.valid)!) {
            
            if ((chartDataResponseModel?.data?.count)! > 0) {
              
              for chartData  in (chartDataResponseModel?.data)! {
               // let chartData : ChartData =  (chartDataResponseModel?.data![0])!
                
                for chartCompleteData in chartData.chartdata! {
                 // let chartCompleteData : ChartCompleteData = chartData.chartdata![0]
                  addChartData(chartData: chartCompleteData)
                }
                
                
              }
              
              
              
              
            }
    
            
            completionHandler(chartDataResponseModel!)
          }
          
          
        } else {
          let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
          failure(error)
        }
      } else {
        let error = NSError(domain: "com.bpcorrect.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
        failure(error)
      }
    }
  }
}


func addChartData(chartData : ChartCompleteData) {
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  let managedObjectContext: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
  
  
  let request = NSFetchRequest<NSFetchRequestResult>(entityName:"ChartDataModel")
  let results = try! managedObjectContext.fetch(request) as! [ChartDataModel]
  
  let chartDataModel : ChartDataModel!
  if results.count == 0 {
    chartDataModel  = ChartDataModel(context: managedObjectContext)
  } else {
    chartDataModel = results[0]
  }
  
  
  
  let avg_value : AvgValue = AvgValue(context: managedObjectContext)
  avg_value.dbp = chartData.avg_values?.dbp
  avg_value.sbp = chartData.avg_values?.sbp
  avg_value.pulse = chartData.avg_values?.pulse
  chartDataModel.avg_values = avg_value
  
  
  for (index, item) in chartData.actual_values!.enumerated() {
    print("Found \(item) at position \(index)")
    
    let actualValueRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"ActualValue")
    actualValueRequest.predicate =  NSPredicate(format: "timestamp == %@", item.timestamp!)
    let actualValueResults = try! managedObjectContext.fetch(actualValueRequest) as! [ActualValue]
    
    if actualValueResults.count == 0 {
      //Add Actual Value if it doesnot exists
      let actual_value : ActualValue = ActualValue(context: managedObjectContext)
      actual_value.dbp = item.dbp
      actual_value.sbp = item.sbp
      actual_value.pulse = item.pulse
      actual_value.timestamp = item.timestamp
      actual_value.protocol_id = item.protocol_id
      if(item.is_abberant == "0") {
        actual_value.is_abberant = false
      } else {
        actual_value.is_abberant = true
      }
      
      chartDataModel.addToActual_values(actual_value)
    }
  }

 try! managedObjectContext.save()
  
}


extension Alamofire.SessionManager{
    
    @discardableResult
    open func requestWithCacheOrLoad(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .returnCacheDataElseLoad
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            print(error)
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}

extension Data{
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
