//
//  FireApi.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 18/04/18.
//  Copyright © 2018 Vaibhav. All rights reserved.
//
import Foundation
import UIKit

struct ImageData {
    var imageData: Data
    var paramName: String
}

struct ModelApiMeta {
    var forceUpdate: Bool
    var hasUpdate: Bool
    var isPro: Bool
    var version: String
    
    static func updateMeta(_ meta: [String:Any]) -> ModelApiMeta {
        let fUpdate = meta["force_update"] as? Bool ?? false
        let hUpdate = meta["has_update"] as? Bool ?? false
        let pro = meta["isPro"] as? Bool ?? false
        let vers = meta["version"] as? String ?? "1.0"
        
        return ModelApiMeta (forceUpdate: fUpdate, hasUpdate: hUpdate, isPro: pro, version: vers)
    }
}

final class FireApi {
    
    // Request method to be used when creating URLRequest
    enum RequestMethod: String {
        case post = "POST"
        case get = "GET"
    }
    
    enum RequestResponse {
        case success([String:Any],Int) // Returns (Dictionary and status code)
        case error(Error) // Returns Error
    }
    
    typealias ResultHandler = (RequestResponse) -> Void
    
    // URLSession object for working with Api's
    private var httpURLSession: URLSession
    private let requestTimeOutInterval: TimeInterval = 40
    
    // Shared variable to return single instance of this class FireApi
    private static var sharedFireApi: FireApi = {
        let fireApiInstance = FireApi ()
        return fireApiInstance
    }()
    
    // init method, initializes the URLSession object
    private init() {
        print("init called")
        httpURLSession = URLSession (configuration: URLSessionConfiguration.default)
    }
    
    // Shared function accesible to users for accessing the methods and variables
    class func shared() -> FireApi {
        return sharedFireApi
    }
    
    func performRequest(for url: URL, method requestMethod: RequestMethod, headers: [String:String]?, parameters: [String:Any]?, resultHandler: ResultHandler?) -> Void {
        
        var urlRequest: URLRequest
        
        let dataTask: URLSessionDataTask
        
        if case .get = requestMethod {
            urlRequest = createGetRequest(using: url)
        }
        else {
            urlRequest = createPostRequest(using: url, parameters: parameters)
        }
        
        if let finalHeaders = headers, !finalHeaders.isEmpty {
            urlRequest.addHeaders(from: finalHeaders)
        }
        
        dataTask = httpURLSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            DispatchQueue.main.async {
                guard let finalResultHandler = resultHandler else {return}
                
                if let err = error {
                    finalResultHandler (FireApi.RequestResponse.error(err))
                    return
                }
                
                let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
                
                if statusCode == FireApi.ErrorCodes.code440.rawValue { // Invalid ssid
                    
                    //UtilityClass.stopAnimating()
                    
                    UIAlertController.showAlertWith(title: "Session Expired", message: "Your account has been logged in to other device.", dismissBloack: {
                        
                        // UserDefaults.deleteUserInformation()
                        //APPDELEGATE.updateRootView(with: .typeLogin)
                    })
                    return
                }
                
                var responseDictionary: [String:Any]
                
                do {
                    responseDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
                    finalResultHandler (FireApi.RequestResponse.success(responseDictionary, statusCode))
                }
                catch {
                    finalResultHandler (FireApi.RequestResponse.error(error))
                }
            }
        })
        
        dataTask.resume()
    }
    
    func performMultiPartRequest(for url: URL, imageArray: [ImageData]?, headers: [String:String]?, parameters: [String:Any]?, requestMethod: RequestMethod, resultHandler: ResultHandler?) -> Void {
        
        var urlRequest: URLRequest
        
        let dataTask: URLSessionDataTask
        
        urlRequest = createMultiPartPostRequest(using: url, imageArray: imageArray, parameters: parameters, requestMethod: requestMethod)
        
        if let finalHeaders = headers, !finalHeaders.isEmpty {
            urlRequest.addHeaders(from: finalHeaders)
        }
        
        dataTask = httpURLSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            DispatchQueue.main.async {
                guard let finalResultHandler = resultHandler else {return}
                
                if let err = error {
                    finalResultHandler (FireApi.RequestResponse.error(err))
                    return
                }
                
                let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
                
                var responseDictionary: [String:Any]
                
                do {
                    responseDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
                    finalResultHandler (FireApi.RequestResponse.success(responseDictionary, statusCode))
                }
                catch {
                    finalResultHandler (FireApi.RequestResponse.error(error))
                }
            }
        })
        
        dataTask.resume()
    }
    
    private func createGetRequest(using url: URL) -> URLRequest {
        var request = URLRequest (url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: requestTimeOutInterval)
        request.httpMethod = RequestMethod.get.rawValue
        return request
    }
    
    private func createPostRequest(using url: URL, parameters: [String:Any]?) -> URLRequest {
        
        var request = URLRequest (url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: requestTimeOutInterval)
        request.httpMethod = RequestMethod.post.rawValue
        
        guard let finalParameters = parameters else {
            return request
        }
        
        guard JSONSerialization .isValidJSONObject(finalParameters) else {
            preconditionFailure("Parameters provided is not valid JSONObject. \n \(finalParameters)")
        }
        
        var postData: Data
        do {
            postData = try JSONSerialization .data(withJSONObject: finalParameters, options: .prettyPrinted)
        }
        catch  {
            preconditionFailure("Failed to create Data from JSONObject. \n \(finalParameters)")
        }
        
        request.httpBody = postData
        
        return request
    }
    
    private func createMultiPartPostRequest(using url: URL, imageArray: [ImageData]?, parameters: [String:Any]?, requestMethod : RequestMethod) -> URLRequest {
        
        let boundary = String(format: "Boundary+%08X%08X", arc4random_uniform(UINT32_MAX), arc4random_uniform(UINT32_MAX))
        let contentType = String(format: "multipart/form-data; boundary=%@", boundary)
        
        var request = URLRequest (url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: requestTimeOutInterval)
        if case .get = requestMethod {
            request.httpMethod = RequestMethod.get.rawValue
        }
        else
        {
            request.httpMethod = RequestMethod.post.rawValue
        }

        
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        var postData = Data ()
        
        if let newImageArray = imageArray, !newImageArray.isEmpty {
            
            newImageArray.forEach({ (imageData) in
                postData.append(String(format:"\r\n--%@\r\n",boundary).data(using: .utf8)!)
                
                let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpg\"\r\n", imageData.paramName, imageData.paramName)
                
                postData.append(string1.data(using: .utf8)!)
                
                postData.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
                
                postData.append(imageData.imageData)
            })
        }
        
        if let finalParams = parameters, !finalParams.isEmpty {
            finalParams.forEach({ (key, value) in
                
                postData.append(String(format:"\r\n--%@\r\n",boundary).data(using: .utf8)!)
                
                if let newValue = value as? [String:Any] {
                    
                    let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, createJSONString(usingDictionary: newValue) as CVarArg)
                    postData.append(string1.data(using: .utf8)!)
                }
                    
                    //                else if let newValue = value as? [Any]
                    //                {
                    //                    let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, self.createJSONString(usingArray: newValue) as CVarArg)
                    //
                    //                    body.append(string1.data(using: .utf8)!)
                    //                }
                    
                else {
                    
                    let string1 = String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, value as! CVarArg)
                    postData.append(string1.data(using: .utf8)!)
                }
            })
        }
        
        postData.append(String(format:"\r\n--%@--\r\n",boundary).data(using: .utf8)!)
        
        // Setting base request http body
        request.httpBody = postData
        
        return request
    }
    
    //MARK:- cancelTask
    func cancelTask(for endpoint:String) {
        
        httpURLSession.getAllTasks(completionHandler: { (taskArray) in
            for task in taskArray {
                if let originalRequest = task.originalRequest {
                    if (originalRequest.url?.absoluteString.contains(endpoint))! {
                        task.cancel()
                        return
                    }
                }
            }
        })
    }
    
    func isPerformingTask(_ endpoint: URL, handler: @escaping (_ isPerforming: Bool) -> ()) {
        httpURLSession.getAllTasks(completionHandler: { (taskArray) in
            for task in taskArray {
                if let originalRequest = task.originalRequest {
                    if originalRequest.url! == endpoint {
                        handler (true)
                        return
                    }
                }
            }
            
            handler (false)
            return
        })
    }
}

extension URLRequest {
    mutating func addHeaders(from values: [String:String]) {
        values.forEach({ (arg) in
            let (key, value) = arg
            self.addValue(value, forHTTPHeaderField: key)
        })
    }
}

extension FireApi {
    //MARK:-
    private func createJSONString(usingDictionary dictionary : [String:Any]) -> String {
        
        // Guarding the Bool and checking for True else AssertionFailure with message. Can not proceed further
        guard JSONSerialization.isValidJSONObject(dictionary) else {
            preconditionFailure("Parameters provided is not valid JSONObject. \n \(dictionary)")
        }
        
        // Do-Catch syntax to handle the Dictionary to Json string conversion. Here we are force unwrapping try because we are sure the object will return proper Json string...
        
        var postData: Data
        var postString : String
        do {
            postData = try JSONSerialization .data(withJSONObject: dictionary, options: .prettyPrinted)
        }
        catch  {
            preconditionFailure("Failed to create Data from JSONObject. \n \(dictionary)")
        }
        
        postString = String (data: postData, encoding: .utf8)!
        
        return postString
    }
    
    static var defaultHeaders: [String:String] {
        
        
        return [
            "version" : String.buildNumber,
            "language" : "en",
            "os" : "ios",
            "Content-Type" : "application/json"
        ]
        
    }
    
    enum ErrorCodes: Int {
        case code200 = 200
        case code203 = 203
        case code440 = 440
    }
}
