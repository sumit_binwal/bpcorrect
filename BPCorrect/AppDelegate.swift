//
//  AppDelegate.swift
//  BPCorrect
//
//  Created by "" on 30/03/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import CoreData
import FMDB
import UserNotifications
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
    let notificationCenter = UNUserNotificationCenter.current()

  func createMenuView() {
    
    try! ANDMedCoreDataManager.shared()?.insertAndInitializeGuestUser("guest@guest.com", andPassword: "guest")
    
    BPUserDefaults.setLoggedIn(true)
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().isTranslucent = true
    UINavigationBar.appearance().barTintColor = UIColor.bpCorrectThemeColor()
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font:UIFont.regularFontOfSize(16)!]
    
    DataBaseManager.sharedInstance()
    
    let backArrowImage  = UIImage(named: "arrow_left") // set your back button image here
    let renderedImage = backArrowImage?.withRenderingMode(.alwaysOriginal)
    UINavigationBar.appearance().backIndicatorImage = renderedImage
    UINavigationBar.appearance().backIndicatorTransitionMaskImage = renderedImage
    

    // create viewController code...
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    let mainViewController = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
    let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
    let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
    
    let nvc: UINavigationController  = UINavigationController(rootViewController: mainViewController)
    
    leftViewController.mainViewController = nvc
    
    let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
    
    slideMenuController.delegate = mainViewController //as! SlideMenuControllerDelegate
    
    self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
    self.window?.rootViewController = slideMenuController
    self.window?.makeKeyAndVisible()
    
  }
  
  func getBPReadings() {
    
  }
  
  func deleteAllRecords(entityName : String) {
    //getting context from your Core Data Manager Class
   
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    do {
      try self.persistentContainer.viewContext.execute(deleteRequest)
      try self.persistentContainer.viewContext.save()
    } catch {
      print ("There is an error in deleting records")
    }
  }
  
  func showLoginScreen() {
    
    deleteAllRecords(entityName: "ChartDataModel")
    deleteAllRecords(entityName: "ActualValue")
    deleteAllRecords(entityName: "AvgValue")
    
    BPUserDefaults.setLoggedIn(false)
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let landingVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    landingVC.setNeedsStatusBarAppearanceUpdate()
    let nvc: UINavigationController = UINavigationController(rootViewController: landingVC)
    nvc.setNeedsStatusBarAppearanceUpdate()
    nvc.isNavigationBarHidden = true
    self.window?.rootViewController = nvc
    self.window?.makeKeyAndVisible()
    
  }
  
    
    func scheduleNotifications(scheduleTIme : Date) {
        
        let content = UNMutableNotificationContent()
        let requestIdentifier = "BPCorrect"
        
        content.badge = 1
        content.title = "BPCOrrect"
        content.subtitle = "Hello there, Its Time to Take Reading"
        content.body = "Its Time to Take Reading"
        content.categoryIdentifier = "actionCategory"
        content.sound = UNNotificationSound.default
        
//        // If you want to attach any image to show in local notification//thermometer_dashboard.png
//        let url = URL.init(string: "https://static.techspot.com/images2/news/bigimage/2018/09/2018-09-04-image-6.png")
//        do {
//            let attachment = try? UNNotificationAttachment(identifier: requestIdentifier, url: url!, options: nil)
//            content.attachments = [attachment!]
//        }
        
//        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 7.0, repeats: false)
        
        // Use a mask to extract the required components. Extract only the required components, since it'll be expensive to compute all available values.

//        let yourFireDate = Calendar.current.date(byAdding: dateComponents, to: scheduleTIme)
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: scheduleTIme)

        let trigger1 = UNCalendarNotificationTrigger.init(dateMatching: triggerDate, repeats: true)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger1)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            
            if error != nil {
                print(error?.localizedDescription)
            }
            print("Notification Register Success")
        }
    }

    func registerForRichNotifications() {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted:Bool, error:Error?) in
            if error != nil {
                print(error?.localizedDescription)
            }
            if granted {
                print("Permission granted")
            } else {
                print("Permission not granted")
            }
        }
        
        //actions defination
        let action1 = UNNotificationAction(identifier: "action1", title: "Action First", options: [.foreground])
        let action2 = UNNotificationAction(identifier: "action2", title: "Action Second", options: [.foreground])
        
        let category = UNNotificationCategory(identifier: "actionCategory", actions: [], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
    }

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    
    registerForRichNotifications()
    UNUserNotificationCenter.current().delegate = self

   // scheduleNotifications()
//    let options: UNAuthorizationOptions = [.alert, .sound, .badge]
//    notificationCenter.requestAuthorization(options: options) {
//        (didAllow, error) in
//        if !didAllow {
//            print("User has declined notifications")
//        }
//    }
//
//    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
//
//    let content = UNMutableNotificationContent() // Содержимое уведомления
//
//    content.title = "notificationType"
//    content.body = "This is example how to create  Notifications"
//    content.sound = UNNotificationSound.default
//    content.badge = 1
//
//
//    let identifier = "Local Notification"
//    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
//
//    notificationCenter.add(request) { (error) in
//        if let error = error {
//            print("Error \(error.localizedDescription)")
//        }
//    }


    
    if BPUserDefaults.isLoggedIn() {
      self.createMenuView()
    } else {
      self.showLoginScreen()
    }
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    self.saveContext()
  }

  // MARK: - Core Data stack

  lazy var persistentContainer: NSPersistentContainer = {
      /*
       The persistent container for the application. This implementation
       creates and returns a container, having loaded the store for the
       application to it. This property is optional since there are legitimate
       error conditions that could cause the creation of the store to fail.
      */
      let container = NSPersistentContainer(name: "BPCorrect")
      container.loadPersistentStores(completionHandler: { (storeDescription, error) in
          if let error = error as NSError? {
              // Replace this implementation with code to handle the error appropriately.
              // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
               
              /*
               Typical reasons for an error here include:
               * The parent directory does not exist, cannot be created, or disallows writing.
               * The persistent store is not accessible, due to permissions or data protection when the device is locked.
               * The device is out of space.
               * The store could not be migrated to the current model version.
               Check the error message to determine what the actual problem was.
               */
              fatalError("Unresolved error \(error), \(error.userInfo)")
          }
      })
      return container
  }()

  // MARK: - Core Data Saving support

  func saveContext () {
      let context = persistentContainer.viewContext
      if context.hasChanges {
          do {
              try context.save()
          } catch {
              // Replace this implementation with code to handle the error appropriately.
              // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
              let nserror = error as NSError
              fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
          }
      }
  }

}

extension DashboardVC : SlideMenuControllerDelegate {
  
  func leftWillOpen() {
    print("SlideMenuControllerDelegate: leftWillOpen")
  }
  
  func leftDidOpen() {
    print("SlideMenuControllerDelegate: leftDidOpen")
  }
  
  func leftWillClose() {
    print("SlideMenuControllerDelegate: leftWillClose")
  }
  
  func leftDidClose() {
    print("SlideMenuControllerDelegate: leftDidClose")
  }
  
  func rightWillOpen() {
    print("SlideMenuControllerDelegate: rightWillOpen")
  }
  
  func rightDidOpen() {
    print("SlideMenuControllerDelegate: rightDidOpen")
  }
  
  func rightWillClose() {
    print("SlideMenuControllerDelegate: rightWillClose")
  }
  
  func rightDidClose() {
    print("SlideMenuControllerDelegate: rightDidClose")
  }
}

extension UIApplication {
  var statusBarView: UIView? {
    if responds(to: Selector("statusBar")) {
      return value(forKey: "statusBar") as? UIView
    }
    return nil
  }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert, .badge, .sound])
    }
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        print(APPDELEGATE.window?.rootViewController)
        if let slideView = APPDELEGATE.window?.rootViewController as? ExSlideMenuController
        {
            
            if let navController = slideView.mainViewController as? UINavigationController
            {
                UIAlertController.showAlertWithTitle(title: "APP Name", message: "Choose Options", onViewController: slideView, withButtonArray: ["Snooz","Take Reading","Cancle"]) { (buttonIndex) in
                    if buttonIndex == 1
                    {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let measureBP_AND_VC = storyboard.instantiateViewController(withIdentifier: "MeasureBPVC") as! MeasureBPVC
                        navController.show(measureBP_AND_VC, sender: self)
                        
                    }
                }
            }
            
            }


        switch response.actionIdentifier {
        case "action1":
            print("Action First Tapped")
        case "action2":
            print("Action Second Tapped")
        default:
            break
        }
        completionHandler()
        print(response.notification.request.content.userInfo)

    }
    
}
extension Date {
    
    // Year
    var currentYear: String? {
        return getDateComponent(dateFormat: "yy")
        //return getDateComponent(dateFormat: "yyyy")
    }
    
    // Month
    var currentMonth: String? {
        return getDateComponent(dateFormat: "M")
        //return getDateComponent(dateFormat: "MM")
        //return getDateComponent(dateFormat: "MMM")
        //return getDateComponent(dateFormat: "MMMM")
    }
    
    
    // Day
    var currentDay: String? {
        return getDateComponent(dateFormat: "dd")
        //return getDateComponent(dateFormat: "d")
    }
    
    
    func getDateComponent(dateFormat: String) -> String? {
        let format = DateFormatter()
        format.dateFormat = dateFormat
        return format.string(from: self)
    }
    
    
}
