//
//  DataBaseManager.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 17/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import FMDB
import UIKit

//MARK:- Table Names
let strDatabaseName = "BPCorrectdb"

enum dbTableName : String {
    case tblProtocol = "protocol"
    case tblReadingData = "measureBp"
}

enum tblReadingColumns : String
{
    case systolic = "systolic"
    case diastolic = "diastolic"
    case device_id = "device_id"
    case pulse_data = "pulse_data"
    case device_name = "device_name"
    case device_mac_address = "device_mac_address"
    case patientId = "patientId"
    case reading_time = "reading_time"
    case is_abberant = "is_abberant"
    case protocol_id = "protocol_id"
    
    
}

//MARK:- Protocol Table Columns
enum tblProtocolColumns : String {
    case protocolID = "protocolID"
    case createdDate = "created"
    case startDate = "startDate"
    case endDate = "endDate"
    case startTime = "startTime"
    case endTime = "endTime"
    case isStart = "isStart"
    case isEnd = "isEnd"
    case isUpdated = "isUpdate"
    case updatedTime = "updatedTime"

}
//MARK: - Table Colums
enum tblRecentChatColumn : String {
    case chattype = "chattype"
    case datetime = "datetime"
    case deviceid = "deviceid"
    case dtype = "dtype"
    case id = "id"
    case message = "message"
    case name = "name"
    case photo = "photo"
    case userstatus = "userstatus"
    
    
    static let allValues = [chattype, datetime, deviceid, dtype, id, message, name, photo, userstatus]
    
    
    
}


class DataBaseManager : NSObject  {
    
    
    var database = FMDatabase()
    
    private static var instance : DataBaseManager =
    {
        let newInstance = DataBaseManager.init()
        
        return newInstance
    }()
    
    class func sharedInstance() -> DataBaseManager {
        return instance
    }
    
    
     override init() {
        super.init()
        
        
        
        let fileURL = try! FileManager.default
            .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("BPCorrectdb.db")
        
        let fileExists = (try? fileURL.checkResourceIsReachable()) ?? false
        if !fileExists {
            let bundleURL = Bundle.main.url(forResource: strDatabaseName, withExtension: "db")!
            try! FileManager.default.copyItem(at: bundleURL, to: fileURL)
        }
        
        database = FMDatabase(url: fileURL)
        
        print(fileURL)
    }
    
    
    func checkIfAlarmDataExist()->Bool
    {
        var isRecordExist:Bool = false
        database.open()
        do {
            if let rs = try? database.executeQuery("SELECT COUNT(*) as id FROM \(dbTableName.tblProtocol.rawValue)", values: nil) {
                while rs.next() {
                    let columnCount = rs.int(forColumn: "id")
                    if columnCount > 0
                    {
                        isRecordExist = true
                    }
                    print("Total Records:", rs.int(forColumn: "id"))
                }
            }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
return isRecordExist

    }
    
    //Get All Record From Protocol Data
    func getAllActiveAlarmData()->[String:Any]
    {
        var dataDict = [String:Any]()
        database.open()
        do {
            if let rs = try? database.executeQuery("SELECT * FROM \(dbTableName.tblProtocol.rawValue)", values: nil) {
                while rs.next() {
                    let id = rs.string(forColumn: "id")
                    let startDate = rs.string(forColumn: "startDate")
                    let endDate = rs.string(forColumn: "endDate")
                    let startTime = rs.string(forColumn: "startTime")
                    let endTime = rs.string(forColumn: "endTime")
                    let updatedTime = rs.string(forColumn: "updatedTime")
                    dataDict = ["id":id ?? 0,"startDate":startDate!,"endDate":endDate!,"startTime":startTime!,"endTime":endTime!,"updatedTime":updatedTime!]
                    print("Total Records iddddd :", id ?? "")
                }
            }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        return dataDict
        
    }

    
    //Get All Record From Protocol Data
    func getAllProtocolReadingData(startDate:String,endDate:String)->Int
    {
        var coulmnCount = Int()
        database.open()
        do {
            //("SELECT COUNT(*) as id FROM \(dbTableName.tblProtocol.rawValue)", values: nil)
//            NSString *startingDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
//            startingDateTime = [NSString stringWithFormat:@"%@ %@:00",startingDateTime,startTime];
//
//
//            FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where createdTime BETWEEN (?) AND (?) AND protocol_id = '';",startingDateTime,stringDate];

            
            if let rs = try? database.executeQuery("SELECT COUNT(*) as protocol_id FROM \(dbTableName.tblReadingData.rawValue) where createdTime BETWEEN (?) AND (?) AND protocol_id != ''", values: [startDate,endDate]) {
                while rs.next() {
                    coulmnCount = Int(rs.int(forColumn: "protocol_id"))
                    print("Total Records:", rs.int(forColumn: "protocol_id"))
                }
            }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        return coulmnCount
        
    }
    //Get All Record From Protocol Data
    func getAllReadingRecordData()->[[String:Any]]
    {
        var dataDictArr = [[String:Any]]()
        database.open()
        do {
            print("SELECT * FROM \(dbTableName.tblReadingData.rawValue) ORDER BY id DESC")
            if let rs = try? database.executeQuery("SELECT * FROM \(dbTableName.tblReadingData.rawValue) ORDER BY id DESC", values: nil)
            {
                while rs.next() {
                    
                    let id = rs.string(forColumn: "id")
                    let systolic = rs.string(forColumn: "systolic")
                    let diastolic = rs.string(forColumn: "diastolic")
                    let device_id = rs.string(forColumn: "device_id")
                    let pulse_data = rs.string(forColumn: "pulse_data")
                    let device_name = rs.string(forColumn: "device_name")
                    let patientId = rs.string(forColumn: "patientId")
                    let reading_time = rs.string(forColumn: "reading_time")
                    let is_abberant = rs.string(forColumn: "is_abberant")
                    let protocol_id = rs.string(forColumn: "protocol_id")
                    
                    var dataDict = [String:Any]()
                    dataDict = ["id":id ?? 0,"systolic":systolic!,"diastolic":diastolic!,"device_id":device_id!,"pulse_data":pulse_data!,"patientId":patientId!,"device_name":device_name!,"reading_time":reading_time!,"is_abberant":is_abberant ?? "0","protocol_id":protocol_id!]
                    print("Total Records iddddd :", id ?? "")
                    
                    dataDictArr.append(dataDict)
                }
            }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        return dataDictArr
        
    }
    func getAllProtocolReadingRecordData(startDate:String,endDate:String)->[[String:Any]]
    {
        var dataDictArr = [[String:Any]]()
        database.open()
        do {
            if let rs = try? database.executeQuery("SELECT * FROM \(dbTableName.tblReadingData.rawValue) where createdTime BETWEEN (?) AND (?) AND protocol_id != ''", values: [startDate,endDate]) {
                while rs.next() {
                    
                    let id = rs.string(forColumn: "id")
                    let systolic = rs.string(forColumn: "systolic")
                    let diastolic = rs.string(forColumn: "diastolic")
                    let device_id = rs.string(forColumn: "device_id")
                    let pulse_data = rs.string(forColumn: "pulse_data")
                    let device_name = rs.string(forColumn: "device_name")
                    let patientId = rs.string(forColumn: "patientId")
                    let reading_time = rs.string(forColumn: "reading_time")
                    let is_abberant = rs.string(forColumn: "is_abberant")
                    let protocol_id = rs.string(forColumn: "protocol_id")
                    
                    var dataDict = [String:Any]()
                    dataDict = ["id":id ?? 0,"systolic":systolic!,"diastolic":diastolic!,"device_id":device_id!,"pulse_data":pulse_data!,"patientId":patientId!,"device_name":device_name!,"reading_time":reading_time!,"is_abberant":is_abberant ?? "0","protocol_id":protocol_id!]
                    print("Total Records iddddd :", id ?? "")
                    
                    dataDictArr.append(dataDict)
                }
            }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        return dataDictArr
        
    }

    
    //MARK: - RecentChat DB Methods
    func insertValuesToReadingData(dictVal : [String:Any])
    {
        database.open()
        do {
            //            try database.executeStatements("DELETE FROM recentChats")
            
              //
            let systolic = dictVal[tblReadingColumns.systolic.rawValue] as? String ?? ""

            let diastolic = dictVal[tblReadingColumns.diastolic.rawValue] as? String ?? ""
            let device_id = dictVal[tblReadingColumns.device_id.rawValue] as? String ?? ""
            let pulse_data = dictVal[tblReadingColumns.pulse_data.rawValue] as? String ?? ""
            let device_name = dictVal[tblReadingColumns.device_name.rawValue] as? String ?? ""
            let device_mac_address = dictVal[tblReadingColumns.device_mac_address.rawValue] as? String ?? ""
            let patientId = dictVal[tblReadingColumns.patientId.rawValue] as? String ?? ""
            let reading_time = dictVal[tblReadingColumns.reading_time.rawValue] as? String ?? ""
            let is_abberant = dictVal[tblReadingColumns.is_abberant.rawValue] as? String ?? ""
            let protocol_id = dictVal[tblReadingColumns.protocol_id.rawValue] as? String ?? ""
            
            
            
            let strQueary = "(systolic,diastolic,device_id,pulse_data,device_name,device_mac_address,patientId,reading_time,is_abberant,protocol_id)"
            
            try database.executeUpdate("insert into \(dbTableName.tblReadingData.rawValue) \(strQueary) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [systolic,diastolic,device_id,pulse_data,device_name,device_mac_address,patientId,reading_time,is_abberant,protocol_id])
            
            
            
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
    func getValuesToRecentChatDB() -> [[String:Any]]
    {
        var arrResultValue = [[String:Any]]()
        
        database.open()
        do {
            
            
            let rs = try database.executeQuery("select * from recentChats", values: nil)
            while rs.next() {
                
                
                if let deviceId = rs.string(forColumn: "deviceid"),let id = rs.string(forColumn: "id"),let name = rs.string(forColumn: "name"),let path = rs.string(forColumn: "path"),let photo = rs.string(forColumn: "photo"),let chatType = rs.string(forColumn: "chattype"),let datetime = rs.string(forColumn: "datetime"),let dtype = rs.string(forColumn: "dtype"),let message = rs.string(forColumn: "message"),let userstatus = rs.string(forColumn: "userstatus"),let admin = rs.string(forColumn: "admin"),let description = rs.string(forColumn: "description")
                {
                    
                    let dbDict = ["deviceid":deviceId, "id":id, "name":name, "path":path, "photo":photo,"chattype":chatType,"datetime":datetime,"dtype":dtype,"message":message, "userstatus":userstatus, "admin":admin, "description":description]
                    
                    print(dbDict)
                    arrResultValue.append(dbDict as [String : AnyObject])
                    
                }
            }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        return arrResultValue
        
    }

    
    //MARK: - RecentChat DB Methods
    func insertValuesToCreateProtocol(dictVal : [String:Any])
    {
        do {
            if checkIfAlarmDataExist()
            {
            try database.executeStatements("DELETE FROM \(dbTableName.tblProtocol.rawValue)")
            }
            database.open()

                //
            let protocolName = dictVal[tblProtocolColumns.protocolID.rawValue] as? String ?? ""
            let createdDate = dictVal[tblProtocolColumns.createdDate.rawValue] as? String ?? ""
            let startDate = dictVal[tblProtocolColumns.startDate.rawValue] as? String ?? ""
            let endDate = dictVal[tblProtocolColumns.endDate.rawValue] as? String ?? ""
            let startTime = dictVal[tblProtocolColumns.startTime.rawValue] as? String ?? ""
            let endTime = dictVal[tblProtocolColumns.endTime.rawValue] as? String ?? ""
            let isStart = dictVal[tblProtocolColumns.isStart.rawValue] as? String ?? ""
            let isEnd = dictVal[tblProtocolColumns.isEnd.rawValue] as? String ?? ""
            let isUpdated = dictVal[tblProtocolColumns.isUpdated.rawValue] as? String ?? ""
            let updatedTime = dictVal[tblProtocolColumns.updatedTime.rawValue] as? String ?? ""
            

            
            let strQueary = "(protocolID,created,startDate,endDate,startTime,endTime,isStart,isEnd,isUpdate,updatedTime)"
                
                try database.executeUpdate("insert into \(dbTableName.tblProtocol.rawValue) \(strQueary) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [protocolName, createdDate, startDate, endDate, startTime, endTime, isStart, isEnd, isUpdated,updatedTime])
                
                
                
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
    
    //MARK: - RecentChat DB Methods
    func updateValuesToCreateProtocol(dictVal : [String:Any],id:String)
    {
        database.open()
        do {
            //
            let protocolName = dictVal[tblProtocolColumns.protocolID.rawValue] as? String ?? ""
            let createdDate = dictVal[tblProtocolColumns.createdDate.rawValue] as? String ?? ""
            let startDate = dictVal[tblProtocolColumns.startDate.rawValue] as? String ?? ""
            let endDate = dictVal[tblProtocolColumns.endDate.rawValue] as? String ?? ""
            let startTime = dictVal[tblProtocolColumns.startTime.rawValue] as? String ?? ""
            let endTime = dictVal[tblProtocolColumns.endTime.rawValue] as? String ?? ""
            let isStart = dictVal[tblProtocolColumns.isStart.rawValue] as? String ?? ""
            let isEnd = dictVal[tblProtocolColumns.isEnd.rawValue] as? String ?? ""
            let isUpdated = dictVal[tblProtocolColumns.isUpdated.rawValue] as? String ?? ""
            let updatedTime = dictVal[tblProtocolColumns.updatedTime.rawValue] as? String ?? ""
            
            
            
            let strQueary = "protocolID=?,created=?,startDate=?,endDate=?,startTime=?,endTime=?,isStart=?,isEnd=?,isUpdate=?,updatedTime=?"
            
        //    try contactDB.executeUpdate("UPDATE Darood SET count=? WHERE title=?", values: [counter, title])

            
            
            try database.executeUpdate("UPDATE \(dbTableName.tblProtocol.rawValue) SET \(strQueary) WHERE id=\(id)", values: [protocolName, createdDate, startDate, endDate, startTime, endTime, isStart, isEnd, isUpdated,updatedTime])
            
            
            
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
//    func getValuesToRecentChatDB() -> [[String:Any]]
//    {
//        var arrResultValue = [[String:Any]]()
//
//        database.open()
//        do {
//
//            //
//            //                let strAdmin = dictVal["admin"] as? String ?? ""
//            //                let strDescription = dictVal["description"] as? String ?? ""
//            //                let strPath = dictVal["path"] as? String ?? ""
//            //
//            //                let strChattype = dictVal["\(tblRecentChatColumn.chattype)"] as! String
//            //                let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
//            //                let strDeviceID = dictVal["\(tblRecentChatColumn.deviceid)"] as! String
//            //                let strDtype = dictVal["\(tblRecentChatColumn.dtype)"] as! String
//            //                let strID = dictVal["\(tblRecentChatColumn.id)"] as! String
//            //                let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
//            //                let strName = dictVal["\(tblRecentChatColumn.name)"] as! String
//            //                let strPhoto = dictVal["\(tblRecentChatColumn.photo)"] as! String
//            //                let strUsrStatus = dictVal["\(tblRecentChatColumn.userstatus)"] as! String
//
//
//
//            //                try database.executeUpdate("insert into recentChats \(strQueary) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strChattype, strDateTime, strDeviceID, strDtype, strID, strMsg, strName, strPhoto, strUsrStatus,strAdmin,strDescription,strPath])
//
//            let rs = try database.executeQuery("select * from recentChats", values: nil)
//            while rs.next() {
//
//
//                if let deviceId = rs.string(forColumn: "deviceid"),let id = rs.string(forColumn: "id"),let name = rs.string(forColumn: "name"),let path = rs.string(forColumn: "path"),let photo = rs.string(forColumn: "photo"),let chatType = rs.string(forColumn: "chattype"),let datetime = rs.string(forColumn: "datetime"),let dtype = rs.string(forColumn: "dtype"),let message = rs.string(forColumn: "message"),let userstatus = rs.string(forColumn: "userstatus"),let admin = rs.string(forColumn: "admin"),let description = rs.string(forColumn: "description")
//                {
//
//                    let dbDict = ["deviceid":deviceId, "id":id, "name":name, "path":path, "photo":photo,"chattype":chatType,"datetime":datetime,"dtype":dtype,"message":message, "userstatus":userstatus, "admin":admin, "description":description]
//
//                    print(dbDict)
//                    arrResultValue.append(dbDict as [String : AnyObject])
//
//                }
//            }
//
//        } catch {
//            print("failed: \(error.localizedDescription)")
//        }
//        database.close()
//        return arrResultValue
//
//    }
    
    
    //MARK: - USer Listing DB Methods
    func insertValuesToUserListingTable(dataArry : [[String:Any]])
    {
        database.open()
        do {
            try database.executeStatements("DELETE FROM users")
            
            for dictVal in dataArry
            {
                //
                
                let strDeviceID = dictVal["deviceid"] as! String
                let strID = dictVal["id"] as! String
                let strName = dictVal["name"] as! String
                let strPath = dictVal["path"] as! String
                let strPhoto = dictVal["photo"] as! String
                try database.executeUpdate("insert into users (deviceid,id,name,path,photo) values (?, ?, ?, ?, ?)", values: [strDeviceID, strID, strName, strPath, strPhoto])
                
            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
    func fetchUserListingFromDB() -> [[String:Any]]
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
            let rs = try database.executeQuery("select deviceid, id, name, path, photo from users", values: nil)
            while rs.next() {
                
                
                if let deviceId = rs.string(forColumn: "deviceid"), let id = rs.string(forColumn: "id"), let name = rs.string(forColumn: "name"), let path = rs.string(forColumn: "path"), let photo = rs.string(forColumn: "photo")  {
                    
                    let dbDict = ["deviceid":deviceId, "id":id, "name":name, "path":path, "photo":photo]
                    
                    print(dbDict)
                    userListingData.append(dbDict as [String : AnyObject])
                    //tblJSON.reloadData()
                    //print("x = \(x); y = \(y); z = \(z)")
                }
            }
        }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
        return userListingData
        
    }
    
    //MARK: - Chat History Listing DB Methods
    func insertValuesToChatTable(dataArry : [[String:Any]])
    {
        database.open()
        do {
            
            try database.executeStatements("DELETE FROM chatList")
            
            for dictVal in dataArry
            {
                //
                
                let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
                let strDeliver = dictVal["deliver"] as! String
                let strID = dictVal["\(tblRecentChatColumn.id)"] as? String ?? "00"
                let strIsRead = dictVal["isread"] as! String
                let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
                let strRecvieID = dictVal["rcv_id"] as? String ?? dictVal["reciverid"]
                let strSendID = dictVal["send_id"] as? String ?? dictVal["senderid"]
                let strType = dictVal["type"] as! String
                let strUid = dictVal["uid"] as! String
                
                
                
                try database.executeUpdate("insert into chatList (datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strDateTime, strDeliver, strID, strIsRead, strMsg, strRecvieID, strSendID, strType, strUid, "1"])
                
            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
    func insertDictToChatTable(dictVal : [String:Any],isMsgSent : String)
    {
        database.open()
        do {
            
            // try database.executeStatements("DELETE FROM chatList")
            
            
            //
            
            let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
            let strDeliver = dictVal["deliver"] as! String
            let strID = dictVal["\(tblRecentChatColumn.id)"] as? String ?? "00"
            let strIsRead = dictVal["isread"] as! String
            let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
            let strRecvieID = dictVal["rcv_id"] as? String ?? dictVal["reciverid"]
            let strSendID = dictVal["send_id"] as? String ?? dictVal["senderid"]
            let strType = dictVal["type"] as? String ?? dictVal["dtype"]
            let strUid = dictVal["uid"] as! String
            
            
            
            try database.executeUpdate("insert into chatList (datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strDateTime, strDeliver, strID, strIsRead, strMsg, strRecvieID, strSendID, strType, strUid, isMsgSent])
            
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
    
    func fetchChatHistoryFromDB(userID : String) -> [[String:Any]]
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
            //SELECT * FROM contents WHERE id = 1
            let rs = try database.executeQuery("select * from chatList WHERE rcv_id = \(userID)", values: nil)
            while rs.next() {
                
                //datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent
                if let datetime = rs.string(forColumn: "datetime"), let id = rs.string(forColumn: "id"), let deliver = rs.string(forColumn:"deliver"), let isread = rs.string(forColumn: "isread"), let message = rs.string(forColumn: "message"), let rcv_id = rs.string(forColumn: "rcv_id"), let send_id = rs.string(forColumn: "send_id"), let type = rs.string(forColumn: "type"), let uid = rs.string(forColumn: "uid"), let isMsgSent = rs.string(forColumn: "isMsgSent")  {
                    
                    let dbDict = ["datetime":datetime, "id":id, "deliver":deliver, "isread":isread, "message":message, "rcv_id":rcv_id, "send_id":send_id, "type":type, "uid":uid, "isMsgSent":isMsgSent]
                    
                    print(dbDict)
                    userListingData.append(dbDict as [String : AnyObject])
                    //tblJSON.reloadData()
                    //print("x = \(x); y = \(y); z = \(z)")
                }
            }
        }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
        return userListingData
        
    }
    
    func fetchUnsendMessageChatFromDB() -> [[String:Any]]
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
            let rs = try database.executeQuery("select * from chatList where isMsgSent = 0", values: nil)
            while rs.next() {
                
                //datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent
                if let datetime = rs.string(forColumn: "datetime"), let id = rs.string(forColumn: "id"), let deliver = rs.string(forColumn:"deliver"), let isread = rs.string(forColumn: "isread"), let message = rs.string(forColumn: "message"), let rcv_id = rs.string(forColumn: "rcv_id"), let send_id = rs.string(forColumn: "send_id"), let type = rs.string(forColumn: "type"), let uid = rs.string(forColumn: "uid"), let isMsgSent = rs.string(forColumn: "isMsgSent")  {
                    
                    let dbDict = ["datetime":datetime, "id":id, "deliver":deliver, "isread":isread, "message":message, "rcv_id":rcv_id, "send_id":send_id, "type":type, "uid":uid, "isMsgSent":isMsgSent]
                    
                    print(dbDict)
                    userListingData.append(dbDict as [String : AnyObject])
                    //tblJSON.reloadData()
                    //print("x = \(x); y = \(y); z = \(z)")
                }
            }
        }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
        return userListingData
        
    }
    
    
    
}
