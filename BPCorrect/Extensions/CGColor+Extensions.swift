//
//  CGColor+Extensions.swift
//  BPCorrect
//
//  Created by "" on 21/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit

extension CGColor  {
    
    static let styleService = BPCorrectStyleService.self
    
    // MARK: Colors
    class func bpCorrectCGThemeColor() -> CGColor? {
        return styleService.bpCorrectCGThemeColor()
    }
}
