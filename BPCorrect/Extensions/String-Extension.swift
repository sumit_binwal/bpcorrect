//
//  String-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 10/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var trimmedCount: Int {
        return trimmed.count
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                returnValue = false
            }
        }
        catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    static var versionNumber: String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        return version
    }
    
    static var buildNumber: String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleVersion"] as! String
        return version
    }
    
 
 

    
    
    struct AWSFolder {
        static let UsersDir = "users/"
        static let JobsDir = "jobs/"
        static let ChatMedia = "chatmedia/"
    }
    
    struct chatStatusString {
        
        static let requestForSurvey = "Request For Site Survey"
        static let waitingFinalOffer = "Waiting For Final Offer"
        static let surveyUnderReview = "Survey Under Review"
        static let reviewOffer = "Review Offer"
        static let surveyConfirm = "Survey Confirmed"
        static let surveyDeclined = "Survey Declined"
        static let offerConfirm = "Confirm Offer"
        static let surveyDecline = "Decline Offer"
        static let awardJob = "Award Job"
        static let awarded = "Awarded"
    }
    
    struct MyApp {
        
        static let AppName = "BPCorrect"
        static let GoogleApiKey = "AIzaSyA_pnzvs_y4pVQOObeLuyjOSJBJtO5td2Y"
        //sumitkonstant@gmail.com/kipl123456
        static let defaultAlertMessage = "WALTZiN User"
        static let defaultErrorMessage = "Some error occured while performing the request. Please try again!"
        static let Alert_TermsAndCond = "You need to accept Terms and Condition before Signing Up."
        static let UserNameContactUS = "Sorry! This username is already in use. If you would like to have access to this username, please Contact Us."
        
        static let TermsAndConditionURL = "http://www.google.com"
        static let PrivacyAndPolicy = "http://www.google.com"
        static let Alert_categoties = "Please pick 3 or more categories to proceed ahead."
        static let UTC_DateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSSZ"
        static let UTC_DateFormatFlights = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        static let AWS_POOL_ID = "ap-south-1:7f166c81-e8a2-413f-8d57-d83616746c0e"
        static let AWS_BUCKET_NAME = "udtalks"
        static let AWS_BASE_URL = "https://s3.ap-south-1.amazonaws.com/waltzin/"
        
        static let Track_Flight_InformationNotFound = "Tracking information could not be found."
        static let SubscriptionPrice = "You can enjoy these perks every month for less than one grande latte >> ONLY $4.99/month!"
        static let SubscriptionDays = "Valid till 30 days"
        static let InAppPurchaseIdentifier = "com.kipl.knackel"
        
        static let ReportUserSuccess = "Reported successfully"
        
        static let ExitRoomTitle = "Exit Room"
        static let ExitRoomDescription = "Do you want to exit this room?"
        
        //StoryBord View Controller Name
        static let languageScreenMessage = "Please select language first."
    }
    
    struct ControllerName {
        static let phoneNumberVC = "phoneNumberVC"
        static let otpVC = "OTPViewController"
        static let chooseLanguageVC = "LanguageViewController"
        static let signupVC = "SignUpViewController"
        static let loginVC = "LoginViewController"
        
        static let sideMenuVC = "sideMenuView"

        //StoryBord View Controller Name
        static let changeLanguageVC = "ChangeLanguageVC"
        static let changePasswordVC = "ChangePasswordVC"
        static let suggestCategoryVC = "SuggestCategoryVC"
        static let bidCreateThankYouVC = "BidCreateThankYouVC"
        static let firstMeetingVC = "FirstMeetingVC"
        static let bidReceivedVC = "BidReceivedVC"
        static let linkdinLoginVC = "LinkdinLoginVC"
        static let countryCodeViewController = "CountryCodeViewController"
        static let awardedJobVC = "AwardedJobVC"
        static let editMyProfileVC = "EditMyProfileVC"
        static let filterAvailableTaskersVC = "FilterAvailableTaskersVC"
        static let pastJobsVC = "PastJobsVC"
        static let inProgressJobsVC = "InProgressJobsVC"
        static let searchCategoryVC = "SearchCategoryVC"
        static let subCategoryVC = "SubCategoryVC"
        static let directRequestJobsVC = "DirectRequestJobsVC"
        static let bidRequestJobsVC = "BidRequestJobsVC"
        static let forgotPasswordViewController = "ForgotPasswordViewController"
        static let staticPagesVC = "StaticPagesVC"
        static let bidsReceivedNSIteSurveyVC = "BidsReceivedNSIteSurveyVC"
        static let rateTaskerVC = "RateTaskerVC"
        static let directMessagesVC = "DirectMessagesVC"
        static let bidMessagesVC = "BidMessagesVC"
        static let viewInvoiceVC = "ViewInvoiceVC"
        static let promoCodeVC = "PromoCodeVC"
        static let directCreateThankYouVC = "DirectCreateThankYouVC"
        static let disputeVC = "DisputeVC"
    }
    
    func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
 
//    static func isSpaceBetweenString(string: String) -> Bool{
//        let whitespaceSet = CharacterSet.whitespaces
//        if !string.trimmingCharacters(in: whitespaceSet).isEmpty {
//            return true
//        }else
//        {
//            return false
//        }
//    }

    static func isValidNumber(textField: UITextField, range: NSRange, string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        if newLength > 10 {
            return false
        }
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if textField.textInputMode?.primaryLanguage == nil || (textField.textInputMode?.primaryLanguage)! == "emoji" || string != numberFiltered  {
            return false;
        }
        return true
    }
    func isEmptyString() -> Bool
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if newString.isEmpty
        {
            return true
        }
        return false
    }
    
    func getTrimmedText() -> String
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString
    }
    
    
    func base64Encoded() -> String {
        
        var encodedString = ""
        let trimmedText = getTrimmedText()
        
        if let data = trimmedText.data(using: .utf8) {
            encodedString = data.base64EncodedString()
        }
        
        return encodedString
    }
    
    func base64Decoded() -> String {
        
        var decodedString = ""
        if let data = Data (base64Encoded: self) {
            decodedString = String (data: data, encoding: .utf8) ?? ""
        }
        
        return decodedString
    }
}
