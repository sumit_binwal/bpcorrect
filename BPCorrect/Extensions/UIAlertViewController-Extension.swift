//
//  UIAlertViewController-Extension.swift
//  Knackel
//
//  Created by Jitendra Singh on 13/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    
    //MARK:- Alert View
    static func showAlertWithTitle(title:String, message:String, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray {
            
            for item in buttonArray! {
                
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else {
                return
            }
            
            dismissHandler!(LONG_MAX)
        })
        
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertWithTitle(title:String, message:String, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], cancelButtonTitle: String, dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray {
            for item in buttonArray! {
                
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else {
                return
            }
            
            dismissHandler!(LONG_MAX)
        })
        
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    static func showTextfieldAlert(_ title: String, message: String, textfieldPlaceholder: String, onViewController:UIViewController, okButtonTitle: String, cancelButtonTitle: String, dismissHandler:((_ buttonIndex:Int, _ text: String)->())?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addTextField { textfield in
            textfield.placeholder = textfieldPlaceholder
        }
        
        let okaction = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action) in
            
            let textField = alertController.textFields![0] as UITextField
            
            if textField.text == nil || textField.text!.trimmedCount == 0  {
                return
            }
            
            alertController.dismiss(animated: true, completion: nil)
            
            guard (dismissHandler != nil) else {
                return
            }
            
            dismissHandler!(0, textField.text ?? "")
        })
        
        alertController.addAction(okaction)
        
        let action = UIAlertAction(title: cancelButtonTitle, style: .default, handler: { (action) in
            
            alertController.dismiss(animated: true, completion: nil)
            
            guard (dismissHandler != nil) else {
                return
            }
            
            dismissHandler!(LONG_MAX, "")
        })
        
        alertController.addAction(action)
        
        onViewController.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Action Sheet
    static func showActionSheetWithTitle(title:String, message:String, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray {
            
            for item in buttonArray! {
            
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
          
            guard (dismissHandler != nil) else {
                return
            }
            
            dismissHandler!(LONG_MAX)
        })
        
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    static func showActionSheetWithConstTitle(title:String, message:String, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [],  dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray {
            
            for item in buttonArray! {
            
                let action = UIAlertAction(title: item, style: .destructive, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
            return
        })
        
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertWith(title: String, message: String, dismissBloack:@escaping () -> Void) {
        
        var alertController: UIAlertController
        
        alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        var alertAction: UIAlertAction
        
        alertAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default, handler: { (alertAction) in
            
            dismissBloack()

            alertController.dismiss(animated: true, completion: {
                print("dismiss")
            })
        })
        
        alertController.addAction(alertAction)
        
        APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
