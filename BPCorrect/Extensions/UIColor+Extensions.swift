//
//  UIColor+Extensions.swift
//  BPCorrect
//
//  Created by "" on 21/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit

extension UIColor  {
    
    static let styleService = BPCorrectStyleService.self

    // MARK: Colors

    class func bpCorrectThemeColor() -> UIColor? {
        return styleService.bpCorrectThemeColor()
    }
}
