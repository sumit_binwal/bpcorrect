//
//  UIFont+Extensions.swift
//  BPCorrect
//
//  Created by "" on 21/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit


extension UIFont {
    
    static let styleService = BPCorrectStyleService.self
    
    // MARK : Fonts
    
    class func regularFontOfSize(_ size: CGFloat) -> UIFont? {
        return styleService.regularFontOfSize(size)
    }
    
    class func boldFontOfSize(_ size: CGFloat) -> UIFont? {
        return styleService.boldFontOfSize(size)
    }
    
    class func lightFontOfSize(_ size: CGFloat) -> UIFont? {
        return styleService.lightFontOfSize(size)
    }
    
}
