//
//  UIView+Extensions.swift
//  Phoenix
//
//  Created by Bhavna on 9/15/17.
//  Copyright © 2017   Software Pvt. Ltd. All rights reserved.
//

import UIKit

extension UIView {

    public class func loadFromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func findFirstResponder() -> UIView? {
        if self.isFirstResponder {
            return self
        }
        for subview: UIView in self.subviews {
            let firstResponder = subview.findFirstResponder()
            if nil != firstResponder {
                return firstResponder
            }
        }
        return nil
    }
}
