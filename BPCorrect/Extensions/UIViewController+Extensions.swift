//
//  UIViewController+Extensions.swift
//  PosFone
//
//  Created by "" on 20/11/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit

extension UIViewController  {
    
    func registerForKeyboradDidShowWithBlock (scrollview:UIScrollView ,block: ((CGSize?)  -> Void)? = nil ){
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardDidShowNotification, object: nil, queue: nil) { (notification) in
            if let userInfo = (notification as NSNotification).userInfo {
                if let keyboarRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                    if self.view.findFirstResponder() != nil {
                        let  keyboarRectNew = self.view .convert(keyboarRect, to: self.view)
                        let scrollViewSpace = scrollview.frame.origin.y + scrollview.contentOffset.y
                        let textFieldRect:CGRect =  self.view.findFirstResponder()!.convert(self.view.findFirstResponder()!.bounds, to: self.view)
                        let textFieldSpace = textFieldRect.origin.y + textFieldRect.size.height
                        let remainingSpace =  self.view.frame.size.height - keyboarRectNew.size.height
                        
                        if  scrollViewSpace + textFieldSpace > remainingSpace {
                            let gap =  scrollViewSpace + textFieldSpace - remainingSpace
                            scrollview .setContentOffset(CGPoint(x: scrollview.contentOffset.x, y: gap), animated: true)
                        }
                    }
                }
            }
            block?(CGSize.zero)
            
        }
    }
    
    func registerForKeyboardWillHideNotificationWithBlock ( scrollview:UIScrollView ,block: (() -> Void)? = nil) {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil, using: { (notification) -> Void in
            scrollview.scrollRectToVisible(CGRect(x: 0, y: 0, width: 0, height: 0), animated: true)
            scrollview.contentOffset = CGPoint(x: 0, y: 0)
            scrollview.contentInset =  UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            scrollview.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0);
            
            block?()
        })
    }
    
    func deregisterKeyboardShowAndHideNotification (){
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.view.findFirstResponder()?.resignFirstResponder()
    }
}
