//
//  LoadingViewProtocol.swift
//  Phoenix
//
//  Created by Bhavna on 9/15/17.
//  Copyright © 2017   Software Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol LoadingViewProtocol: class {
    var loadingView: LoadingView? { get set }
    var view: UIView! { get }
    func loadingMessage() -> String?
    func loadingStatusUpdated(_ isLoading: Bool)
}

extension LoadingViewProtocol {
    
    func setIsLoading(_ isLoading: Bool, animated: Bool = false) {
        if isLoading {
            showIsLoading(animated)
        } else {
            hideIsLoading(animated)
        }
        loadingStatusUpdated(isLoading)
    }
    
    func loadingMessage() -> String? {
        return nil
    }
    
    func loadingStatusUpdated(_ isLoading: Bool) {
    }
    
    fileprivate func showIsLoading(_ animated: Bool) {
        if loadingView == nil {
            // .loadFromNib can be called as UIView.loadFromNib
            loadingView = .loadFromNib()
        }
        
        loadingView?.spinnerView.startAnimating()
        loadingView?.translatesAutoresizingMaskIntoConstraints = false
        loadingView?.backgroundImage.backgroundColor = UIColor.black
        loadingView?.spinnerContainerView.layer.cornerRadius = 5.0
        loadingView?.isHidden = animated
        loadingView?.loadingMessageLabel.textColor = UIColor.gray
        loadingView?.loadingMessageLabel.text = loadingMessage()
        
        view.addSubview(loadingView!)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v]|", options: .alignAllCenterX, metrics: nil, views: ["v": loadingView!]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: .alignAllCenterY, metrics: nil, views: ["v": loadingView!]))
        view.setNeedsLayout()
        
        if animated {
            loadingView?.alpha = 0
            loadingView?.isHidden = false
            UIView.animate(withDuration: 0.3, animations: { self.loadingView?.alpha = 1.0 })
        } else {
            self.loadingView?.alpha = 0.5
        }
    }
    
    fileprivate func hideIsLoading(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3,
                animations: {
                    self.loadingView?.alpha = 0
                },
                completion: { finished in
                    self.loadingView?.removeFromSuperview()
                }
            )
        } else {
            self.loadingView?.removeFromSuperview()
        }
    }
}
