//
//  LoadingView.swift
//  Phoenix
//
//  Created by Bhavna on 9/15/17.
//  Copyright © 2017   Software Pvt. Ltd. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var spinnerContainerView: UIView!
    @IBOutlet weak var spinnerView: SpinnerView!
    @IBOutlet weak var loadingMessageLabel: UILabel!

}
