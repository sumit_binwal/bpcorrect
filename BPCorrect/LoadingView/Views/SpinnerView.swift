//
//  SpinnerView.swift
//  Phoenix
//
//  Created by Bhavna on 9/15/17.
//  Copyright © 2017   Software Pvt. Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class SpinnerView: UIView {
    
    override func draw(_ rect: CGRect) {
        let aPath = UIBezierPath()
        
        UIColor.gray.setStroke()
        UIColor.clear.setFill()

        aPath.lineWidth = 5
        
        aPath.move(to: CGPoint(x: rect.maxX - aPath.lineWidth, y: rect.midY - aPath.lineWidth))
        
        aPath.addArc(withCenter: CGPoint(x: rect.midX, y: rect.midY), radius: rect.height * 0.5 - aPath.lineWidth, startAngle: 0, endAngle: CGFloat(Double.pi*1.7), clockwise: true)

        aPath.fill()
        aPath.stroke()
    }
    
    override func awakeFromNib() {
        backgroundColor = UIColor.clear
    }
    
    func startAnimating() {
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .curveLinear], animations: {
            UIView.animateKeyframes(withDuration: 0, delay: 0, options: .repeat, animations: {
                let fullRotation = CGFloat(Double.pi * 2)
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/3, animations: {
                    self.transform = CGAffineTransform(rotationAngle: 1/3 * fullRotation)
                })
                UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3, animations: {
                    self.transform = CGAffineTransform(rotationAngle: 2/3 * fullRotation)
                })
                UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3, animations: {
                    self.transform = CGAffineTransform(rotationAngle: 3/3 * fullRotation)
                })
                
                }, completion:nil)
        }, completion: nil)
    }
}
