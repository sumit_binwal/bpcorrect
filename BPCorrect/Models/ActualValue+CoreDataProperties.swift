//
//  ActualValue+CoreDataProperties.swift
//  BPCorrect
//
//  Created by "" on 01/05/19.
//  Copyright © 2019 "". All rights reserved.
//
//

import Foundation
import CoreData


extension ActualValue {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ActualValue> {
        return NSFetchRequest<ActualValue>(entityName: "ActualValue")
    }

    @NSManaged public var dbp: String?
    @NSManaged public var is_abberant: Bool
    @NSManaged public var protocol_id: String?
    @NSManaged public var sbp: String?
    @NSManaged public var timestamp: String?
    @NSManaged public var pulse: String?
    @NSManaged public var chartData: ChartDataModel?

}
