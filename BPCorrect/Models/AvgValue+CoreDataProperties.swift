//
//  AvgValue+CoreDataProperties.swift
//  BPCorrect
//
//  Created by "" on 01/05/19.
//  Copyright © 2019 "". All rights reserved.
//
//

import Foundation
import CoreData


extension AvgValue {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AvgValue> {
        return NSFetchRequest<AvgValue>(entityName: "AvgValue")
    }

    @NSManaged public var dbp: String?
    @NSManaged public var sbp: String?
    @NSManaged public var pulse: String?
    @NSManaged public var chartData: ChartDataModel?

}
