//
//  BPReadingResponseModel.swift
//  BPCorrect
//
//  Created by "" on 25/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import ObjectMapper

class BPReadingResponseModel: Mappable, Codable {
  
  var valid: Bool?
  var message: String?
  var data: String?
  
  
  required init?(map: Map){
    
  }
  
  func mapping(map: Map) {
    valid <- map["valid"]
    message <- map["message"]
    data <- map["data"]
  }
  
  required init(coder decoder: NSCoder) {
    self.valid = decoder.decodeObject(forKey: "valid") as? Bool
    self.message = decoder.decodeObject(forKey: "message") as? String
    self.data = decoder.decodeObject(forKey: "data") as? String
    
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(valid, forKey: "valid")
    coder.encode(message, forKey: "message")
    coder.encode(data, forKey: "data")
  }
}
