//
//  ChartDataModel+CoreDataProperties.swift
//  BPCorrect
//
//  Created by "" on 28/04/19.
//  Copyright © 2019 "". All rights reserved.
//
//

import Foundation
import CoreData


extension ChartDataModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ChartDataModel> {
        return NSFetchRequest<ChartDataModel>(entityName: "ChartDataModel")
    }

    @NSManaged public var date: String?
    @NSManaged public var avg_values: AvgValue?
    @NSManaged public var actual_values: NSSet?

}

// MARK: Generated accessors for actual_values
extension ChartDataModel {

    @objc(addActual_valuesObject:)
    @NSManaged public func addToActual_values(_ value: ActualValue)

    @objc(removeActual_valuesObject:)
    @NSManaged public func removeFromActual_values(_ value: ActualValue)

    @objc(addActual_values:)
    @NSManaged public func addToActual_values(_ values: NSSet)

    @objc(removeActual_values:)
    @NSManaged public func removeFromActual_values(_ values: NSSet)

}
