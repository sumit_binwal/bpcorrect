//
//  LoginModel.swift
//  BPCorrect
//
//  Created by "" on 23/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit
import ObjectMapper

class RefreshToken: Mappable {
  var value: String?
  var expiration: Int64?
  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    value <- map["value"]
    expiration <- map["expiration"]
  }
}

class AdditionalInformation: Mappable {
  var role: String?
  var enable: Int32?
  var id: Int32?
  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    role <- map["role"]
    enable <- map["enable"]
    id <- map["id"]
  }
}

class LoginModel: Mappable {
  var value: String?
  var expiration: Int64?
  var tokenType: String?
  var expired: Bool?
  var expiresIn: Int32?
  var scope : [String]?
  var refreshToken : RefreshToken?
  var additionalInformation : AdditionalInformation?
  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    value <- map["value"]
    expiration <- map["expiration"]
    tokenType <- map["tokenType"]
    expired <- map["expired"]
    expiresIn <- map["expiresIn"]
    scope <- map["scope"]
    refreshToken <- map["refreshToken"]
    additionalInformation <- map["additionalInformation"]
  }
}
