//
//  LoginModel.swift
//  BPCorrect
//
//  Created by "" on 23/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit
import ObjectMapper


class RegisterModel: Mappable {
  var message: String?
  var data: [Dictionary<String,AnyObject>]?
  var valid: Bool?

  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    message <- map["message"]
    data <- map["data"]
    valid <- map["valid"]
    
  }
}
