//
//  UserModel.swift
//  BPCorrect
//
//  Created by "" on 09/10/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit
import ObjectMapper

@objc
class UserModel : NSObject, Mappable, Codable {
  
    var valid: Bool?
    var message: String?
    var data: [User]?

    
    required init?(map: Map){
    
    }
    
    func mapping(map: Map) {
        valid <- map["valid"]
        message <- map["message"]
        data <- map["data"]
    }
    
    required init(coder decoder: NSCoder) {
        self.valid = decoder.decodeObject(forKey: "valid") as? Bool
        self.message = decoder.decodeObject(forKey: "message") as? String
        self.data = decoder.decodeObject(forKey: "data") as? [User]
      
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(valid, forKey: "valid")
        coder.encode(message, forKey: "message")
        coder.encode(data, forKey: "data")
    }
}


class User: Mappable, Codable {
  
  var end_date: String?
  var firstname: String?
  var gender: String?
  var isGraduated: Bool?
  var address2: String?
  var patientId: Int?
  var address1: String?
  var mobile1: String?
  var isAlphanumericPatientId: String?
  var total_readings: Int?
  var middlename: String?
  var weight: String?
  var protocol_id: String?
  var userId: Int?
  var lastname: String?
  var dob: String?
  var mobile2: String?
  var photo_url: String?
  var email: String?
  var height: String?
  var start_date: String?
  
  var country: String?
  var city: String?
  var zipcode: String?
  var state: String?
  
  var accessRole: String?
  
  
  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    end_date <- map["end_date"]
    firstname <- map["firstname"]
    gender <- map["gender"]
    isGraduated <- map["isGraduated"]
    address2 <- map["address2"]
    patientId <- map["patientId"]
    address1 <- map["address1"]
    mobile1 <- map["mobile1"]
    isAlphanumericPatientId <- map["isAlphanumericPatientId"]
    total_readings <- map["total_readings"]
    middlename <- map["middlename"]
    weight <- map["weight"]
    protocol_id <- map["protocol_id"]
    userId <- map["userId"]
    lastname <- map["lastname"]
    dob <- map["dob"]
    mobile2 <- map["mobile2"]
    photo_url <- map["photo_url"]
    email <- map["email"]
    height <- map["height"]
    start_date <- map["start_date"]
    
    country <- map["country"]
    city <- map["city"]
    zipcode <- map["zipcode"]
    state <- map["state"]
    accessRole <- map["accessRole"]
  }
}

