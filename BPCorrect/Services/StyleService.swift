//
//  StyleService.swift
//  BPCorrect
//
//  Created by "" on 21/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit

protocol StyleService {

    static func bpCorrectThemeColor() -> UIColor
    static func bpCorrectCGThemeColor() -> CGColor
    static func regularFontOfSize(_ size: CGFloat) -> UIFont
    static func boldFontOfSize(_ size: CGFloat) -> UIFont
    static func lightFontOfSize(_ size: CGFloat) -> UIFont
}

extension StyleService {
    static func bpCorrectThemeColor() -> UIColor? {
        return UIColor(red: 78.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    }
    
    static func bpCorrectCGThemeColor() -> CGColor? {
        return UIColor(red: 78.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1.0).cgColor
    }
    
    // MARK: Fonts
    static func regularFontOfSize(_ size: CGFloat) -> UIFont? {
        return UIFont(name: "Titillium-Regular", size: size)
    }
    
    static func boldFontOfSize(_ size: CGFloat) -> UIFont? {
        return UIFont(name: "Titillium-BoldUpright", size: size)
    }
    
    static func lightFontOfSize(_ size: CGFloat) -> UIFont? {
        return UIFont(name: "Titillium-Light", size: size)
    }
}
