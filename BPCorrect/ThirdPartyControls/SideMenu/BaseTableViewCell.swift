//
//  BaseTableViewCell.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//
import UIKit

public class BaseTableViewCell : UITableViewCell {
    class var identifier: String { return String.className(self) }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    public override func awakeFromNib() {
    }
    
    public func setup() {
    }
    
    public class func height() -> CGFloat {
        return 50
    }
    
    public func setData(data: Any?) {
        //self.backgroundColor = UIColor(hex: "F1F8E9")
        //self.textLabel?.textColor = astroBlueColor
        self.textLabel?.font = UIFont.regularFontOfSize(16)
        self.backgroundColor = UIColor.clear
        
        if let menuText = data as? String {
            self.textLabel?.text = menuText
            self.textLabel?.textColor = UIColor.white
        }
    }
    
    
    override public func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.alpha = 0.4
        } else {
            self.alpha = 1.0
        }
    }
    
    // ignore the default handling
    override public func setSelected(_ selected: Bool, animated: Bool) {
        
    }
}
