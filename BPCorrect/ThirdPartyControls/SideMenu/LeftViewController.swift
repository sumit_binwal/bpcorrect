//
//  LeftViewController.swift
//  BPCorrect
//
//  Created by "" on 17/09/18.
//  Copyright © 2018 "". All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case dashboard = 0
    case profile
    case measureBP
    case myBPVCReadings
    case learn
    case manageDevices
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Dashboard", "Profile", "Measure Blood Pressure", "My Blood Pressure Readings", "Learn", "Manage Devices"]
  
    var images = ["dashboard","aside_profile", "aside_messure_bloodpressure_selected-1", "aside_bp_readings",  "aside_learn","aside_manage_devices"]
  
    var mainViewController: UIViewController!
    var dashboardVC: UIViewController!
    var profileVC: UIViewController!
    var measureBPVC: UIViewController!
    var myBPVCReadingsVC: UIViewController!
    var learnVC: UIViewController!
    var manageDevicesVC: UIViewController!
  
  
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var cityLabel: UILabel!
    var userModel : UserModel?
    
    @IBAction func signOutButtonIsPressed(_ sender: Any) {
        
        let appDelegate : AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.showLoginScreen()
self.clearAllFile()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.imageView.layer.cornerRadius = (self.imageView.frame.size.width) / 2
        self.imageView.layer.masksToBounds = true
        self.imageView.layer.borderColor = UIColor.white.cgColor
        self.imageView.layer.borderWidth = 2.0

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
      
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.dashboardVC = UINavigationController(rootViewController: dashboardVC)

        let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.profileVC = UINavigationController(rootViewController: profileVC)
      
        let measureBPVC = storyboard.instantiateViewController(withIdentifier: "MeasureBPVC") as! MeasureBPVC
        self.measureBPVC = UINavigationController(rootViewController: measureBPVC)
      
        let myBPVCReadingsVC = storyboard.instantiateViewController(withIdentifier: "MyBPVCReadingsVC") as! MyBPVCReadingsVC
        self.myBPVCReadingsVC = UINavigationController(rootViewController: myBPVCReadingsVC)
      
        let learnVC = storyboard.instantiateViewController(withIdentifier: "LearnVC") as! LearnVC
        self.learnVC = UINavigationController(rootViewController: learnVC)
      
        let manageDevicesVC = storyboard.instantiateViewController(withIdentifier: "ManageDevicesVC") as! ManageDevicesVC
        self.manageDevicesVC = UINavigationController(rootViewController: manageDevicesVC)
    }
    
    override func viewWillAppear(_ animated: Bool) {
          
      self.setUpView()
      loadUserProfile()
    }
  
  
  func loadUserProfile() {
    
    userModel = BPUserDefaults.getUserModel()
    
    if (userModel != nil && (userModel?.data?.count)! > 0) {
      
      var address : String = userModel?.data![0].address1 ?? ""
      
      if address != "" {
        let address2 : String = userModel?.data![0].address2 ?? ""
        if address2 != "" {
          address = "\(address) , \(address2)"
        }
        
      } else {
        address = userModel?.data![0].address2 ?? ""
      }
      
      
      nameLabel.text = userModel?.data![0].firstname ?? ""
      cityLabel.text = address

    }
  }
    
    func setUpView() {
        
        self.imageView.layer.cornerRadius = self.imageView.frame.width / 2
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
        case .dashboard:
          
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            

            
            self.dashboardVC = UINavigationController(rootViewController: dashboardVC)
            self.slideMenuController()?.changeMainViewController(self.dashboardVC, close: true)
        case .profile:
            self.slideMenuController()?.changeMainViewController(self.profileVC, close: true)
        case .measureBP:
            self.slideMenuController()?.changeMainViewController(self.measureBPVC, close: true)
        case .myBPVCReadings:
          
            self.slideMenuController()?.changeMainViewController(self.myBPVCReadingsVC, close: true)
        case .learn:
            self.slideMenuController()?.changeMainViewController(self.learnVC, close: true)
        case .manageDevices:
             self.slideMenuController()?.changeMainViewController(self.manageDevicesVC, close: true)
        }
 
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return BaseTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = BaseTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: BaseTableViewCell.identifier)
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.setData(data: menus[indexPath.row])
        cell.imageView?.image = UIImage(named: (images[indexPath.row] as String?)!)
        return cell
    }
    
    func clearAllFile() {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        do {
            try fileManager.removeItem(at: myDocuments)
        } catch {
            return
        }
    }

}

