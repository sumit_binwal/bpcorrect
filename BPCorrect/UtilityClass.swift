//
//  UtilityClass.swift
//  MyanCareDoctor
//
//  Created by Santosh on 19/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import SafariServices

/// usr default keys
///
/// - userInfoData: userInfoData description
/// - notificationStatus: notificationStatus description
/// - fcmDeviceToken: fcmDeviceToken description
/// - chatUnReadCount: chatUnReadCount description
/// - apnsDeviceToken: apnsDeviceToken description
private enum UserDefaultEnum : String {
    
    case userInfoData = "userInfoData"
    case notificationStatus = "notificationStatus"
    case fcmDeviceToken = "FCM_Token"
    case chatUnReadCount = "chat_un_read_count"
    case apnsDeviceToken = "APNS_Token"
}

class UtilityClass: NSObject {
    
    
    class func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM dd, yyyy"
        return  dateFormatter.string(from: date!)
        
    }

    //MARK:- Extract Number
    
    /// extract number from string
    ///
    /// - Parameter string: string description
    /// - Returns: return value description
    class func extractNumber(fromString string:String) -> String {
        
        let characterSet = CharacterSet.decimalDigits.inverted
        let stringArray = string.components(separatedBy: characterSet)
        let newString = stringArray.joined(separator: "")
        
        return newString
    }
    
    
    //MARK:- get timeStamp from current data
    
    /// get current time stump
    ///
    /// - Returns: return value description
    class func getCurrentTimeStamp () -> String {
        
        // Get the Unix timestamp
        let timestamp = Date().timeIntervalSince1970
        //print(timestamp)
        
        return String(Int(timestamp))
    }
    

    
    /// get time stamp from date string
    ///
    /// - Parameter dateString: dateString description
    /// - Returns: return value description
    class func getTimeStampFromDate (dateString : String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM,yyyy"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: dateString)
        
        // Get the Unix timestamp
        let timestamp = date?.timeIntervalSince1970
        //print(timestamp ?? 0)
        
        return Int(timestamp!*1000)
    }
    
    //MARK:- get timeStamp from time
    
    /// get time stamp from time string
    ///
    /// - Parameter dateString: dateString description
    /// - Returns: return value description
    class func getTimeStampFromTime (dateString : String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: dateString)
        
        // Get the Unix timestamp
        let timestamp = date?.timeIntervalSince1970
        //print(timestamp ?? 0)
        
        return Int(timestamp!*1000)
    }

    //MARK:- Get Comma Seprated String From Array
    
    /// get comma seperated string from dictionary
    ///
    /// - Parameters:
    ///   - completeArray: completeArray description
    ///   - key: key description
    /// - Returns: return value description
    class func getCommaSepratedStringFromArrayDict(completeArray:[[String:Any]], withKeyName key:String) -> String {
        
        var nameArr: [String] = []
        
        for name in completeArray {
            let nameStr = name[key] as! String
            nameArr.append(nameStr)
        }
       
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    /// get comma seperated string from array
    ///
    /// - Parameter arr: arr description
    /// - Returns: return value description
    class func getCommaSepratedStringFromArray(completeArray arr : NSMutableArray) -> String {
        
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    /// get comma sepearted string from NSMutableArray
    ///
    /// - Parameter arr: arr description
    /// - Returns: return value description
    class func getCommaSepratedStringFromArrayDoctorFilter(completeArray arr : NSMutableArray) -> String {
        
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    /// convert date format
    ///
    /// - Parameter date: date description
    /// - Returns: return value description
//    class func convertDateFormater(_ date: String) -> String {
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/mm/yyyy"
//
//        dateFormatter.timeZone = NSTimeZone.system
//
//        let date = dateFormatter.date(from: date)
//        dateFormatter.dateFormat = "mm/dd/yyyy"
//
//        return  dateFormatter.string(from: date!)
//    }
    
    /// convert date format
    ///
    /// - Parameter date: date description
    /// - Returns: return value description
    class func getTimeFromDateString(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        
        return  dateFormatter.string(from: date!)
    }
    
    /// convert date format
    ///
    /// - Parameter date: date description
    /// - Returns: return value description
    class func getDateFromDateString(_ date: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        
        return date!
    }
 
    
    class func getDateFromDateString(_ date: String, _ formate: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        
        return date!
    }

    
    
    class func getDatenTime24HrFromDateString(_ date: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        
        return date!
    }

    /// function to perform date format
    ///
    /// - Parameters:
    ///   - givenDateStr: Date string
    ///   - currentDateFormat: current format
    ///   - resultingDateFormat: result format
    /// - Returns: Date string
    class func getDateStringFromStr(givenDateStr : String, andCurrentDateFormat currentDateFormat : String, andResultingDateFormat resultingDateFormat : String) -> String {
        
        let dateformatter = DateFormatter()
        
        dateformatter.dateFormat = currentDateFormat
        
        if let date = dateformatter.date(from: givenDateStr) {
            dateformatter.dateFormat = resultingDateFormat
            let dateStr = dateformatter.string(from: date)
            
            return dateStr
        }
        else {
            dateformatter.dateFormat = resultingDateFormat
            let dateStr = dateformatter.string(from: Date())
            
            return dateStr
        }
    }
    
    class func getEndDateFromNow() -> String
    {
        let formatter2 = DateFormatter()
        
        formatter2.timeZone = TimeZone.current
        
        formatter2.dateFormat = "yyyy-MM-dd"
        
        let tempEndDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        
        let endDate = formatter2.string(from: tempEndDate!)
        
        return endDate

    }
    
    class func getCurrentDateString() -> String
    {
        let someDate = Date()
        let formatter1 = DateFormatter()
        
        formatter1.timeZone = TimeZone.current
        
        formatter1.dateFormat = "yyyy-MM-dd HH:mm"
        
        let createdDate = formatter1.string(from: Date())
        
        return createdDate
    }
    
    class func convert12Timeto24(stringTime : String) -> String
    {
        let dateAsString = stringTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "HH:mm"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        
        return Date24
    }
    
    class func convert24Timeto12(stringTime : String) -> String
    {
        let dateAsString = stringTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        
        return Date24
    }

     class func convertAMPMto24Time(stringTime : String) -> String
    {
        let dateAsString = stringTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "a"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        
        return Date24
    }

    /// get image url
    ///
    /// - Parameters:
    ///   - imageURL: imageURL description
    ///   - image: image description
    /// - Returns: return value description
    class func getImageURL(imageURL: String, image:UIImage)-> NSURL {
  
        var documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        documentDirectory = documentDirectory! + "/"
    
        let localPath = documentDirectory?.appending(imageURL)
    
        let image = image
        let data = image.pngData()! as NSData
        data.write(toFile: localPath!, atomically: true)
    
        _ = NSData(contentsOfFile: localPath!)!
    
        let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
    
        return photoURL as NSURL
    }
    
    /// function to get Documents Directory Path
    ///
    /// - Returns: URL value
    class func getDocumentsDirectoryPath() -> URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    /// function to get Documents Folder
    ///
    /// - Parameter folderName: string value
    /// - Returns: URL value
    class func getDocumentsFolder(withName folderName: String) -> URL? {
        
        let documentsPath = UtilityClass.getDocumentsDirectoryPath()
        
        let newDirectoryPath = documentsPath.appendingPathComponent(folderName)
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: newDirectoryPath.path) {
            do {
                try fileManager.createDirectory(atPath: newDirectoryPath.path, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                return nil
            }
        }
        
        return newDirectoryPath
    }
    
    /// function to add file in folder
    ///
    /// - Parameters:
    ///   - folder: folder name -> string value
    ///   - fileName: file name -> string value
    ///   - fileData: Data value
    /// - Returns: bool value
    class func addFileToFolder(_ folder: String, fileName: String, fileData: Data) -> Bool {
        
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return false
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        
        let isSaved = fileManager.createFile(atPath: newFilePath.path, contents: fileData, attributes: nil)
        
        return isSaved
    }
    
    /// function to get file url from folder
    ///
    /// - Parameters:
    ///   - folder: folder name -> string value
    ///   - fileName: file name -> string value
    /// - Returns: URL value
    class func getFileURLFromFolder(_ folder: String, fileName: String) -> URL? {
        
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        
        if fileManager.fileExists(atPath: newFilePath.path) {
            return newFilePath
        }
        
        return nil
    }
}


