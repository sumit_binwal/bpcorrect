//
//  AlertUtils.swift
//  BPCorrect
//
//  Created by "" on 22/09/18.
//  Copyright © 2018 "". All rights reserved.
//


import UIKit

class AlertUtils {
    
    static func showAlertMessage(
        _ targetVC: UIViewController, withTitle title: String?, andMessage message: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        targetVC.present(alert, animated: true, completion: nil)
    }
}
