//
//  PfUserDefaults.swift
//  Posefone
//
//  Created by "" on 22/09/18.
//  Copyright © 2018   Software Pvt. Ltd. All rights reserved.
//

import UIKit

@objc
class BPUserDefaults : NSObject{
    
    static func setLoggedIn(_ status:Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(status, forKey: kIsUserLoggedIn)
        userDefaults.synchronize()
    }
    
    static func isLoggedIn() -> Bool {
        if let isLogin = UserDefaults.standard.object(forKey: kIsUserLoggedIn) as? Bool {
            return isLogin
        }
        return false
    }
  
  @objc
    static func getUserModel() -> UserModel {

        let user : UserModel? = nil
        if let objects = UserDefaults.standard.value(forKey: "user") as? Data {
            let decoder = JSONDecoder()
            if let objectDecoded = try? decoder.decode(UserModel.self, from: objects) as UserModel {
                return objectDecoded
            } else {
                return user!
            }
        } else {
            return user!
        }
    }

  
    static func saveUserModel(user: UserModel) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user){
            UserDefaults.standard.set(encoded, forKey: "user")
        }
    }

    static func setUserId(_ userId:String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(userId, forKey: kUserId)
        userDefaults.synchronize()
    }
    
    @objc static func userId() -> String {
        if let userId = UserDefaults.standard.object(forKey: kUserId) as? String {
            print("userId" + userId)
            return userId
        }
        return ""
    }
  
  static func setAuthToken(_ token:String) {
    let userDefaults = UserDefaults.standard
    userDefaults.set(token, forKey: kAccessToken)
    userDefaults.synchronize()
  }
  
  @objc
  static func accessToken() -> String {
    if let accessToken = UserDefaults.standard.object(forKey: kAccessToken) as? String {
      print("accessToken" + accessToken)
      return accessToken
    }
    return ""
  }
  
  @objc
  static func setAuthTokenExpiry(_ tokenExpiry:String) {
    let userDefaults = UserDefaults.standard
    userDefaults.set(tokenExpiry, forKey: kAccessTokenExpiry)
    userDefaults.synchronize()
  }
  
  static func accessTokenExpiry() -> String {
    if let accessTokenExpiry = UserDefaults.standard.object(forKey: kAccessTokenExpiry) as? String {
      print("accessTokenExpiry" + accessTokenExpiry)
      return accessTokenExpiry
    }
    return ""
  }

}
