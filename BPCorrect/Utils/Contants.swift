//
//  Constants.swift
//  BPCorrect
//
//  Created by "" on 07/04/19.
//  Copyright © 2016 Gravity Ecom. All rights reserved.
//


import Foundation

let kIsUserLoggedIn = "isUserLoggedIn"
let kIsAppLaunchedOnce = "appLaunchedOnce"
let kUserId = "userId"
let kAccessToken = "access_token"
let kAccessTokenExpiry = "access_token_expiry"
let kDOB = "dob"
let kFullName = "fullName"
let kEmail = "email"
let kDeviceId = "deviceId"
