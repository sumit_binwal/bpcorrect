//
//  Validations.swift
//  BPCorrect
//
//  Created by "" on 22/09/18.
//  Copyright © 2018 "". All rights reserved.
//


import UIKit

class Validations {

    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidPhoneNumber(_ testStr:String) -> Bool {
        let phoneNumberRegEx = "\\+?[0-9]*[1-9][0-9]*$"
        
        let phoneNumberTest = NSPredicate(format:"SELF MATCHES %@", phoneNumberRegEx)
        return phoneNumberTest.evaluate(with: testStr)
    }
}
