//
//  ANDBaseController.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 3/7/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "ANDBaseController.h"

@interface ANDBaseController ()

@end

@implementation ANDBaseController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // [self.navigationController.navigationBar setBarTintColor:backGroundColor];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
