//
//  DevicePairingStatusViewController.h
//  ANDMedical
//
//  Created by simantini bhattacharya on 1/8/15.
//  Copyright (c) 2015 a1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANDBaseController.h"
#import "DevicePairingViewController.h"

@interface DevicePairingStatusViewController : ANDBaseController
{
    UIWindow *window;
    
    UIView *view_background;
    NSTimer  *timer_userGuide, *timer_lifeTrack;
    int lifeTrackNumber;
    UIView *view_whiteSettingPopUp;


}
@property (nonatomic, retain) NSString *selected_device_type;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
