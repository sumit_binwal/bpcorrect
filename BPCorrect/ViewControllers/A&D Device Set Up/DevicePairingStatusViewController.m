//
//  DevicePairingStatusViewController.m
//  ANDMedical
//
//  Created by simantini bhattacharya on 1/8/15.
//  Copyright (c) 2015 a1. All rights reserved.
//

#import "DevicePairingStatusViewController.h"
#import "DeviceSetupViewController.h"
#import "ANDDevice.h"
#import "ADActivityMonitor.h"
#import "UIView+Animation.h"
#import "ADBloodPressure.h"
#import "DeviceSetUpCell.h"
#import "ADWeightScale.h"
#import "WeightScale.h"

#import "ADThermometer.h"
#import "ANDBLEDefines.h" //Sim added for WELWORLD-29
#import "DevicePairingViewController.h" //Sim added to implement the new device pairing
#import <sys/utsname.h> //WEL-373

#define kTimeDelay 2

@interface DevicePairingStatusViewController ()<ANDDeviceDelegate>
@property NSString *str_activityModelName;
@property NSString *str_bloodPressureModelName;
@property NSString *str_weightScaleModelName;
@property NSString *str_thermometerModelName; //Sim added for Thermometer Profile
@property ANDDevice *device; //Sim added for the delegate function
@property NSString *type;
@property NSString *display;
@property NSMutableArray *arr_activityMonitor;
@property Boolean created;

@property Boolean isAlertViewShown;
@end

@implementation DevicePairingStatusViewController



- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self setTitle:@"Pair Device"];
     window = [[[UIApplication sharedApplication]delegate] window];
    self.navigationController.navigationBarHidden = NO;
  
   
  
    //First time when this view is launched this dialogue is false since no confirmation is created
    _created = false;
    
    //Set the respective image over here
    if ([_selected_device_type isEqualToString:@"bp"]) {
        //Set a specific image view , map the image view from the view controller to the .h file
       _imageView.image = [UIImage imageNamed:@"pairing-bpm_notext-@2x"];
        //Define UILabel for the individual message
        //WEL-373 screen resize
        UILabel *lbl_message;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                                   encoding:NSUTF8StringEncoding];
        //Added iphone4s, ipad 3, ipad Air, ipad Air2
        if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
           || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
           || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
           || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
           || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
           || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"])) {
            
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 243, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
            
            
        } else {
           lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 285, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
           
        } //End of WEL-373
        
        lbl_message.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_message.numberOfLines = 0;
       // lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        lbl_message.textAlignment = NSTextAlignmentCenter;
        lbl_message.textColor = [UIColor whiteColor];
        lbl_message.text = NSLocalizedString(@"Pairing.BloodPressure.Message", @"");
        lbl_message.layer.cornerRadius = 1.0f;
        lbl_message.layer.borderWidth = 0.2f;
        [_imageView addSubview:lbl_message];

    } else if ([_selected_device_type isEqualToString:@"ws"]) {
        //Set a specific image view
        _imageView.image = [UIImage imageNamed:@"pairing-weight_notext-@2x"];
        //Define UILabel for the individual message
        //Universal Bluetooth ON message to be displayed for all the devices
        
        //WEL-373 screen resize
        UILabel *lbl_message;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                                   encoding:NSUTF8StringEncoding];
        //Added iphone4s, ipad 3, ipad Air, ipad Air2
        if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
           || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
           || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
           || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
           || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
           || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"]))
        {
            
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 240, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
            
        } else {
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 285, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        } //End of WEL-373
       
        
        lbl_message.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_message.numberOfLines = 0;
       // lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        lbl_message.textAlignment = NSTextAlignmentCenter;
        lbl_message.textColor = [UIColor whiteColor];
        lbl_message.text = NSLocalizedString(@"Pairing.Weight.Message", @"");
        lbl_message.layer.cornerRadius = 1.0f;
        lbl_message.layer.borderWidth = 0.2f;
        [_imageView addSubview:lbl_message];
    } else if ([_selected_device_type isEqualToString:@"th"]) {
        //Set a specific image view
        _imageView.image = [UIImage imageNamed:@"pairing-thermometer_notext-@2x"];
        //Define UILabel for the individual message
        //Universal Bluetooth ON message to be displayed for all the devices
        
        //WEL-373 screen resize
        UILabel *lbl_message;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                                   encoding:NSUTF8StringEncoding];
        //Added iphone4s, ipad 3, ipad Air, ipad Air2
        if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
           || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
           || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
           || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
           || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
           || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"])) {
            
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 245, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
            
        } else {
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 285, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        } //End of WEL-373
        
        
        lbl_message.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_message.numberOfLines = 0;
       // lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        lbl_message.textAlignment = NSTextAlignmentCenter;
        lbl_message.textColor = [UIColor whiteColor];
        lbl_message.text = NSLocalizedString(@"Pairing.Thermometer.Message", @"");
        lbl_message.layer.cornerRadius = 1.0f;
        lbl_message.layer.borderWidth = 0.2f;
        [_imageView addSubview:lbl_message];
        
        
    }
    else if ([_selected_device_type isEqualToString:@"uw302_ac"]) { //Case of Activity Monitor wel-287
        //Set a specific image view
        _imageView.image = [UIImage imageNamed:@"pairingpage_sleep_8@2x"];
        //Define UILabel for the individual message
        //Universal Bluetooth ON message to be displayed for all the devices
        
        //WEL-373 screen resize
        UILabel *lbl_message;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                                   encoding:NSUTF8StringEncoding];
        //Added iphone4s, ipad 3, ipad Air, ipad Air2
        if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
           || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
           || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
           || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
           || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
           || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"])) {
            
           // lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 245, 320, 200)];
            //lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
            
        } else {
          //  lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 294, 320, 200)];
            //lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        } //End of WEL-373
        
        
     /*   lbl_message.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_message.numberOfLines = 0;
        //  lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        lbl_message.textAlignment = NSTextAlignmentCenter;
        lbl_message.textColor = [UIColor whiteColor];
        lbl_message.text = NSLocalizedString(@"Pairing.Activity.Message", @"");
        lbl_message.layer.cornerRadius = 1.0f;
        lbl_message.layer.borderWidth = 0.2f;
        [_imageView addSubview:lbl_message];*/
        
        //Adding the 4 instructions - WELWORLD-121
        
        UILabel *lbl_pairing1;
        lbl_pairing1 = [[UILabel alloc]initWithFrame:CGRectMake(40, 25, 180, 80)];
        lbl_pairing1.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        lbl_pairing1.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_pairing1.numberOfLines = 3;
        lbl_pairing1.textAlignment = NSTextAlignmentLeft;
        lbl_pairing1.textColor = [UIColor blackColor];
        lbl_pairing1.text = NSLocalizedString(@"Pairing.Instruction.one", @"");
        lbl_pairing1.layer.cornerRadius = 1.0f;
        lbl_pairing1.layer.borderWidth = 0.0f;
        [_imageView addSubview:lbl_pairing1];
        
        UILabel *lbl_pairing2;
        lbl_pairing2 = [[UILabel alloc]initWithFrame:CGRectMake(240, 160, 60, 50)];
        lbl_pairing2.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        lbl_pairing2.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_pairing2.numberOfLines = 3;
        lbl_pairing2.textAlignment = NSTextAlignmentLeft;
        lbl_pairing2.textColor = [UIColor blackColor];
        lbl_pairing2.text = NSLocalizedString(@"Pairing.Instruction.two", @"");
        lbl_pairing2.layer.cornerRadius = 1.0f;
        lbl_pairing2.layer.borderWidth = 0.0f;
        [_imageView addSubview:lbl_pairing2];
        
        UILabel *lbl_pairing3;
        lbl_pairing3 = [[UILabel alloc]initWithFrame:CGRectMake(52, 390, 60, 50)];
        lbl_pairing3.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        lbl_pairing3.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_pairing3.numberOfLines = 3;
        lbl_pairing3.textAlignment = NSTextAlignmentLeft;
        lbl_pairing3.textColor = [UIColor blackColor];
        lbl_pairing3.text = NSLocalizedString(@"Pairing.Instruction.two", @"");
        lbl_pairing3.layer.cornerRadius = 1.0f;
        lbl_pairing3.layer.borderWidth = 0.0f;
        [_imageView addSubview:lbl_pairing3];
        
        UILabel *lbl_pairing4;
        lbl_pairing4 = [[UILabel alloc]initWithFrame:CGRectMake(152, 390, 60, 50)];
        lbl_pairing4.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        lbl_pairing4.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_pairing4.numberOfLines = 3;
        lbl_pairing4.textAlignment = NSTextAlignmentLeft;
        lbl_pairing4.textColor = [UIColor blackColor];
        lbl_pairing4.text = NSLocalizedString(@"Pairing.Instruction.two", @"");
        lbl_pairing4.layer.cornerRadius = 1.0f;
        lbl_pairing4.layer.borderWidth = 0.0f;
        [_imageView addSubview:lbl_pairing4];
        
        UILabel *lbl_pairing5;
        lbl_pairing5 = [[UILabel alloc]initWithFrame:CGRectMake(246, 390, 60, 50)];
        lbl_pairing5.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
        lbl_pairing5.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_pairing5.numberOfLines = 3;
        lbl_pairing5.textAlignment = NSTextAlignmentLeft;
        lbl_pairing5.textColor = [UIColor blackColor];
        lbl_pairing5.text = NSLocalizedString(@"Pairing.Instruction.three", @"");
        lbl_pairing5.layer.cornerRadius = 1.0f;
        lbl_pairing5.layer.borderWidth = 0.0f;
        [_imageView addSubview:lbl_pairing5];
UIImageView *imgViewSetting = [[UIImageView alloc] initWithFrame:CGRectMake(168, 80, 25, 25)];
        imgViewSetting.image = [UIImage imageNamed:@"setting_icon_instruction.png"];
        [_imageView addSubview:imgViewSetting];
        
        
    }else if ([_selected_device_type isEqualToString:@"ac"]) { //Case of Activity Monitor wel-287
        //Set a specific image view
        _imageView.image = [UIImage imageNamed:@"and-pairing-activity-notext-@2x"];
        //Define UILabel for the individual message
        //Universal Bluetooth ON message to be displayed for all the devices
        
        //WEL-373 screen resize
        UILabel *lbl_message;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                                   encoding:NSUTF8StringEncoding];
        //Added iphone4s, ipad 3, ipad Air, ipad Air2
        if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
           || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
           || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
           || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
           || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
           || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"])) {
            
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 245, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
            
        } else {
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 294, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        } //End of WEL-373
        
        
        lbl_message.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_message.numberOfLines = 0;
      //  lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        lbl_message.textAlignment = NSTextAlignmentCenter;
        lbl_message.textColor = [UIColor whiteColor];
        lbl_message.text = NSLocalizedString(@"Pairing.Activity.Message", @"");
        lbl_message.layer.cornerRadius = 1.0f;
        lbl_message.layer.borderWidth = 0.2f;
        [_imageView addSubview:lbl_message];
        
        
    }else if ([_selected_device_type isEqualToString:@"th"]) {
        //Set a specific image view
        //_imageView.image = [UIImage imageNamed:@"pairing-thermometer-100-@2x"];
        _imageView.image = [UIImage imageNamed:@"pairing-thermometer_notext-@2x"];
        //Define UILabel for the individual message
        //Universal Bluetooth ON message to be displayed for all the devices
        
        //WEL-373 screen resize
        UILabel *lbl_message;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                                   encoding:NSUTF8StringEncoding];
        //Added iphone4s, ipad 3, ipad Air, ipad Air2
        if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
           || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
           || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
           || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
           || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
           || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"])){
            
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 250, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:17];
            
        } else {
            lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(2, 285, 320, 200)];
            lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        } //End of WEL-373
        
        
        lbl_message.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_message.numberOfLines = 0;
        //lbl_message.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
        lbl_message.textAlignment = NSTextAlignmentCenter;
        lbl_message.textColor = [UIColor whiteColor];
        lbl_message.text = NSLocalizedString(@"Pairing.Thermometer.Message", @"");
        lbl_message.layer.cornerRadius = 1.0f;
        lbl_message.layer.borderWidth = 0.2f;
        [_imageView addSubview:lbl_message];
        
        
    }
    //Universal Bluetooth ON message to be displayed for all the devices
    
    //WEL-373 screen resize
    UILabel *lbl_bton;
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *machinename = [NSString stringWithCString:systemInfo.machine
                                               encoding:NSUTF8StringEncoding];
    //Added iphone4s, ipad 3, ipad Air, ipad Air2
    if(([machinename isEqualToString:@"iPhone4,1"]) || ([machinename isEqualToString:@"iPad3,1"])
       || ([machinename isEqualToString:@"iPad3,2"]) || ([machinename isEqualToString:@"iPad3,3"])
       || ([machinename isEqualToString:@"iPad4,1"]) || ([machinename isEqualToString:@"iPad4,2"])
       || ([machinename isEqualToString:@"iPad4,3"]) || ([machinename isEqualToString:@"iPad5,3"])
       || ([machinename isEqualToString:@"iPad5,4"])|| ([machinename isEqualToString:@"iPad3,4"])
       || ([machinename isEqualToString:@"iPad3,5"])|| ([machinename isEqualToString:@"iPad3,6"])) {
        
        lbl_bton = [[UILabel alloc]initWithFrame:CGRectMake(2, 365, 320, 70)];
    } else {
        lbl_bton = [[UILabel alloc]initWithFrame:CGRectMake(2, 445, 320, 70)];
    } //End of WEL-373
    
    lbl_bton.lineBreakMode = NSLineBreakByWordWrapping; //Added for WELWORLD-94
    lbl_bton.numberOfLines = 0; //Added for WELWORLD-94
    lbl_bton.font = [UIFont fontWithName:@"Roboto-Bold" size:14];
    lbl_bton.textAlignment = NSTextAlignmentLeft;
    lbl_bton.textColor = [UIColor whiteColor];
    lbl_bton.text = NSLocalizedString(@"Pairing.Message", @"");
    lbl_bton.layer.cornerRadius = 1.0f;
    lbl_bton.layer.borderWidth = 0.0f;
    [_imageView addSubview:lbl_bton];
    self.device = [ANDDevice new];
    
    // Do any additional setup after loading the view.
    [self startPairingConnection];
}


//Below methods are to handle the left and right option for the Navigation Controller.
#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDeviceTypemeasurement:(NSString *)data { //Setting the device type selected
    _selected_device_type = data;
}

-(void)startPairingConnection
{
#if TARGET_IPHONE_SIMULATOR
    DLog(@"This is Simulator...");
#else
    //Start the process of device discovery , pairing and connection here.
    self.device = [ANDDevice new];
    [self.device controlSetup];
    self.device.delegate = self;
    if (self.device.activePeripheral.state != CBPeripheralStateConnected) {
        if (self.device.activePeripheral) {
            self.device.peripherials = nil;
        }
        [self.device findBLEPeripherals];
    }
#endif
    
//    @synchronized(self) {
//        if (timer_userGuide && timer_userGuide.isValid) {
//            [timer_userGuide invalidate];
//            timer_userGuide = nil;
//        }
//    }
//    timer_userGuide = [NSTimer scheduledTimerWithTimeInterval:10.0f
//                                                       target:self
//                                                     selector:nil
//                                                     userInfo:nil
//                                                      repeats:NO];

    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTimeDelay * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        NSLock *objLock = [[NSLock alloc] init];
        [objLock lock];
       
        [objLock unlock];
    });
}

//Function to handle the list of received BLE devices
#pragma mark - Device Tracker and Device Connectivity Methods.....
- (void) gotDevice:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData
{
//    if (timer_userGuide.isValid) {
//        [timer_userGuide invalidate];
//        timer_userGuide = nil;
//    }
  
    
    NSLog(@"%@ is a paired device", peripheral.identifier.UUIDString);
    NSLog(@"%@ is a paired device", peripheral.name);
    if (peripheral.name != nil) {
        
        //Perform the check before connecting
        if ([peripheral.name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
        {
            
            _str_weightScaleModelName = peripheral.name;
            NSLog(@"ws");
            _type = @"ws";
            if ([_selected_device_type isEqualToString:_type]) { //If user had clicked on weight Scale
                [self.device deviceTypeConnect:@"ws"];
                [self.device connectPeripheral:peripheral];
            } else {
                [self removeAllPopUpWhileWrongDeivePairing];
                return;
            }
            
        }
        //Perform the check before connecting - Sim transtek
        else if ([peripheral.name rangeOfString:@"3171"].location != NSNotFound)
        {
            
            _str_weightScaleModelName = peripheral.name;
            NSLog(@"transket_ws");
            _type = @"transtek_ws";
            if ([_selected_device_type isEqualToString:_type]) { //If user had clicked on weight Scale
                [self.device deviceTypeConnect:@"transtek_ws"];
                [self.device connectPeripheral:peripheral];
            } else {
                [self removeAllPopUpWhileWrongDeivePairing];
                return;
            }
            
        }
        
        else if ([peripheral.name rangeOfString:@"651"].location != NSNotFound || [peripheral.name rangeOfString:@"BLP"].location != NSNotFound)
        {
            _str_bloodPressureModelName = peripheral.name;
            _type = @"bp";
            if ([_selected_device_type isEqualToString:_type]) { //If user had clicked on Blood Pressure Monitor
                [self.device deviceTypeConnect:@"bp"];
                [self.device connectPeripheral:peripheral];
            } else {
                [self removeAllPopUpWhileWrongDeivePairing];
                return;
            }
            
            
        }
//#ifndef appType //Applicable only for US app - Commented the ifdef for WELWORLD-121
        else if ([peripheral.name rangeOfString:@"Life Trak"].location != NSNotFound) //Sim comment out Activity
//        {   //WEL-287
//            [self.device stopScanning];
//            //Launch the DashBoard screen
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//            id <SlideNavigationContorllerAnimator> revealAnimator;
//            UIViewController *vc ;
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
//            [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//                [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//            }];

            
            return;
            /*
             _str_activityModelName = peripheral.name;
             _type = @"am";
             NSLog(@"list is %@", self.device.peripherials);
             [self.device connectPeripheral:peripheral];*/
        }
//#endif
        else if ([peripheral.name rangeOfString:@"A&D_UT201"].location != NSNotFound) //Sim added code for Thermometer
        {
            _str_thermometerModelName = peripheral.name;
            _type = @"th";
            if ([_selected_device_type isEqualToString:_type]) { //If user had clicked on Thermometer only then attempt to connect to it
                [self.device deviceTypeConnect:@"th"];
                [self.device connectPeripheral:peripheral];
            } else {
                [self removeAllPopUpWhileWrongDeivePairing];
                return;
            }
           
            
        }
        
        //Sim added for UW-302
        else if ([peripheral.name rangeOfString:@"UW-302BLE"].location != NSNotFound)
        {
            NSLog(@"DBG, found the activity tracker 302 with advertising data as %@ ", advertisementData);
            NSData *adv = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
            NSUInteger length = [adv length];
            
            if (length == 2) {
                //Implies that there is not data to send and its pairing case
                _str_activityModelName = peripheral.name;
                _type = @"uw302_ac";
                if ([_selected_device_type isEqualToString:_type]) {
                    [self.device deviceTypeConnect:@"uw302_ac"];
                    [self.device connectPeripheral:peripheral];
                } else {
                    [self removeAllPopUpWhileWrongDeivePairing];
                    return;
                }
                
            } else {
              //  [self removeAllPopUpWhileWrongDeivePairing];
                return;
            }
            
            
        }
        else
        {
            return; //AJAY: in case if device detect another BLE device other than our three devices.... : 16 July 2014
        }
        
     /*   if (_type.length != 0)
        {
            if (![_selected_device_type isEqualToString:_type]) //Case when when we received a different BLE device
            {
                [self removeAllPopUpWhileWrongDeivePairing];
                //SIM TO DO , ADD LOGIC TO GO BACK TO THE DEVICE SETUP SCREEN
                return;
            }
        }*/
        
        //SIM TODO , TRYING TO UNDERSTAND THE NEED OF THIS DISPATCH OPERATION
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
        dispatch_async(queue, ^{
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            
            NSDate *currentDate = [NSDate date];
            NSDateComponents *dc = [calendar components:NSCalendarUnitMinute fromDate:currentDate];
            [dc setMinute:1];
            
            NSDate *endDate = [calendar dateByAddingComponents:dc toDate:currentDate options:0];
          
        });
        
   
        
        //    weight is 352,  bp is 651
    }
//}

//Delegate function of ANDDevice.m
- (void) deviceReadyWithInfoPeripheral:(CBPeripheral *)peripheral
{
    
   /* if ([_type isEqual:@"am"]) //Sim code commented out for activity monitor
    {
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am readHeader];
    }
    else */ if ([_type isEqual:@"bp"])
    {
        ADBloodPressure *bp = [[ADBloodPressure alloc]initWithDevice:self.device];
        [bp readMeasurementForSetup];
        NSLog(@"BP Device");
    }
    else if ([_type isEqual:@"ws"])
    {
        ADWeightScale *ws = [[ADWeightScale alloc]initWithDevice:self.device];
        NSArray *primaryServices = peripheral.services;
        for (CBService *service in primaryServices) {
            CBUUID *uuid = service.UUID;
            NSLog(@"Sim , the uuid is %@", uuid);
            if ([uuid isEqual:[CBUUID UUIDWithString:WeightScaleble_Service]]) {//AND BLE certified weight scale
                
                [ws readMeasurementbleForSetup];
            } else if ([uuid isEqual:[CBUUID UUIDWithString:WeightScale_Service ]]) { //AND Proprietary Weight Scale
                
                [ws readMeasurementForSetup];
            }
            
        }
        //  [ws readMeasurementForSetup]; //Sim code commented for WELWORLD-29
        NSLog(@"WS Device");
    }
    else if ([_type isEqual:@"th"]) //Sim added for Thermometer Profile
    {
        ADThermometer *th = [[ADThermometer alloc]initWithDevice:self.device];
        [th readMeasurementForSetup];
        NSLog(@"Thermometer Device");
    }
    //Sim added for UW-302
    else if ([_type isEqual:@"uw302_ac"]) {
        NSLog(@"DBG, enter case for info ready for tracker");
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am readMeasurementForADTracker];
        
    }    
    //SIM TO DO , UNDERSTAND THE REQUIREMENT OF THIS DISPATCH REQUEST
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(queue, ^{
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        
        NSDate *currentDate = [NSDate date];
        NSDateComponents *dc = [calendar components:NSCalendarUnitMinute fromDate:currentDate];
        [dc setMinute:1];
        
        NSDate *endDate = [calendar dateByAddingComponents:dc toDate:currentDate options:0];
   
    });
    
    
}

//WEL-484, UW-302 Implementation
//Delegate function of ANDDevice.m to disconnect UW-302 activity tracker
- (void) trackerDeviceDisconnect:(CBPeripheral *)peripheral
{
    NSLog(@"DBG, enter the trackerDeviceDisconnect");
    if ([_type isEqual:@"ac"]) {
        NSLog(@"DBG, enter case for info ready for tracker");
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am setDisconnectForADTracker];
        
    }
    
    
}


-(void) deviceReady
{
    NSLog(@"deviceReady called ");
    
    if ([_type isEqual:@"am"])
    {
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am readHeader];
    }else if ([_type isEqual:@"bp"])
    {
        ADBloodPressure *bp = [[ADBloodPressure alloc]initWithDevice:self.device];
        [bp readMeasurement];
    }else if ([_type isEqual:@"ws"])
    {
        
        ADWeightScale *ws = [[ADWeightScale alloc]initWithDevice:self.device];
        /*
         NSArray *primaryServices = peripheral.services;
         for (CBService *service in primaryServices) {
         CBUUID *uuid = service.UUID;
         NSLog(@"Sim , the uuid is %@", uuid);
         if ([uuid isEqual:[CBUUID UUIDWithString:WeightScaleble_Service]]) {//AND BLE certified weight scale
         [ws setTimeble];
         [ws readMeasurementble];
         } else if ([uuid isEqual:[CBUUID UUIDWithString:WeightScale_Service ]]) { //AND Proprietary Weight Scale
         [ws setTime];
         [ws readMeasurement];
         }
         
         }*/
        
        [ws readMeasurement];
    }else if ([_type isEqual:@"th"])
    {
        ADThermometer *th = [[ADThermometer alloc]initWithDevice:self.device];
        [th readMeasurement];
    }
}


//Delegate function called when the characteristics has been updated
- (void) gotDeviceUpdateNotificationWithError:(NSError *)error
{
    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTimeDelay * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        NSLock *objLock = [[NSLock alloc] init];
        [objLock lock];
        [objLock unlock];
    
    });
    
    if (error) {
        
    } else { //Success case , SIM add logic to go to DashBoard screen
        //Display Congratulations and go to Device dashboard
        if (!_created) { //This dialog is created twice since the notification is entered twice, hence adding a check
            _created = true;
            
            //Logic to save the paired device list for the user
            NSLog(@"DBG, entered case of pairing success");
            NSError *err = nil;
            if ([_selected_device_type isEqualToString:@"bp"]) {
                NSLog(@"DBG, the type is bp");
                NSString *status = @"bp-";
                //Adding this password for the database
                //Issue a device disconnect here
                
                [Users addPairedDevices:status
                           withUserName:[[ANDMedCoreDataManager sharedManager].currentUser user_firstName]
                              inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                  error:&err];
            } else if ([_selected_device_type isEqualToString:@"ws"]) {
                NSString *status = @"ws-";
                [Users addPairedDevices:status
                           withUserName:[[ANDMedCoreDataManager sharedManager].currentUser user_firstName]
                              inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                  error:&err];
                
            } else if ([_selected_device_type isEqualToString:@"transtek_ws"]) {
                NSString *status = @"transtek-";
                [Users addPairedDevices:status
                           withUserName:[[ANDMedCoreDataManager sharedManager].currentUser user_firstName]
                              inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                  error:&err];
                
            } else if ([_selected_device_type isEqualToString:@"th"]) {
                NSString *status = @"th-";
                [Users addPairedDevices:status
                           withUserName:[[ANDMedCoreDataManager sharedManager].currentUser user_firstName]
                              inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                  error:&err];
                
            }else if ([_selected_device_type isEqualToString:@"ac"]) {
                NSString *status = @"ac-";
                [Users addPairedDevices:status
                           withUserName:[[ANDMedCoreDataManager sharedManager].currentUser user_firstName]
                              inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                  error:&err];
                
            }else if ([_selected_device_type isEqualToString:@"uw302_ac"]) {
                NSString *status = @"ac-";
                [Users addPairedDevices:status
                           withUserName:[[ANDMedCoreDataManager sharedManager].currentUser user_firstName]
                              inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                  error:&err];
                
            }
            
            
            NSLog(@"the current paired device list is  %@", [[ANDMedCoreDataManager sharedManager].currentUser pairedDevices]);
            
            [self createBackGroundViewtoView];
            [self createWhiteViewPopUpWithButtonTitle: @"OK" withtag:2];//[type isEqualToString:@"bp"] ? 1 : 2
            UILabel *lbl_header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height/3)];
            lbl_header.textAlignment = NSTextAlignmentCenter;
            lbl_header.backgroundColor = [UIColor clearColor];
            lbl_header.textColor = backGroundColor;
            lbl_header.font = [UIFont fontWithName:@"Roboto" size:20];
            lbl_header.numberOfLines = 2;
            lbl_header.text = error ? NSLocalizedString(@"Alert.Error.Title", @"") : NSLocalizedString(@"Device.Congratulations", @""); //Sim localization WELWORLD-19 . @"Error , @"Congratulations...!"
            [view_whiteSettingPopUp addSubview:lbl_header];
            
            UILabel *lbl_middle = [[UILabel alloc] initWithFrame:CGRectMake(10, lbl_header.frame.origin.y+lbl_header.frame.size.height, view_whiteSettingPopUp.frame.size.width-20, view_whiteSettingPopUp.frame.size.height/3)];
            lbl_middle.textAlignment = NSTextAlignmentCenter;
            lbl_middle.backgroundColor = [UIColor clearColor];
            lbl_middle.numberOfLines = 4;
            lbl_middle.font = [UIFont fontWithName:@"Helvetica" size:16];
            //Sim Localization , WELWORLD-19
            // lbl_middle.text = error ? @"Device is not able to pair, please go to bluetooth settings on your phone and tap on forget current device." : @"The device is successfully paired, press OK to continue";
            lbl_middle.text = error ? NSLocalizedString(@"Device.Pairing.MessageFailure", @"") : NSLocalizedString(@"Device.Pairing.MessageSuccess", @"");
            [view_whiteSettingPopUp addSubview:lbl_middle];
            
            [view_background addSubviewWithZoomInAnimation:view_whiteSettingPopUp duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
        }
       
 
    }
    
    
}

-(void)createWhiteViewPopUpWithButtonTitle:(NSString *)title withtag:(int)tag
{
    view_whiteSettingPopUp = [[UIView alloc] init];
    view_whiteSettingPopUp.frame = tag == 1 ? CGRectMake(0, 0, 250, 330): CGRectMake(0, 0, 250, 235);
    
    view_whiteSettingPopUp.backgroundColor = [UIColor whiteColor];
    view_whiteSettingPopUp.layer.cornerRadius = 5;
    view_whiteSettingPopUp.center = view_background.center;
    [view_background addSubview:view_whiteSettingPopUp];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = tag == 1 ? CGRectMake(0, 295, 250, 35) : CGRectMake(0, 200, 250, 35);
    btn.backgroundColor = backGroundColor;
    btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if ([title isEqualToString:@"Cancel"])
    {
        [btn setTitle:title forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_cancel_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([title isEqualToString:@"OK"])
    {
        [btn setTitle:@"OK" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_OK_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [btn setTitle:@"OK" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_error_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [view_whiteSettingPopUp addSubview:btn];
    
}

#pragma mark - Get error while connectivity
-(void)gotErrorWhileConnectivity:(NSError *)error
{
    
//    if (timer_userGuide.isValid) {
//        [timer_userGuide invalidate];
//        timer_userGuide = nil;
//    }
  
   
    [view_background removeFromSuperview];
   
    return;
    UIAlertView *err_alertview = [[UIAlertView alloc] initWithTitle:@"Error !"
                                                            message:[error description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [err_alertview show];
    
}
//Need to map this properly
#pragma mark - Dashboard Button Clicked
- (IBAction)dashBoardButtonAction:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    //id <SlideNavigationContorllerAnimator> revealAnimator;
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
//    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//
//    }];
}



-(void)removeAllPopUpWhileWrongDeivePairing
{
//    @synchronized(self) {
//        if (timer_userGuide.isValid) {
//            [timer_userGuide invalidate];
//        }
//        timer_userGuide = nil;
//    }

  if (!_isAlertViewShown) {
    _isAlertViewShown = true;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Device Pairing Initiated" message:@"Please select the correct Device to pair!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      _isAlertViewShown = false;
     // [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
    
  }
  
 
}

#pragma mark - Pop Up Button Clicked Methods
//Sim - this function handles the OK button click when the success screen is displayed
-(void)btn_OK_clicked:(UIButton *)sender
{

    [view_background removeFromSuperview];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
   // id <SlideNavigationContorllerAnimator> revealAnimator;
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
//    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//    }];
}
//SIM TO DO , DECIDE HOW THE SCREEN NEEDS TO BE DISPLAYED FOR ERROR SCREEN
-(void)btn_error_clicked:(UIButton *)sender
{
    
    [view_background removeFromSuperview];
}

-(void)createBackGroundViewtoView
{
    view_background = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    view_background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.7];
    [window addSubview:view_background];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
