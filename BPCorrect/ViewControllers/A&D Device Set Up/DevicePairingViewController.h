//
//  DevicePairingViewController.h
//  ANDMedical
//
//  Created by simantini bhattacharya on 1/7/15.
//  Copyright (c) 2015 a1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANDBaseController.h"

@interface DevicePairingViewController : ANDBaseController
{
    UIWindow *window;
    
   
    
    UIView *view_whiteSettingPopUp;
    UIView *view_background;
    UIActivityIndicatorView *activityIndicator;
    NSTimer  *timer_userGuide, *timer_lifeTrack;
    __block BOOL isFirstStepComplete;
    __block BOOL isGotDeviceStepComplete;
    __block BOOL isDeviceReadyStepComplete;
    __block BOOL isFourthStepComplete;
    __block BOOL isDeviceUpdateNotificationComplete;
    
    UIScrollView *scroll_lifeTrack;
    
    int lifeTrackNumber;
 

}
//Sim , defining delegate to implement new Device pairing screen


@end
