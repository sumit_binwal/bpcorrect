//
//  DevicePairingViewController.m
//  ANDMedical
//
//  Created by simantini bhattacharya on 1/7/15.
//  Copyright (c) 2015 a1. All rights reserved.
//

#import "DevicePairingViewController.h"
#import "DeviceSetupViewController.h"
#import "ANDDevice.h"
#import "ADActivityMonitor.h"
#import "UIView+Animation.h"
#import "ADBloodPressure.h"
#import "DeviceSetUpCell.h"
#import "ADWeightScale.h"
#import "WeightScale.h"

#import "ADThermometer.h"
#import "ANDBLEDefines.h" //Sim added for WELWORLD-29
#import "DevicePairingStatusViewController.h"

#define kTimeDelay 2

@interface DevicePairingViewController () {
     NSArray *recipeImages;
}
@property NSString *str_activityModelName;
@property NSString *str_bloodPressureModelName;
@property NSString *str_weightScaleModelName;
@property NSString *str_thermometerModelName; //Sim added for Thermometer Profile
@property ANDDevice *device;
@property NSString *type;
@property NSString *display;
@property NSMutableArray *arr_activityMonitor;

@property NSString *device_type;


@end


@implementation DevicePairingViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    window = [[[UIApplication sharedApplication]delegate] window];
    self.navigationController.navigationBarHidden = NO;
    
    
    // Do any additional setup after loading the view.
  recipeImages =  [NSArray arrayWithObjects:@"icon-bpm-cropped", @"icon-scale-cropped",nil];
}

-(void)viewWillAppear:(BOOL)animated
{
   // [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    lifeTrackNumber = 0;
}

//Below methods are to handle the left and right option for the Navigation Controller.
#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu{
    //Added for Cesars new design - to display dashboard shortcut on right
    self.navigationItem.rightBarButtonItem = (UIBarButtonItem *)[CommonClass addingDashBoardButton:self];
    return YES;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#ifdef appType //For Global return 3
    return 4; //Sim this gets modified as and when the number of devices increase
#else  //For US return 4
    return 4; //Sim this gets modified as and when the number of devices increase
#endif
    
}



/***************************************************************************************************/
/* Collection view methods to handle the individual cells **************/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    if (indexPath.row == 0) { //Case for Blood Pressure
        recipeImageView.image = [UIImage imageNamed:@"bmp-device-@2x"];
     
    }
    if (indexPath.row == 1) { //Case for Weight Scale
        recipeImageView.image = [UIImage imageNamed:@"weight-device-@2x"];
    }
#ifdef appType //If its global then the 3rd position is for thermometer
    if (indexPath.row == 2) { //Case for Thermometer
        
        recipeImageView.image = [UIImage imageNamed:@"thermometer-device-@2x"];
      
        
    }
    if (indexPath.row == 3) { //Case for Activity monitor - WELWORLD-121
        
        recipeImageView.image = [UIImage imageNamed:@"activity-device-@2x"];
        
        
    }
#endif
#ifndef appType //If its US then the 3rd position is for activity
    if (indexPath.row == 2) { //Case for Activity monitor
        
        recipeImageView.image = [UIImage imageNamed:@"activity-device-@2x"];
        
        
    }
    if (indexPath.row == 3) {
        recipeImageView.image = [UIImage imageNamed:@"oximeter-device-@2x"];
    }
#endif
    
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    DevicePairingStatusViewController *secondViewController = [segue destinationViewController];
    secondViewController.selected_device_type = _device_type;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    /* Here you can do any code for Selected item at indexpath.*/
    if (indexPath.row == 0) { //Clicked for the Blood Pressure
        NSLog(@"Clicked at the Blood Pressure one");
       _device_type = @"bp";
        [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
        
    } else if (indexPath.row == 1) { //Clicked for the Weight Scale
        NSLog(@"Clicked at the WeightScale");
        _device_type = @"ws";
        [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
    }
#ifdef appType
    //Applicable for global app
    else if (indexPath.row == 2) { //Clicked for Thermometer
        NSLog(@"Clicked at the Thermometer");
        _device_type = @"th";
        [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
    }else if (indexPath.row == 3) { //Clicked for Activity - WELWORLD-121
        NSLog(@"Clicked at the Activity");
        _device_type = @"ac";
        [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
    }
#else
   //WEL-296
    else if (indexPath.row == 2) { //Clicked for Activity
        NSLog(@"Clicked at the Activity");
        _device_type = @"ac";
        [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
    }
#endif
    
        
   
}

/************************************************************************************/ //End of CollectionView methods

//Sim added for Cesar
-(void)btn_dashboard_clicked:(UIButton *)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
   // id <SlideNavigationContorllerAnimator> revealAnimator;
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    NSLog(@"This is entering the dashboad button click");
//    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//
//    }];
}

@end
