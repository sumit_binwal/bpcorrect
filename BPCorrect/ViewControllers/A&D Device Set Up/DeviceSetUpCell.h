//
//  DeviceSetUpCell.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 3/21/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceSetUpCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imgView_device;
@property (weak, nonatomic) IBOutlet UILabel *lbl_deviceName;

@property (weak, nonatomic) IBOutlet UILabel *lbl_modelName;
@property (weak, nonatomic) IBOutlet UITextView *txtView_description;

@property (weak, nonatomic) IBOutlet UIButton *btn_cell;

@end
