//
//  DeviceSetUpCell.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 3/21/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "DeviceSetUpCell.h"

@implementation DeviceSetUpCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib
{
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
