//
//  DeviceSetUpCellCoolDesignHeader.h
//  ANDMedical
//
//  Created by simantini bhattacharya on 12/10/15.
//  Copyright © 2015 a1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceSetUpCellCoolDesignHeader : UITableViewCell
{
    

}

@property (weak, nonatomic) IBOutlet UIImageView *imgView_deviceHeader;
@property (weak, nonatomic) IBOutlet UILabel *lbl_deviceNameHeader;

@end
