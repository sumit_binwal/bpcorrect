//
//  DeviceSetupViewController.h
//  ANDMedical
//
//  Created by Ajay Chaudhary on 3/4/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANDBaseController.h"

@interface DeviceSetupViewController : ANDBaseController
{
    UIWindow *window;
    


    UIView *view_whiteSettingPopUp;
    UIView *view_background;
    UIActivityIndicatorView *activityIndicator;
    NSTimer  *timer_userGuide, *timer_lifeTrack;
    __weak IBOutlet UITableView *deviceSetupTableView;
    
    __block BOOL isFirstStepComplete;
    __block BOOL isGotDeviceStepComplete;
    __block BOOL isDeviceReadyStepComplete;
    __block BOOL isFourthStepComplete;
    __block BOOL isDeviceUpdateNotificationComplete;
    
    UIScrollView *scroll_lifeTrack;
    
    int lifeTrackNumber;

    
}

//@property (nonatomic, retain)UINavigationController *tempNavi;
@end
