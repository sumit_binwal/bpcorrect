//
//  DeviceSetupViewController.m
//  ANDMedical
//
//  Created by Ajay Chaudhary on 3/4/14.
//  Copyright (c) 2014 a1. All rights reserved.
//

#import "DeviceSetupViewController.h"
#import "ANDDevice.h"
#import "ADActivityMonitor.h"
#import "UIView+Animation.h"
#import "ADBloodPressure.h"
#import "DeviceSetUpCell.h"
#import "ADWeightScale.h"
#import "WeightScale.h"
#import "ADThermometer.h"
#import "ANDBLEDefines.h" //Sim added for WELWORLD-29

#define kTimeDelay 2

@interface DeviceSetupViewController ()<ANDDeviceDelegate>

@property NSString *str_activityModelName;
@property NSString *str_bloodPressureModelName;
@property NSString *str_weightScaleModelName;
@property NSString *str_thermometerModelName; //Sim added for Thermometer Profile
@property ANDDevice *device;
@property NSString *type;
@property NSString *display;
@property NSMutableArray *arr_activityMonitor;

@property NSString *selected_device_type;
@end

@implementation DeviceSetupViewController
UIImageView *imgView_popup;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    window = [[[UIApplication sharedApplication]delegate] window];
    deviceSetupTableView.backgroundColor = [UIColor clearColor];
    
    //self.navigationController.navigationBarHidden = NO;
    
  
}


-(void)viewWillAppear:(BOOL)animated
{
    //[SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    lifeTrackNumber = 0;
}


#pragma mark - Device Tracker and Device Connectivity Methods.....
- (void) gotDevice:(CBPeripheral *)peripheral
{
    if (timer_userGuide.isValid) {
        [timer_userGuide invalidate];
        timer_userGuide = nil;
    }
    
    
    NSLog(@"%@ is a paired device", peripheral.identifier.UUIDString);
    NSLog(@"%@ is a paired device", peripheral.name);
    if (peripheral.name != nil) {
        
        
        if ([peripheral.name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
       {
           
            _str_weightScaleModelName = peripheral.name;
            NSLog(@"ws");
            _type = @"ws";
            [self.device connectPeripheral:peripheral];
        }
        else if ([peripheral.name rangeOfString:@"651"].location != NSNotFound || [peripheral.name rangeOfString:@"BLP"].location != NSNotFound)
        {
            _str_bloodPressureModelName = peripheral.name;
            _type = @"bp";
            [self.device connectPeripheral:peripheral];
            
        }
        else if ([peripheral.name rangeOfString:@"Life Trak"].location != NSNotFound)
        {
            [self.device stopScanning];
            return;
        }
        else if ([peripheral.name rangeOfString:@"A&D_UT201"].location != NSNotFound) //Sim added code for Thermometer
        {
            _str_thermometerModelName = peripheral.name;
            _type = @"th";
            [self.device connectPeripheral:peripheral];
            
        }
        else
        {
            return; //AJAY: in case if device detect another BLE device other than our three devices.... : 16 July 2014
        }
        
        if (_type.length != 0)
        {
            if (![_selected_device_type isEqualToString:_type])
            {
                [self removeAllPopUpWhileWrongDeivePairing];
                return;
            }
        }
        
       
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
        dispatch_async(queue, ^{
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            
            NSDate *currentDate = [NSDate date];
            NSDateComponents *dc = [calendar components:NSCalendarUnitMinute fromDate:currentDate];
            [dc setMinute:1];
            
            NSDate *endDate = [calendar dateByAddingComponents:dc toDate:currentDate options:0];
            
            while (!isFirstStepComplete) {
                
                if ([currentDate compare:endDate] != NSOrderedAscending)
                {
                    break;
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (isFirstStepComplete) {
                    
                    [activityIndicator stopAnimating];
                    [view_whiteSettingPopUp removeWithZoomOutAnimation:0.5 option:UIViewAnimationOptionCurveEaseIn];
                    [self methodForSecondPopUpWithDeviceName:_type];
                }
            });
            
        });
        
        [deviceSetupTableView reloadData];
    }
}




- (void) deviceReadyWithInfoPeripheral:(CBPeripheral *)peripheral
{
    
    if ([_type isEqual:@"am"])
    {
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am readHeader];
    }
    else if ([_type isEqual:@"bp"])
    {
        ADBloodPressure *bp = [[ADBloodPressure alloc]initWithDevice:self.device];
        [bp readMeasurementForSetup];
        NSLog(@"BP Device");
    }
    else if ([_type isEqual:@"ws"])
    {
        ADWeightScale *ws = [[ADWeightScale alloc]initWithDevice:self.device];
        NSArray *primaryServices = peripheral.services;
        for (CBService *service in primaryServices) {
            CBUUID *uuid = service.UUID;
            NSLog(@"Sim , the uuid is %@", uuid);
            if ([uuid isEqual:[CBUUID UUIDWithString:WeightScaleble_Service]]) {//AND BLE certified weight scale
                [ws readMeasurementbleForSetup];
            } else if ([uuid isEqual:[CBUUID UUIDWithString:WeightScale_Service ]]) { //AND Proprietary Weight Scale
                [ws readMeasurementForSetup];
            }
            
        }
      //  [ws readMeasurementForSetup]; //Sim code commented for WELWORLD-29
        NSLog(@"WS Device");
    }
    else if ([_type isEqual:@"th"]) //Sim added for Thermometer Profile
    {
        ADThermometer *th = [[ADThermometer alloc]initWithDevice:self.device];
        [th readMeasurementForSetup];
        NSLog(@"Thermometer Device");
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(queue, ^{
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        
        NSDate *currentDate = [NSDate date];
        NSDateComponents *dc = [calendar components:NSCalendarUnitMinute fromDate:currentDate];
        [dc setMinute:1];
        
        NSDate *endDate = [calendar dateByAddingComponents:dc toDate:currentDate options:0];
        
        while (!isGotDeviceStepComplete) {
            
            if ([currentDate compare:endDate] != NSOrderedAscending)
            {
                break;
            }
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isGotDeviceStepComplete) {
                
                [activityIndicator stopAnimating];
                [view_whiteSettingPopUp removeWithZoomOutAnimation:0.5 option:UIViewAnimationOptionCurveEaseIn];
                [self methodForThirdPopUpWithDeviceName:_type andModelName:peripheral.name];
            }
        });
        
    });
    
    
}


-(void) deviceReady
{
    NSLog(@"deviceReady called ");
    
    [activityIndicator stopAnimating];
    [view_whiteSettingPopUp removeWithZoomOutAnimation:0.5 option:UIViewAnimationOptionCurveEaseIn];
    
    if ([_type isEqual:@"am"])
    {
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am readHeader];
    }else if ([_type isEqual:@"bp"])
    {
        ADBloodPressure *bp = [[ADBloodPressure alloc]initWithDevice:self.device];
        [bp readMeasurement];
    }else if ([_type isEqual:@"ws"])
    {
        
        ADWeightScale *ws = [[ADWeightScale alloc]initWithDevice:self.device];

        [ws readMeasurement];
    }else if ([_type isEqual:@"th"])
    {
        ADThermometer *th = [[ADThermometer alloc]initWithDevice:self.device];
        [th readMeasurement];
    }
}


- (void) gotDeviceUpdateNotificationWithError:(NSError *)error
{
    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTimeDelay * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        NSLock *objLock = [[NSLock alloc] init];
        [objLock lock];
        isDeviceUpdateNotificationComplete = YES;
        [objLock unlock];
        
        [activityIndicator stopAnimating];
        [view_whiteSettingPopUp removeWithZoomOutAnimation:0.5 option:UIViewAnimationOptionCurveEaseIn];
        [self methodForFourthPopUpWithDeviceName:_type andError:error];
    });
    
    
}

#pragma mark - Got Activity Monitor  Data
- (void) gotActivityWithData:(NSData *)data forPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"gotActivity");
    [self.device.CM cancelPeripheralConnection:self.device.activePeripheral];
    [self.device findBLEPeripherals];
    
    //need to parse this group of data...
    [self parseActivity:data forPeripheral:peripheral];
}

- (void) parseActivity:(NSData *)data forPeripheral:(CBPeripheral *)peripheral
{
    NSMutableData *myData = [[NSMutableData alloc] initWithData:data];
    
    //remove first 2 bytes of datalength
    NSLog(@"myData is %@", myData);
    [myData replaceBytesInRange:NSMakeRange(0, 2) withBytes:NULL length:0];
    NSLog(@"myData without dataLength %@", myData);
    
    //goes into a while that delete everyday data
    //each day has 24 bytes
    self.arr_activityMonitor = [[NSMutableArray alloc]init];
    while (myData.length > 24) {
        NSData *day = [myData subdataWithRange:NSMakeRange(0, 24)];
        [self parseDayData:day forIdentifier:peripheral];
        [myData replaceBytesInRange:NSMakeRange(0, 24) withBytes:NULL length:0];
    }
    
    NSError *error = nil;
    [ActivityTracker insertORUpdateActivitiesRecords:self.arr_activityMonitor
                                             forUser:[ANDMedCoreDataManager sharedManager].currentUser
                                          forContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                               error:&error];
    
    if (error) {
        [CommonClass showAlertForError:error];
        return;
    }
    
    
    ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
    am.uuid = peripheral.identifier.UUIDString;
    [am endConnection];
}


#pragma mark - Parse Data For Activity Monitor
- (void) parseDayData:(NSData *)today forIdentifier:(CBPeripheral *)perfipheral
{
    //  da mo ye se mn hr se mn hr bi steps    dist     calorie  sleep
    //  06 01 72 00 00 00 3b 3b 17 02 9d100000 19010000 e9530000 aa01
    //  1  1  1  1  1  1  1  1  1  1  4        4        4        2      = 24
    //get all data then save to database...
    //  int year = *(int *)[[date subdataWithRange:NSMakeRange(0, 2)] bytes];
    int day = *(int *)[[today subdataWithRange:NSMakeRange(0, 1)] bytes];
    int month = *(int *)[[today subdataWithRange:NSMakeRange(1, 1)] bytes];
    int year = *(int *)[[today subdataWithRange:NSMakeRange(2, 1)] bytes];
    int startSecond = *(int *)[[today subdataWithRange:NSMakeRange(3, 1)] bytes];
    int startMinute = *(int *)[[today subdataWithRange:NSMakeRange(4, 1)] bytes];
    int startHour = *(int *)[[today subdataWithRange:NSMakeRange(5, 1)] bytes];
    int endSecond = *(int *)[[today subdataWithRange:NSMakeRange(6, 1)] bytes];
    int endMinutes = *(int *)[[today subdataWithRange:NSMakeRange(7, 1)] bytes];
    int endHour = *(int *)[[today subdataWithRange:NSMakeRange(8, 1)] bytes];
    int blockIndex = *(int *)[[today subdataWithRange:NSMakeRange(9, 1)] bytes];
    int steps = *(int *)[[today subdataWithRange:NSMakeRange(10, 4)] bytes];
    int distances = *(int *)[[today subdataWithRange:NSMakeRange(14, 4)] bytes];
    int calories = *(int *)[[today subdataWithRange:NSMakeRange(18, 4)] bytes];
    int sleep = *(int *)[[today subdataWithRange:NSMakeRange(22, 2)] bytes];
    
    NSNumber *date = [NSNumber numberWithInteger:[[NSString stringWithFormat:@"%d%d%d", day, month, year] integerValue]];
    NSString *startTime = [NSString stringWithFormat:@"%d:%d:%d", startHour, startMinute, startSecond];
    NSString *endTime = [NSString stringWithFormat:@"%d:%d:%d", endHour, endMinutes, endSecond];
    NSString *dateString = [NSString stringWithFormat:@"%d/%02d/%02d",year,month,day];
    
    //First Convert the dateString to logical date object but it may give hours, min, second fields as 00:00:00
    NSDate *measurementDateTime = [ANDDateManager convertLifeTrackWatchDateToLogicalDateObject:dateString];
    
    //Next task is to get the hour/Min/Seconds from current NSDate->date and assign to captured date.
    NSCalendarUnit unit = NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    
    NSDateComponents *component = [[NSCalendar currentCalendar] components:unit fromDate:measurementDateTime];
    NSDateComponents *componentCurrentDate = [[NSCalendar currentCalendar] components:unit fromDate:[NSDate date]];
    
    //Setting hour/Minute/seconds value from NSDate->date component to captured date component.
    [component setHour:componentCurrentDate.hour];
    [component setMinute:componentCurrentDate.minute];
    [component setSecond:componentCurrentDate.second];
    
    //Getting back the modified date.
    measurementDateTime = [[NSCalendar currentCalendar] dateFromComponents:component];
    
    
    ADActivityMonitor  *activityMonitor_obj = nil;
    activityMonitor_obj = [[ADActivityMonitor alloc] initWithDate:date
                                                        startTime:startTime
                                                          endTime:endTime
                                                            steps:[[NSNumber alloc]initWithInt:steps]
                                                        distances:[[NSNumber alloc] initWithInt:distances]
                                                         calories:[[NSNumber alloc] initWithDouble:calories/10]
                                                            sleep:[[NSNumber alloc] initWithInt:sleep]
                                                              bpm:[[NSNumber alloc] initWithInt:0]];
    
    activityMonitor_obj.uuid = perfipheral.identifier.UUIDString;
    activityMonitor_obj.dateString = dateString;
    activityMonitor_obj.deviceType = [CommonClass getDeviceTypeForPeripheral:perfipheral.name];
    activityMonitor_obj.startTimeInterval = startTime;
    activityMonitor_obj.endTimeInterval = endTime;
    activityMonitor_obj.measurementReceivedDateTime = measurementDateTime;
    
    [self.arr_activityMonitor addObject:activityMonitor_obj];
    
    NSLog(@"Sleep: %@",activityMonitor_obj.sleep);
    NSLog(@"Steps: %@",activityMonitor_obj.steps);
    NSLog(@"Calories: %@",activityMonitor_obj.calories);
    NSLog(@"Distance: %@",activityMonitor_obj.distances);
    NSLog(@"Date: %@",activityMonitor_obj.date);
}


#pragma mark - Got Blood Pressure Data
-(void) gotBloodPressure:(NSDictionary *)data
{
    NSLog(@"%@",data);
}

#pragma mark - Got Thermometer Data //Sim added for Thermometer
-(void) gotThermometer:(NSDictionary *)data
{
    NSLog(@"%@",data);
}

#pragma mark - Got Weight Scale Data
- (void)gotWeight:(NSDictionary *)data
{
    NSLog(@"GOT WEIGHT DATA: %@",data);
    
    NSMutableArray *arrayWS = [[NSMutableArray alloc] init];
    [arrayWS addObject:data];
    
    @try
    {

        NSError *error = nil;
        [WeightScale insertORUpdateWeightScaleRecords:arrayWS
                                              forUser:[ANDMedCoreDataManager sharedManager].currentUser
                                           forContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                                error:&error];
        
        
        if (error)
        {
            [CommonClass showAlertForError:error];
            return;
        }
        [[ANDMedCoreDataManager sharedManager] refreshDataManagerWithUserWeightScaleInfo:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (error) {
                    [CommonClass showAlertForError:error];
                    return;
                }
            });
        }];
        
    }
    @catch (NSException *exception)
    {
        NSError *error = [exception exceptionError];
        [CommonClass showAlertForError:error];
    }
    @finally
    {
    }
    
}


#pragma mark - Get error while connectivity
-(void)gotErrorWhileConnectivity:(NSError *)error
{
    
    if (timer_userGuide.isValid) {
        [timer_userGuide invalidate];
        timer_userGuide = nil;
    }
    
    [activityIndicator removeFromSuperview];
    [view_background removeFromSuperview];
    [view_whiteSettingPopUp removeFromSuperview];
    return;
    UIAlertView *err_alertview = [[UIAlertView alloc] initWithTitle:@"Error !"
                                                            message:[error description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [err_alertview show];
    
}


#pragma mark - Dashboard Button Clicked
- (IBAction)dashBoardButtonAction:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    //id <SlideNavigationContorllerAnimator> revealAnimator;
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    
//    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//
//    }];
}


#pragma mark - SlideNavigationController Methods
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return YES;
}


- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
	return YES;
}



#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,5.0f)];
    tempView.backgroundColor=[UIColor whiteColor];
    return tempView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4; //Sim added for Thermometer
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cell_identifier = @"DeviceSetUpCell";
    
    DeviceSetUpCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    if (cell == nil)
    {
        cell = [[DeviceSetUpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
        
    }
    
    if (indexPath.section == 0)
    {
        
        cell.lbl_deviceName.text = NSLocalizedString(@"Device.BloodPressure.Name", @""); //Sim changed for localization
        cell.lbl_modelName.text = _str_bloodPressureModelName;
        cell.btn_cell.tag = indexPath.section;
        [cell.imgView_device setImage:[UIImage imageNamed:@"icon_bp"]];

    }
    else if (indexPath.section == 1)
    {
        cell.lbl_deviceName.text = NSLocalizedString(@"Device.Weight.Name", @""); //Sim changed for localization
        cell.lbl_modelName.text = _str_weightScaleModelName;
        [cell.imgView_device setImage:[UIImage imageNamed:@"icon_ws"]];
        cell.btn_cell.tag = indexPath.section;
       
    }
    else if (indexPath.section == 2) //Sim code added for Thermometer
    {
        cell.lbl_deviceName.text = @"Thermometer";
        cell.lbl_modelName.text = _str_thermometerModelName;
        [cell.imgView_device setImage:[UIImage imageNamed:@"th_device"]]; //Sim modified for WELWORLD-26
        cell.btn_cell.tag = indexPath.section;
        
    }
    else if (indexPath.section == 3)
    {
        cell.lbl_deviceName.text = NSLocalizedString(@"Device.Activity.Name", @""); //Sim changed for localization
        cell.lbl_modelName.text = _str_activityModelName;
        cell.btn_cell.tag = indexPath.section;
        [cell.imgView_device setImage:[UIImage imageNamed:@"icon_ac"]];
        
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.borderWidth = 1.0;
    cell.layer.cornerRadius = 2.0;
    cell.layer.borderColor = [[UIColor grayColor]CGColor];
    
    return cell;
    
}



#pragma mark - Pop Up Methods to show connectivity
-(void)addActivityIndicatorToView:(UIView *)view
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    //activityIndicator.alpha = 1.0;
    activityIndicator.center = CGPointMake(view.frame.size.width/2, view.frame.size.height/6);
    activityIndicator.hidesWhenStopped = YES;
    [view addSubview:activityIndicator];
    [CommonClass popOutView:activityIndicator withAnimation:1 withDelay:0];
    [activityIndicator startAnimating];
}


#pragma mark - Cell Button Clicked
- (IBAction)btn_cell_clicked:(UIButton *)sender
{
    if (sender.tag == 0)
        _selected_device_type = @"bp";
    else if (sender.tag == 1)
        _selected_device_type = @"ws";
    else if (sender.tag == 2)
        _selected_device_type = @"th"; //Sim added for thermometer*/

    
    [self createBackGroundViewtoView];
    
  //  if (sender.tag != 0) { Sim modified for world wide version --> rearranging device display
    
      if (sender.tag != 3) {
        
#if TARGET_IPHONE_SIMULATOR
        DLog(@"This is Simulator...");
#else
        
        isFirstStepComplete = NO;
        isGotDeviceStepComplete = NO;
        isDeviceReadyStepComplete = NO;
        
        self.device = [ANDDevice new];
        [self.device controlSetup];
        self.device.delegate = self;
        if (self.device.activePeripheral.state != CBPeripheralStateConnected) {
            if (self.device.activePeripheral) {
                self.device.peripherials = nil;
            }
            [self.device findBLEPeripherals];
        }
#endif
        
        
        @synchronized(self) {
            if (timer_userGuide && timer_userGuide.isValid) {
                [timer_userGuide invalidate];
                timer_userGuide = nil;
            }
        }
        
        timer_userGuide = [NSTimer scheduledTimerWithTimeInterval:30.0f
                                                           target:self
                                                         selector:@selector(removeAllPopUp)
                                                         userInfo:nil
                                                          repeats:NO];
        
        
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:sender.tag];
        
        [view_background addSubviewWithZoomInAnimation:view_whiteSettingPopUp duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
        
        [self addActivityIndicatorToView:view_background];
        
        
    }
    //Sim modified sender.tag for device display list prioritization
    if (sender.tag == 3)
    {
        
        [self createScrollViewForLifeTrackTutorial];
        
        return;
        
    }
    else if (sender.tag == 0)
    {
        imgView_popup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height-50)];
        imgView_popup.backgroundColor = [UIColor clearColor];
        imgView_popup.image = [UIImage imageNamed:@"bp_first.png"];
        [imgView_popup setContentMode:UIViewContentModeScaleAspectFit];
        [view_whiteSettingPopUp addSubview:imgView_popup];
    }
    else if (sender.tag == 1)
    {
        imgView_popup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height-40)];
        imgView_popup.image = [UIImage imageNamed:@"ws_first.png"];
        [imgView_popup setContentMode:UIViewContentModeScaleAspectFit];
        [view_whiteSettingPopUp addSubview:imgView_popup];
    }
    else if (sender.tag == 2) //Sim added for Thermometer
    {
        imgView_popup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height-50)];
        imgView_popup.backgroundColor = [UIColor clearColor];
        imgView_popup.image = [UIImage imageNamed:@"bp_first.png"];
        [imgView_popup setContentMode:UIViewContentModeScaleAspectFit];
        [view_whiteSettingPopUp addSubview:imgView_popup];
        
    }
    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTimeDelay * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        NSLock *objLock = [[NSLock alloc] init];
        [objLock lock];
        isFirstStepComplete = YES;
        [objLock unlock];
    });
    
}

#pragma mark - Remove all popup message if device is not able to connect
-(void)removeAllPopUp
{
    @synchronized(self) {
        if (timer_userGuide.isValid) {
            [timer_userGuide invalidate];
        }
        timer_userGuide = nil;
    }
    
    [activityIndicator removeFromSuperview];
    [view_whiteSettingPopUp removeFromSuperview];
    [view_background removeFromSuperview];
    
    UIAlertView *err_alertview = [[UIAlertView alloc] initWithTitle:@"Sorry !" message:@"Unable to set up device, please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [err_alertview show];
}


-(void)removeAllPopUpWhileWrongDeivePairing
{
    @synchronized(self) {
        if (timer_userGuide.isValid) {
            [timer_userGuide invalidate];
        }
        timer_userGuide = nil;
    }
    
    [activityIndicator removeFromSuperview];
    [view_whiteSettingPopUp removeFromSuperview];
    [view_background removeFromSuperview];
    
    UIAlertView *err_alertview = [[UIAlertView alloc] initWithTitle:@"Device Pairing Initiated" message:@"Please select the correct Device to pair!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [err_alertview show];
}


#pragma mark - Connectivity Popup Message...

-(void)methodForSecondPopUpWithDeviceName:(NSString *)type
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
    [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:[type isEqualToString:@"bp"] ? 1 : 2];
    
    [activityIndicator startAnimating];
    
    [imgView_popup removeFromSuperview];
    imgView_popup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height-50)];
    [view_whiteSettingPopUp addSubview:imgView_popup];
    
    
    
    if ([type isEqualToString:@"bp"])
        imgView_popup.image = [UIImage imageNamed:@"bp_pair.png"];
    else if ([type isEqualToString:@"ws"])
        imgView_popup.image = [UIImage imageNamed:@"ws_pair.png"];
    else if ([type isEqualToString:@"th"]) //Sim added for Thermometer
        imgView_popup.image = [UIImage imageNamed:@"bp_pair.png"];
    
    
    [view_background addSubviewWithZoomInAnimation:view_whiteSettingPopUp duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTimeDelay * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        NSLock *objLock = [[NSLock alloc] init];
        [objLock lock];
        isGotDeviceStepComplete = YES;
        [objLock unlock];
    });
    
}

-(void)methodForThirdPopUpWithDeviceName:(NSString *)type andModelName:(NSString *)name
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
      //Sim commented for thermometer
  //  [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:[type isEqualToString:@"bp"] ? 1 : 2];
    
    /* Sim added for thermometer */
    if ([type isEqualToString:@"bp"])
    {
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:1];
    } else if ([type isEqualToString:@"ws"])
    {
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:2];
    }else if ([type isEqualToString:@"th"])
    {
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:3];
    }
        
    
    [activityIndicator startAnimating];
    
    [imgView_popup removeFromSuperview];
    imgView_popup = nil;
    imgView_popup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height-50)];
    [view_whiteSettingPopUp addSubview:imgView_popup];
    
    
    if ([type isEqualToString:@"bp"])
        imgView_popup.image = [UIImage imageNamed:@"bp_see.png"];
    else if ([type isEqualToString:@"th"]) //Sim added code for Thermometer
        imgView_popup.image = [UIImage imageNamed:@"bp_see.png"];
    else
        imgView_popup.image = [UIImage imageNamed:@"ws_pairBT.png"];
    
    
    [view_background addSubviewWithZoomInAnimation:view_whiteSettingPopUp duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
    
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(queue, ^{
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        
        NSDate *currentDate = [NSDate date];
        NSDateComponents *dc = [calendar components:NSCalendarUnitMinute fromDate:currentDate];
        [dc setMinute:1];
        
        NSDate *endDate = [calendar dateByAddingComponents:dc toDate:currentDate options:0];
        
        while (!isDeviceUpdateNotificationComplete) {
            
            if ([currentDate compare:endDate] != NSOrderedAscending)
            {
                break;
            }
            
        }
        
        
    });
    
}


-(void)methodForFourthPopUpWithDeviceName:(NSString *)type andError:(NSError *)error
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    
  //  [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:[type isEqualToString:@"bp"] ? 1 : 2]; //Sim commented for thermometer
    
    //Sim added for Thermometer
    if ([type isEqualToString:@"bp"])
    {
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:1];
    } else if ([type isEqualToString:@"ws"])
    {
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:2];
    }else if ([type isEqualToString:@"th"])
    {
        [self createWhiteViewPopUpWithButtonTitle:@"Cancel" withtag:3];
    }

    
    [activityIndicator startAnimating];
    
    [imgView_popup removeFromSuperview];
    imgView_popup = nil;
    imgView_popup = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height-50)];
    [view_whiteSettingPopUp addSubview:imgView_popup];
    
    
    if ([type isEqualToString:@"bp"])
        imgView_popup.image = error ? [UIImage imageNamed:@"bp_err10.png"]: [UIImage imageNamed:@"bp_end.png"];
    else if ([type isEqualToString:@"th"]) //Sim added for Thermometer
        imgView_popup.image = error ? [UIImage imageNamed:@"bp_err10.png"]: [UIImage imageNamed:@"bp_end.png"];
    else
        imgView_popup.image = error ? [UIImage imageNamed:@"ws_err10.png"] : [UIImage imageNamed:@"ws_end.png"];
    
    
    [view_background addSubviewWithZoomInAnimation:view_whiteSettingPopUp duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
    
    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTimeDelay * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        NSLock *objLock = [[NSLock alloc] init];
        [objLock lock];
        isFourthStepComplete = YES;
        [objLock unlock];
        
        [activityIndicator stopAnimating];
        [view_whiteSettingPopUp removeWithZoomOutAnimation:0.5 option:UIViewAnimationOptionCurveEaseIn];
        [self methodForFinalPopUpWithDeviceName:_type andError:error];
    });
    
    
}

-(void)methodForFinalPopUpWithDeviceName:(NSString *)type andError:(NSError *)error
{
    DLog(@"%s",__PRETTY_FUNCTION__);
    

    [self createWhiteViewPopUpWithButtonTitle:error ? @"Error" : @"OK" withtag:2];//[type isEqualToString:@"bp"] ? 1 : 2
    
    [activityIndicator stopAnimating];
    
    [imgView_popup removeFromSuperview];
    imgView_popup = nil;
    
    UILabel *lbl_header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view_whiteSettingPopUp.frame.size.width, view_whiteSettingPopUp.frame.size.height/3)];
    lbl_header.textAlignment = NSTextAlignmentCenter;
    lbl_header.backgroundColor = [UIColor clearColor];
    lbl_header.textColor = backGroundColor;
    lbl_header.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    lbl_header.numberOfLines = 2;
    //lbl_header.text = error ? @"Error !" : @"Congratulations...!";
    lbl_header.text = error ? NSLocalizedString(@"Alert.Error.Title", @"") : NSLocalizedString(@"Device.Congratulations", @""); //Sim localization WELWORLD-19 . @"Error , @"Congratulations...!"
    [view_whiteSettingPopUp addSubview:lbl_header];
    
    UILabel *lbl_middle = [[UILabel alloc] initWithFrame:CGRectMake(10, lbl_header.frame.origin.y+lbl_header.frame.size.height, view_whiteSettingPopUp.frame.size.width-20, view_whiteSettingPopUp.frame.size.height/3)];
    lbl_middle.textAlignment = NSTextAlignmentCenter;
    lbl_middle.backgroundColor = [UIColor clearColor];
    lbl_middle.numberOfLines = 4;
    lbl_middle.font = [UIFont fontWithName:@"Helvetica" size:16];
    //Sim Localization , WELWORLD-19
   // lbl_middle.text = error ? @"Device is not able to pair, please go to bluetooth settings on your phone and tap on forget current device." : @"The device is successfully paired, press OK to continue";
    lbl_middle.text = error ? NSLocalizedString(@"Device.Pairing.MessageFailure", @"") : NSLocalizedString(@"Device.Pairing.MessageSuccess", @"");
    [view_whiteSettingPopUp addSubview:lbl_middle];
    
    [view_background addSubviewWithZoomInAnimation:view_whiteSettingPopUp duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
}


#pragma mark - Pop Up Button Clicked Methods
-(void)btn_OK_clicked:(UIButton *)sender
{
    
    [activityIndicator removeFromSuperview];
    [view_whiteSettingPopUp removeFromSuperview];
    [view_background removeFromSuperview];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
  //  id <SlideNavigationContorllerAnimator> revealAnimator;
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
  
  
    
//    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//    }];
}


-(void)btn_error_clicked:(UIButton *)sender
{
    [activityIndicator removeFromSuperview];
    [view_whiteSettingPopUp removeFromSuperview];
    [view_background removeFromSuperview];
}


-(void)btn_cancel_clicked:(UIButton *)sender
{
    @synchronized(self) {
        if (timer_lifeTrack.isValid) {
            [timer_lifeTrack invalidate];
            
        }
        timer_lifeTrack = nil;
    }
    
    @synchronized(self) {
        if (timer_userGuide.isValid) {
            [timer_userGuide invalidate];
        }
        timer_userGuide = nil;
    }
    [scroll_lifeTrack removeFromSuperview];
    scroll_lifeTrack = nil;
    
    
    [view_whiteSettingPopUp removeWithZoomOutAnimation:0.5 option:UIViewAnimationOptionCurveEaseIn];
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         view_background.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [view_background removeFromSuperview];
                         view_background = nil;
                         [activityIndicator removeFromSuperview];
                     }];
    
    if (self.device) {
        [self.device stopScanning];
        self.device.delegate = nil;
        self.device = nil;
    }
}



#pragma mark - Create White PopUp View For Connectivity Alert Message

-(void)createScrollViewForLifeTrackTutorial
{
    lifeTrackNumber = 0;
    @synchronized(self) {
        if (timer_lifeTrack.isValid) {
            [timer_lifeTrack invalidate];
        }
        timer_lifeTrack = nil;
    }
    
    timer_lifeTrack = [NSTimer scheduledTimerWithTimeInterval:4.0f
                                                       target:self
                                                     selector:@selector(slideLifeTrackImages:)
                                                     userInfo:nil
                                                      repeats:YES];
    
    scroll_lifeTrack = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 330)];
    scroll_lifeTrack.scrollEnabled = NO;
    scroll_lifeTrack.backgroundColor = [UIColor clearColor];
    scroll_lifeTrack.pagingEnabled = YES;
    scroll_lifeTrack.showsHorizontalScrollIndicator = NO;
    scroll_lifeTrack.showsVerticalScrollIndicator = NO;
    scroll_lifeTrack.center = view_background.center;
    scroll_lifeTrack.contentSize = CGSizeMake(320*6, 280);
    
    for (int i=0; i<6; i++)
    {
        UIView *bView = [[UIView alloc] initWithFrame:CGRectMake(i*250+70*i+35, 15, 250, 315)];
        bView.layer.cornerRadius = 5;
        bView.backgroundColor = [UIColor whiteColor];
        [scroll_lifeTrack addSubview:bView];
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 250, 280)];
        img.backgroundColor = [UIColor clearColor];
        img.image = [UIImage imageNamed:[NSString stringWithFormat:@"life_track_%d.png",i+1]];
        img.userInteractionEnabled = YES;
        [bView addSubview:img];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 280, 250, 35);
        btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        btn.backgroundColor = backGroundColor;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       // [btn setTitle:@"Cancel" forState:UIControlStateNormal];
        [btn setTitle:@"Cancel" forState:UIControlStateNormal]; //Sim localization WELWORLD-19  , @"Cancel"
        [btn addTarget:self action:@selector(btn_cancel_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [bView addSubview:btn];
    }
    
    [view_background addSubview:scroll_lifeTrack];
    
    [view_background addSubviewWithZoomInAnimation:scroll_lifeTrack duration:0.5 option:UIViewAnimationOptionCurveEaseOut];
    
    
}


-(void) slideLifeTrackImages:(NSTimer *)timer
{
    lifeTrackNumber ++;
    if (lifeTrackNumber<6)
    {
        CGRect frm = CGRectMake(lifeTrackNumber*260+60*lifeTrackNumber, scroll_lifeTrack.frame.origin.y, scroll_lifeTrack.frame.size.width, scroll_lifeTrack.frame.size.height);
        [scroll_lifeTrack scrollRectToVisible:frm animated:YES];
    }
    else
    {
        @synchronized(self) {
            if (timer_lifeTrack.isValid) {
                [timer_lifeTrack invalidate];
            }
            timer_lifeTrack = nil;
            [view_background removeFromSuperview];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            
           // id <SlideNavigationContorllerAnimator> revealAnimator;
            
            UIViewController *vc ;
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
            
//            [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//                [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//            }];
        }
    }
    
}

-(void)createWhiteViewPopUpWithButtonTitle:(NSString *)title withtag:(int)tag
{
    view_whiteSettingPopUp = [[UIView alloc] init];
    view_whiteSettingPopUp.frame = tag == 1 ? CGRectMake(0, 0, 250, 330): CGRectMake(0, 0, 250, 235);
    
    view_whiteSettingPopUp.backgroundColor = [UIColor whiteColor];
    view_whiteSettingPopUp.layer.cornerRadius = 5;
    view_whiteSettingPopUp.center = view_background.center;
    [view_background addSubview:view_whiteSettingPopUp];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = tag == 1 ? CGRectMake(0, 295, 250, 35) : CGRectMake(0, 200, 250, 35);
    btn.backgroundColor = backGroundColor;
    btn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if ([title isEqualToString:@"Cancel"])
    {
        [btn setTitle:title forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_cancel_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if ([title isEqualToString:@"OK"])
    {
        [btn setTitle:@"OK" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_OK_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [btn setTitle:@"OK" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btn_error_clicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [view_whiteSettingPopUp addSubview:btn];
    
}

-(void)createBackGroundViewtoView
{
    view_background = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    view_background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.7];
    [window addSubview:view_background];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
