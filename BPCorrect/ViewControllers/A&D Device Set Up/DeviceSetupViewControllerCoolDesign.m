//
//  DeviceSetupViewControllerCoolDesign.m
//  ANDMedical
//
//  Created by simantini bhattacharya on 12/10/15.
//  Copyright © 2015 a1. All rights reserved.
//

#import "DeviceSetupViewControllerCoolDesign.h"
#import "ANDDevice.h"
#import "ADActivityMonitor.h"
#import "UIView+Animation.h"
#import "ADBloodPressure.h"
#import "ADWeightScale.h"
#import "WeightScale.h"
#import "ADThermometer.h"
#import "ANDBLEDefines.h" //Sim added for WELWORLD-29
#import "DeviceSetUpCellCoolDesign.h"
#import "DeviceSetUpCellCoolDesignHeader.h"
#import "DevicePairingStatusViewController.h"
#import "BPCorrect-Swift.h"

@interface DeviceSetupViewControllerCoolDesign ()<ANDDeviceDelegate>
@property NSString *str_activityModelName;
@property NSString *str_bloodPressureModelName;
@property NSString *str_weightScaleModelName;
@property NSString *str_thermometerModelName; //Sim added for Thermometer Profile
@property ANDDevice *device;
@property NSString *type;
@property NSString *display;
@property NSMutableArray *arr_activityMonitor;

@property NSString *selected_device_type;
@property NSString *device_type;

@end

@implementation DeviceSetupViewControllerCoolDesign
#define bpactiveColor [UIColor colorWithRed:((float)((0xF04323 & 0xFF0000) >> 16))/255.0 green:((float)((0xF04323 & 0xFF00) >> 8))/255.0 blue:((float)(0xF04323 & 0xFF))/255.0 alpha:1.0]
#define wsactiveColor [UIColor colorWithRed:((float)((0x2AC478 & 0xFF0000) >> 16))/255.0 green:((float)((0x2AC478 & 0xFF00) >> 8))/255.0 blue:((float)(0x2AC478 & 0xFF))/255.0 alpha:1.0]
#define thactiveColor [UIColor colorWithRed:((float)((0xF09315 & 0xFF0000) >> 16))/255.0 green:((float)((0xF09315 & 0xFF00) >> 8))/255.0 blue:((float)(0xF09315 & 0xFF))/255.0 alpha:1.0]
#define activityactiveColor [UIColor colorWithRed:((float)((0x0E7BD9 & 0xFF0000) >> 16))/255.0 green:((float)((0x0E7BD9 & 0xFF00) >> 8))/255.0 blue:((float)(0x0E7BD9 & 0xFF))/255.0 alpha:1.0]
#define activityactiveColorPurple [UIColor colorWithRed:((float)((0x6763C3 & 0xFF0000) >> 16))/255.0 green:((float)((0x6763C3 & 0xFF00) >> 8))/255.0 blue:((float)(0x6763C3 & 0xFF))/255.0 alpha:1.0]


- (void)viewDidLoad {
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    window = [[[UIApplication sharedApplication]delegate] window];
    self.navigationController.navigationBarHidden = NO;
    self.title = NSLocalizedString(@"Label.DeviceSetup.Heading", @"");
    
    //appdel = [[UIApplication sharedApplication]delegate];
    //appdel.navControl = self.navigationController;
    self.automaticallyAdjustsScrollViewInsets = NO;
    //Decide the scroll view width
    if ([[UIScreen mainScreen] bounds].size.height == 480)
    {
        mScrollView.frame = CGRectMake(0, 0, 320, 480);
        mScrollView.contentSize = CGSizeMake(320, 480);
    }
    else
    {
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {

  [UIApplication sharedApplication].statusBarHidden = NO;
  [self setNavigationBarItem];
   
}


//Below methods are to handle the left and right option for the Navigation Controller.
#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu{
    //Added for Cesars new design - to display dashboard shortcut on right
    self.navigationItem.rightBarButtonItem = (UIBarButtonItem *)[CommonClass addingDashBoardButton:self];
    return YES;
}

/*********** Start the table view methods ****************************************************************/
#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0; //The table should start right from top
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
         return 4;//4 Main classification of devices
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Break it down based on the section
    if (section == 0) {

        return 2; //Show only the 651 for Global app
        

        
      
    } else if (section == 1) {
        return 2; //Global app
        

    } else if (section == 2) {
        //Activity Monitor Section - For now hide it, do not show AD tracker
        return 3; //Made it three for the activity
    } else if (section == 3) {
        return 2; //Thermometer section
    } else {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 45;
        } else if (indexPath.row == 1) {
            return  80;
        } else if (indexPath.row == 2) {
            return 80;
        } else if (indexPath.row == 3) {
            return 80;
        } else {
            return 100;
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 45;
        } else if (indexPath.row == 1) {
            return  80;
        } else if (indexPath.row == 2) {
            return 80; //Sim added for Transtek
        } else {
            return 100;
        }
        
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            return 45;
        } else if (indexPath.row == 1) {
            return  80;
        } else if (indexPath.row == 2)  {
            return 80;
        } else {
            return 100;
        }
        
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            return 45;
        } else if (indexPath.row == 1) {
            return  80;
        }else if (indexPath.row == 2) {
            return  80;
        } else {
            return 100;
        }
       
        
    } else {
        return 100;
    }
    
    return 0;
}
    

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cell_identifier1 = @"DeviceSetUpCellCoolDesign";
    NSString *cell_identifier2 = @"DeviceSetUpCellCoolDesignHeader";
    
    DeviceSetUpCellCoolDesign *cell1 = [tableView dequeueReusableCellWithIdentifier:cell_identifier1];
    DeviceSetUpCellCoolDesignHeader *cell2 = [tableView dequeueReusableCellWithIdentifier:cell_identifier2];
    
    if (cell1 == nil)
    {
        cell1 = [[DeviceSetUpCellCoolDesign alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier1];
        
    }
    
    
    if (cell2 == nil)
    {
        cell2 = [[DeviceSetUpCellCoolDesignHeader alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier2];
        
    }
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0) {
            [cell2.imgView_deviceHeader setImage:[UIImage imageNamed:@"Heart_cdl"]];
            cell2.lbl_deviceNameHeader.text = NSLocalizedString(@"BP.Monitor.Display", @"");
            cell2.preservesSuperviewLayoutMargins = false;
            cell2.separatorInset = UIEdgeInsetsZero;
            cell2.layoutMargins = UIEdgeInsetsZero;
            cell2.backgroundColor = bpactiveColor;
            cell2.userInteractionEnabled = NO;
            return cell2;
        } else {
            //Sim modified for Transtek weight scale
            if (indexPath.row == 1) {
                    cell1.lbl_deviceName2.hidden = FALSE;
                    cell1.lbl_deviceName.text = @"DeluxeCONNECT Upper Arm";
                    cell1.lbl_deviceName2.text = @"UA-651BLE";
                    [cell1.imgView_device setImage:[UIImage imageNamed:@"UA-651BLE_EC"]];
            }
            cell1.preservesSuperviewLayoutMargins = false;
            cell1.separatorInset = UIEdgeInsetsZero;
            cell1.layoutMargins = UIEdgeInsetsZero;
            return cell1;
        }
        
        
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0) {
            [cell2.imgView_deviceHeader setImage:[UIImage imageNamed:@"WSicon_cdl"]];
            cell2.lbl_deviceNameHeader.text = NSLocalizedString(@"WeightScale.Text", @"") ;
            cell2.preservesSuperviewLayoutMargins = false;
            cell2.separatorInset = UIEdgeInsetsZero;
            cell2.layoutMargins = UIEdgeInsetsZero;
            cell2.backgroundColor = wsactiveColor;
            cell2.userInteractionEnabled = NO;
            return cell2;
        } else {
            if (indexPath.row == 1) {
                    cell1.lbl_deviceName2.hidden = FALSE;
                    cell1.lbl_deviceName.text = @"DeluxeCONNECT Scale";
                    cell1.lbl_deviceName2.text = @"UC-352BLE";
                    [cell1.imgView_device setImage:[UIImage imageNamed:@"UC-352BLE_EC"]];
            } else if (indexPath.row == 2) {
                cell1.lbl_deviceName2.hidden = FALSE;
                cell1.lbl_deviceName.text = @"PlusCONNECT Scale";
                cell1.lbl_deviceName2.text = @"UC-350BLE";
                [cell1.imgView_device setImage:[UIImage imageNamed:@"UC350BLE_cdl"]];
                
            }
            
            cell1.preservesSuperviewLayoutMargins = false;
            cell1.separatorInset = UIEdgeInsetsZero;
            cell1.layoutMargins = UIEdgeInsetsZero;
            return cell1;
        }
        
    }
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 0) {
            [cell2.imgView_deviceHeader setImage:[UIImage imageNamed:@"RunningMan_Icon_cdl"]];
            cell2.lbl_deviceNameHeader.text = NSLocalizedString(@"Device.Activity.Name", @"");
            
            cell2.preservesSuperviewLayoutMargins = false;
            cell2.separatorInset = UIEdgeInsetsZero;
            cell2.layoutMargins = UIEdgeInsetsZero;
            cell2.backgroundColor = activityactiveColorPurple;
            cell2.userInteractionEnabled = NO;
            return cell2;
        } else {
            if (indexPath.row == 1) {
                cell1.lbl_deviceName.text = @"UW-302BLE";
               
                [cell1.imgView_device setImage:[UIImage imageNamed:@"uw302_thumbnail"]];
            } else if (indexPath.row ==2) {
                
            }
            cell1.preservesSuperviewLayoutMargins = false;
            cell1.separatorInset = UIEdgeInsetsZero;
            cell1.layoutMargins = UIEdgeInsetsZero;
            return cell1;
        }
        
    }
    else if (indexPath.section == 3) //Sim TODO, For Global app , this needs to be thermometer
    {
        if (indexPath.row == 0) {
            [cell2.imgView_deviceHeader setImage:[UIImage imageNamed:@"Thermometer_icon_cdl"]];
            cell2.lbl_deviceNameHeader.text =  NSLocalizedString(@"Device.Thermometer.MainDashboard", @"");;
            cell2.preservesSuperviewLayoutMargins = false;
            cell2.separatorInset = UIEdgeInsetsZero;
            cell2.layoutMargins = UIEdgeInsetsZero;
            cell2.backgroundColor = thactiveColor;
            cell2.userInteractionEnabled = NO;
            return cell2;
        } else {
            cell1.lbl_deviceName.text = @"UT-201";
           cell1.lbl_deviceName2.hidden = TRUE;
            [cell1.imgView_device setImage:[UIImage imageNamed:@"UT-201_cdl"]];
            cell1.preservesSuperviewLayoutMargins = false;
            cell1.separatorInset = UIEdgeInsetsZero;
            cell1.layoutMargins = UIEdgeInsetsZero;
            return cell1;
        }
        
    }
    
    return cell1;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            _device_type = @"bp";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen - Sim hack for now

          } else if (indexPath.row == 2) {
            _device_type = @"bpc_ua";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
        } else if(indexPath.row == 3) {
            _device_type = @"bpc_wb";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil];
        }
    }else if (indexPath.section == 1) {
        //Sim made changes for Transtek build - commenting out CDL
        if (indexPath.row == 1) {
            _device_type = @"ws";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil];
        } else if (indexPath.row == 2) { //Sim added for Transtek Weight Scale
            _device_type = @"transtek_ws";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil];
            
        }
        
        
    }else if (indexPath.section == 2) {
        
      /*  if (indexPath.row == 1) {
            _device_type = @"ac";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
        } */
        if (indexPath.row == 1) {
            //Cool design custom code - Later change it to cool design custome code
            _device_type = @"uw302_ac"; //Change this later to the custom tracker code
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
            
        } else if(indexPath.row == 2) {
            _device_type = @"ac";
            [self performSegueWithIdentifier:@"PairingStatus" sender:nil]; //Launching the new controller screen
        } 
        
    }else if (indexPath.section == 3) {
        _device_type = @"th";
        [self performSegueWithIdentifier:@"PairingStatus" sender:nil];
        
    }
}
-(void)btn_dashboard_clicked:(UIButton *)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    //id <SlideNavigationContorllerAnimator> revealAnimator;
    
    UIViewController *vc ;
    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DashboardVC"];
    NSLog(@"This is entering the dashboad button click");
//    [[SlideNavigationController sharedInstance] switchToViewController:vc withCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//    }];
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PairingStatus"])
    {
        DevicePairingStatusViewController *secondViewController = [segue destinationViewController];
        secondViewController.selected_device_type = _device_type;
    }
}

-(void) action_ManualEntry:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"launchSettings" sender:nil];
}


@end
