//
//  BPReadingsCell.swift
//  BPCorrect
//
//  Created by "" on 28/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit

class BPReadingsCell: UITableViewCell {

  @IBOutlet var dateLabel: UILabel!
  @IBOutlet var timeLabel: UILabel!
  
  @IBOutlet var systolicLabel: UILabel!
  @IBOutlet var diasystolicLabel: UILabel!
  @IBOutlet var pulseLabel: UILabel!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
