//
//  DevicesTableViewCell.swift
//  BPCorrect
//
//  Created by "" on 24/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit

protocol DevicesTableViewCellDelegate: class {
  
  func disclosureButtonIsPressed(cell:UITableViewCell)
}

class DevicesTableViewCell: UITableViewCell {

  @IBOutlet var deviceImageView: UIImageView!
  
  @IBOutlet var deviceNameLabel: UILabel!
  var delegate : DevicesTableViewCellDelegate?
  
  
  @IBAction func disclosureButtonIsPressed(_ sender: Any) {
    self.delegate?.disclosureButtonIsPressed(cell: self)
  }
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
