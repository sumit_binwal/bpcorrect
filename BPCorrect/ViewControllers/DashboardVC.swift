//
//  DashboardVC.swift
//  BPCorrect
//
//  Created by "" on 25/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import AVFoundation
import MZTimerLabel
class DashboardVC: UIViewController {
var player: AVPlayer?
    @IBOutlet var vwTimer: UIView!
    @IBOutlet weak var labelTimer: MZTimerLabel!
    @IBOutlet var imageView: UIImageView!
  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var emailLabel: UILabel!
  var userModel : UserModel?
  
  @IBOutlet var measureBPView: UIView!
  @IBOutlet var myBPView: UIView!
  @IBOutlet var learnView: UIView!
  @IBOutlet var manageDevicesView: UIView!
  
    @IBOutlet weak var vwVideo: UIView!
    
  
    override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = "Dashboard"
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
    self.imageView.layer.masksToBounds = true
    self.imageView.clipsToBounds = true
    self.imageView.layer.borderWidth = 1.0
    self.imageView.layer.borderColor = UIColor.white.cgColor
    
    let measureBPTap = UITapGestureRecognizer(target: self, action: #selector(self.measureBPTap(_:)))
    measureBPView.addGestureRecognizer(measureBPTap)

    let myBPTap = UITapGestureRecognizer(target: self, action: #selector(self.myBPTap(_:)))
    myBPView.addGestureRecognizer(myBPTap)

    let learnTap = UITapGestureRecognizer(target: self, action: #selector(self.learnTap(_:)))
    learnView.addGestureRecognizer(learnTap)

    let manageDevicesTap = UITapGestureRecognizer(target: self, action: #selector(self.manageDevicesTap(_:)))
    manageDevicesView.addGestureRecognizer(manageDevicesTap)

        DataBaseManager.sharedInstance()

    // Do any additional setup after loading the view.
  }
    @IBAction func skipButtonAction(_ sender: Any)
    {
        self.vwTimer.removeFromSuperview()
        self.player?.pause()

    }
    
    func initializeVideoPlayerWithVideo() {
        
        // get the path string for the video from assets//bpCorrect.mp4
        let videoString:String? = Bundle.main.path(forResource: "bpCorrect", ofType: "mov")
        guard let unwrappedVideoPath = videoString else {return}
        
        // convert the path string to a url
        let videoUrl = URL(fileURLWithPath: unwrappedVideoPath)
        
        // initialize the video player with the url
        self.player = AVPlayer(url: videoUrl)
        
        // create a video layer for the player
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        
        // make the layer the same size as the container view
        layer.frame = vwVideo.bounds
        
        // make the video fill the layer as much as possible while keeping its aspect size
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        // add the layer to the container view
        vwVideo.layer.addSublayer(layer)
        player?.play()

    }
    
    @IBAction func playVideoButtonTapped(_ sender: UIButton) {
        // play the video if the player is initialized
    }

    
    
  @objc func measureBPTap(_ sender: UITapGestureRecognizer) {
    
    
    
    UIAlertController.showAlertWithTitle(title: "Caution", message: "Its Important to rest for 5 minutes before measuring your blood pressure to get an accurate reading.", onViewController: self, withButtonArray: ["START 5-MINUTE TIMER","READY TO CHECK MY BP"]) { (buttonIndex) in

    if buttonIndex == 1
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let measureBP_AND_VC = storyboard.instantiateViewController(withIdentifier: "MeasureBPVC") as! MeasureBPVC
        self.navigationController?.show(measureBP_AND_VC, sender: self)
        }
        else if buttonIndex == 0
    {
self.view.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        self.initializeVideoPlayerWithVideo()
        self.view .addSubview(self.vwTimer)
        self.labelTimer.reset()
        self.labelTimer.timerType = MZTimerLabelTypeTimer
        self.labelTimer.setCountDownTime(300)
        self.labelTimer.start(ending: { (timeInterval) in
            
            self.vwTimer.removeFromSuperview()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let measureBP_AND_VC = storyboard.instantiateViewController(withIdentifier: "MeasureBPVC") as! MeasureBPVC
            self.navigationController?.show(measureBP_AND_VC, sender: self)

        })
        self.labelTimer.timeFormat = "mm:ss"
        self.labelTimer.delegate = self
        self.labelTimer.start()
        
        
        }

    }
    
    
  }
  
  @objc func myBPTap(_ sender: UITapGestureRecognizer) {
    
    

    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let myBPVCReadingsVC = storyboard.instantiateViewController(withIdentifier: "MyBPVCReadingsVC") as! MyBPVCReadingsVC
    self.navigationController?.show(myBPVCReadingsVC, sender: self)
  }
  
  @objc func manageDevicesTap(_ sender: UITapGestureRecognizer) {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let manageDevicesVC = storyboard.instantiateViewController(withIdentifier: "ManageDevicesVC") as! ManageDevicesVC
    self.navigationController?.show(manageDevicesVC, sender: self)
  }
  
  @objc func learnTap(_ sender: UITapGestureRecognizer) {
   
  }
  func loadUserProfile() {
    
    userModel = BPUserDefaults.getUserModel()
    
    if (userModel != nil && (userModel?.data?.count)! > 0) {
      
      nameLabel.text = userModel?.data![0].firstname ?? ""
      emailLabel.text = userModel?.data![0].email ?? "-"
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.setNavigationBarItem()
    loadUserProfile()
  }

}
extension DashboardVC : MZTimerLabelDelegate
{
    func timerLabel(_ timerLabel: MZTimerLabel!, finshedCountDownTimerWithTime countTime: TimeInterval) {
        
    }
}
