//
//  EditProfileVC.swift
//  BPCorrect
//
//  Created by "" on 01/05/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import Alamofire

class EditProfileVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  var imagePicker : UIImagePickerController!
  
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var nameTextfield: FloatLabelTextField!
  @IBOutlet var genderTextfield: FloatLabelTextField!
  @IBOutlet var heightTextfield: FloatLabelTextField!
  
  @IBOutlet var weightTextfield: FloatLabelTextField!
  @IBOutlet var emailTextfield: FloatLabelTextField!
  @IBOutlet var dateOfBirthTextfield: FloatLabelTextField!
  @IBOutlet var phoneTextfield: FloatLabelTextField!
  @IBOutlet var addressTextfield: FloatLabelTextField!
  @IBOutlet var cityTextfield: FloatLabelTextField!
  @IBOutlet var stateTextfield: FloatLabelTextField!
  @IBOutlet var zipCodeTextfield: FloatLabelTextField!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var submitButton: UIView!
  
  var loadingView: LoadingView?
  var userModel : UserModel?
  var user : User?
  var activeField: UITextField?
  var profileURL : String = ""
  
  @IBOutlet var profileImageView: UIView!
  
  override func viewDidLoad() {
        super.viewDidLoad()
    
    self.title = "Edit Profile"
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EditProfileVC.dismissKeyboard))
    view.addGestureRecognizer(tap)
    
    userModel = BPUserDefaults.getUserModel()
    user =  userModel!.data?[0]
    self.nameTextfield.text = user!.firstname
    self.genderTextfield.text = user!.gender
    self.heightTextfield.text = user!.height
    self.weightTextfield.text = user!.weight
    
    self.emailTextfield.text = user!.email
    self.dateOfBirthTextfield.text = user!.dob
    
    self.phoneTextfield.text = user!.mobile1
    self.addressTextfield.text = user!.address1
    
    setUpView()

        // Do any additional setup after loading the view.
    }
  
  
  @IBAction func submitButtonIsPressed(_ sender: Any) {
    
    let name : String = nameTextfield.text!
    
    let params : Parameters = ["access_token":BPUserDefaults.accessToken(),
                               "firstname":"\(nameTextfield.text!)",
      "lastname":"",
      "dob":"\(dateOfBirthTextfield.text!)",
      "address1":"\(addressTextfield.text!)",
      "email":"\(emailTextfield.text!)",
      "mobile1":"\(phoneTextfield.text!)",
      "gender":"\(genderTextfield.text!)",
      "city":"\(cityTextfield.text!)",
      "state":"\(stateTextfield.text!)",
      "zipcode":"\(zipCodeTextfield.text!)",
      "userId":BPUserDefaults.userId(),
    "height":"\(heightTextfield.text!)",
    "weight":"\(weightTextfield.text!)",
    "photo_url":self.profileURL]
    
    
    self.setIsLoading(true)
    APIManager.sharedInstance.editUserProfile(params: params, completionHandler: { (userModel) in
      self.setIsLoading(false)
      //Save User Profile
      BPUserDefaults.saveUserModel(user: userModel)
      
    }) { (error) in
      self.setIsLoading(false)
     AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Something went wrong")
    }
    
  }
  
  func setUpView() {
    
    self.extendedLayoutIncludesOpaqueBars = true
    self.navigationController?.isNavigationBarHidden = false
    self.navigationController?.navigationBar.isTranslucent = false
    
    UIApplication.shared.statusBarView?.backgroundColor =  UIColor.bpCorrectThemeColor()
    self.view.insetsLayoutMarginsFromSafeArea = true
    self.scrollView.insetsLayoutMarginsFromSafeArea = true
    
    self.profileImageView.layer.cornerRadius = 5.0
    
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
    self.imageView.clipsToBounds = true
    self.imageView.layer.borderColor = UIColor.white.cgColor
    self.imageView.layer.borderWidth = 3.0
    
    self.imagePicker = UIImagePickerController()
    self.imagePicker.delegate = self
    let imageViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewIsTapped))
    self.imageView?.addGestureRecognizer(imageViewTapGesture)

    
    setUpTextField(textField: self.nameTextfield)
    setUpTextField(textField: self.genderTextfield)
    setUpTextField(textField: self.heightTextfield)
    setUpTextField(textField: self.weightTextfield)
    setUpTextField(textField: self.emailTextfield)
    setUpTextField(textField: self.dateOfBirthTextfield)
    setUpTextField(textField: self.phoneTextfield)
    setUpTextField(textField: self.addressTextfield)
    setUpTextField(textField: self.cityTextfield)
    setUpTextField(textField: self.stateTextfield)
    setUpTextField(textField: self.zipCodeTextfield)
    
    addTextFieldRightView(textField: genderTextfield)
    
    self.scrollView.keyboardDismissMode = .interactive
    self.submitButton.layer.cornerRadius = 3.0
    self.submitButton.clipsToBounds = true
    
    
    
    registerForKeyboradDidShowWithBlock(scrollview: self.scrollView)
    registerForKeyboardWillHideNotificationWithBlock(scrollview: self.scrollView)
    deregisterKeyboardShowAndHideNotification()
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    
    registerForKeyboardNotifications()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    deregisterFromKeyboardNotifications()
  }
  
  func addTextFieldRightView(textField: FloatLabelTextField) {
    let imageView = UIImageView();
    imageView.frame = CGRect(x: CGFloat(textField.frame.size.width - 20), y: CGFloat(5), width: CGFloat(10), height: CGFloat(10))
    let image = UIImage(named: "dropdown")
    imageView.image = image;
    imageView.sizeToFit()
    textField.rightView = imageView
    textField.rightViewMode = .always
  }
  
  func addTextFieldLeftView(textField: FloatLabelTextField) {
    let imageView = UIImageView();
    imageView.frame = CGRect(x: CGFloat(textField.frame.size.width - 20), y: CGFloat(5), width: CGFloat(10), height: CGFloat(10))
    
    let image = UIImage(named: "dropdown")
    imageView.image = image;
    imageView.sizeToFit()
    textField.rightView = imageView
    textField.rightViewMode = .always
  }
  
  func setUpTextField(textField: FloatLabelTextField) {
    textField.titleTextColour = UIColor.gray
    textField.titleActiveTextColour = UIColor.gray
    textField.textColor = UIColor.black
    textField.delegate = self
  }
  
  @objc func dismissKeyboard() {
    self.view.endEditing(true)
  }
  
  func registerForKeyboardNotifications()
  {
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(self, selector: #selector(EditProfileVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(EditProfileVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  
  func deregisterFromKeyboardNotifications()
  {
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    
    //Need to calculate keyboard exact size due to Apple suggestions
    if(!self.scrollView.isScrollEnabled ) {
      self.scrollView.isScrollEnabled = true
    }
    
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if activeField != nil
    {
      if (!aRect.contains(activeField!.frame.origin))
      {
        self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
      }
    }
    
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    //Once keyboard disappears, restore original positions
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
  }
  
  func validateForm() -> Bool {
    if nameTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter first name.")
      return false
    } else if (genderTextfield.text?.trimmingCharacters(in: .whitespaces).count)! < 3 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"First name must have atleast 2 letters.")
      return false
    } else if heightTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter your height")
      return false
    } else if weightTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please select your weight")
      return false
    } else if (emailTextfield.text?.trimmingCharacters(in: .whitespaces).count)! < 3 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"First name must have atleast 2 letters.")
      return false
    } else if dateOfBirthTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter your date of birth")
      return false
    } else if phoneTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter your phone")
      return false
    } else if (addressTextfield.text?.trimmingCharacters(in: .whitespaces).count)! < 3 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter your address")
      return false
    }
    return true
  }
  
  
  @IBAction func imageViewIsTapped() {
    
    if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
      print("Button capture")
      
      self.imagePicker.delegate = self
      self.imagePicker.sourceType = .savedPhotosAlbum;
      self.imagePicker.allowsEditing = false
      
      self.present(imagePicker, animated: true, completion: nil)
    }
  }
  
  
  @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    

    if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage {

      dismiss(animated: true, completion: nil)
      self.imageView.image = image

      let imageData = image.jpegData(compressionQuality: 0.5)
      let test = "file"
      
      let urlString = "http://67.211.223.164:8080/ProtechSentinel/common/upload/image?userId=\(user!.userId!)"
      let url = URL(string: (urlString as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? "")
      let session = URLSession(configuration: URLSessionConfiguration.default)

      let mutableURLRequest = NSMutableURLRequest(url: url!)
      mutableURLRequest.cachePolicy = .reloadIgnoringLocalCacheData
      mutableURLRequest.httpShouldHandleCookies = false

      mutableURLRequest.httpMethod = "POST"


      let boundary = "---------------------------14737809831466499882746641449"
      let contentType = "multipart/form-data; boundary=\(boundary)"
      mutableURLRequest.addValue(contentType, forHTTPHeaderField: "Content-Type")

      // create upload data to send
      let uploadData = NSMutableData()

      // add image
      uploadData.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
      //            uploadData.append("Content-Disposition: form-data; name=\"profile_photo\"; filename=\"file.jpg\"\r\n".data(using: String.Encoding.utf8)!)
      if imageData != nil {
        if let data = "\("Content-Disposition: form-data; name=\"file\"; filename=\"")\(test)\(".jpg\"\r\n")".data(using: .utf8) {
          uploadData.append(data)
        }
      }
      uploadData.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
      uploadData.append(imageData!)
      uploadData.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

      mutableURLRequest.httpBody = uploadData as Data


      setIsLoading(true)

      let task = session.dataTask(with: mutableURLRequest as URLRequest, completionHandler: { (data, response, error) -> Void in

        DispatchQueue.main.async {

          self.setIsLoading(false, animated: true)
          guard let dataResponse = data,
            error == nil else {
              print(error?.localizedDescription ?? "Response Error")
              return }
          do{
            
            
            let jsonString : String = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as! String
           
            self.profileURL = jsonString
            print(jsonString) //Response result
          
            
          } catch let parsingError {
            print("Error", parsingError)
          }
        }

        
      })

      task.resume()

    }
    
  }
  
  
  @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion:nil)
  }

}

extension EditProfileVC: LoadingViewProtocol {
  
  func loadingMessage() -> String? {
    return nil
  }
}
