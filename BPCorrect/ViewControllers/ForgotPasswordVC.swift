//
//  ForgotPasswordVC.swift
//  BPCorrect
//
//  Created by "" on 14/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController {

  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var emailTextfield: UITextField!
  @IBOutlet var forgotPasswordButton: UIButton!
  
  var activeField: UITextField?
  var loadingView: LoadingView?
  
  
  @IBAction func forgotPasswordButtonIsPressed(_ sender: Any) {
    
    if validateLoginForm() {
      
      setIsLoading(true)
      let params : Parameters = ["email" : emailTextfield.text!]
      
      APIManager.sharedInstance.forgotPassword(params: params, completionHandler: { [weak self](responseModel) in
        guard let strongSelf = self else { return }
        strongSelf.setIsLoading(false)
       
        
        let dict : Dictionary<String,String> = responseModel.data![0] as! Dictionary<String, String>
        
        if dict["message"] == "Send Password On Email. Please Check." {
          let alert = UIAlertController(title: "", message: "A reset password link is sent to your email.", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self?.navigationController?.popViewController(animated: true)
          }))
          self!.present(alert, animated: true, completion: nil)
        } else {
          AlertUtils.showAlertMessage(self!, withTitle: "", andMessage:dict["message"])
        }
      }) { (error) in
        self.setIsLoading(false)
        AlertUtils.showAlertMessage(self, withTitle: "", andMessage:error.userInfo["NSLocalizedDescription"] as? String)
      }
    }
    
  }
  
  @IBAction func backButtonIsPressed(_ sender: Any) {
    self.view.endEditing(true)
    self.navigationController?.popViewController(animated: true)
  }
  
  override func viewDidLoad() {
        super.viewDidLoad()

    self.view.insetsLayoutMarginsFromSafeArea = true
    self.scrollView.insetsLayoutMarginsFromSafeArea = true
    self.forgotPasswordButton.layer.cornerRadius = 3.0
    
    registerForKeyboardNotifications()
        // Do any additional setup after loading the view.
    }
    

  override func viewWillDisappear(_ animated: Bool) {
    deregisterFromKeyboardNotifications()
  }
  
  func registerForKeyboardNotifications()
  {
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  
  func deregisterFromKeyboardNotifications()
  {
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    
    //Need to calculate keyboard exact size due to Apple suggestions
    if(!self.scrollView.isScrollEnabled ) {
      self.scrollView.isScrollEnabled = true
    }
    
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if activeField != nil
    {
      if (!aRect.contains(activeField!.frame.origin))
      {
        self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
      }
    }
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    //Once keyboard disappears, restore original positions
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    // self.view.endEditing(true)
    // self.scrollView.isScrollEnabled = false
  }
  
  @objc func dismissKeyboard() {
    self.view.endEditing(true)
  }
  

  func validateLoginForm() -> Bool {
    if emailTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter email.")
      return false
    }
    return true
  }

}


extension ForgotPasswordVC: LoadingViewProtocol {
  
  func loadingMessage() -> String? {
    return nil
  }
}
