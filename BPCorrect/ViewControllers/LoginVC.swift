//
//  ViewController.swift
//  BPCorrect
//
//  Created by "" on 30/03/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {

  @IBOutlet var loginButton: UIButton!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var usernameTextfield: UITextField!
  @IBOutlet var passwordTextfield: UITextField!
  var activeField: UITextField?
   var loadingView: LoadingView?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationController?.isNavigationBarHidden = true
    self.usernameTextfield.delegate = self
    self.passwordTextfield.delegate = self
    loginButton.layer.cornerRadius = 3.0
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                             action: #selector(LoginVC.dismissKeyboard))
    view.addGestureRecognizer(tap)
    
    self.view.insetsLayoutMarginsFromSafeArea = true
    self.scrollView.insetsLayoutMarginsFromSafeArea = true
    
    registerForKeyboardNotifications()
    
    // Do any additional setup after loading the view, typically from a nib.
  }

 
  override func viewWillDisappear(_ animated: Bool) {
    deregisterFromKeyboardNotifications()
  }
  
  func registerForKeyboardNotifications()
  {
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  
  func deregisterFromKeyboardNotifications()
  {
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    
    //Need to calculate keyboard exact size due to Apple suggestions
    if(!self.scrollView.isScrollEnabled ) {
      self.scrollView.isScrollEnabled = true
    }
    
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if activeField != nil
    {
      if (!aRect.contains(activeField!.frame.origin))
      {
        self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
      }
    }
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    //Once keyboard disappears, restore original positions
    let info : NSDictionary = notification.userInfo! as NSDictionary
    let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
  }

  @objc func dismissKeyboard() {
    self.view.endEditing(true)
  }
  
  func validateLoginForm() -> Bool {
    if usernameTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0 {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter username.")
      return false
    } else if passwordTextfield.text?.trimmingCharacters(in: .whitespaces).count == 0  {
      AlertUtils.showAlertMessage(self, withTitle: "" , andMessage:"Please enter password.")
      return false
    }
    return true
  }
  
  @IBAction func loginButtonIsPressed(_ sender: Any) {
    if validateLoginForm() {
      setIsLoading(true)
      
      let params : Parameters = ["username":usernameTextfield.text!,
                                 "password":passwordTextfield.text!,
                                 "client_id" :"test",
                                 "grant_type" : "password"]
      
      APIManager.sharedInstance.loginUser(params: params, completionHandler: { [weak self](loginModel) in
        
        guard let strongSelf = self else { return }
        strongSelf.setIsLoading(false, animated: true)
        BPUserDefaults.setLoggedIn(true)
        
        let appDelegate : AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.createMenuView()
        
      }) { (error) in
          self.setIsLoading(false)
        self.usernameTextfield.text = ""
        self.passwordTextfield.text = ""
         AlertUtils.showAlertMessage(self, withTitle: "", andMessage:error.userInfo["NSLocalizedDescription"] as? String)
      }
    }
  }
  
}


extension LoginVC : UITextFieldDelegate {
  /**
   * Called when 'return' key pressed. return NO to ignore.
   */
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTextfield{
      textField.resignFirstResponder()
      passwordTextfield.becomeFirstResponder()
    } else if textField == passwordTextfield {
      textField.resignFirstResponder()
    }
    return true
  }
  
  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
    
    if textField == self.passwordTextfield  {
      let nsString:NSString? = textField.text as NSString?
      let updatedString = nsString?.replacingCharacters(in:range, with:string)
      
      textField.text = updatedString
      
      
      //Setting the cursor at the right place
      let selectedRange = NSMakeRange(range.location + string.count, 0)
      let from = textField.position(from: textField.beginningOfDocument, offset:selectedRange.location)
      let to = textField.position(from: from!, offset:selectedRange.length)
      textField.selectedTextRange = textField.textRange(from: from!, to: to!)
      
      //Sending an action
      textField.sendActions(for: UIControl.Event.editingChanged)
      
      return false
    }
    
    return true
    
  }
}

extension LoginVC: LoadingViewProtocol {
  
  func loadingMessage() -> String? {
    return nil
  }
}
