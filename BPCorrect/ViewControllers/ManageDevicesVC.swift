//
//  ManageDevicesVC.swift
//  BPCorrect
//
//  Created by "" on 13/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit

class ManageDevicesVC: UIViewController, DevicesTableViewCellDelegate, UITableViewDelegate, UITableViewDataSource  {
  
  
  
    @IBOutlet var tableView: UITableView!
  
    var deviceTypes : [String]?
    var deviceType : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
      
        deviceTypes = ["AND Blood Pressure Device"]
      
        self.title = "Manage Devices"
        self.tableView.delegate = self
        self.tableView.dataSource = self
      
      self.tableView.tableFooterView = UIView()
      
        // Do any additional setup after loading the view.
    }
  
  
  // MARK: - Table View
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.deviceTypes!.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "DevicesTableViewCell", for: indexPath) as! DevicesTableViewCell
    let deviceType : String = self.deviceTypes![indexPath.row]
    cell.deviceNameLabel!.text = deviceType
    
    if indexPath.row == 0 {
      cell.deviceImageView.image = UIImage(named: "icon_bp")
    }
    
//    if callLog.isIncoming {
//      cell.callTypeImageView.image = UIImage(named: "incoming-call")
//    } else {
//      cell.callTypeImageView.image = UIImage(named: "outgoing-call")
//    }
    
    cell.delegate = self
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var deviceType: String = self.deviceTypes![indexPath.row]
    
    if deviceType == "AND Blood Pressure Device" {
      self.deviceType = "bp"
      self.performSegue(withIdentifier: "PairingStatus", sender: self)
    }
  }
  
  //MARK: - CallLogCellDelegate
  func disclosureButtonIsPressed(cell: UITableViewCell) {
    let deviceType: String = self.deviceTypes![(self.tableView.indexPath(for: cell)?.row)!]
    
    if deviceType == "AND Blood Pressure Device" {
      self.deviceType = "bp"
      self.performSegue(withIdentifier: "PairingStatus", sender: self)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let destVC : DevicePairingStatusViewController = segue.destination as! DevicePairingStatusViewController
    destVC.selected_device_type = self.deviceType
    
  }


}
