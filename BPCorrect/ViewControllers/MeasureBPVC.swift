//
//  MeasureBPVC.swift
//  BPCorrect
//
//  Created by "" on 12/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit

class MeasureBPVC: UIViewController, DevicesTableViewCellDelegate, UITableViewDelegate, UITableViewDataSource  {
  
  
  
  @IBOutlet var tableView: UITableView!
  
  var deviceTypes : [String]?
  var deviceType : String?
  var userModel : UserModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setNavigationBarItem()
    
    deviceTypes = ["AND Blood Pressure Device"]
    
    self.title = "Measure Blood Pressure"
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.tableView.tableFooterView = UIView()
    self.userModel = BPUserDefaults.getUserModel()
    
    // Do any additional setup after loading the view.
  }
  
  
  
  // MARK: - Table View
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.deviceTypes!.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "DevicesTableViewCell", for: indexPath) as! DevicesTableViewCell
    let deviceType : String = self.deviceTypes![indexPath.row]
    cell.deviceNameLabel!.text = deviceType
    
    if indexPath.row == 0 {
      cell.deviceImageView.image = UIImage(named: "icon_bp")
    }
    
    cell.delegate = self
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let deviceType: String = self.deviceTypes![indexPath.row]
    
    if deviceType == "AND Blood Pressure Device" {
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let measureBP_AND_VC = storyboard.instantiateViewController(withIdentifier: "MeasureBP_AND_VC") as! MeasureBP_AND_VC
      //measureBP_AND_VC.patientId = "\(self.userModel?.data![0].patientId ?? 0)";
        measureBP_AND_VC.patientId = BPUserDefaults.userId()
      self.navigationController?.pushViewController(measureBP_AND_VC, animated: true)
    }
  }
  
  //MARK: - CallLogCellDelegate
  func disclosureButtonIsPressed(cell: UITableViewCell) {
    let deviceType: String = self.deviceTypes![(self.tableView.indexPath(for: cell)?.row)!]
    
    if deviceType == "AND Blood Pressure Device" {
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let measureBP_AND_VC = storyboard.instantiateViewController(withIdentifier: "MeasureBP_AND_VC") as! MeasureBP_AND_VC
      measureBP_AND_VC.patientId = "\(self.userModel?.data![0].patientId ?? 0)";
      self.navigationController?.pushViewController(measureBP_AND_VC, animated: true)
      
    }
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let destVC : DevicePairingStatusViewController = segue.destination as! DevicePairingStatusViewController
    destVC.selected_device_type = self.deviceType
    
  }
}
