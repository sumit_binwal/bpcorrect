//
//  MeasureBP_AND_VC.h
//  BPCorrect
//
//  Created by "" on 25/04/19.
//  Copyright © 2019 "". All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANDBaseController.h"
@class DatabaseManager;

@interface MeasureBP_AND_VC : ANDBaseController <UITableViewDataSource , UITableViewDelegate>
{
  
  NSTimer *timer_error;
  
  NSTimer *timer_wrongSetup;
  
}
@property (strong, nonatomic) IBOutlet UILabel *bpReadingsLabel;
@property (strong, nonatomic) IBOutlet UILabel *pulseLabel;
@property (strong, nonatomic) IBOutlet UILabel *instructionLabel;


@property (nonatomic , weak) IBOutlet UIView        *view_pop_base;
@property (nonatomic , weak) IBOutlet UIView        *view_pop_middle;
@property (nonatomic , weak) IBOutlet UILabel        *lbl_popup;

@property (nonatomic , weak) IBOutlet UITableView   *tblView_obj;
@property (weak, nonatomic) IBOutlet UILabel *lbl_displayMessage;

@property (nonatomic, strong) NSMutableArray *arr_dashboardItems;

@property (nonatomic , strong) NSString         *str_AM_name;
@property (nonatomic , strong) NSString         *patientId;

@end
