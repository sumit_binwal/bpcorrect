//
//  MeasureBP_AND_VC.m
//  BPCorrect
//
//  Created by "" on 25/04/19.
//  Copyright © 2019 "". All rights reserved.
//

#import "MeasureBP_AND_VC.h"
#import "ADActivityMonitor.h"
#import "ADBloodPressure.h"
#import "ANDDevice.h"
#import "ActivityTracker.h"
#import "MZTimerLabel.h"
#import "ANDActivityDashboardCell.h"
#import <CoreText/CoreText.h>
#import "ANDDateManager.h"
#import "BloodPressureTracker.h"
#import "WeightScale.h"
#import "ANDBloodPressureDashboardCell.h"
#import "ANDWeightScaleDashboardCell.h"
#import "ANDThermometerDashboardCell.h" //Sim added for Thermometer
#import "ADWeightScale.h"
#import "ADThermometer.h" //Sim added for Thermometer
#import "UIAlertView+AlertCustomMethod.h"
#import "ANDMedCoreDataManager.h"

#import "ANDBLEDefines.h" //Sim changes for WELWORLD-29
#import "ANDBLEDefines.h" //Sim changes for WELWORLD-29
#import "BPCorrect-Swift.h"
#import "FMDB.h"

@interface MeasureBP_AND_VC ()<ANDDeviceDelegate,MZTimerLabelDelegate>

@property ANDDevice *device;
@property NSString *type;
@property NSString *display;
@property NSString *protocolNumber;
@property NSMutableArray *arr_activityMonitor;
@property ADActivityMonitor  *activityMonitor_obj;
@property UserModel *user;

@property NSString *protocolID;
@property UIView *view_background;
@property UILabel *lbl_message;

@property NSMutableArray *dateArrayForBPM;

@property (strong, nonatomic) IBOutlet UIView *viewAlert;
@property NSString *activityMonitorUnit;

@property int headerIndex;

@property __weak NSBlockOperation* myWeakOp1;
@property __weak NSBlockOperation* myWeakOp2;

@property NSBlockOperation *operation1;
@property NSInvocationOperation *operation2;

@property UIRefreshControl *refreshControl;
@property NSString *app;
@property (weak, nonatomic) IBOutlet UIView *viewMeasurement;
@property (weak, nonatomic) IBOutlet UILabel *labelCountDownTimer;

//WELWORLD-121 - Support for UW-302
@property (nonatomic, strong) NSMutableArray *scannedCoolActivityTracker;
@end
@implementation MeasureBP_AND_VC

bool didGotActivityData; //UW-302 Used to track the UI blocking for tracker


#pragma mark - Memory management methods...
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - View Initialization/Load/Appear Methods...
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)refresh:(UIRefreshControl *)refreshControl {
}

- (IBAction)okButtonClicked:(id)sender {
    
    [self.viewAlert removeFromSuperview];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    

    self.protocolID = @"";
    [self setTitle:@"AND Blood Pressure Device"];
    self.instructionLabel.layer.cornerRadius = 3.0;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.protocolNumber = @"0";
 

    
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    NSURL *pathURL = [documentsURL URLByAppendingPathComponent:@"BPCorrectdb.db"];
    
    NSString *path = pathURL.absoluteString;
    
    
    
    FMDatabase *db = [FMDatabase databaseWithPath:path];
    NSLog(@"PAATH : %@",path);
    if (![db open]) {
        // [db release];   // uncomment this line in manual referencing code; in ARC, this is not necessary/permitted
        
        db = nil;
        return;
    }
    
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
////  //  self.protocolID = @"fsdfsfs";
//    [self setupFMDBDatabase:params];
    
}


-(void)setupFMDBDatabase:(NSDictionary *)dictVal
{
    // NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"BPCorrectdb.db"];
    self.protocolNumber = @"0";

    if ([_pulseLabel.text isEqualToString:@"Err"])
    {
        return;
    }
    
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    NSURL *pathURL = [documentsURL URLByAppendingPathComponent:@"BPCorrectdb.db"];
    
    NSString *path = pathURL.absoluteString;
    
    
    
    
    FMDatabase *db = [FMDatabase databaseWithPath:path];
    NSLog(@"PAATH : %@",path);
    
    if (![db open]) {
        // [db release];   // uncomment this line in manual referencing code; in ARC, this is not necessary/permitted
        
        db = nil;
        return;
    }
    
    
    NSString *systolic = [NSString stringWithFormat:@"%@",dictVal[@"systolic"]];
    NSString *diastolic = [NSString stringWithFormat:@"%@",dictVal[@"diastolic"]];
    NSString *device_id = [NSString stringWithFormat:@"%@",dictVal[@"device_id"]];
    NSString *pulse_data = [NSString stringWithFormat:@"%@",dictVal[@"pulse_data"]];
    NSString *device_name = [NSString stringWithFormat:@"%@",dictVal[@"device_name"]];
    NSString *device_mac_address = [NSString stringWithFormat:@"%@",dictVal[@"device_mac_address"]];
    NSString *patientId = [NSString stringWithFormat:@"%@",dictVal[@"patientId"]];
    
    NSString *reading_time = [NSString stringWithFormat:@"%@",dictVal[@"reading_time"]];
    NSString *is_abberant = [NSString stringWithFormat:@"%@",dictVal[@"is_abberant"]];
    NSString *protocol_id = [NSString stringWithFormat:@"%@",dictVal[@"protocol_id"]];
    
    
    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM protocol"];
    while ([rs next]) {
        NSString* startDate = [rs objectForColumn:@"startDate"];
        NSString* endDate = [rs objectForColumn:@"endDate"];
        NSString* startTime = [rs objectForColumn:@"startTime"];
        NSString* endTime = [rs objectForColumn:@"endTime"];
        
        NSString* startFullDate = [NSString stringWithFormat:@"%@ %@",startDate,startTime];
        NSString* endFullDate = [NSString stringWithFormat:@"%@ %@",endDate,endTime];
        NSString* startEveningDate = [NSString stringWithFormat:@"%@ %@",startDate,endTime];
        
        NSDate *strtDateTime = [self convertDateFromString:startFullDate withDateFormate:@"yyyy-MM-dd HH:mm"];
        NSDate *startEveningDateTime = [self convertDateFromString:startEveningDate withDateFormate:@"yyyy-MM-dd HH:mm"];
        NSDate  *endDateTime = [self convertDateFromString:endFullDate withDateFormate:@"yyyy-MM-dd HH:mm"];
        
        
        NSLog(@"%@",strtDateTime);
        
        NSDate * now = [NSDate date];
        
        NSDate *someDateInUTC = now;
        NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
        now = [someDateInUTC dateByAddingTimeInterval:timeZoneSeconds];
        
        
        NSString *currentDayDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
        currentDayDateTime = [NSString stringWithFormat:@"%@ %@",currentDayDateTime,startTime];
        NSDate *currentCustomizedDate = [self convertDateFromString:currentDayDateTime withDateFormate:@"yyyy-MM-dd HH:mm"];
        protocol_id = @"";
        if ([now compare:strtDateTime] == NSOrderedDescending)
        {
            if ([now compare:endDateTime] == NSOrderedAscending)
            {
                if ([now compare:currentCustomizedDate] == NSOrderedDescending)
                {
                    NSDate *endProtocolDate = [currentCustomizedDate dateByAddingTimeInterval:30*60];
                    
                    if ([now compare:endProtocolDate] == NSOrderedAscending)
                    {
                        NSLog(@"%@ is in future from %@", endProtocolDate, now);
                        
                        NSString *stringDate = [self stringfromNowDate:@"yyyy-MM-dd HH:mm:ss"];
                        
                        NSString *startingDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
                        startingDateTime = [NSString stringWithFormat:@"%@ %@:00",startingDateTime,startTime];
                        
                        
                        //                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where reading_time < DATETIME(?);",stringDate];
                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where createdTime BETWEEN (?) AND (?) AND protocol_id != '';",startingDateTime,stringDate];                        while ([results next])
                        while ([results next])
                        {
                            int columnCount = [results intForColumn:@"protocol_id"];
                            
                            NSLog(@"%d",columnCount);
                            if (columnCount < 2)
                            {
                                if (columnCount == 0)
                                {
                                    self.protocolNumber = @"1";
                                }
                                if (columnCount == 1)
                                {
                                    self.protocolNumber = @"2";
                                }
                                
                                protocol_id = [NSString stringWithFormat:@"%f",NSDate.date.timeIntervalSince1970];
                                self.protocolID = protocol_id;
                            }
                        }
                    }
                }
            }
        }
        
        
        if ([now compare:startEveningDateTime] == NSOrderedDescending)
        {
            if ([now compare:endDateTime] == NSOrderedAscending)
            {
                NSString *currentDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
                currentDateTime = [NSString stringWithFormat:@"%@ %@",currentDateTime,endTime];
                NSDate *customizedDateTime = [self convertDateFromString:currentDateTime withDateFormate:@"yyyy-MM-dd HH:mm"];

                if ([now compare:customizedDateTime] == NSOrderedDescending)
                {
                    NSDate *endProtocolDate = [customizedDateTime dateByAddingTimeInterval:30*60];
                    
                    if ([now compare:endProtocolDate] == NSOrderedAscending)
                    {
                        NSLog(@"%@ is in future from %@", endProtocolDate, now);
                        
                        NSString *stringDate = [self stringfromNowDate:@"yyyy-MM-dd HH:mm:ss"];
                        
                        NSString *endDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
                        endDateTime = [NSString stringWithFormat:@"%@ %@:00",endDateTime,endTime];

                        
                        //                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where reading_time < DATETIME(?);",stringDate];
                        
                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where createdTime BETWEEN (?) AND (?) AND protocol_id != '';",endDateTime,stringDate];                        while ([results next])
                        {
                            int columnCount = [results intForColumn:@"protocol_id"];
                            
                            NSLog(@"%d",columnCount);
             
                            if (columnCount < 2)
                            {
                                if (columnCount == 0)
                                {
                                    self.protocolNumber = @"1";
                                }
                                if (columnCount == 1)
                                {
                                    self.protocolNumber = @"2";
                                }
                                protocol_id = [NSString stringWithFormat:@"%f",NSDate.date.timeIntervalSince1970];
                                self.protocolID = protocol_id;

                            }
                        }
                    }
                }
            }
        }
    }
    
    NSString *createTimeM = [self stringfromNowDate:@"yyyy-MM-dd HH:mm:ss"];
   // protocol_id = @"fdsfsffsf";
    //Insert Karne Ki Queary
    BOOL success = [db executeUpdate:@"INSERT INTO measureBp(systolic,diastolic,device_id,pulse_data,device_name,device_mac_address,patientId,reading_time,is_abberant,protocol_id,createdTime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", systolic,diastolic,device_id,pulse_data,device_name,device_mac_address,patientId,reading_time,is_abberant,protocol_id,createTimeM];
    if (!success) {
        NSLog(@"error = %@", [db lastErrorMessage]);
    }
    
    
    
    FMResultSet *rs1 = [db executeQuery:@"SELECT * FROM protocol"];
    while ([rs1 next]) {
        NSString* startDate = [rs1 objectForColumn:@"startDate"];
        NSString* endDate = [rs1 objectForColumn:@"endDate"];
        NSString* startTime = [rs1 objectForColumn:@"startTime"];
        NSString* endTime = [rs1 objectForColumn:@"endTime"];
        
        NSString* startFullDate = [NSString stringWithFormat:@"%@ %@",startDate,startTime];
        NSString* endFullDate = [NSString stringWithFormat:@"%@ %@",endDate,endTime];
        NSString* startEveningDate = [NSString stringWithFormat:@"%@ %@",startDate,endTime];
        
        NSDate *strtDateTime = [self convertDateFromString:startFullDate withDateFormate:@"yyyy-MM-dd HH:mm"];
        NSDate *startEveningDateTime = [self convertDateFromString:startEveningDate withDateFormate:@"yyyy-MM-dd HH:mm"];
        NSDate  *endDateTime = [self convertDateFromString:endFullDate withDateFormate:@"yyyy-MM-dd HH:mm"];
        
        
        NSLog(@"%@",strtDateTime);
        
        NSDate * now = [NSDate date];
        
        NSDate *someDateInUTC = now;
        NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
        now = [someDateInUTC dateByAddingTimeInterval:timeZoneSeconds];
        
        
        NSString *currentDayDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
        currentDayDateTime = [NSString stringWithFormat:@"%@ %@",currentDayDateTime,startTime];
        NSDate *currentCustomizedDate = [self convertDateFromString:currentDayDateTime withDateFormate:@"yyyy-MM-dd HH:mm"];
        protocol_id = @"";
        if ([now compare:strtDateTime] == NSOrderedDescending)
        {
            if ([now compare:endDateTime] == NSOrderedAscending)
            {
                if ([now compare:currentCustomizedDate] == NSOrderedDescending)
                {
                    NSDate *endProtocolDate = [currentCustomizedDate dateByAddingTimeInterval:30*60];
                    
                    if ([now compare:endProtocolDate] == NSOrderedAscending)
                    {
                        NSLog(@"%@ is in future from %@", endProtocolDate, now);
                        
                        NSString *stringDate = [self stringfromNowDate:@"yyyy-MM-dd HH:mm:ss"];
                        
                        NSString *startingDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
                        startingDateTime = [NSString stringWithFormat:@"%@ %@:00",startingDateTime,startTime];
                        
                        
                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where createdTime BETWEEN (?) AND (?) AND protocol_id != '';",startingDateTime,stringDate];
                        while ([results next])
                        {
                            int columnCount = [results intForColumn:@"protocol_id"];
                            
                            NSLog(@"%d",columnCount);
                            if (columnCount == 1)
                            {
                                [self performSelector:@selector(appearTimer:) withObject:nil afterDelay:2.0];

                            }
                        }
                    }
                }
            }
        }
        
        
        if ([now compare:startEveningDateTime] == NSOrderedDescending)
        {
            if ([now compare:endDateTime] == NSOrderedAscending)
            {
                NSString *currentDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
                currentDateTime = [NSString stringWithFormat:@"%@ %@",currentDateTime,endTime];
                NSDate *customizedDateTime = [self convertDateFromString:currentDateTime withDateFormate:@"yyyy-MM-dd HH:mm"];
                
                if ([now compare:customizedDateTime] == NSOrderedDescending)
                {
                    NSDate *endProtocolDate = [customizedDateTime dateByAddingTimeInterval:30*60];
                    
                    if ([now compare:endProtocolDate] == NSOrderedAscending)
                    {
                        NSLog(@"%@ is in future from %@", endProtocolDate, now);
                        
                        NSString *stringDate = [self stringfromNowDate:@"yyyy-MM-dd HH:mm:ss"];
                        
                        NSString *endDateTime = [self stringfromNowDate:@"yyyy-MM-dd"];
                        endDateTime = [NSString stringWithFormat:@"%@ %@:00",endDateTime,endTime];
                        
                        
                        //                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where reading_time < DATETIME(?);",stringDate];
                        FMResultSet *results = [db executeQuery:@"SELECT COUNT(*) as protocol_id FROM measureBp where createdTime BETWEEN (?) AND (?) AND protocol_id != '';",endDateTime,stringDate];
                        while ([results next])
                        {
                            int columnCount = [results intForColumn:@"protocol_id"];
                            
                            NSLog(@"%d",columnCount);
                            
                            if (columnCount == 1)
                            {
                                [self performSelector:@selector(appearTimer:) withObject:nil afterDelay:2.0];

                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    db.close;
    
}

-(void)appearTimer:(NSTimer *)timer1 {
    [_viewMeasurement setHidden:true];
    
    MZTimerLabel *timer = [[MZTimerLabel alloc] initWithLabel:_labelCountDownTimer andTimerType:MZTimerLabelTypeTimer];
    [timer setCountDownTime:60];
    [timer startWithEndingBlock:^(NSTimeInterval countTime) {
        [self.viewMeasurement setHidden:false];
        self.labelCountDownTimer.text  = @"";
        self.pulseLabel.text = @"0";
        self.bpReadingsLabel.text = @"0/0";

    }];
    
    [timer setTimeFormat:@"mm:ss"];
    timer.delegate = self;
    [timer start];
}


-(NSDate *)convertDateFromString:(NSString *)dateStr withDateFormate:(NSString *)dateFormate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormate];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    NSTimeInterval timeZoneSeconds1 = [[NSTimeZone localTimeZone] secondsFromGMT];
    date = [date dateByAddingTimeInterval:timeZoneSeconds1];
    return date;
}

-(NSString *)stringfromNowDate:(NSString *)dateFormate
{
    NSDate *dateNow = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormate];
    //    NSTimeInterval timeZoneSeconds1 = [[NSTimeZone localTimeZone] secondsFromGMT];
    //    dateNow = [dateNow dateByAddingTimeInterval:timeZoneSeconds1];
    
    NSString *dateString = [dateFormatter stringFromDate:dateNow];
    
    return dateString;
}

-(NSString *)stringfromCustomeDate:(NSString *)dateFormate date:(NSDate *)dateNow
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormate];
        NSTimeInterval timeZoneSeconds1 = [[NSTimeZone localTimeZone] secondsFromGMT];
        dateNow = [dateNow dateByAddingTimeInterval:timeZoneSeconds1];
    
    NSString *dateString = [dateFormatter stringFromDate:dateNow];
    
    return dateString;
}


-(void) viewWillAppear:(BOOL)animated
{
    self.user = [BPUserDefaults getUserModel];
    
    if (IS_IPHONE_5);
    else
        [self.view_pop_middle setFrame:CGRectMake(20, 100, 274, 200)];
    
#ifdef appType //Code applicable for Global app
    
    //Just retain logic as if there was no server connection : Sim
    [self reloadOnSynDownCompleted];
    //Sim -add code for stand alone server
    if ([Users isAnyRecordExistsInDBForUser:[ANDMedCoreDataManager sharedManager].currentUser] == NO) {
        //Check if their are paired devices
        NSString *deviceName = [[ANDMedCoreDataManager sharedManager].currentUser pairedDevices];
        NSUInteger length = [deviceName length];
        if ((length == 0) || (length <= 2) || (deviceName == NULL)) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Alert.message", @"") message:NSLocalizedString(@"Alert.message.Text", @"") preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
        } else {
            //There is paired device present
            [UIAlertView showAlertWithTitle:NSLocalizedString(@"Alert.message", @"") message:NSLocalizedString(@"Alert.message.Reading.Text", @"")];
        }
        
    }
    
#else //Code applicable for US app
    
    if (![[[ANDMedCoreDataManager sharedManager].currentUser user_name] isEqualToString:@"guest@guest.com"])
    {
        // [self performSelectorOnMainThread:@selector(callToSyndownData) withObject:nil waitUntilDone:YES];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOnSynDownCompleted) name:kNotificationSyncDownComplete object:nil];
        
        [self reloadOnSynDownCompleted];
        
    }
    else
    {
        [self reloadOnSynDownCompleted];
        
    }
#endif
    
    self.navigationController.navigationBarHidden = NO;
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    int count = [self.arr_dashboardItems count];
    NSLog(@"DBG, the count of the array in view will appear %i", count);
    
    DLog(@"viewWillAppear");
    
    
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadBlueToothSettings];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.device stopScanning];
    
}


-(void)reloadBlueToothSettings
{
    //wel-484, UW-302 UI Hack , WELWORLD-121
    self.scannedCoolActivityTracker = [NSMutableArray array];
    
#if TARGET_IPHONE_SIMULATOR
    DLog(@"It is simulator...");
#else
    NSLog(@"DBG, entered the reloadBluetoothSettings");
    [self removeAllPopUp];
    self.device = [ANDDevice new];
    [self.device controlSetup];
    self.device.delegate = self;
    
    
    if ((self.device.activePeripheral.state != CBPeripheralStateConnected) || (self.device.activePeripheral.state != CBPeripheralStateConnecting)) {
        if (self.device.activePeripheral) {
            self.device.peripherials = nil;
        }
        [self.device findBLEPeripherals];
    }
    
#endif
}


#pragma mark - Custom Alert with timer methods.........
-(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message isRemoved:(BOOL)remove
{
    
    
    if (remove == YES)
    {
        @synchronized(self) {
            if (timer_error && timer_error.isValid) {
                [timer_error invalidate];
                timer_error = nil;
            }
        }
        
        [_operation1 cancel];
        [_operation2 cancel];
        timer_error = [NSTimer scheduledTimerWithTimeInterval:3.0f
                                                       target:self
                                                     selector:@selector(removeAllPopUp)
                                                     userInfo:nil
                                                      repeats:NO];
    }
    else
    {
        // this timer is for while we get pairing issue in dashboard screen: AJAY: 24 July 2014....
        @synchronized(self) {
            if (timer_wrongSetup && timer_wrongSetup.isValid) {
                [timer_wrongSetup invalidate];
                timer_wrongSetup = nil;
            }
        }
        
        timer_wrongSetup = [NSTimer scheduledTimerWithTimeInterval:30.0f
                                                            target:self
                                                          selector:@selector(removeAllPopUp)
                                                          userInfo:nil
                                                           repeats:NO];
    }
    
    
    UIView *view_pop = (UIView *)[self.view viewWithTag:9877];
    if (view_pop) {
        self.view_pop_base.hidden = NO;
        [CommonClass slideView:self.view_pop_base withX:0 withY:0 andAnimation:0.5];
        
        SHOW_LOADING();
        
    }
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self->_lbl_popup.text = [NSString stringWithFormat:@"%@ %@",title,message];
                     }];
    
    
}

-(void)removeAllPopUp
{
    self.view_pop_base.hidden = YES;
    self.lbl_popup.text = @"";
    HIDE_LOADING();
}

#pragma mark - Method Called When Sync Gets Completetd
-(void)reloadOnSynDownCompleted
{
    self.arr_dashboardItems = [NSMutableArray array];
    
    [[ANDMedCoreDataManager sharedManager] refreshDataManagerWithUserBloodPressureInfo:^(NSError *error) {
        
        if ([ANDMedCoreDataManager sharedManager].currentBloodPressureInfo) {
            
            NSString *strType = NSStringFromClass([[ANDMedCoreDataManager sharedManager].currentBloodPressureInfo class]);
            if (![self.arr_dashboardItems containsObject:strType])
                [self.arr_dashboardItems addObject:strType];
            
        } else {
            
            NSString *deviceName = [[ANDMedCoreDataManager sharedManager].currentUser pairedDevices];
            if ([deviceName localizedCaseInsensitiveContainsString:@"bp-"]) {
                if (![self.arr_dashboardItems containsObject:@"BloodPressureTracker"])
                    [self.arr_dashboardItems addObject:@"BloodPressureTracker"];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //[self updateViewWithBPReadings:false];
            if (error) {
                [CommonClass showAlertForError:error];
                return;
            }
            else if (!error)
            {
                
                self.viewAlert.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
                
                [self.view addSubview:self.viewAlert];


            }
        });
    }];
    
}

-(void) updateViewWithBPReadings:(BOOL)uploadOnServer {
    
    @try {
        
        NSString *systolic = @"0";
        NSString *diastolic = @"0";
        NSString *pulse = @"0";
        NSString *date = @"";
        
        BloodPressureTracker *tracker = [ANDMedCoreDataManager sharedManager].currentBloodPressureInfo;
        
        if (tracker != nil) {
            
            if ([tracker.systolic intValue] == 65287 | [tracker.systolic intValue] == 2047 | [tracker.systolic intValue] == 255)
            {
                systolic = @"Err";
                diastolic = @"Err";
                pulse = @"Err";
                uploadOnServer = false;
            }
            else
            {
                //WEL-361 - IHB
                if (([tracker.systolic intValue] > 1000) && ([tracker.systolic intValue] != 2047)){
                    int sys = [tracker.systolic intValue];
                    sys = sys - 1000;
                    systolic = NUMBER_INT_TO_STRING([NSNumber numberWithInt:sys]);
                    
                } else {
                    systolic = NUMBER_INT_TO_STRING(tracker.systolic);
                }
                
                diastolic = NUMBER_INT_TO_STRING(tracker.diastolic);
                pulse = NUMBER_INT_TO_STRING(tracker.pulse);
                
            }
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            //Sim WEL-400 , Comment code to support different calendar types
            
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            date = [dateFormatter stringFromDate:tracker.measurementRecieveDateTime];
        }
        
        self.bpReadingsLabel.text = [NSString stringWithFormat:@"%@/%@",systolic,diastolic];
        self.pulseLabel.text = pulse;
        
        NSTimeInterval interval = [tracker.measurementRecieveDateTime timeIntervalSince1970];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setValue:systolic forKey:@"systolic"];
        [params setValue:diastolic forKey:@"diastolic"];
        [params setValue:@"23"  forKey:@"device_id"];
        [params setValue:pulse forKey:@"pulse_data"];
        [params setValue:@"A&D" forKey:@"device_name"];
        [params setValue:@"" forKey:@"device_mac_address"];
        [params setValue:self.patientId forKey:@"patientId"];
        [params setValue:[NSString stringWithFormat:@"%.f",interval] forKey:@"reading_time"];
        [params setValue:@"0" forKey:@"is_abberant"];
        [params setValue:[NSString stringWithFormat:@"%@",self.protocolID] forKey:@"protocol_id"];
        [params setValue:[NSString stringWithFormat:@"%@",self.protocolID] forKey:@"protocol_id"];
        
        //      DataBaseManager *datBase = [DataBaseManager new];
        [self setupFMDBDatabase:params];
        
        
        //DataBaseManager.sharedInstance().insertValuesToCreateProtocol(dictVal: dataDict)
        
        if (uploadOnServer) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
//             //   [self sendBPReadings:systolic
//                           diastolic:diastolic
//                               pulse:pulse
//                                date:tracker.measurementRecieveDateTime];
                [self sendBPReadings:systolic diastolic:diastolic pulse:pulse date:tracker.measurementRecieveDateTime protocolID:self.protocolID protocolNo:self.protocolNumber];
            });
            
        }
    }
    @catch (NSException *exception) {
        //Error
    }
    @finally {
    }
}





-(void)sendBPReadings:(NSString *)systolic
            diastolic:(NSString*)diastolic
                pulse:(NSString*)pulse
                 date:(NSDate*)date
           protocolID:(NSString*)protocolID protocolNo : (NSString*) protocolNo{
    
    if (self.patientId) {
        NSTimeInterval interval = [date timeIntervalSince1970];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setValue:systolic forKey:@"systolic"];
        [params setValue:diastolic forKey:@"diastolic"];
        [params setValue:@"23"  forKey:@"device_id"];
        [params setValue:pulse forKey:@"pulse_data"];
        [params setValue:@"A&D" forKey:@"device_name"];
        [params setValue:@"" forKey:@"device_mac_address"];
        [params setValue:self.patientId forKey:@"patientId"];
        [params setValue:[NSString stringWithFormat:@"%.f",interval] forKey:@"reading_time"];
        [params setValue:@"0" forKey:@"is_abberant"];
        [params setValue:protocolID forKey:@"protocol_id"];
        [params setValue:protocolNo forKey:@"protocol_no"];
        
        
        
        
        NSString *accessToken = [BPUserDefaults accessToken];
        
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        
        NSString *urlStr =  [NSString stringWithFormat:@"http://67.211.223.164:8080/ProtechSentinel/common/add/patient/reading?access_token=%@",accessToken];
        NSURL * url = [NSURL URLWithString:urlStr];
        
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setHTTPMethod:@"POST"];
        
        
        
        if (params) {
            NSError *error;
            NSData *postDeviceData = [NSJSONSerialization dataWithJSONObject:@[params] options:0 error:&error];
            [urlRequest setHTTPBody:postDeviceData];
        }
        
        
        NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSInteger statusCode = httpResponse.statusCode;
            if(200 == statusCode) {
                if (data) {
                    NSError *e = nil;
                    NSDictionary *response = [NSJSONSerialization
                                              JSONObjectWithData: data
                                              options: NSJSONReadingMutableContainers
                                              error: &e];
                    
                    [[APIManager sharedInstance] getAllBPReadings];
                    NSLog(@"%@",response);
                }
            } else {
                
                if (data) {
                    
                    NSError *error;
                    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData: data
                                                                                   options: NSJSONReadingMutableContainers
                                                                                     error: &error];
                    
                    NSLog(@"%@",responseObject);
                }
            }
        }];
        [dataTask resume];
    }
    
    
}


#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100)
    {
        if (buttonIndex == 1) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            UIViewController *vc ;
            
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"DeviceViewControllerNew"]; //Sim modified for the new device setup screen
        }
    }
    else if (alertView.tag == 101) {
        if (buttonIndex == 0)
        {
            [[ANDMedCoreDataManager sharedManager] resetOnLogout];
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
    }
}


#pragma mark - Device Pairing & Handling Methods

// this three methods is for while we get pairing issue in dashboard screen: AJAY: 24 July 2014....
-(void)callTimerWhenDevicePairing
{
    @synchronized(self) {
        if (timer_wrongSetup && timer_wrongSetup.isValid) {
            [timer_wrongSetup invalidate];
            timer_wrongSetup = nil;
        }
    }
    
    timer_wrongSetup = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                                        target:self
                                                      selector:@selector(gotDevicePairing)
                                                      userInfo:nil
                                                       repeats:NO];
}


- (void) gotDevicePairing
{
    @synchronized(self) {
        if (timer_wrongSetup && timer_wrongSetup.isValid) {
            [timer_wrongSetup invalidate];
            timer_wrongSetup = nil;
        }
    }
    
    [self removeAllPopUp];
    
    /*    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning !" message:@"Unable to sync! Please use Device Setup screen for pairing the device" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
     alert.tag = 100;
     [alert show];
     */
}



-(void) gotDeviceWithOutPairing:(NSString *)device_name
{
    [self removeAllPopUp];
    
    if (device_name) {
        // [UIAlertView showAlertWithTitle:@"Warning !" message:[NSString stringWithFormat:@"A new device is try to connect, Please go to device setup screen to pair"]];
        [UIAlertView showAlertWithTitle:NSLocalizedString(@"Device.Warning", @"") message:[NSString stringWithFormat:NSLocalizedString(@"Device.newMessage", @"")]]; //Sim localization WELWORLD-9
    }
}


#pragma mark - ADNDevice Delegate Methods
- (void) deviceReadyWithInfoPeripheral:(CBPeripheral *)peripheral
{
    if ([self.type isEqual:@"bp"])
    {
        // [self showAlertWithTitle:@"Blood Pressure:" andMessage:@"Syncing..." isRemoved:NO];
        [self showAlertWithTitle:NSLocalizedString(@"BloodPressure.Text", @"") andMessage:NSLocalizedString(@"Activity.Sync", @"") isRemoved:NO]; //Sim localization WELWORLD-19, @"Blood Pressure": @"Syncing"
        ADBloodPressure *bp = [[ADBloodPressure alloc]initWithDevice:self.device];
        [bp setTime];
        [bp readMeasurement];
        
        //TODO:: Need to test it properly.....
        [self callTimerWhenDevicePairing];
        
        NSLog(@"BP Device");
    }
    
}

-(void)gotErrorWhileConnectivity:(NSError *)error
{
    return;
    
    /* [UIAlertView showAlertWithTitle:@"Error"
     message:[NSString stringWithFormat:@"%@",[error description]]];*/
    [UIAlertView showAlertWithTitle:NSLocalizedString(@"Alert.Error.Title", @"") //Sim localization WELWORLD-19 ,@"Error"
                            message:[NSString stringWithFormat:@"%@",[error description]]];
}




-(void)deviceDisconnectWithDeviceName:(NSString *)device_name
{
    NSLog(@"PERIPHERAL Name = %@",device_name);
    if ([device_name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
    {
        DLog(@"ws");
        
        // [self showAlertWithTitle:@"Weight Scale:" andMessage:@"Disconnected" isRemoved:YES];
        [self showAlertWithTitle:NSLocalizedString(@"WeightScale.Text", @"") andMessage:NSLocalizedString(@"Alert.Disconnected", @"") isRemoved:YES]; //Sim localization WELWORLD-19
        //Construct the URL logic
#ifndef appType
        if (isURLLaunch) {
            //Check if the deviceType measurement is BloodPressure
            if ([measurementType isEqualToStringCaseInsensitive:@"Weight"]) {
                //URL Launched, need to send back the systolic and diastolic value and launch URL
                NSArray *urlString = [self parseReturnURL:launchURL];
                NSString * finalURL;
                for (NSString *pair in urlString) {
                    if ([pair containsString:@"ReturnURL="]) {
                        finalURL = [pair stringByReplacingOccurrencesOfString:@"ReturnURL=" withString:@""];
                        break;
                    } else {
                        continue;
                    }
                }
                finalURL = @"lstathome://measurement?"; //hack remove later
                NSLog(@"The return URL at this point is %@",finalURL);
                
                NSInteger count = 0;
                if (measurementTime != NULL)
                    count = [measurementTime count];
                
                NSString *wsURL = @"";
                for (int i = 1; i <=count; i++) {
                    NSString *indexString = [NSString stringWithFormat:@"[%@]", [NSString stringWithFormat:@"%d", i]];
                    wsURL = [wsURL stringByAppendingString:indexString];
                    wsURL = [wsURL stringByAppendingString:@".type=weight&"];
                    wsURL = [wsURL stringByAppendingString:indexString];
                    wsURL = [wsURL stringByAppendingString:@".weight="];
                    NSString *weightValue = [[measurementValue1 objectAtIndex:i -1] stringValue];
                    wsURL = [wsURL stringByAppendingString:weightValue];
                    wsURL = [wsURL stringByAppendingString:@"&"];
                    wsURL = [wsURL stringByAppendingString:indexString];
                    wsURL = [wsURL stringByAppendingString:@".unit="];
                    NSString *unitValue = [[measurementValue2 objectAtIndex:i -1] stringValue];
                    wsURL = [wsURL stringByAppendingString:unitValue];
                    wsURL = [wsURL stringByAppendingString:@"&"];
                    wsURL = [wsURL stringByAppendingString:indexString];
                    if (i == count) {
                        wsURL = [wsURL stringByAppendingString:@".timestamp="];
                        NSString *timeValue = [measurementTime objectAtIndex:i -1];
                        wsURL = [wsURL stringByAppendingString:timeValue];
                    }
                    
                    else {
                        wsURL = [wsURL stringByAppendingString:@".timestamp="];
                        NSString *timeValue = [measurementTime objectAtIndex:i -1];
                        wsURL = [wsURL stringByAppendingString:timeValue];
                        wsURL = [wsURL stringByAppendingString:@"&"];
                    }
                    
                }
                NSLog(@"Sim. the prototype URL created is %@", wsURL);
                //This URL is sent back for now
                
                //Reinitializing the values and then calling the application
                isURLLaunch = false;
                [AppDelegate sharedDelegate].launchURL = @"";
                launchURL = @"";
                returnURL = @"" ;
                measurementType = @"";
                
                //Logic to delete all Arrays and Dictionary
                [measurementValue1 removeAllObjects];
                [measurementValue2 removeAllObjects];
                [measurementValue3 removeAllObjects];
                [measurementTime removeAllObjects];
                
                //Add Logic to delete this data from the local database -
                WeightScale *tracker = [ANDMedCoreDataManager sharedManager].currentWeightScaleInfo;
                NSError *err = nil;
                [[ANDMedCoreDataManager sharedManager].managedObjectContext deleteObject:tracker];
                [[ANDMedCoreDataManager sharedManager].managedObjectContext save:&err];
                
                err = nil;
                [[ANDMedCoreDataManager sharedManager].fetchedControllerWeightScale performFetch:&err];
                //[_tblView_obj reloadData]; //Check the impact of this line
                
                finalURL = [finalURL stringByAppendingString:wsURL];
                
                NSLog(@"DBG, the final return URL is %@", finalURL);
                if ([[UIApplication sharedApplication]
                     canOpenURL:[NSURL URLWithString:finalURL]])
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL]];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"URL error"
                                                                    message:[NSString stringWithFormat:
                                                                             @"No custom URL defined "]
                                                                   delegate:self cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            
        }
        
#endif
        
        
    }
    else if([device_name rangeOfString:@"651"].location != NSNotFound ||[device_name rangeOfString:@"BLP"].location != NSNotFound)
    {
        DLog(@"bp");
        // [self showAlertWithTitle:@"Blood Pressure:" andMessage:@"Disconnected" isRemoved:YES];
        [self showAlertWithTitle:NSLocalizedString(@"BloodPressure.Text", @"") andMessage:NSLocalizedString(@"Alert.Disconnected", @"") isRemoved:YES];//Sim localization WELWORLD-19
        
        
        
#ifndef appType
        //Add code to process the information if its URL launched scenario
        if (isURLLaunch) {
            
            //Check if the deviceType measurement is BloodPressure
            if ([measurementType isEqualToStringCaseInsensitive:@"BloodPressure"]) {
                
                //URL Launched, need to send back the systolic and diastolic value and launch URL
                NSArray *urlString = [self parseReturnURL:launchURL];
                
                //Add Logic here to retrieve the systolic , diastolic, pulse and time measurement values
                NSString * finalURL;
                for (NSString *pair in urlString) {
                    if ([pair containsString:@"ReturnURL="]) {
                        finalURL = [pair stringByReplacingOccurrencesOfString:@"ReturnURL=" withString:@""];
                        break;
                    } else {
                        continue;
                    }
                }
                finalURL = @"lstathome://measurement?";
                NSLog(@"The return URL at this point is %@",finalURL);
                
                NSInteger count = 0;
                if (measurementTime != NULL)
                    count = [measurementTime count];
                
                NSString *bpURL = @"";
                for (int i = 1; i <=count; i++) {
                    NSString *indexString = [NSString stringWithFormat:@"[%@]", [NSString stringWithFormat:@"%d", i]];
                    bpURL = [bpURL stringByAppendingString:indexString];
                    bpURL = [bpURL stringByAppendingString:@".type=blood_pressure&"];
                    bpURL = [bpURL stringByAppendingString:indexString];
                    bpURL = [bpURL stringByAppendingString:@".systolic="];
                    NSString *systolicValue = [[measurementValue1 objectAtIndex:i -1] stringValue];
                    bpURL = [bpURL stringByAppendingString:systolicValue];
                    bpURL = [bpURL stringByAppendingString:@"&"];
                    bpURL = [bpURL stringByAppendingString:indexString];
                    bpURL = [bpURL stringByAppendingString:@".diastolic="];
                    NSString *diastolicValue = [[measurementValue2 objectAtIndex:i -1] stringValue];
                    bpURL = [bpURL stringByAppendingString:diastolicValue];
                    bpURL = [bpURL stringByAppendingString:@"&"];
                    bpURL = [bpURL stringByAppendingString:indexString];
                    bpURL = [bpURL stringByAppendingString:@".heartrate="];
                    NSString *heartrateValue = [[measurementValue3 objectAtIndex:i -1] stringValue];
                    bpURL = [bpURL stringByAppendingString:heartrateValue];
                    bpURL = [bpURL stringByAppendingString:@"&"];
                    bpURL = [bpURL stringByAppendingString:indexString];
                    if (i == count) {
                        bpURL = [bpURL stringByAppendingString:@".timestamp="];
                        NSString *timeValue = [measurementTime objectAtIndex:i -1];
                        bpURL = [bpURL stringByAppendingString:timeValue];
                    }
                    
                    else {
                        bpURL = [bpURL stringByAppendingString:@".timestamp="];
                        NSString *timeValue = [measurementTime objectAtIndex:i -1];
                        bpURL = [bpURL stringByAppendingString:timeValue];
                        bpURL = [bpURL stringByAppendingString:@"&"];
                    }
                    
                }
                NSLog(@"Sim. the prototype URL created is %@", bpURL);
                //This URL is sent back for now
                
                //Reinitializing the values and then calling the application
                isURLLaunch = false;
                [AppDelegate sharedDelegate].launchURL = @"";
                launchURL = @"";
                returnURL = @"" ;
                measurementType = @"";
                
                //Logic to delete all Arrays and Dictionary
                [measurementValue1 removeAllObjects];
                [measurementValue2 removeAllObjects];
                [measurementValue3 removeAllObjects];
                [measurementTime removeAllObjects];
                
                //Add Logic to delete this data from the local database - (Query all based the time stamp filter)
                BloodPressureTracker *tracker = [ANDMedCoreDataManager sharedManager].currentBloodPressureInfo;
                NSError *err = nil;
                //Query based on the time stamp and delete
                
                [[ANDMedCoreDataManager sharedManager].managedObjectContext deleteObject:tracker];
                [[ANDMedCoreDataManager sharedManager].managedObjectContext save:&err];
                
                err = nil;
                [[ANDMedCoreDataManager sharedManager].fetchedControllerBloodPressure performFetch:&err];
                
                finalURL = [finalURL stringByAppendingString:bpURL];
                NSLog(@"DBG, the final return URL is %@", finalURL);
                if ([[UIApplication sharedApplication]
                     canOpenURL:[NSURL URLWithString:finalURL]])
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalURL]];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"URL error"
                                                                    message:[NSString stringWithFormat:
                                                                             @"No custom URL defined "]
                                                                   delegate:self cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            
        }
        
#endif
        
        
        
    }
#ifdef appType //Thermometer is applicable only for global
    else if ([device_name rangeOfString:@"A&D_UT201"].location != NSNotFound) //Sim added for Thermometer
    {
        DLog(@"th");
        
        [self showAlertWithTitle:NSLocalizedString(@"Device.Thermometer.MainDashboard", @"") andMessage:NSLocalizedString(@"Alert.Disconnected", @"") isRemoved:YES]; //Sim modified for Thermometer
        
        
        
    }
#endif
    else if ([device_name rangeOfString:@"Life Trak"].location != NSNotFound)
    {
        
        DLog(@"am");
    }
    else if ([device_name rangeOfString:@"UW-302BLE"].location != NSNotFound)
    {
        
        DLog(@"Entered case of UW-302 tracker");
        if (!didGotActivityData) {
            //   [self showAlertWithTitle:NSLocalizedString(@"ActivityMonitor.Heading", @"") andMessage:NSLocalizedString(@"Alert.Disconnected", @"") isRemoved:YES];
            
        }
        [self.device findBLEPeripherals];//Need to refresh Sync icon
        
    }
}


- (void) gotDevice:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData
{
    NSLog(@"PERIPHERAL UUID  = %@",peripheral.identifier);
    
    if (peripheral.name != nil)
    {
        NSLog(@"PERIPHERAL Name = %@",peripheral.name);
        if ([peripheral.name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
        {
            DLog(@"ws");
            NSArray *primaryServices = peripheral.services;
            for (CBService *service in primaryServices) {
                CBUUID *uuid = service.UUID;
                NSLog(@"Sim , the uuid is %@", uuid);
                
            }
            self.type = @"ws";
            [self.device connectPeripheral:peripheral];
        }
        else if([peripheral.name rangeOfString:@"651"].location != NSNotFound ||[peripheral.name rangeOfString:@"BLP"].location != NSNotFound)
        {
            DLog(@"bp");
            self.type = @"bp";
            [self.device connectPeripheral:peripheral];
        }
        //WEL-484 , Implementation of UW-302
        else if ([peripheral.name rangeOfString:@"UW-302BLE"].location != NSNotFound)
        {
            Users *loginUser = [ANDMedCoreDataManager sharedManager].currentUser;
            NSLog(@"Found tracker and loginUser stored for tracker is %@", loginUser.uw_name);
            NSLog(@"DBG, the advertisement data is %@", advertisementData);
            
            //Do additional checks based on the broadcast information
            if ([peripheral.name isEqualToString:loginUser.uw_name]) {
                NSLog(@"case of ad activity tracker with the name match");
                //SIM TODO, also add the state of advertising to connect only if it is 690001
                NSData *adv = [advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
                NSInteger length = [adv length];
                if (length >= 3) {
                    int state = *(int *)[[adv subdataWithRange:NSMakeRange(2, 1)] bytes];
                    if (state == 1) {
                        self.type = @"uw_302";
                        //[self.scannedCoolActivityTracker removeAllObjects];
                        [self addScannedActivityCoolDevice:peripheral];
                        didGotActivityData = false;
                        NSLog(@"Discovered an advertising uw-302");
                        //WELWORLD-121 , Update Icon showing data is present
                        if (peripheral != nil) {
                            // [activityCell.activityIcon setImage:[UIImage imageNamed:@"syncIcon_purple"]];
                        }
                        
                        // [self.device deviceTypeConnect:@"uw_302"];
                        // [self.device connectPeripheral:peripheral];
                        
                    }
                }
                
                
            }
            
            
        }
#ifdef appType //Change applicable only for Global app
        else if ([peripheral.name rangeOfString:@"A&D_UT201"].location != NSNotFound) //Sim added for Thermometer
        {
            DLog(@"th");
            self.type = @"th";
            [self.device connectPeripheral:peripheral];
        }
#endif
        //#ifndef appType //If its case of US app -WELWORLD-121
        else if ([peripheral.name rangeOfString:@"Life"].location != NSNotFound)
        {
            [self.device stopScanning];
            self.str_AM_name = peripheral.name;
            
#if !TARGET_IPHONE_SIMULATOR
            
            
#endif
            DLog(@"am");
            self.type = @"am";
            //  [self.device connectPeripheral:peripheral];
        }
    }
}


- (void) deviceReady
{
    DLog(@"deviceReady called ");
    if ([self.type isEqual:@"am"]) {
        ADActivityMonitor *am = [[ADActivityMonitor alloc] initWithDevice:self.device];
        [am readHeader];
    }
}

#pragma mark - Blood Preasure Data Methods....

-(void)gotBloodPressureCufError:(NSString *)device_name
{
    if ([device_name isEqualToString:@"bp"]) {
        //   [self showAlertWithTitle:@"Blood Pressure:" andMessage:@"Cuf Error" isRemoved:YES];
        [self showAlertWithTitle:NSLocalizedString(@"BloodPressure.Text", @"") andMessage:NSLocalizedString(@"BloodPressure.Error", @"") isRemoved:YES]; ////Sim localization WELWORLD-19 @"Blood Pressure:" @"Cuf Error"
    }
}

- (void) gotBloodPressure:(NSMutableDictionary *)dictData forPeripheral:(CBPeripheral *)peripheral
{
    NSMutableArray *arrayBP = [[NSMutableArray alloc] init];
    [arrayBP addObject:dictData];
    NSLog(@"entered got blood pressure in dash");
    @try
    {
        if (dictData) {
            [self showAlertWithTitle:NSLocalizedString(@"BloodPressure.Text", @"") andMessage:NSLocalizedString(@"Activity.Synced", @"") isRemoved:NO]; //Sim localization WELWORLD-19 @"Blood Pressure" @"Synced"
        }
        
        
#ifndef appType
        if (!isURLLaunch) //For US app, only if its not URL launch scenario then write for Healthkit and HealthVault
#endif
            
#ifdef appType //Sim-code for global app
            NSError *error = nil;
        
        Users *objUser = [Users getUserWithName:@"guest@guest.com"
                                    andPassword:@"guest"
                                      inContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                          error:&error];
        [BloodPressureTracker insertORUpdateBloodPressureRecords:arrayBP
                                                         forUser:objUser
                                                      forContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                                           error:&error];
        
        if (error) {
            [_tblView_obj reloadData];
            [CommonClass showAlertForError:error];
            return;
        }
        
#else //Sim - code for the US app
        
        NSError *error = nil;
        [BloodPressureTracker insertORUpdateBloodPressureRecords:arrayBP
                                                         forUser:[ANDMedCoreDataManager sharedManager].currentUser
                                                      forContext:[ANDMedCoreDataManager sharedManager].managedObjectContext
                                                           error:&error];
        if (error) {
            [_tblView_obj reloadData];
            [CommonClass showAlertForError:error];
            return;
        }
        if (!([[ANDMedCoreDataManager sharedManager].currentUser.user_name isEqualToString:@"guest@guest.com"]) || !([ANDMedCoreDataManager sharedManager].currentUser.auth_token == nil))
        {
            if (!isURLLaunch) { //Sync with cloud only when its not URL launched app
                // Syncing with server
                [[ANDSyncUpManager sharedManager] syncUpDataWithPersistanceCoordinator:[ANDMedCoreDataManager sharedManager].persistentStoreCoordinator
                                                                               forUser:[ANDMedCoreDataManager sharedManager].currentUser];
            }
            
            
        }
        
#endif
        
        
        
        
        [[ANDMedCoreDataManager sharedManager] refreshDataManagerWithUserBloodPressureInfo:^(NSError *error) {
            
            NSString *strClassName = NSStringFromClass([BloodPressureTracker class]);
            BloodPressureTracker *tr = [ANDMedCoreDataManager sharedManager].currentBloodPressureInfo;
            
            
            if ([self.arr_dashboardItems containsObject:strClassName]== NO && tr) {
                [self.arr_dashboardItems addObject:strClassName];
            }
            else if (!tr) {
                [self.arr_dashboardItems removeObject:strClassName];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [_tblView_obj reloadData];
                [self updateViewWithBPReadings:true];
                if (error) {
                    [CommonClass showAlertForError:error];
                    return;
                }
            });
        }];
        
#ifndef appType
        //Add code to process the information if its URL launched scenario
        if (isURLLaunch) {
            //Saving the value to the Dictionary
            if ([measurementType isEqualToStringCaseInsensitive:@"BloodPressure"]) {
                int num_systolic = [[dictData objectForKey:BP_DATA_KEY_SYSTOLIC] intValue];
                if ((num_systolic > 1000) && (num_systolic != 2047))
                    num_systolic = num_systolic - 1000; //IHB WEL-361
                int num_diastolic = [[dictData objectForKey:BP_DATA_KEY_DIASTOLIC] intValue];
                int pulseValue = [[dictData objectForKey:BP_DATA_KEY_PULSE] intValue];
                NSDate *reading_date =[dictData objectForKey:BP_USERINFO_KEY_MEASUREMENTDATETIME];
                NSString *str_postDate = [CommonClass getTimeStampFromDate:reading_date];
                
                
                [measurementValue1 addObject:[NSNumber numberWithInt:num_systolic]];
                [measurementValue2 addObject:[NSNumber numberWithInt:num_diastolic]];
                [measurementValue3 addObject:[NSNumber numberWithInt:pulseValue]];
                [measurementTime addObject:str_postDate];
                
                /* [measuremeantList setObject:measurementValue1 forKey: @"systolic"];
                 [measuremeantList setObject:measurementValue2 forKey: @"diastolic"];
                 [measuremeantList setObject:measurementValue3 forKey: @"heartrate"];
                 [measuremeantList setObject:measurementTime forKey: @"timestamp"];*/
                
            }
            
        }
        
#endif
        
    }
    @catch (NSException *exception) {
        NSError *error = [exception exceptionError];
        [CommonClass showAlertForError:error];
    }
    @finally {
    }
    
}




- (void)trackerReconnect:(CBPeripheral *)peripheral {
    NSLog(@"DBG, start the tracker reconnect");
    Users *loginUser = [ANDMedCoreDataManager sharedManager].currentUser;
    if ([peripheral.name isEqualToString:loginUser.uw_name]) {
        NSLog(@"case of ad activity tracker with the name match to reconnect");
        self.type = @"uw_302";
        [self.device deviceTypeConnect:@"uw_302"];
        [self.device connectPeripheral:peripheral];
    }
    
}



#if !TARGET_IPHONE_SIMULATOR

- (NSArray *)reversedArray:(NSMutableArray *)temp
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[temp count]];
    NSEnumerator *enumerator = [temp reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}

#endif

//#endif - WELWORLD-121


#pragma mark - SlideNavigationController Delegate
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    
    return YES;
}

- (UIImage *) screenshot {
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
#pragma mark - getAttributesStringHavingUnitSuffix
-(NSMutableAttributedString *) getAttributesStringHavingUnitSuffix:(NSString *)unitValue inString:(NSString *)fullValue withColor:(UIColor *)fullColor
{
    NSRange unitRange = [fullValue rangeOfString:unitValue];
    NSMutableAttributedString* calAttrString = [[NSMutableAttributedString alloc] initWithString:fullValue];
    [calAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:24] range:NSMakeRange(0, unitRange.location)];
    [calAttrString addAttribute:NSForegroundColorAttributeName value:fullColor range:NSMakeRange(0, unitRange.location)];
    [calAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:14] range:unitRange];
    [calAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:unitRange];
    
    return calAttrString;
}




//WELWORLD-121
#pragma mark - Button event for Activity
-(void) onSyncButtonClick:(UIButton*)sender
{
    NSLog(@"DBG, started the onSyncButton click function");
    //if (self.scannedCoolActivityTracker.count == 1) {
    //Hack, remove later and uncomment the commented code
    CBPeripheral *peripheral = [self.scannedCoolActivityTracker lastObject];
    int length = [self.scannedCoolActivityTracker count];
    NSLog(@"DBG, the length of the device array is %lu", length);
    if ([peripheral.name rangeOfString:@"UW-302BLE"].location != NSNotFound) {
        if (peripheral != nil ) {
            [self.device stopScanning];
            [self.scannedCoolActivityTracker removeAllObjects];
            [self.device deviceTypeConnect:@"uw_302"];
            NSLog(@"DBG, starting the onSyncButtonClick function");
            [self.device connectPeripheral:peripheral];
            
        } else {
            NSLog(@"Peripheral is nil so change the icon");
            //[activityCell.activityIcon setImage:[UIImage imageNamed:@"activity-dashboard-icon"]];
        }
        
    }
    
    //  }
    
    
    
}
#ifndef appType  //This is only applicable for US app

//Adding logic to parse string for URL Launch method
-(void) parseQueryString:(NSString *)query {
    NSLog(@"DBG, the query string is %@", query);
    //Split the Query to distinguish between the Device and the return URL
    NSMutableDictionary *dict =[[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [dict setObject:val forKey:key];
    }
    
    //Parse the Value here and display an alert dialogue and then set a global variable
    measurementType = [dict valueForKey:@"Device"];
    NSString *retUrl = [dict valueForKey:@"ReturnURL"];
    
    //Hardcode the check for measurementType and returnURL - //SIM TODO - Add some better checks
    NSString *displayMessage = @"";
    
    if (retUrl.length != 0) {
        if ([measurementType isEqualToStringCaseInsensitive:@"BloodPressure"]) {
            isURLLaunch = true;
            displayMessage = @"Please Take a Blood Pressure Measurement";
            
        } else if ([measurementType isEqualToStringCaseInsensitive:@"Weight"]) {
            isURLLaunch = true;
            displayMessage = @"Please Weigh Yourself";
            
        }
        
        //Reinitiazlise this so that legacy code does not display the dialogue
        [AppDelegate sharedDelegate].launchURL = @"";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Take Measurement"
                                                        message:[NSString stringWithFormat:
                                                                 displayMessage]
                                                       delegate:self cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
        
        
        
    }
    
}

//Adding logic to parse string for URL Launch method
-(NSArray *) parseReturnURL:(NSString *)returnURL {
    
    //Split the Query to distinguish between the Device and the return URL
    NSArray *pairs = [returnURL componentsSeparatedByString:@"&"];
    return pairs;
    
}

#endif


//Utility function to get the age of the user
#pragma mark - Get Age From date
-(NSString *)getAgeFromDatePickerDate:(NSDate *)pickerDate
{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    
    NSNumber *num_dateOfBirth = pickerDate ? [NSNumber numberWithLongLong:[[CommonClass getTimeStampFromDate:pickerDate] longLongValue]] : @(0);
    
    NSLog(@"str_dateOfBirth %@",num_dateOfBirth);
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                               fromDate:pickerDate
                                                 toDate:currentDate
                                                options:0];
    NSLog(@"Difference in date components: %i/%i/%i", components.day, components.month, components.year);
    
    return [NSString stringWithFormat:@"%d",components.year];
    
}


//Customize alert function for activity
#pragma mark - Custom Alert with timer methods.........
-(void)showAlertWithTitleForActivity:(NSString *)title andMessage:(NSString *)message
{
    UIView *view_pop = (UIView *)[self.view viewWithTag:9877];
    if (view_pop) {
        self.view_pop_base.hidden = NO;
        [CommonClass slideView:self.view_pop_base withX:0 withY:0 andAnimation:0.5];
        
        SHOW_LOADING();
        
    }
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         _lbl_popup.text = [NSString stringWithFormat:@"%@ %@",title,message];
                     }];
    
    
}

-(void)removeAllPopUpActivity
{
    self.view_pop_base.hidden = YES;
    self.lbl_popup.text = @"";
    HIDE_LOADING();
}

- (void)addScannedActivityCoolDevice:(CBPeripheral *)peripheral
{
    NSInteger index = [self.scannedCoolActivityTracker indexOfObject:peripheral];
    if (index != NSNotFound)
    {
        [self.scannedCoolActivityTracker replaceObjectAtIndex:index withObject:peripheral];
    }
    else
    {
        [self.scannedCoolActivityTracker addObject:peripheral];
    }
}
static inline char itoh(int i) {
    if (i > 9) return 'A' + (i - 10);
    return '0' + i;
}

@end

