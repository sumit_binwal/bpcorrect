//
//  MyBPVCReadingsVC.swift
//  BPCorrect
//
//  Created by "" on 12/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit
import CoreData
import UICircularProgressRing

class MyBPVCReadingsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var vwReading: UIView!
    
    @IBOutlet weak var vwBloodPressure: UIView!
    @IBOutlet weak var labelStartEndDate: UILabel!
    @IBOutlet weak var labelReadingMissed: UILabel!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var labelReadingTaken: UILabel!
    @IBOutlet weak var progressView: UICircularProgressRing!
    @IBOutlet weak var labelHeartAvarage: UILabel!
    @IBOutlet weak var labelAvrageSyst: UILabel!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet var scheduledView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var tableView0: UITableView!
    @IBOutlet var getStartedView: UIView!
    var startDatetime = String()
    var endDateTime = String()

    var bpReadings : [ActualValue]?
  var actualValues : [ActualValue]?
    var arryDataValue = [[String:Any]]()
    var arryProtocolValue = [[String:Any]]()

    var managedObjectContext: NSManagedObjectContext? = nil
  
    override func viewDidLoad() {
      super.viewDidLoad()
      self.setNavigationBarItem()
      self.title = "My Blood Pressure Readings"
        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    managedObjectContext = appDelegate.persistentContainer.viewContext
    loadBPReadings()
    self.tableView0.tableFooterView = UIView()
    
    let leftButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "scheduleIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.toggleRgt))
    navigationItem.rightBarButtonItem = leftButton
    
    vwBloodPressure.layer.cornerRadius = 5

    vwReading.layer.cornerRadius = 5
    vwReading.layer.borderColor = UIColor.gray.cgColor
    vwReading.layer.borderWidth = 1

    if self.view.subviews .contains(scheduledView)
    {
        scheduledView .removeFromSuperview()
        

    }
    if self.view.subviews .contains(getStartedView)
    {
        getStartedView .removeFromSuperview()
    }
    
    tableView0.isHidden = true
scheduledButtonClicked(self)
    
    if DataBaseManager.sharedInstance().checkIfAlarmDataExist()
    {
        let alarmData = DataBaseManager.sharedInstance().getAllActiveAlarmData()
        if alarmData.count > 0
        {//"yyyy-MM-dd HH:mm"
            if let updatedTime = alarmData["updatedTime"] as? String
            {
            let startDate = UtilityClass.getDateFromDateString(updatedTime, "yyyy-MM-dd HH:mm")
                var startTimestamp = "\(Int(startDate.timeIntervalSince1970))"
                startTimestamp = startTimestamp.replacingOccurrences(of: ".", with: "")
                var endTimestamp = "\(Int(NSDate().timeIntervalSince1970))"
                endTimestamp = endTimestamp.replacingOccurrences(of: ".", with: "")
          //  getProtoColDataFromServer()
            getBpReadingData(fromDate: startTimestamp, toDate: endTimestamp)

        }
    }

  }
    }
    @IBAction func infoBtnClickAction(_ sender: UIButton)
    {
        var alertString = String()
        switch sender.tag {
        case 0:
            alertString = "your blood pressure falls within the normal range."
            break
        case 1:
            alertString =  "According to the American Heart Association you have elevated blood pressure. People with elevated blood pressure are likely to develop high blood pressure unless steps are taken to control the blood pressure."
            break
        case 2:
            alertString =  "According to the American Heart Association, you have Hypertension Stage 1. At this stage of high blood pressure, doctors are likely to prescribe lifestyle changes and may consider adding blood pressure medication based on your risk of atherosclerotic cardiovascular disease (ASCVD), such as heart attack or stroke."

            break
        case 3:
            alertString =  "According to the American Heart Association, you have Hypertension Stage 2. At this stage of high blood pressure, doctors are likely to prescribe a combination of blood pressure medications and lifestyle changes"
            break
        case 4:
            alertString =  "Anytime a blood pressure is obtained and it is higher than 180/120, please include this text:"
            break
            
        default:
            break
        }
        UIAlertController.showAlertWith(title: String.MyApp.AppName, message: alertString) {
            
        }
    }
    @objc func toggleRgt() {
        
        if DataBaseManager.sharedInstance().checkIfAlarmDataExist()
        {
            let storyboardNew = UIStoryboard(name: "NewScreens", bundle: nil)
            let activeAlarm = storyboardNew.instantiateViewController(withIdentifier: "ActiveAlarmViewController") as! ActiveAlarmViewController
            self.navigationController?.pushViewController(activeAlarm, animated: true)
        }
        else
        {
            let storyboardNew = UIStoryboard(name: "NewScreens", bundle: nil)
            let notification = storyboardNew.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(notification, animated: true)
        }
    }

  
    @IBAction func addButtonClikcedAction(_ sender: Any)
    {

            let storyboardNew = UIStoryboard(name: "NewScreens", bundle: nil)
            let notification = storyboardNew.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(notification, animated: true)

    }

    func timeStringFromUnixTime(unixTime: Double) -> String {
    let date = NSDate(timeIntervalSince1970: unixTime)
    let dateFormatter = DateFormatter()
    // Returns date formatted as 12 hour time.
    dateFormatter.dateFormat = "hh:mm a"
    return dateFormatter.string(from: date as Date)
  }
  
  func dayStringFromTime(unixTime: Double) -> String {
    let date = NSDate(timeIntervalSince1970: unixTime)
    let dateFormatter = DateFormatter()
    dateFormatter.locale = NSLocale(localeIdentifier: NSLocale.current.identifier) as Locale
    dateFormatter.dateFormat = "MMM dd YYYY"
    return dateFormatter.string(from: date as Date)
  }
  
    @IBAction func scheduledButtonClicked(_ sender: AnyObject?) {
        
        var strStartDateTime = String()
        if DataBaseManager.sharedInstance().checkIfAlarmDataExist()
        {
            let alarmData = DataBaseManager.sharedInstance().getAllActiveAlarmData()
                        if alarmData.count > 0
            {
                let startDate = UtilityClass.getDateStringFromStr(givenDateStr: alarmData["startDate"] as! String, andCurrentDateFormat: "yyyy-MM-dd", andResultingDateFormat: "yyyy MMM, dd")
                

                let endDate = UtilityClass.getDateStringFromStr(givenDateStr: alarmData["endDate"] as! String, andCurrentDateFormat: "yyyy-MM-dd", andResultingDateFormat: "yyyy MMM, dd")

                
                let endDate1 = UtilityClass.getDateStringFromStr(givenDateStr: alarmData["endDate"] as! String, andCurrentDateFormat: "yyyy-MM-dd", andResultingDateFormat: "yyyy-MM-dd")

                let startDate1 = UtilityClass.getDateStringFromStr(givenDateStr: alarmData["startDate"] as! String, andCurrentDateFormat: "yyyy-MM-dd", andResultingDateFormat: "yyyy-MM-dd")

                
                startDatetime = "\(startDate1) \(alarmData["startTime"] as! String):00"
                endDateTime = "\(endDate1) \(alarmData["endTime"] as! String):00"
                
                strStartDateTime = "\(startDate1) \(alarmData["startTime"] as! String)"
                
                
                labelStartEndDate.text = "\(startDate)-\(endDate)"
                
            }

            
            let countProtocolVal = DataBaseManager.sharedInstance().getAllProtocolReadingData(startDate: startDatetime, endDate: endDateTime)
            scheduledView.frame = CGRect.init(x: 0, y: buttonView.frame.origin.y + buttonView.frame.height , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - (buttonView.frame.origin.y + buttonView.frame.height))
            self.view .addSubview(scheduledView)
            
            let readerData = DataBaseManager.sharedInstance().getAllProtocolReadingRecordData(startDate: startDatetime, endDate: endDateTime)
            if readerData.count > 0
            {
                arryProtocolValue = readerData
                tableView1.reloadData()
                var systolic = Int()
                var diastolic = Int()
                var pulseData = Int()
                for dictVal in readerData
                {
//                    let systolic = rs.string(forColumn: "systolic")
//                    let diastolic = rs.string(forColumn: "diastolic")
//                    let device_id = rs.string(forColumn: "device_id")
//                    let pulse_data = rs.string(forColumn: "pulse_data")
                    if let sysData = dictVal["systolic"] as? String, sysData != "(null)"
                    {
                        systolic += Int(sysData)!
                        diastolic += Int(dictVal["diastolic"] as! String)!
                        pulseData += Int(dictVal["pulse_data"] as! String)!
                        
                        let systolicAvg = systolic/countProtocolVal
                        let diastolicAvg = diastolic/countProtocolVal
                        let pulseAvg = pulseData/countProtocolVal
                        
                        
                        if systolicAvg < 120 && diastolicAvg < 80
                        {
                            vwBloodPressure.backgroundColor = UIColor.init(red: 167/255, green: 205/255, blue: 69/255, alpha: 1)
                            infoBtn.tag = 0
                        }
                        else if systolicAvg >= 129 && diastolicAvg < 80
                        {
                            vwBloodPressure.backgroundColor = UIColor.init(red: 248/255, green: 228/255, blue: 51/255, alpha: 1)
                            infoBtn.tag = 1

                        }
                        else if systolicAvg >= 130 && systolicAvg < 139  || diastolicAvg >= 80 && diastolicAvg < 89
                        {
                            vwBloodPressure.backgroundColor = UIColor.init(red: 253/255, green: 181/255, blue: 43/255, alpha: 1)
                            infoBtn.tag = 2
                            
                        }
                        else if systolicAvg >= 140 && systolicAvg < 180  || diastolicAvg >= 90 && diastolicAvg < 120
                        {
                            vwBloodPressure.backgroundColor = UIColor.init(red: 185/255, green: 59/255, blue: 20/255, alpha: 1)
                            infoBtn.tag = 3
                            
                        }
                        else if systolicAvg >= 180 || diastolicAvg >= 120
                        {
                            vwBloodPressure.backgroundColor = UIColor.init(red: 152/255, green: 12/255, blue: 23/255, alpha: 1)
                            infoBtn.tag = 4
                            
                        }

                        
                        
                        labelAvrageSyst.text = "\(systolic/countProtocolVal)/\(diastolic/countProtocolVal)"
                        labelHeartAvarage.text = " \(pulseData/countProtocolVal)"

                    }
                }
                
                labelReadingTaken.text = "\(countProtocolVal)"
                
                progressView.value = CGFloat(100 * countProtocolVal / 28)
                
                let getStartDate = UtilityClass.getDatenTime24HrFromDateString(strStartDateTime)
                let diffInDays = Calendar.current.dateComponents([.day], from: getStartDate, to: Date()).day
                
                var missedCount = diffInDays! * 4 - countProtocolVal
                if missedCount < 0
                {
                    missedCount = 0
                }
                labelReadingMissed.text = "\(missedCount)"

            }
            
            
        }
        else
        {
            loadBPReadings()
            getStartedView.frame = CGRect.init(x: 0, y: buttonView.frame.origin.y + buttonView.frame.height , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - (buttonView.frame.origin.y + buttonView.frame.height))
            self.view .addSubview(getStartedView)

        }
        
        tableView0.isHidden = true
    }
    @IBAction func allButtonCLicked(_ sender: UIButton) {
        
        if self.view.subviews .contains(scheduledView)
        {
            scheduledView .removeFromSuperview()
        }
        if self.view.subviews .contains(getStartedView)
        {
            getStartedView .removeFromSuperview()
        }


        tableView0.isHidden = false
    }
    func loadBPReadings() {
  
        arryDataValue = DataBaseManager.sharedInstance().getAllReadingRecordData()
       
        self.tableView0.delegate = self
        self.tableView0.dataSource = self
        self.tableView0.reloadData()

        
//    let request = NSFetchRequest<NSFetchRequestResult>(entityName:"ActualValue")
//    let timestampSort = NSSortDescriptor(key:"timestamp", ascending:false)
//    request.sortDescriptors = [timestampSort]
//    do {
//      let results = try self.managedObjectContext?.fetch(request) as! [ActualValue]
//
//      if results.count > 0 {
//        self.actualValues = results
//        //self.actualValues = self.bpReadings![0].actual_values?.allObjects as! [ActualValue]
//
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
//        self.tableView.reloadData()
//      }
//
//    } catch let error {
//      print(error.localizedDescription)
//    }
  }
  
  
  // MARK: - Table View
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView1 == tableView
    {
        return self.arryProtocolValue.count
    }else
    {
    return self.arryDataValue.count
    }
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView1 == tableView
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BPReadingsCell", for: indexPath) as! BPReadingsCell
        
        
        let dictVal : [String:Any] = self.arryProtocolValue[indexPath.row]
        
        //    let systolic = rs.string(forColumn: "systolic")
        //    let diastolic = rs.string(forColumn: "diastolic")
        //    let device_id = rs.string(forColumn: "device_id")
        //    let pulse_data = rs.string(forColumn: "pulse_data")
        //    let device_name = rs.string(forColumn: "device_name")
        //    let patientId = rs.string(forColumn: "patientId")
        //    let reading_time = rs.string(forColumn: "reading_time")
        //    let is_abberant = rs.string(forColumn: "is_abberant")
        //    let protocol_id = rs.string(forColumn: "protocol_id")
        
        
        cell.diasystolicLabel.text = dictVal["diastolic"] as! String
        cell.systolicLabel.text = dictVal["systolic"] as! String
        cell.pulseLabel.text = dictVal["pulse_data"] as! String
        cell.timeLabel.text = timeStringFromUnixTime(unixTime: (dictVal["reading_time"]! as! NSString).doubleValue)
        cell.dateLabel.text = dayStringFromTime(unixTime: (dictVal["reading_time"]! as! NSString).doubleValue)
        return cell

    }
    else
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BPReadingsCell", for: indexPath) as! BPReadingsCell
        
        
        let dictVal : [String:Any] = self.arryDataValue[indexPath.row]
        
        //    let systolic = rs.string(forColumn: "systolic")
        //    let diastolic = rs.string(forColumn: "diastolic")
        //    let device_id = rs.string(forColumn: "device_id")
        //    let pulse_data = rs.string(forColumn: "pulse_data")
        //    let device_name = rs.string(forColumn: "device_name")
        //    let patientId = rs.string(forColumn: "patientId")
        //    let reading_time = rs.string(forColumn: "reading_time")
        //    let is_abberant = rs.string(forColumn: "is_abberant")
        //    let protocol_id = rs.string(forColumn: "protocol_id")
        
        
        cell.diasystolicLabel.text = dictVal["diastolic"] as! String
        cell.systolicLabel.text = dictVal["systolic"] as! String
        cell.pulseLabel.text = dictVal["pulse_data"] as! String
        cell.timeLabel.text = timeStringFromUnixTime(unixTime: (dictVal["reading_time"]! as! NSString).doubleValue)
        cell.dateLabel.text = dayStringFromTime(unixTime: (dictVal["reading_time"]! as! NSString).doubleValue)
        return cell

    }
    
  }
  
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    var deviceType: String = self.deviceTypes![indexPath.row]
//
//    if deviceType == "AND Blood Pressure Device" {
//      self.deviceType = "bp"
//      self.performSegue(withIdentifier: "PairingStatus", sender: self)
//    }
  }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyBPVCReadingsVC
{
    func getProtoColDataFromServer()
    {
        
        self.view.endEditing(true)
        
        let accessToken = BPUserDefaults.accessToken()
        let  patientID = BPUserDefaults.userId()
        
        let urlToHit = EndPoints.getProtocolData(accessToken, patientID).path
        print(urlToHit)
        
        print(FireApi.defaultHeaders)
        
        
        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: nil, parameters: nil) { [weak self] result in
            
            // UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    
                }
                
                return
            }
        }
//        FireApi.shared().performMultiPartRequest(for: urlToHit, imageArray: nil, headers: nil, parameters: nil ) { [weak self] result in
//
//            // UtilityClass.stopAnimating()
//
//            guard let `self` = self else {return}
//            print(result)
//            switch result {
//
//            case .error(let error):
//                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
//
//            case .success(let responseDict, let statusCode):
//
//                print(responseDict)
//                print(statusCode)
//
//                // Error
//                if statusCode == FireApi.ErrorCodes.code203.rawValue {
//
//                    if let msg = responseDict["message"] as? String {
//                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
//                    }
//                    else {
//                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
//                    }
//
//                    return
//                }
//
//                // Success
//                if statusCode == FireApi.ErrorCodes.code200.rawValue {
//
//
//                }
//
//                return
//            }
//        }
        
        
        }
    
    
    func getBpReadingData(fromDate:String, toDate:String)
    {
        
        self.view.endEditing(true)
        let accessToken = BPUserDefaults.accessToken()
        let  patientID = BPUserDefaults.userId()

        let urlToHit = EndPoints.getBPReading(accessToken, patientID,fromDate,toDate).path
        print(urlToHit)

        print(FireApi.defaultHeaders)
        FireApi.shared().performMultiPartRequest(for: urlToHit, imageArray: nil, headers: nil, parameters: nil, requestMethod: .get) { [weak self] result in
            
            // UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    
                }
                
                return
            }
        }
      
        
//        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: nil, parameters: nil) { [weak self] result in
//
//            // UtilityClass.stopAnimating()
//
//            guard let `self` = self else {return}
//            print(result)
//            switch result {
//
//            case .error(let error):
//                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
//
//            case .success(let responseDict, let statusCode):
//
//                print(responseDict)
//                print(statusCode)
//
//                // Error
//                if statusCode == FireApi.ErrorCodes.code203.rawValue {
//
//                    if let msg = responseDict["message"] as? String {
//                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
//                    }
//                    else {
//                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
//                    }
//
//                    return
//                }
//
//                // Success
//                if statusCode == FireApi.ErrorCodes.code200.rawValue {
//
//
//                }
//
//                return
//            }
//        }
    }
}
