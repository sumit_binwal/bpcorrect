//
//  ProfileVC.swift
//  BPCorrect
//
//  Created by "" on 12/04/19.
//  Copyright © 2019 "". All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
  
  
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var genderLabel: UILabel!
  @IBOutlet var measurementlabel: UILabel!
  
  @IBOutlet var emailLabel: UILabel!
  @IBOutlet var dobLabel: UILabel!
  @IBOutlet var addressLabel: UILabel!
  @IBOutlet var phoneLabel: UILabel!
  @IBOutlet var aboutLabel: UILabel!
  
  var userModel : UserModel?
  
  override func viewDidLoad() {
        super.viewDidLoad()
      self.title = "Profile"
      self.imageView.layer.cornerRadius = (self.imageView.frame.size.width) / 2
      self.imageView.layer.masksToBounds = true
      self.imageView.layer.borderColor = UIColor.white.cgColor
      self.imageView.layer.borderWidth = 2.0
    
      self.setNavigationBarItem()
      addRightBarButton()
        // Do any additional setup after loading the view.
    }
  
  override func viewWillAppear(_ animated: Bool) {
    loadUserProfile()
  }
  
  func addRightBarButton() {
    //create a new button
    let button = UIButton(type: .custom)
    //set image for button
    button.setImage(UIImage(named: "devices_add"), for: .normal)
    //add function for button
    button.addTarget(self, action: #selector(editProfileButtonPressed), for: .touchUpInside)
    //set frame
    button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
    
    let barButton = UIBarButtonItem(customView: button)
    //assign button to navigationbar
    self.navigationItem.rightBarButtonItem = barButton
  }
  
  //This method will call when you press button.
  @objc func editProfileButtonPressed() {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let signUpVC = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
    self.navigationController?.pushViewController(signUpVC, animated: true)
    
  }
  
  func loadUserProfile() {
  
    userModel = BPUserDefaults.getUserModel()
    
    if (userModel != nil && (userModel?.data?.count)! > 0) {
      
      var address : String = userModel?.data![0].address1 ?? ""
      
      if address != "" {
        let address2 : String = userModel?.data![0].address2 ?? ""
        if address2 != "" {
          address = "\(address) , \(address2)"
        }
        
      } else {
        address = userModel?.data![0].address2 ?? ""
      }
      
      var phoneNumber : String = userModel?.data![0].mobile1 ?? ""
      
      if phoneNumber != "" {
        let mobile2 : String = userModel?.data![0].mobile2 ?? ""
        if mobile2 != "" {
          phoneNumber = "\(phoneNumber) , \(mobile2)"
        }
        
      } else {
        phoneNumber = userModel?.data![0].mobile2 ?? ""
      }
      
      let height : String = userModel?.data![0].height ?? "-"
      let weight : String = userModel?.data![0].weight ?? "-"

      let measurement : String = "Height: \(height)ft | Weight: \(weight)kg"
      
      nameLabel.text = userModel?.data![0].firstname ?? ""
      genderLabel.text = userModel?.data![0].gender ?? ""
      measurementlabel.text = measurement
      
      emailLabel.text = userModel?.data![0].email ?? ""
      dobLabel.text = userModel?.data![0].dob ?? ""
      addressLabel.text = address
      phoneLabel.text = phoneNumber
    }
  }
    


}
