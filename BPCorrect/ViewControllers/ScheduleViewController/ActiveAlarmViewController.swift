//
//  ActiveAlarmViewController.swift
//  BPCorrect
//
//  Created by Sumit Sharma on 05/05/19.
//  Copyright © 2019 Preeti Gaur. All rights reserved.
//

import UIKit

class ActiveAlarmViewController: UIViewController {
    @IBOutlet weak var labelStartDate: UILabel!
    @IBOutlet weak var buttonMorning: UIButton!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var buttonMainSwitch: UIButton!
    @IBOutlet weak var buttonEvening: UIButton!
    @IBOutlet weak var labelMorningTimeAM: UILabel!
    @IBOutlet weak var labelEveningTime: UILabel!
    @IBOutlet weak var labelMorningTime: UILabel!
    
    @IBOutlet weak var labelEveningAM: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
self.title = "Reminder"
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if DataBaseManager.sharedInstance().checkIfAlarmDataExist()

        {
            let dataDict = DataBaseManager.sharedInstance().getAllActiveAlarmData()
            labelEndDate.text = UtilityClass.convertDateFormater((dataDict["endDate"] as! String))
            labelStartDate.text = UtilityClass.convertDateFormater((dataDict["startDate"] as! String))
            labelMorningTime.text = UtilityClass.convert24Timeto12(stringTime: (dataDict["startTime"] as! String))
            labelEveningTime.text = UtilityClass.convert24Timeto12(stringTime: (dataDict["endTime"] as! String))
            
            labelMorningTimeAM.text = UtilityClass.convertAMPMto24Time(stringTime: (dataDict["startTime"] as! String))
            labelEveningAM.text = UtilityClass.convertAMPMto24Time(stringTime: (dataDict["endTime"] as! String))

        }

    }
    

    @IBAction func mainSwitchButtonClicked(_ sender: UIButton)
    {
        if sender.tag == 0
        {
            sender.setImage(#imageLiteral(resourceName: "toggleOn"), for: .normal)

            sender.tag = 1
        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "toggleOff"), for: .normal)

            sender.tag = 0
        }
    }
    @IBAction func deleteButtonClicked(_ sender: UIButton)
    {
    
            let storyboardNew = UIStoryboard(name: "NewScreens", bundle: nil)
            let notification = storyboardNew.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(notification, animated: true)
        

    }
    @IBAction func buttonEveningSwitchClicked(_ sender: UIButton) {
        if sender.tag == 0
        {
            sender.setImage(#imageLiteral(resourceName: "toggleOn"), for: .normal)
            
            sender.tag = 1
        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "toggleOff"), for: .normal)

            sender.tag = 0
        }

    }
    @IBAction func buttonMorningSwitchClicked(_ sender: UIButton) {
        if sender.tag == 0
        {
            sender.setImage(#imageLiteral(resourceName: "toggleOn"), for: .normal)
            
            sender.tag = 1
        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "toggleOff"), for: .normal)

            sender.tag = 0
        }

    }

}
