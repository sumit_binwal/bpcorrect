//
//  ScheduleViewController.swift
//  BPCorrect
//
//  Created by Sumit Sharma on 04/05/19.
//  Copyright © 2019 Preeti Gaur. All rights reserved.
//

import UIKit
import UserNotifications
class ScheduleViewController: UIViewController {

    @IBOutlet weak var textFieldMorningTime: UITextField!
    @IBOutlet weak var textFieldEveningTime: UITextField!
    var strIdValue = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldMorningTime.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed),minimumTime: "06:00",maximumTime: "15:00", valueChangeSelector: #selector(doneButtonPressed))
        textFieldEveningTime.addInputViewDatePicker(target: self, selector: #selector(eveningButtonPressed),minimumTime: "16:00",maximumTime: "22:00", valueChangeSelector: #selector(eveningButtonPressed))

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if DataBaseManager.sharedInstance().checkIfAlarmDataExist()
        {
            let dataDict = DataBaseManager.sharedInstance().getAllActiveAlarmData()
            
            self.textFieldMorningTime.text = "\(UtilityClass.convert24Timeto12(stringTime: (dataDict["startTime"] as! String))) \(UtilityClass.convertAMPMto24Time(stringTime: (dataDict["startTime"] as! String)))"
            self.textFieldEveningTime.text = "\(UtilityClass.convert24Timeto12(stringTime: (dataDict["endTime"] as! String))) \(UtilityClass.convertAMPMto24Time(stringTime: (dataDict["endTime"] as! String)))"
            strIdValue  = dataDict["id"] as! String
            

        }
    }
    
    @objc func doneButtonPressed() {
        if let  datePicker = self.textFieldMorningTime.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short
            self.textFieldMorningTime.text = dateFormatter.string(from: datePicker.date)
        }
        self.textFieldMorningTime.resignFirstResponder()
    }
    
    @objc func eveningButtonPressed() {
        if let  datePicker = self.textFieldEveningTime.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short
            self.textFieldEveningTime.text = dateFormatter.string(from: datePicker.date)
        }
        self.textFieldEveningTime.resignFirstResponder()
    }

  
    
    @IBAction func nextButtonClickAction(_ sender: UIButton)
    {
        if textFieldEveningTime.text!.count > 0  && textFieldMorningTime.text!.count > 0
        {
            
            
            let createdDate = UtilityClass.getCurrentDateString()
            
            
            let endDate = UtilityClass.getEndDateFromNow()

            let startTime =  UtilityClass.convert12Timeto24(stringTime: textFieldMorningTime.text!)
            let endTime =  UtilityClass.convert12Timeto24(stringTime: textFieldEveningTime.text!)
            
          
            
            let formatter = DateFormatter()
            
            formatter.timeZone = TimeZone.current
            
            formatter.dateFormat = "yyyy-MM-dd"
            
            let startDateTime = formatter.string(from: Date())
        
            let center = UNUserNotificationCenter.current()
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
            center.removeAllPendingNotificationRequests()

            //Schedule Notifications
            let startTimeVal = "\(startDateTime) \(startTime)"
            print(startTimeVal)
            let dateValue = UtilityClass.getDatenTime24HrFromDateString(startTimeVal)
            APPDELEGATE.scheduleNotifications(scheduleTIme: dateValue)
            
            
            let endTimeVal = "\(startDateTime) \(endTime)"
            print(endTimeVal)
            let enddateValue = UtilityClass.getDatenTime24HrFromDateString(endTimeVal)
            APPDELEGATE.scheduleNotifications(scheduleTIme: enddateValue)


            let dataDict = ["protocolID":"","created":createdDate,"startDate":startDateTime,"endDate":endDate,"startTime":startTime,"endTime":endTime,"isStart":"1","isEnd":"1","isUpdate":"0","updatedTime":createdDate] as [String : Any]
            
            print(dataDict)
            
            if DataBaseManager.sharedInstance().checkIfAlarmDataExist()
            {
                DataBaseManager.sharedInstance().updateValuesToCreateProtocol(dictVal: dataDict, id: strIdValue)
                self.navigationController?.popViewController(animated: true)
                
            }
            else
            {
            DataBaseManager.sharedInstance().insertValuesToCreateProtocol(dictVal: dataDict)
                self.navigationController?.popToRootViewController(animated: true)
            }
            protocolDataOnServer(dictData: dataDict)

            //DataBaseManager.sharedInstance().insertValuesToCreateProtocol(dictVal: dataDict) as [String : Any]
        }
        
    }
    
    func getCurrentTimestamp()-> String
    {
        let now = Date()
        
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        let dateString = formatter.string(from: now)
return dateString
    }
    
}
extension UITextField {
    
    func addInputViewDatePicker(target: Any, selector: Selector, minimumTime : String, maximumTime : String, valueChangeSelector: Selector) {
        
        let screenWidth = UIScreen.main.bounds.width
        
        //Add DatePicker as inputView
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .time
       // datePicker.addTarget(target, action:#selector(valueChangeSelector), for: UIControl.Event.valueChanged)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        
        let min = dateFormatter.date(from: minimumTime)      //createing min time
        let max = dateFormatter.date(from: maximumTime) //creating max time
        datePicker.minimumDate = min  //setting min time to picker
        datePicker.maximumDate = max  //setting max time to picker

        self.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
    @objc func cancelPressed() {
        self.resignFirstResponder()
    }
}

extension ScheduleViewController
{
    func protocolDataOnServer(dictData:[String:Any])
    {
        
        self.view.endEditing(true)
        
        
        
        let accessToken = BPUserDefaults.accessToken()
        let  patientID = BPUserDefaults.userId()
        
        
        let startDate = UtilityClass.getDateFromDateString(dictData["startDate"] as! String, "yyyy-MM-dd")
        var startTimestamp = "\(Int(startDate.timeIntervalSince1970))"
        startTimestamp = startTimestamp.replacingOccurrences(of: ".", with: "")

        let endDate = UtilityClass.getDateFromDateString(dictData["endDate"] as! String, "yyyy-MM-dd")
        var endTimestamp = "\(Int(endDate.timeIntervalSince1970))"
        endTimestamp = endTimestamp.replacingOccurrences(of: ".", with: "")


        
        let param =  ["access_token": accessToken,
                      "patient_id": patientID,"startdate"    : endTimestamp,"enddate": endTimestamp,"protocol_id": "fsdfasfsafsa","morning_alarm":dictData["startTime"] as! String,"evening_alarm":dictData["endTime"] as! String]
        let urlToHit = EndPoints.createProtocol.path
        
        print(urlToHit)
        print(param)
        print(FireApi.defaultHeaders)
        

        FireApi.shared().performMultiPartRequest(for: urlToHit, imageArray: nil, headers: nil, parameters: param as [String : Any],requestMethod: .post) { [weak self] result in
            
            // UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    
                }
                
                return
            }
        }
        
        
    }
    
//    //Get All Reading From BP Reading
//    func getBPReadingData()
//    {
//        
//        self.view.endEditing(true)
//        
//        let accessToken = BPUserDefaults.accessToken()
//        let  patientID = BPUserDefaults.userId()
//        
//        let param =  ["access_token": accessToken,
//                      "patient_id": patientID,"startdate"    : dictData["startDate"] as! String,"enddate": dictData["endDate"] as! String,"protocol_id": "fsdfasfsafsa","morning_alarm":dictData["startTime"] as! String,"evening_alarm":dictData["endTime"] as! String]
//        let urlToHit = EndPoints.createProtocol.path
//        
//        print(urlToHit)
//        print(param)
//        print(FireApi.defaultHeaders)
//        
//        FireApi.shared().performMultiPartRequest(for: urlToHit, imageArray: nil, headers: nil, parameters: param as [String : Any]) { [weak self] result in
//            
//            // UtilityClass.stopAnimating()
//            
//            guard let `self` = self else {return}
//            print(result)
//            switch result {
//                
//            case .error(let error):
//                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
//                
//            case .success(let responseDict, let statusCode):
//                
//                print(responseDict)
//                print(statusCode)
//                
//                // Error
//                if statusCode == FireApi.ErrorCodes.code203.rawValue {
//                    
//                    if let msg = responseDict["message"] as? String {
//                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
//                    }
//                    else {
//                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
//                    }
//                    
//                    return
//                }
//                
//                // Success
//                if statusCode == FireApi.ErrorCodes.code200.rawValue {
//                    
//                    
//                }
//                
//                return
//            }
//        }
//        
//        
//    }
}
